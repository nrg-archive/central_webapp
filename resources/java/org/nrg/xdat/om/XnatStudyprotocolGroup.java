/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:49 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatStudyprotocolGroup extends BaseXnatStudyprotocolGroup {

	public XnatStudyprotocolGroup(ItemI item)
	{
		super(item);
	}

	public XnatStudyprotocolGroup(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatStudyprotocolGroup(UserI user)
	 **/
	public XnatStudyprotocolGroup()
	{}

	public XnatStudyprotocolGroup(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
