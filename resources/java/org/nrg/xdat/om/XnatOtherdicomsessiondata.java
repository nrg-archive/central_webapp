/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:50 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatOtherdicomsessiondata extends BaseXnatOtherdicomsessiondata {

	public XnatOtherdicomsessiondata(ItemI item)
	{
		super(item);
	}

	public XnatOtherdicomsessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatOtherdicomsessiondata(UserI user)
	 **/
	public XnatOtherdicomsessiondata()
	{}

	public XnatOtherdicomsessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
