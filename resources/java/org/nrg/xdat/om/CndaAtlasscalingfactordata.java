/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:48 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaAtlasscalingfactordata extends BaseCndaAtlasscalingfactordata {

	public CndaAtlasscalingfactordata(ItemI item)
	{
		super(item);
	}

	public CndaAtlasscalingfactordata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaAtlasscalingfactordata(UserI user)
	 **/
	public CndaAtlasscalingfactordata()
	{}

	public CndaAtlasscalingfactordata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
