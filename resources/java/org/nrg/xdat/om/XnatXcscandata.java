/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:50 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatXcscandata extends BaseXnatXcscandata {

	public XnatXcscandata(ItemI item)
	{
		super(item);
	}

	public XnatXcscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatXcscandata(UserI user)
	 **/
	public XnatXcscandata()
	{}

	public XnatXcscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
