/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:48 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaDtiregion extends BaseCndaDtiregion {

	public CndaDtiregion(ItemI item)
	{
		super(item);
	}

	public CndaDtiregion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaDtiregion(UserI user)
	 **/
	public CndaDtiregion()
	{}

	public CndaDtiregion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
