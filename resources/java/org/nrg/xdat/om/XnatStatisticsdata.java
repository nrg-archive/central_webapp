/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:49 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatStatisticsdata extends BaseXnatStatisticsdata {

	public XnatStatisticsdata(ItemI item)
	{
		super(item);
	}

	public XnatStatisticsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatStatisticsdata(UserI user)
	 **/
	public XnatStatisticsdata()
	{}

	public XnatStatisticsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
