/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:49 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatFielddefinitiongroupField extends BaseXnatFielddefinitiongroupField {

	public XnatFielddefinitiongroupField(ItemI item)
	{
		super(item);
	}

	public XnatFielddefinitiongroupField(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatFielddefinitiongroupField(UserI user)
	 **/
	public XnatFielddefinitiongroupField()
	{}

	public XnatFielddefinitiongroupField(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
