/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:49 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatXa3dscandata extends BaseXnatXa3dscandata {

	public XnatXa3dscandata(ItemI item)
	{
		super(item);
	}

	public XnatXa3dscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatXa3dscandata(UserI user)
	 **/
	public XnatXa3dscandata()
	{}

	public XnatXa3dscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
