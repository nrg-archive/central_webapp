/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:48 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaClinicalassessmentdata extends BaseCndaClinicalassessmentdata {

	public CndaClinicalassessmentdata(ItemI item)
	{
		super(item);
	}

	public CndaClinicalassessmentdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaClinicalassessmentdata(UserI user)
	 **/
	public CndaClinicalassessmentdata()
	{}

	public CndaClinicalassessmentdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
