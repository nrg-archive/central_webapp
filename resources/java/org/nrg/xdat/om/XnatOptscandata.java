/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:50 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatOptscandata extends BaseXnatOptscandata {

	public XnatOptscandata(ItemI item)
	{
		super(item);
	}

	public XnatOptscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatOptscandata(UserI user)
	 **/
	public XnatOptscandata()
	{}

	public XnatOptscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
