/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:48 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaHandednessdata extends BaseCndaHandednessdata {

	public CndaHandednessdata(ItemI item)
	{
		super(item);
	}

	public CndaHandednessdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaHandednessdata(UserI user)
	 **/
	public CndaHandednessdata()
	{}

	public CndaHandednessdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
