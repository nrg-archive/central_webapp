/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:49 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class FsLongfsdataHemisphereRegion extends BaseFsLongfsdataHemisphereRegion {

	public FsLongfsdataHemisphereRegion(ItemI item)
	{
		super(item);
	}

	public FsLongfsdataHemisphereRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsLongfsdataHemisphereRegion(UserI user)
	 **/
	public FsLongfsdataHemisphereRegion()
	{}

	public FsLongfsdataHemisphereRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
