/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:48 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaDtidata extends BaseCndaDtidata {

	public CndaDtidata(ItemI item)
	{
		super(item);
	}

	public CndaDtidata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaDtidata(UserI user)
	 **/
	public CndaDtidata()
	{}

	public CndaDtidata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
