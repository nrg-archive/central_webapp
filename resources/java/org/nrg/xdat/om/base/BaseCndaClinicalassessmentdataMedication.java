/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:48 CST 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaClinicalassessmentdataMedication extends AutoCndaClinicalassessmentdataMedication {

	public BaseCndaClinicalassessmentdataMedication(ItemI item)
	{
		super(item);
	}

	public BaseCndaClinicalassessmentdataMedication(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaClinicalassessmentdataMedication(UserI user)
	 **/
	public BaseCndaClinicalassessmentdataMedication()
	{}

	public BaseCndaClinicalassessmentdataMedication(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
