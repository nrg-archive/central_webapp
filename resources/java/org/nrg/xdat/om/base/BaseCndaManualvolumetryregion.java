/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:48 CST 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaManualvolumetryregion extends AutoCndaManualvolumetryregion {

	public BaseCndaManualvolumetryregion(ItemI item)
	{
		super(item);
	}

	public BaseCndaManualvolumetryregion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaManualvolumetryregion(UserI user)
	 **/
	public BaseCndaManualvolumetryregion()
	{}

	public BaseCndaManualvolumetryregion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
