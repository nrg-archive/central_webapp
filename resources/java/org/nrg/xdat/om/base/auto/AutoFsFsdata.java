/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:47 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoFsFsdata extends XnatImageassessordata implements org.nrg.xdat.model.FsFsdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoFsFsdata.class);
	public static String SCHEMA_ELEMENT_NAME="fs:fsData";

	public AutoFsFsdata(ItemI item)
	{
		super(item);
	}

	public AutoFsFsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoFsFsdata(UserI user)
	 **/
	public AutoFsFsdata(){}

	public AutoFsFsdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "fs:fsData";
	}
	 private org.nrg.xdat.om.XnatImageassessordata _Imageassessordata =null;

	/**
	 * imageAssessorData
	 * @return org.nrg.xdat.om.XnatImageassessordata
	 */
	public org.nrg.xdat.om.XnatImageassessordata getImageassessordata() {
		try{
			if (_Imageassessordata==null){
				_Imageassessordata=((XnatImageassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("imageAssessorData")));
				return _Imageassessordata;
			}else {
				return _Imageassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for imageAssessorData.
	 * @param v Value to Set.
	 */
	public void setImageassessordata(ItemI v) throws Exception{
		_Imageassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * imageAssessorData
	 * set org.nrg.xdat.model.XnatImageassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatImageassessordataI> void setImageassessordata(A item) throws Exception{
	setImageassessordata((ItemI)item);
	}

	/**
	 * Removes the imageAssessorData.
	 * */
	public void removeImageassessordata() {
		_Imageassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/imageAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _FsVersion=null;

	/**
	 * @return Returns the fs_version.
	 */
	public String getFsVersion(){
		try{
			if (_FsVersion==null){
				_FsVersion=getStringProperty("fs_version");
				return _FsVersion;
			}else {
				return _FsVersion;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for fs_version.
	 * @param v Value to Set.
	 */
	public void setFsVersion(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/fs_version",v);
		_FsVersion=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _IlpAge=null;

	/**
	 * @return Returns the ilp_age.
	 */
	public Double getIlpAge() {
		try{
			if (_IlpAge==null){
				_IlpAge=getDoubleProperty("ilp_age");
				return _IlpAge;
			}else {
				return _IlpAge;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ilp_age.
	 * @param v Value to Set.
	 */
	public void setIlpAge(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ilp_age",v);
		_IlpAge=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_icv=null;

	/**
	 * @return Returns the measures/volumetric/ICV.
	 */
	public Double getMeasures_volumetric_icv() {
		try{
			if (_Measures_volumetric_icv==null){
				_Measures_volumetric_icv=getDoubleProperty("measures/volumetric/ICV");
				return _Measures_volumetric_icv;
			}else {
				return _Measures_volumetric_icv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/ICV.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_icv(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/ICV",v);
		_Measures_volumetric_icv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_lhcortexvol=null;

	/**
	 * @return Returns the measures/volumetric/lhCortexVol.
	 */
	public Double getMeasures_volumetric_lhcortexvol() {
		try{
			if (_Measures_volumetric_lhcortexvol==null){
				_Measures_volumetric_lhcortexvol=getDoubleProperty("measures/volumetric/lhCortexVol");
				return _Measures_volumetric_lhcortexvol;
			}else {
				return _Measures_volumetric_lhcortexvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/lhCortexVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_lhcortexvol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/lhCortexVol",v);
		_Measures_volumetric_lhcortexvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_rhcortexvol=null;

	/**
	 * @return Returns the measures/volumetric/rhCortexVol.
	 */
	public Double getMeasures_volumetric_rhcortexvol() {
		try{
			if (_Measures_volumetric_rhcortexvol==null){
				_Measures_volumetric_rhcortexvol=getDoubleProperty("measures/volumetric/rhCortexVol");
				return _Measures_volumetric_rhcortexvol;
			}else {
				return _Measures_volumetric_rhcortexvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/rhCortexVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_rhcortexvol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/rhCortexVol",v);
		_Measures_volumetric_rhcortexvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_cortexvol=null;

	/**
	 * @return Returns the measures/volumetric/CortexVol.
	 */
	public Double getMeasures_volumetric_cortexvol() {
		try{
			if (_Measures_volumetric_cortexvol==null){
				_Measures_volumetric_cortexvol=getDoubleProperty("measures/volumetric/CortexVol");
				return _Measures_volumetric_cortexvol;
			}else {
				return _Measures_volumetric_cortexvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/CortexVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_cortexvol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/CortexVol",v);
		_Measures_volumetric_cortexvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_subcortgrayvol=null;

	/**
	 * @return Returns the measures/volumetric/SubCortGrayVol.
	 */
	public Double getMeasures_volumetric_subcortgrayvol() {
		try{
			if (_Measures_volumetric_subcortgrayvol==null){
				_Measures_volumetric_subcortgrayvol=getDoubleProperty("measures/volumetric/SubCortGrayVol");
				return _Measures_volumetric_subcortgrayvol;
			}else {
				return _Measures_volumetric_subcortgrayvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/SubCortGrayVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_subcortgrayvol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/SubCortGrayVol",v);
		_Measures_volumetric_subcortgrayvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_totalgrayvol=null;

	/**
	 * @return Returns the measures/volumetric/TotalGrayVol.
	 */
	public Double getMeasures_volumetric_totalgrayvol() {
		try{
			if (_Measures_volumetric_totalgrayvol==null){
				_Measures_volumetric_totalgrayvol=getDoubleProperty("measures/volumetric/TotalGrayVol");
				return _Measures_volumetric_totalgrayvol;
			}else {
				return _Measures_volumetric_totalgrayvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/TotalGrayVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_totalgrayvol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/TotalGrayVol",v);
		_Measures_volumetric_totalgrayvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_supratentorialvol=null;

	/**
	 * @return Returns the measures/volumetric/SupraTentorialVol.
	 */
	public Double getMeasures_volumetric_supratentorialvol() {
		try{
			if (_Measures_volumetric_supratentorialvol==null){
				_Measures_volumetric_supratentorialvol=getDoubleProperty("measures/volumetric/SupraTentorialVol");
				return _Measures_volumetric_supratentorialvol;
			}else {
				return _Measures_volumetric_supratentorialvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/SupraTentorialVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_supratentorialvol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/SupraTentorialVol",v);
		_Measures_volumetric_supratentorialvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_lhcorticalwhitemattervol=null;

	/**
	 * @return Returns the measures/volumetric/lhCorticalWhiteMatterVol.
	 */
	public Double getMeasures_volumetric_lhcorticalwhitemattervol() {
		try{
			if (_Measures_volumetric_lhcorticalwhitemattervol==null){
				_Measures_volumetric_lhcorticalwhitemattervol=getDoubleProperty("measures/volumetric/lhCorticalWhiteMatterVol");
				return _Measures_volumetric_lhcorticalwhitemattervol;
			}else {
				return _Measures_volumetric_lhcorticalwhitemattervol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/lhCorticalWhiteMatterVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_lhcorticalwhitemattervol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/lhCorticalWhiteMatterVol",v);
		_Measures_volumetric_lhcorticalwhitemattervol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_rhcorticalwhitemattervol=null;

	/**
	 * @return Returns the measures/volumetric/rhCorticalWhiteMatterVol.
	 */
	public Double getMeasures_volumetric_rhcorticalwhitemattervol() {
		try{
			if (_Measures_volumetric_rhcorticalwhitemattervol==null){
				_Measures_volumetric_rhcorticalwhitemattervol=getDoubleProperty("measures/volumetric/rhCorticalWhiteMatterVol");
				return _Measures_volumetric_rhcorticalwhitemattervol;
			}else {
				return _Measures_volumetric_rhcorticalwhitemattervol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/rhCorticalWhiteMatterVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_rhcorticalwhitemattervol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/rhCorticalWhiteMatterVol",v);
		_Measures_volumetric_rhcorticalwhitemattervol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_corticalwhitemattervol=null;

	/**
	 * @return Returns the measures/volumetric/CorticalWhiteMatterVol.
	 */
	public Double getMeasures_volumetric_corticalwhitemattervol() {
		try{
			if (_Measures_volumetric_corticalwhitemattervol==null){
				_Measures_volumetric_corticalwhitemattervol=getDoubleProperty("measures/volumetric/CorticalWhiteMatterVol");
				return _Measures_volumetric_corticalwhitemattervol;
			}else {
				return _Measures_volumetric_corticalwhitemattervol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/CorticalWhiteMatterVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_corticalwhitemattervol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/CorticalWhiteMatterVol",v);
		_Measures_volumetric_corticalwhitemattervol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_brainsegvol=null;

	/**
	 * @return Returns the measures/volumetric/BrainSegVol.
	 */
	public Double getMeasures_volumetric_brainsegvol() {
		try{
			if (_Measures_volumetric_brainsegvol==null){
				_Measures_volumetric_brainsegvol=getDoubleProperty("measures/volumetric/BrainSegVol");
				return _Measures_volumetric_brainsegvol;
			}else {
				return _Measures_volumetric_brainsegvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/BrainSegVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/BrainSegVol",v);
		_Measures_volumetric_brainsegvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_brainsegvolnotvent=null;

	/**
	 * @return Returns the measures/volumetric/BrainSegVolNotVent.
	 */
	public Double getMeasures_volumetric_brainsegvolnotvent() {
		try{
			if (_Measures_volumetric_brainsegvolnotvent==null){
				_Measures_volumetric_brainsegvolnotvent=getDoubleProperty("measures/volumetric/BrainSegVolNotVent");
				return _Measures_volumetric_brainsegvolnotvent;
			}else {
				return _Measures_volumetric_brainsegvolnotvent;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/BrainSegVolNotVent.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvolnotvent(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/BrainSegVolNotVent",v);
		_Measures_volumetric_brainsegvolnotvent=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_brainsegvolnotventsurf=null;

	/**
	 * @return Returns the measures/volumetric/BrainSegVolNotVentSurf.
	 */
	public Double getMeasures_volumetric_brainsegvolnotventsurf() {
		try{
			if (_Measures_volumetric_brainsegvolnotventsurf==null){
				_Measures_volumetric_brainsegvolnotventsurf=getDoubleProperty("measures/volumetric/BrainSegVolNotVentSurf");
				return _Measures_volumetric_brainsegvolnotventsurf;
			}else {
				return _Measures_volumetric_brainsegvolnotventsurf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/BrainSegVolNotVentSurf.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvolnotventsurf(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/BrainSegVolNotVentSurf",v);
		_Measures_volumetric_brainsegvolnotventsurf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_supratentorialvolnotvent=null;

	/**
	 * @return Returns the measures/volumetric/SupraTentorialVolNotVent.
	 */
	public Double getMeasures_volumetric_supratentorialvolnotvent() {
		try{
			if (_Measures_volumetric_supratentorialvolnotvent==null){
				_Measures_volumetric_supratentorialvolnotvent=getDoubleProperty("measures/volumetric/SupraTentorialVolNotVent");
				return _Measures_volumetric_supratentorialvolnotvent;
			}else {
				return _Measures_volumetric_supratentorialvolnotvent;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/SupraTentorialVolNotVent.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_supratentorialvolnotvent(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/SupraTentorialVolNotVent",v);
		_Measures_volumetric_supratentorialvolnotvent=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_supratentorialvolnotventvox=null;

	/**
	 * @return Returns the measures/volumetric/SupraTentorialVolNotVentVox.
	 */
	public Double getMeasures_volumetric_supratentorialvolnotventvox() {
		try{
			if (_Measures_volumetric_supratentorialvolnotventvox==null){
				_Measures_volumetric_supratentorialvolnotventvox=getDoubleProperty("measures/volumetric/SupraTentorialVolNotVentVox");
				return _Measures_volumetric_supratentorialvolnotventvox;
			}else {
				return _Measures_volumetric_supratentorialvolnotventvox;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/SupraTentorialVolNotVentVox.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_supratentorialvolnotventvox(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/SupraTentorialVolNotVentVox",v);
		_Measures_volumetric_supratentorialvolnotventvox=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_maskvol=null;

	/**
	 * @return Returns the measures/volumetric/MaskVol.
	 */
	public Double getMeasures_volumetric_maskvol() {
		try{
			if (_Measures_volumetric_maskvol==null){
				_Measures_volumetric_maskvol=getDoubleProperty("measures/volumetric/MaskVol");
				return _Measures_volumetric_maskvol;
			}else {
				return _Measures_volumetric_maskvol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/MaskVol.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_maskvol(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/MaskVol",v);
		_Measures_volumetric_maskvol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_brainsegvolToEtiv=null;

	/**
	 * @return Returns the measures/volumetric/BrainSegVol-to-eTIV.
	 */
	public Double getMeasures_volumetric_brainsegvolToEtiv() {
		try{
			if (_Measures_volumetric_brainsegvolToEtiv==null){
				_Measures_volumetric_brainsegvolToEtiv=getDoubleProperty("measures/volumetric/BrainSegVol-to-eTIV");
				return _Measures_volumetric_brainsegvolToEtiv;
			}else {
				return _Measures_volumetric_brainsegvolToEtiv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/BrainSegVol-to-eTIV.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_brainsegvolToEtiv(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/BrainSegVol-to-eTIV",v);
		_Measures_volumetric_brainsegvolToEtiv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_maskvolToEtiv=null;

	/**
	 * @return Returns the measures/volumetric/MaskVol-to-eTIV.
	 */
	public Double getMeasures_volumetric_maskvolToEtiv() {
		try{
			if (_Measures_volumetric_maskvolToEtiv==null){
				_Measures_volumetric_maskvolToEtiv=getDoubleProperty("measures/volumetric/MaskVol-to-eTIV");
				return _Measures_volumetric_maskvolToEtiv;
			}else {
				return _Measures_volumetric_maskvolToEtiv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/MaskVol-to-eTIV.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_maskvolToEtiv(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/MaskVol-to-eTIV",v);
		_Measures_volumetric_maskvolToEtiv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_lhsurfaceholes=null;

	/**
	 * @return Returns the measures/volumetric/lhSurfaceHoles.
	 */
	public Double getMeasures_volumetric_lhsurfaceholes() {
		try{
			if (_Measures_volumetric_lhsurfaceholes==null){
				_Measures_volumetric_lhsurfaceholes=getDoubleProperty("measures/volumetric/lhSurfaceHoles");
				return _Measures_volumetric_lhsurfaceholes;
			}else {
				return _Measures_volumetric_lhsurfaceholes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/lhSurfaceHoles.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_lhsurfaceholes(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/lhSurfaceHoles",v);
		_Measures_volumetric_lhsurfaceholes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_rhsurfaceholes=null;

	/**
	 * @return Returns the measures/volumetric/rhSurfaceHoles.
	 */
	public Double getMeasures_volumetric_rhsurfaceholes() {
		try{
			if (_Measures_volumetric_rhsurfaceholes==null){
				_Measures_volumetric_rhsurfaceholes=getDoubleProperty("measures/volumetric/rhSurfaceHoles");
				return _Measures_volumetric_rhsurfaceholes;
			}else {
				return _Measures_volumetric_rhsurfaceholes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/rhSurfaceHoles.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_rhsurfaceholes(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/rhSurfaceHoles",v);
		_Measures_volumetric_rhsurfaceholes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Measures_volumetric_surfaceholes=null;

	/**
	 * @return Returns the measures/volumetric/SurfaceHoles.
	 */
	public Double getMeasures_volumetric_surfaceholes() {
		try{
			if (_Measures_volumetric_surfaceholes==null){
				_Measures_volumetric_surfaceholes=getDoubleProperty("measures/volumetric/SurfaceHoles");
				return _Measures_volumetric_surfaceholes;
			}else {
				return _Measures_volumetric_surfaceholes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for measures/volumetric/SurfaceHoles.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_surfaceholes(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/measures/volumetric/SurfaceHoles",v);
		_Measures_volumetric_surfaceholes=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.FsFsdataRegion> _Measures_volumetric_regions_region =null;

	/**
	 * measures/volumetric/regions/region
	 * @return Returns an List of org.nrg.xdat.om.FsFsdataRegion
	 */
	public <A extends org.nrg.xdat.model.FsFsdataRegionI> List<A> getMeasures_volumetric_regions_region() {
		try{
			if (_Measures_volumetric_regions_region==null){
				_Measures_volumetric_regions_region=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("measures/volumetric/regions/region"));
			}
			return (List<A>) _Measures_volumetric_regions_region;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.FsFsdataRegion>();}
	}

	/**
	 * Sets the value for measures/volumetric/regions/region.
	 * @param v Value to Set.
	 */
	public void setMeasures_volumetric_regions_region(ItemI v) throws Exception{
		_Measures_volumetric_regions_region =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/measures/volumetric/regions/region",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/measures/volumetric/regions/region",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * measures/volumetric/regions/region
	 * Adds org.nrg.xdat.model.FsFsdataRegionI
	 */
	public <A extends org.nrg.xdat.model.FsFsdataRegionI> void addMeasures_volumetric_regions_region(A item) throws Exception{
	setMeasures_volumetric_regions_region((ItemI)item);
	}

	/**
	 * Removes the measures/volumetric/regions/region of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeMeasures_volumetric_regions_region(int index) throws java.lang.IndexOutOfBoundsException {
		_Measures_volumetric_regions_region =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/measures/volumetric/regions/region",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.FsFsdataHemisphere> _Measures_surface_hemisphere =null;

	/**
	 * measures/surface/hemisphere
	 * @return Returns an List of org.nrg.xdat.om.FsFsdataHemisphere
	 */
	public <A extends org.nrg.xdat.model.FsFsdataHemisphereI> List<A> getMeasures_surface_hemisphere() {
		try{
			if (_Measures_surface_hemisphere==null){
				_Measures_surface_hemisphere=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("measures/surface/hemisphere"));
			}
			return (List<A>) _Measures_surface_hemisphere;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.FsFsdataHemisphere>();}
	}

	/**
	 * Sets the value for measures/surface/hemisphere.
	 * @param v Value to Set.
	 */
	public void setMeasures_surface_hemisphere(ItemI v) throws Exception{
		_Measures_surface_hemisphere =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/measures/surface/hemisphere",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/measures/surface/hemisphere",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * measures/surface/hemisphere
	 * Adds org.nrg.xdat.model.FsFsdataHemisphereI
	 */
	public <A extends org.nrg.xdat.model.FsFsdataHemisphereI> void addMeasures_surface_hemisphere(A item) throws Exception{
	setMeasures_surface_hemisphere((ItemI)item);
	}

	/**
	 * Removes the measures/surface/hemisphere of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeMeasures_surface_hemisphere(int index) throws java.lang.IndexOutOfBoundsException {
		_Measures_surface_hemisphere =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/measures/surface/hemisphere",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.FsFsdata> getAllFsFsdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.FsFsdata> al = new ArrayList<org.nrg.xdat.om.FsFsdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.FsFsdata> getFsFsdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.FsFsdata> al = new ArrayList<org.nrg.xdat.om.FsFsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.FsFsdata> getFsFsdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.FsFsdata> al = new ArrayList<org.nrg.xdat.om.FsFsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static FsFsdata getFsFsdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("fs:fsData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (FsFsdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //imageAssessorData
	        XnatImageassessordata childImageassessordata = (XnatImageassessordata)this.getImageassessordata();
	            if (childImageassessordata!=null){
	              for(ResourceFile rf: ((XnatImageassessordata)childImageassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("imageAssessorData[" + ((XnatImageassessordata)childImageassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("imageAssessorData/" + ((XnatImageassessordata)childImageassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //measures/volumetric/regions/region
	        for(org.nrg.xdat.model.FsFsdataRegionI childMeasures_volumetric_regions_region : this.getMeasures_volumetric_regions_region()){
	            if (childMeasures_volumetric_regions_region!=null){
	              for(ResourceFile rf: ((FsFsdataRegion)childMeasures_volumetric_regions_region).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("measures/volumetric/regions/region[" + ((FsFsdataRegion)childMeasures_volumetric_regions_region).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("measures/volumetric/regions/region/" + ((FsFsdataRegion)childMeasures_volumetric_regions_region).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //measures/surface/hemisphere
	        for(org.nrg.xdat.model.FsFsdataHemisphereI childMeasures_surface_hemisphere : this.getMeasures_surface_hemisphere()){
	            if (childMeasures_surface_hemisphere!=null){
	              for(ResourceFile rf: ((FsFsdataHemisphere)childMeasures_surface_hemisphere).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("measures/surface/hemisphere[" + ((FsFsdataHemisphere)childMeasures_surface_hemisphere).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("measures/surface/hemisphere/" + ((FsFsdataHemisphere)childMeasures_surface_hemisphere).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
