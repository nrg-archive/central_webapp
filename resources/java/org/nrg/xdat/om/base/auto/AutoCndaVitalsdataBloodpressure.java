/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaVitalsdataBloodpressure extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CndaVitalsdataBloodpressureI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaVitalsdataBloodpressure.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:vitalsData_bloodPressure";

	public AutoCndaVitalsdataBloodpressure(ItemI item)
	{
		super(item);
	}

	public AutoCndaVitalsdataBloodpressure(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaVitalsdataBloodpressure(UserI user)
	 **/
	public AutoCndaVitalsdataBloodpressure(){}

	public AutoCndaVitalsdataBloodpressure(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:vitalsData_bloodPressure";
	}

	//FIELD

	private Integer _Systolic=null;

	/**
	 * @return Returns the systolic.
	 */
	public Integer getSystolic() {
		try{
			if (_Systolic==null){
				_Systolic=getIntegerProperty("systolic");
				return _Systolic;
			}else {
				return _Systolic;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for systolic.
	 * @param v Value to Set.
	 */
	public void setSystolic(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/systolic",v);
		_Systolic=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Diastolic=null;

	/**
	 * @return Returns the diastolic.
	 */
	public Integer getDiastolic() {
		try{
			if (_Diastolic==null){
				_Diastolic=getIntegerProperty("diastolic");
				return _Diastolic;
			}else {
				return _Diastolic;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for diastolic.
	 * @param v Value to Set.
	 */
	public void setDiastolic(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/diastolic",v);
		_Diastolic=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Condition=null;

	/**
	 * @return Returns the condition.
	 */
	public String getCondition(){
		try{
			if (_Condition==null){
				_Condition=getStringProperty("condition");
				return _Condition;
			}else {
				return _Condition;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for condition.
	 * @param v Value to Set.
	 */
	public void setCondition(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/condition",v);
		_Condition=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CndaVitalsdataBloodpressureId=null;

	/**
	 * @return Returns the cnda_vitalsData_bloodPressure_id.
	 */
	public Integer getCndaVitalsdataBloodpressureId() {
		try{
			if (_CndaVitalsdataBloodpressureId==null){
				_CndaVitalsdataBloodpressureId=getIntegerProperty("cnda_vitalsData_bloodPressure_id");
				return _CndaVitalsdataBloodpressureId;
			}else {
				return _CndaVitalsdataBloodpressureId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda_vitalsData_bloodPressure_id.
	 * @param v Value to Set.
	 */
	public void setCndaVitalsdataBloodpressureId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cnda_vitalsData_bloodPressure_id",v);
		_CndaVitalsdataBloodpressureId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaVitalsdataBloodpressure> getAllCndaVitalsdataBloodpressures(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaVitalsdataBloodpressure> al = new ArrayList<org.nrg.xdat.om.CndaVitalsdataBloodpressure>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaVitalsdataBloodpressure> getCndaVitalsdataBloodpressuresByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaVitalsdataBloodpressure> al = new ArrayList<org.nrg.xdat.om.CndaVitalsdataBloodpressure>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaVitalsdataBloodpressure> getCndaVitalsdataBloodpressuresByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaVitalsdataBloodpressure> al = new ArrayList<org.nrg.xdat.om.CndaVitalsdataBloodpressure>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaVitalsdataBloodpressure getCndaVitalsdataBloodpressuresByCndaVitalsdataBloodpressureId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:vitalsData_bloodPressure/cnda_vitalsData_bloodPressure_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaVitalsdataBloodpressure) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
