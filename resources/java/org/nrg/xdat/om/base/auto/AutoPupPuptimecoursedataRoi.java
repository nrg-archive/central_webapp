/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:47 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoPupPuptimecoursedataRoi extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.PupPuptimecoursedataRoiI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoPupPuptimecoursedataRoi.class);
	public static String SCHEMA_ELEMENT_NAME="pup:pupTimeCourseData_roi";

	public AutoPupPuptimecoursedataRoi(ItemI item)
	{
		super(item);
	}

	public AutoPupPuptimecoursedataRoi(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoPupPuptimecoursedataRoi(UserI user)
	 **/
	public AutoPupPuptimecoursedataRoi(){}

	public AutoPupPuptimecoursedataRoi(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "pup:pupTimeCourseData_roi";
	}

	//FIELD

	private Integer _Nvox=null;

	/**
	 * @return Returns the NVox.
	 */
	public Integer getNvox() {
		try{
			if (_Nvox==null){
				_Nvox=getIntegerProperty("NVox");
				return _Nvox;
			}else {
				return _Nvox;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for NVox.
	 * @param v Value to Set.
	 */
	public void setNvox(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/NVox",v);
		_Nvox=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Bp=null;

	/**
	 * @return Returns the BP.
	 */
	public Double getBp() {
		try{
			if (_Bp==null){
				_Bp=getDoubleProperty("BP");
				return _Bp;
			}else {
				return _Bp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BP.
	 * @param v Value to Set.
	 */
	public void setBp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BP",v);
		_Bp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _R2=null;

	/**
	 * @return Returns the R2.
	 */
	public Double getR2() {
		try{
			if (_R2==null){
				_R2=getDoubleProperty("R2");
				return _R2;
			}else {
				return _R2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for R2.
	 * @param v Value to Set.
	 */
	public void setR2(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/R2",v);
		_R2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Intc=null;

	/**
	 * @return Returns the INTC.
	 */
	public Double getIntc() {
		try{
			if (_Intc==null){
				_Intc=getDoubleProperty("INTC");
				return _Intc;
			}else {
				return _Intc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INTC.
	 * @param v Value to Set.
	 */
	public void setIntc(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INTC",v);
		_Intc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Suvr=null;

	/**
	 * @return Returns the SUVR.
	 */
	public Double getSuvr() {
		try{
			if (_Suvr==null){
				_Suvr=getDoubleProperty("SUVR");
				return _Suvr;
			}else {
				return _Suvr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SUVR.
	 * @param v Value to Set.
	 */
	public void setSuvr(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SUVR",v);
		_Suvr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _BpRsf=null;

	/**
	 * @return Returns the BP_RSF.
	 */
	public Double getBpRsf() {
		try{
			if (_BpRsf==null){
				_BpRsf=getDoubleProperty("BP_RSF");
				return _BpRsf;
			}else {
				return _BpRsf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BP_RSF.
	 * @param v Value to Set.
	 */
	public void setBpRsf(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BP_RSF",v);
		_BpRsf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _R2Rsf=null;

	/**
	 * @return Returns the R2_RSF.
	 */
	public Double getR2Rsf() {
		try{
			if (_R2Rsf==null){
				_R2Rsf=getDoubleProperty("R2_RSF");
				return _R2Rsf;
			}else {
				return _R2Rsf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for R2_RSF.
	 * @param v Value to Set.
	 */
	public void setR2Rsf(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/R2_RSF",v);
		_R2Rsf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _IntcRsf=null;

	/**
	 * @return Returns the INTC_RSF.
	 */
	public Double getIntcRsf() {
		try{
			if (_IntcRsf==null){
				_IntcRsf=getDoubleProperty("INTC_RSF");
				return _IntcRsf;
			}else {
				return _IntcRsf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INTC_RSF.
	 * @param v Value to Set.
	 */
	public void setIntcRsf(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INTC_RSF",v);
		_IntcRsf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _SuvrRsf=null;

	/**
	 * @return Returns the SUVR_RSF.
	 */
	public Double getSuvrRsf() {
		try{
			if (_SuvrRsf==null){
				_SuvrRsf=getDoubleProperty("SUVR_RSF");
				return _SuvrRsf;
			}else {
				return _SuvrRsf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SUVR_RSF.
	 * @param v Value to Set.
	 */
	public void setSuvrRsf(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SUVR_RSF",v);
		_SuvrRsf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _BpPvc2c=null;

	/**
	 * @return Returns the BP_PVC2C.
	 */
	public Double getBpPvc2c() {
		try{
			if (_BpPvc2c==null){
				_BpPvc2c=getDoubleProperty("BP_PVC2C");
				return _BpPvc2c;
			}else {
				return _BpPvc2c;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BP_PVC2C.
	 * @param v Value to Set.
	 */
	public void setBpPvc2c(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BP_PVC2C",v);
		_BpPvc2c=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _R2Pvc2c=null;

	/**
	 * @return Returns the R2_PVC2C.
	 */
	public Double getR2Pvc2c() {
		try{
			if (_R2Pvc2c==null){
				_R2Pvc2c=getDoubleProperty("R2_PVC2C");
				return _R2Pvc2c;
			}else {
				return _R2Pvc2c;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for R2_PVC2C.
	 * @param v Value to Set.
	 */
	public void setR2Pvc2c(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/R2_PVC2C",v);
		_R2Pvc2c=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _IntcPvc2c=null;

	/**
	 * @return Returns the INTC_PVC2C.
	 */
	public Double getIntcPvc2c() {
		try{
			if (_IntcPvc2c==null){
				_IntcPvc2c=getDoubleProperty("INTC_PVC2C");
				return _IntcPvc2c;
			}else {
				return _IntcPvc2c;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for INTC_PVC2C.
	 * @param v Value to Set.
	 */
	public void setIntcPvc2c(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/INTC_PVC2C",v);
		_IntcPvc2c=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _SuvrPvc2c=null;

	/**
	 * @return Returns the SUVR_PVC2C.
	 */
	public Double getSuvrPvc2c() {
		try{
			if (_SuvrPvc2c==null){
				_SuvrPvc2c=getDoubleProperty("SUVR_PVC2C");
				return _SuvrPvc2c;
			}else {
				return _SuvrPvc2c;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SUVR_PVC2C.
	 * @param v Value to Set.
	 */
	public void setSuvrPvc2c(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SUVR_PVC2C",v);
		_SuvrPvc2c=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Name=null;

	/**
	 * @return Returns the name.
	 */
	public String getName(){
		try{
			if (_Name==null){
				_Name=getStringProperty("name");
				return _Name;
			}else {
				return _Name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/name",v);
		_Name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _PupPuptimecoursedataRoiId=null;

	/**
	 * @return Returns the pup_pupTimeCourseData_roi_id.
	 */
	public Integer getPupPuptimecoursedataRoiId() {
		try{
			if (_PupPuptimecoursedataRoiId==null){
				_PupPuptimecoursedataRoiId=getIntegerProperty("pup_pupTimeCourseData_roi_id");
				return _PupPuptimecoursedataRoiId;
			}else {
				return _PupPuptimecoursedataRoiId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pup_pupTimeCourseData_roi_id.
	 * @param v Value to Set.
	 */
	public void setPupPuptimecoursedataRoiId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pup_pupTimeCourseData_roi_id",v);
		_PupPuptimecoursedataRoiId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi> getAllPupPuptimecoursedataRois(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi> al = new ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi> getPupPuptimecoursedataRoisByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi> al = new ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi> getPupPuptimecoursedataRoisByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi> al = new ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static PupPuptimecoursedataRoi getPupPuptimecoursedataRoisByPupPuptimecoursedataRoiId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("pup:pupTimeCourseData_roi/pup_pupTimeCourseData_roi_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (PupPuptimecoursedataRoi) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
