/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:47 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoSapssansSymptomssapssans extends XnatSubjectassessordata implements org.nrg.xdat.model.SapssansSymptomssapssansI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoSapssansSymptomssapssans.class);
	public static String SCHEMA_ELEMENT_NAME="sapssans:symptomsSAPSSANS";

	public AutoSapssansSymptomssapssans(ItemI item)
	{
		super(item);
	}

	public AutoSapssansSymptomssapssans(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoSapssansSymptomssapssans(UserI user)
	 **/
	public AutoSapssansSymptomssapssans(){}

	public AutoSapssansSymptomssapssans(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "sapssans:symptomsSAPSSANS";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps1=null;

	/**
	 * @return Returns the saps/saps1.
	 */
	public String getSaps_saps1(){
		try{
			if (_Saps_saps1==null){
				_Saps_saps1=getStringProperty("saps/saps1");
				return _Saps_saps1;
			}else {
				return _Saps_saps1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps1.
	 * @param v Value to Set.
	 */
	public void setSaps_saps1(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps1",v);
		_Saps_saps1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps2=null;

	/**
	 * @return Returns the saps/saps2.
	 */
	public String getSaps_saps2(){
		try{
			if (_Saps_saps2==null){
				_Saps_saps2=getStringProperty("saps/saps2");
				return _Saps_saps2;
			}else {
				return _Saps_saps2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps2.
	 * @param v Value to Set.
	 */
	public void setSaps_saps2(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps2",v);
		_Saps_saps2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps3=null;

	/**
	 * @return Returns the saps/saps3.
	 */
	public String getSaps_saps3(){
		try{
			if (_Saps_saps3==null){
				_Saps_saps3=getStringProperty("saps/saps3");
				return _Saps_saps3;
			}else {
				return _Saps_saps3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps3.
	 * @param v Value to Set.
	 */
	public void setSaps_saps3(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps3",v);
		_Saps_saps3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps4=null;

	/**
	 * @return Returns the saps/saps4.
	 */
	public String getSaps_saps4(){
		try{
			if (_Saps_saps4==null){
				_Saps_saps4=getStringProperty("saps/saps4");
				return _Saps_saps4;
			}else {
				return _Saps_saps4;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps4.
	 * @param v Value to Set.
	 */
	public void setSaps_saps4(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps4",v);
		_Saps_saps4=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps5=null;

	/**
	 * @return Returns the saps/saps5.
	 */
	public String getSaps_saps5(){
		try{
			if (_Saps_saps5==null){
				_Saps_saps5=getStringProperty("saps/saps5");
				return _Saps_saps5;
			}else {
				return _Saps_saps5;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps5.
	 * @param v Value to Set.
	 */
	public void setSaps_saps5(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps5",v);
		_Saps_saps5=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps6=null;

	/**
	 * @return Returns the saps/saps6.
	 */
	public String getSaps_saps6(){
		try{
			if (_Saps_saps6==null){
				_Saps_saps6=getStringProperty("saps/saps6");
				return _Saps_saps6;
			}else {
				return _Saps_saps6;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps6.
	 * @param v Value to Set.
	 */
	public void setSaps_saps6(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps6",v);
		_Saps_saps6=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps7=null;

	/**
	 * @return Returns the saps/saps7.
	 */
	public String getSaps_saps7(){
		try{
			if (_Saps_saps7==null){
				_Saps_saps7=getStringProperty("saps/saps7");
				return _Saps_saps7;
			}else {
				return _Saps_saps7;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps7.
	 * @param v Value to Set.
	 */
	public void setSaps_saps7(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps7",v);
		_Saps_saps7=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps8=null;

	/**
	 * @return Returns the saps/saps8.
	 */
	public String getSaps_saps8(){
		try{
			if (_Saps_saps8==null){
				_Saps_saps8=getStringProperty("saps/saps8");
				return _Saps_saps8;
			}else {
				return _Saps_saps8;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps8.
	 * @param v Value to Set.
	 */
	public void setSaps_saps8(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps8",v);
		_Saps_saps8=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps9=null;

	/**
	 * @return Returns the saps/saps9.
	 */
	public String getSaps_saps9(){
		try{
			if (_Saps_saps9==null){
				_Saps_saps9=getStringProperty("saps/saps9");
				return _Saps_saps9;
			}else {
				return _Saps_saps9;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps9.
	 * @param v Value to Set.
	 */
	public void setSaps_saps9(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps9",v);
		_Saps_saps9=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps10=null;

	/**
	 * @return Returns the saps/saps10.
	 */
	public String getSaps_saps10(){
		try{
			if (_Saps_saps10==null){
				_Saps_saps10=getStringProperty("saps/saps10");
				return _Saps_saps10;
			}else {
				return _Saps_saps10;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps10.
	 * @param v Value to Set.
	 */
	public void setSaps_saps10(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps10",v);
		_Saps_saps10=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps11=null;

	/**
	 * @return Returns the saps/saps11.
	 */
	public String getSaps_saps11(){
		try{
			if (_Saps_saps11==null){
				_Saps_saps11=getStringProperty("saps/saps11");
				return _Saps_saps11;
			}else {
				return _Saps_saps11;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps11.
	 * @param v Value to Set.
	 */
	public void setSaps_saps11(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps11",v);
		_Saps_saps11=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps12=null;

	/**
	 * @return Returns the saps/saps12.
	 */
	public String getSaps_saps12(){
		try{
			if (_Saps_saps12==null){
				_Saps_saps12=getStringProperty("saps/saps12");
				return _Saps_saps12;
			}else {
				return _Saps_saps12;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps12.
	 * @param v Value to Set.
	 */
	public void setSaps_saps12(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps12",v);
		_Saps_saps12=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps13=null;

	/**
	 * @return Returns the saps/saps13.
	 */
	public String getSaps_saps13(){
		try{
			if (_Saps_saps13==null){
				_Saps_saps13=getStringProperty("saps/saps13");
				return _Saps_saps13;
			}else {
				return _Saps_saps13;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps13.
	 * @param v Value to Set.
	 */
	public void setSaps_saps13(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps13",v);
		_Saps_saps13=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps14=null;

	/**
	 * @return Returns the saps/saps14.
	 */
	public String getSaps_saps14(){
		try{
			if (_Saps_saps14==null){
				_Saps_saps14=getStringProperty("saps/saps14");
				return _Saps_saps14;
			}else {
				return _Saps_saps14;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps14.
	 * @param v Value to Set.
	 */
	public void setSaps_saps14(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps14",v);
		_Saps_saps14=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps15=null;

	/**
	 * @return Returns the saps/saps15.
	 */
	public String getSaps_saps15(){
		try{
			if (_Saps_saps15==null){
				_Saps_saps15=getStringProperty("saps/saps15");
				return _Saps_saps15;
			}else {
				return _Saps_saps15;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps15.
	 * @param v Value to Set.
	 */
	public void setSaps_saps15(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps15",v);
		_Saps_saps15=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps16=null;

	/**
	 * @return Returns the saps/saps16.
	 */
	public String getSaps_saps16(){
		try{
			if (_Saps_saps16==null){
				_Saps_saps16=getStringProperty("saps/saps16");
				return _Saps_saps16;
			}else {
				return _Saps_saps16;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps16.
	 * @param v Value to Set.
	 */
	public void setSaps_saps16(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps16",v);
		_Saps_saps16=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps17=null;

	/**
	 * @return Returns the saps/saps17.
	 */
	public String getSaps_saps17(){
		try{
			if (_Saps_saps17==null){
				_Saps_saps17=getStringProperty("saps/saps17");
				return _Saps_saps17;
			}else {
				return _Saps_saps17;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps17.
	 * @param v Value to Set.
	 */
	public void setSaps_saps17(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps17",v);
		_Saps_saps17=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps18=null;

	/**
	 * @return Returns the saps/saps18.
	 */
	public String getSaps_saps18(){
		try{
			if (_Saps_saps18==null){
				_Saps_saps18=getStringProperty("saps/saps18");
				return _Saps_saps18;
			}else {
				return _Saps_saps18;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps18.
	 * @param v Value to Set.
	 */
	public void setSaps_saps18(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps18",v);
		_Saps_saps18=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps19=null;

	/**
	 * @return Returns the saps/saps19.
	 */
	public String getSaps_saps19(){
		try{
			if (_Saps_saps19==null){
				_Saps_saps19=getStringProperty("saps/saps19");
				return _Saps_saps19;
			}else {
				return _Saps_saps19;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps19.
	 * @param v Value to Set.
	 */
	public void setSaps_saps19(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps19",v);
		_Saps_saps19=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps20=null;

	/**
	 * @return Returns the saps/saps20.
	 */
	public String getSaps_saps20(){
		try{
			if (_Saps_saps20==null){
				_Saps_saps20=getStringProperty("saps/saps20");
				return _Saps_saps20;
			}else {
				return _Saps_saps20;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps20.
	 * @param v Value to Set.
	 */
	public void setSaps_saps20(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps20",v);
		_Saps_saps20=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps21=null;

	/**
	 * @return Returns the saps/saps21.
	 */
	public String getSaps_saps21(){
		try{
			if (_Saps_saps21==null){
				_Saps_saps21=getStringProperty("saps/saps21");
				return _Saps_saps21;
			}else {
				return _Saps_saps21;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps21.
	 * @param v Value to Set.
	 */
	public void setSaps_saps21(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps21",v);
		_Saps_saps21=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps22=null;

	/**
	 * @return Returns the saps/saps22.
	 */
	public String getSaps_saps22(){
		try{
			if (_Saps_saps22==null){
				_Saps_saps22=getStringProperty("saps/saps22");
				return _Saps_saps22;
			}else {
				return _Saps_saps22;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps22.
	 * @param v Value to Set.
	 */
	public void setSaps_saps22(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps22",v);
		_Saps_saps22=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps23=null;

	/**
	 * @return Returns the saps/saps23.
	 */
	public String getSaps_saps23(){
		try{
			if (_Saps_saps23==null){
				_Saps_saps23=getStringProperty("saps/saps23");
				return _Saps_saps23;
			}else {
				return _Saps_saps23;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps23.
	 * @param v Value to Set.
	 */
	public void setSaps_saps23(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps23",v);
		_Saps_saps23=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps24=null;

	/**
	 * @return Returns the saps/saps24.
	 */
	public String getSaps_saps24(){
		try{
			if (_Saps_saps24==null){
				_Saps_saps24=getStringProperty("saps/saps24");
				return _Saps_saps24;
			}else {
				return _Saps_saps24;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps24.
	 * @param v Value to Set.
	 */
	public void setSaps_saps24(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps24",v);
		_Saps_saps24=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps25=null;

	/**
	 * @return Returns the saps/saps25.
	 */
	public String getSaps_saps25(){
		try{
			if (_Saps_saps25==null){
				_Saps_saps25=getStringProperty("saps/saps25");
				return _Saps_saps25;
			}else {
				return _Saps_saps25;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps25.
	 * @param v Value to Set.
	 */
	public void setSaps_saps25(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps25",v);
		_Saps_saps25=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps26=null;

	/**
	 * @return Returns the saps/saps26.
	 */
	public String getSaps_saps26(){
		try{
			if (_Saps_saps26==null){
				_Saps_saps26=getStringProperty("saps/saps26");
				return _Saps_saps26;
			}else {
				return _Saps_saps26;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps26.
	 * @param v Value to Set.
	 */
	public void setSaps_saps26(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps26",v);
		_Saps_saps26=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps27=null;

	/**
	 * @return Returns the saps/saps27.
	 */
	public String getSaps_saps27(){
		try{
			if (_Saps_saps27==null){
				_Saps_saps27=getStringProperty("saps/saps27");
				return _Saps_saps27;
			}else {
				return _Saps_saps27;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps27.
	 * @param v Value to Set.
	 */
	public void setSaps_saps27(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps27",v);
		_Saps_saps27=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps28=null;

	/**
	 * @return Returns the saps/saps28.
	 */
	public String getSaps_saps28(){
		try{
			if (_Saps_saps28==null){
				_Saps_saps28=getStringProperty("saps/saps28");
				return _Saps_saps28;
			}else {
				return _Saps_saps28;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps28.
	 * @param v Value to Set.
	 */
	public void setSaps_saps28(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps28",v);
		_Saps_saps28=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps29=null;

	/**
	 * @return Returns the saps/saps29.
	 */
	public String getSaps_saps29(){
		try{
			if (_Saps_saps29==null){
				_Saps_saps29=getStringProperty("saps/saps29");
				return _Saps_saps29;
			}else {
				return _Saps_saps29;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps29.
	 * @param v Value to Set.
	 */
	public void setSaps_saps29(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps29",v);
		_Saps_saps29=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps30=null;

	/**
	 * @return Returns the saps/saps30.
	 */
	public String getSaps_saps30(){
		try{
			if (_Saps_saps30==null){
				_Saps_saps30=getStringProperty("saps/saps30");
				return _Saps_saps30;
			}else {
				return _Saps_saps30;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps30.
	 * @param v Value to Set.
	 */
	public void setSaps_saps30(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps30",v);
		_Saps_saps30=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps31=null;

	/**
	 * @return Returns the saps/saps31.
	 */
	public String getSaps_saps31(){
		try{
			if (_Saps_saps31==null){
				_Saps_saps31=getStringProperty("saps/saps31");
				return _Saps_saps31;
			}else {
				return _Saps_saps31;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps31.
	 * @param v Value to Set.
	 */
	public void setSaps_saps31(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps31",v);
		_Saps_saps31=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps32=null;

	/**
	 * @return Returns the saps/saps32.
	 */
	public String getSaps_saps32(){
		try{
			if (_Saps_saps32==null){
				_Saps_saps32=getStringProperty("saps/saps32");
				return _Saps_saps32;
			}else {
				return _Saps_saps32;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps32.
	 * @param v Value to Set.
	 */
	public void setSaps_saps32(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps32",v);
		_Saps_saps32=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps33=null;

	/**
	 * @return Returns the saps/saps33.
	 */
	public String getSaps_saps33(){
		try{
			if (_Saps_saps33==null){
				_Saps_saps33=getStringProperty("saps/saps33");
				return _Saps_saps33;
			}else {
				return _Saps_saps33;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps33.
	 * @param v Value to Set.
	 */
	public void setSaps_saps33(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps33",v);
		_Saps_saps33=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Saps_saps34=null;

	/**
	 * @return Returns the saps/saps34.
	 */
	public String getSaps_saps34(){
		try{
			if (_Saps_saps34==null){
				_Saps_saps34=getStringProperty("saps/saps34");
				return _Saps_saps34;
			}else {
				return _Saps_saps34;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for saps/saps34.
	 * @param v Value to Set.
	 */
	public void setSaps_saps34(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/saps/saps34",v);
		_Saps_saps34=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans1=null;

	/**
	 * @return Returns the sans/sans1.
	 */
	public String getSans_sans1(){
		try{
			if (_Sans_sans1==null){
				_Sans_sans1=getStringProperty("sans/sans1");
				return _Sans_sans1;
			}else {
				return _Sans_sans1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans1.
	 * @param v Value to Set.
	 */
	public void setSans_sans1(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans1",v);
		_Sans_sans1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans2=null;

	/**
	 * @return Returns the sans/sans2.
	 */
	public String getSans_sans2(){
		try{
			if (_Sans_sans2==null){
				_Sans_sans2=getStringProperty("sans/sans2");
				return _Sans_sans2;
			}else {
				return _Sans_sans2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans2.
	 * @param v Value to Set.
	 */
	public void setSans_sans2(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans2",v);
		_Sans_sans2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans3=null;

	/**
	 * @return Returns the sans/sans3.
	 */
	public String getSans_sans3(){
		try{
			if (_Sans_sans3==null){
				_Sans_sans3=getStringProperty("sans/sans3");
				return _Sans_sans3;
			}else {
				return _Sans_sans3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans3.
	 * @param v Value to Set.
	 */
	public void setSans_sans3(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans3",v);
		_Sans_sans3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans4=null;

	/**
	 * @return Returns the sans/sans4.
	 */
	public String getSans_sans4(){
		try{
			if (_Sans_sans4==null){
				_Sans_sans4=getStringProperty("sans/sans4");
				return _Sans_sans4;
			}else {
				return _Sans_sans4;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans4.
	 * @param v Value to Set.
	 */
	public void setSans_sans4(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans4",v);
		_Sans_sans4=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans5=null;

	/**
	 * @return Returns the sans/sans5.
	 */
	public String getSans_sans5(){
		try{
			if (_Sans_sans5==null){
				_Sans_sans5=getStringProperty("sans/sans5");
				return _Sans_sans5;
			}else {
				return _Sans_sans5;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans5.
	 * @param v Value to Set.
	 */
	public void setSans_sans5(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans5",v);
		_Sans_sans5=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans6=null;

	/**
	 * @return Returns the sans/sans6.
	 */
	public String getSans_sans6(){
		try{
			if (_Sans_sans6==null){
				_Sans_sans6=getStringProperty("sans/sans6");
				return _Sans_sans6;
			}else {
				return _Sans_sans6;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans6.
	 * @param v Value to Set.
	 */
	public void setSans_sans6(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans6",v);
		_Sans_sans6=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans7=null;

	/**
	 * @return Returns the sans/sans7.
	 */
	public String getSans_sans7(){
		try{
			if (_Sans_sans7==null){
				_Sans_sans7=getStringProperty("sans/sans7");
				return _Sans_sans7;
			}else {
				return _Sans_sans7;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans7.
	 * @param v Value to Set.
	 */
	public void setSans_sans7(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans7",v);
		_Sans_sans7=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans8=null;

	/**
	 * @return Returns the sans/sans8.
	 */
	public String getSans_sans8(){
		try{
			if (_Sans_sans8==null){
				_Sans_sans8=getStringProperty("sans/sans8");
				return _Sans_sans8;
			}else {
				return _Sans_sans8;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans8.
	 * @param v Value to Set.
	 */
	public void setSans_sans8(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans8",v);
		_Sans_sans8=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans9=null;

	/**
	 * @return Returns the sans/sans9.
	 */
	public String getSans_sans9(){
		try{
			if (_Sans_sans9==null){
				_Sans_sans9=getStringProperty("sans/sans9");
				return _Sans_sans9;
			}else {
				return _Sans_sans9;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans9.
	 * @param v Value to Set.
	 */
	public void setSans_sans9(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans9",v);
		_Sans_sans9=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans10=null;

	/**
	 * @return Returns the sans/sans10.
	 */
	public String getSans_sans10(){
		try{
			if (_Sans_sans10==null){
				_Sans_sans10=getStringProperty("sans/sans10");
				return _Sans_sans10;
			}else {
				return _Sans_sans10;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans10.
	 * @param v Value to Set.
	 */
	public void setSans_sans10(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans10",v);
		_Sans_sans10=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans11=null;

	/**
	 * @return Returns the sans/sans11.
	 */
	public String getSans_sans11(){
		try{
			if (_Sans_sans11==null){
				_Sans_sans11=getStringProperty("sans/sans11");
				return _Sans_sans11;
			}else {
				return _Sans_sans11;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans11.
	 * @param v Value to Set.
	 */
	public void setSans_sans11(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans11",v);
		_Sans_sans11=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans12=null;

	/**
	 * @return Returns the sans/sans12.
	 */
	public String getSans_sans12(){
		try{
			if (_Sans_sans12==null){
				_Sans_sans12=getStringProperty("sans/sans12");
				return _Sans_sans12;
			}else {
				return _Sans_sans12;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans12.
	 * @param v Value to Set.
	 */
	public void setSans_sans12(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans12",v);
		_Sans_sans12=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans13=null;

	/**
	 * @return Returns the sans/sans13.
	 */
	public String getSans_sans13(){
		try{
			if (_Sans_sans13==null){
				_Sans_sans13=getStringProperty("sans/sans13");
				return _Sans_sans13;
			}else {
				return _Sans_sans13;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans13.
	 * @param v Value to Set.
	 */
	public void setSans_sans13(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans13",v);
		_Sans_sans13=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans14=null;

	/**
	 * @return Returns the sans/sans14.
	 */
	public String getSans_sans14(){
		try{
			if (_Sans_sans14==null){
				_Sans_sans14=getStringProperty("sans/sans14");
				return _Sans_sans14;
			}else {
				return _Sans_sans14;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans14.
	 * @param v Value to Set.
	 */
	public void setSans_sans14(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans14",v);
		_Sans_sans14=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans15=null;

	/**
	 * @return Returns the sans/sans15.
	 */
	public String getSans_sans15(){
		try{
			if (_Sans_sans15==null){
				_Sans_sans15=getStringProperty("sans/sans15");
				return _Sans_sans15;
			}else {
				return _Sans_sans15;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans15.
	 * @param v Value to Set.
	 */
	public void setSans_sans15(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans15",v);
		_Sans_sans15=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans16=null;

	/**
	 * @return Returns the sans/sans16.
	 */
	public String getSans_sans16(){
		try{
			if (_Sans_sans16==null){
				_Sans_sans16=getStringProperty("sans/sans16");
				return _Sans_sans16;
			}else {
				return _Sans_sans16;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans16.
	 * @param v Value to Set.
	 */
	public void setSans_sans16(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans16",v);
		_Sans_sans16=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans17=null;

	/**
	 * @return Returns the sans/sans17.
	 */
	public String getSans_sans17(){
		try{
			if (_Sans_sans17==null){
				_Sans_sans17=getStringProperty("sans/sans17");
				return _Sans_sans17;
			}else {
				return _Sans_sans17;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans17.
	 * @param v Value to Set.
	 */
	public void setSans_sans17(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans17",v);
		_Sans_sans17=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans18=null;

	/**
	 * @return Returns the sans/sans18.
	 */
	public String getSans_sans18(){
		try{
			if (_Sans_sans18==null){
				_Sans_sans18=getStringProperty("sans/sans18");
				return _Sans_sans18;
			}else {
				return _Sans_sans18;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans18.
	 * @param v Value to Set.
	 */
	public void setSans_sans18(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans18",v);
		_Sans_sans18=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans19=null;

	/**
	 * @return Returns the sans/sans19.
	 */
	public String getSans_sans19(){
		try{
			if (_Sans_sans19==null){
				_Sans_sans19=getStringProperty("sans/sans19");
				return _Sans_sans19;
			}else {
				return _Sans_sans19;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans19.
	 * @param v Value to Set.
	 */
	public void setSans_sans19(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans19",v);
		_Sans_sans19=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans20=null;

	/**
	 * @return Returns the sans/sans20.
	 */
	public String getSans_sans20(){
		try{
			if (_Sans_sans20==null){
				_Sans_sans20=getStringProperty("sans/sans20");
				return _Sans_sans20;
			}else {
				return _Sans_sans20;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans20.
	 * @param v Value to Set.
	 */
	public void setSans_sans20(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans20",v);
		_Sans_sans20=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans21=null;

	/**
	 * @return Returns the sans/sans21.
	 */
	public String getSans_sans21(){
		try{
			if (_Sans_sans21==null){
				_Sans_sans21=getStringProperty("sans/sans21");
				return _Sans_sans21;
			}else {
				return _Sans_sans21;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans21.
	 * @param v Value to Set.
	 */
	public void setSans_sans21(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans21",v);
		_Sans_sans21=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans22=null;

	/**
	 * @return Returns the sans/sans22.
	 */
	public String getSans_sans22(){
		try{
			if (_Sans_sans22==null){
				_Sans_sans22=getStringProperty("sans/sans22");
				return _Sans_sans22;
			}else {
				return _Sans_sans22;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans22.
	 * @param v Value to Set.
	 */
	public void setSans_sans22(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans22",v);
		_Sans_sans22=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans23=null;

	/**
	 * @return Returns the sans/sans23.
	 */
	public String getSans_sans23(){
		try{
			if (_Sans_sans23==null){
				_Sans_sans23=getStringProperty("sans/sans23");
				return _Sans_sans23;
			}else {
				return _Sans_sans23;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans23.
	 * @param v Value to Set.
	 */
	public void setSans_sans23(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans23",v);
		_Sans_sans23=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans24=null;

	/**
	 * @return Returns the sans/sans24.
	 */
	public String getSans_sans24(){
		try{
			if (_Sans_sans24==null){
				_Sans_sans24=getStringProperty("sans/sans24");
				return _Sans_sans24;
			}else {
				return _Sans_sans24;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans24.
	 * @param v Value to Set.
	 */
	public void setSans_sans24(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans24",v);
		_Sans_sans24=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sans_sans25=null;

	/**
	 * @return Returns the sans/sans25.
	 */
	public String getSans_sans25(){
		try{
			if (_Sans_sans25==null){
				_Sans_sans25=getStringProperty("sans/sans25");
				return _Sans_sans25;
			}else {
				return _Sans_sans25;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sans/sans25.
	 * @param v Value to Set.
	 */
	public void setSans_sans25(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sans/sans25",v);
		_Sans_sans25=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.SapssansSymptomssapssans> getAllSapssansSymptomssapssanss(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SapssansSymptomssapssans> al = new ArrayList<org.nrg.xdat.om.SapssansSymptomssapssans>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SapssansSymptomssapssans> getSapssansSymptomssapssanssByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SapssansSymptomssapssans> al = new ArrayList<org.nrg.xdat.om.SapssansSymptomssapssans>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SapssansSymptomssapssans> getSapssansSymptomssapssanssByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SapssansSymptomssapssans> al = new ArrayList<org.nrg.xdat.om.SapssansSymptomssapssans>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static SapssansSymptomssapssans getSapssansSymptomssapssanssById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("sapssans:symptomsSAPSSANS/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (SapssansSymptomssapssans) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
