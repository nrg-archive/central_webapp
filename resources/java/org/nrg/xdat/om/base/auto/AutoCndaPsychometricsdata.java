/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaPsychometricsdata extends XnatSubjectassessordata implements org.nrg.xdat.model.CndaPsychometricsdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaPsychometricsdata.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:psychometricsData";

	public AutoCndaPsychometricsdata(ItemI item)
	{
		super(item);
	}

	public AutoCndaPsychometricsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaPsychometricsdata(UserI user)
	 **/
	public AutoCndaPsychometricsdata(){}

	public AutoCndaPsychometricsdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:psychometricsData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Wms_logmemory=null;

	/**
	 * @return Returns the WMS/LogMemory.
	 */
	public Double getWms_logmemory() {
		try{
			if (_Wms_logmemory==null){
				_Wms_logmemory=getDoubleProperty("WMS/LogMemory");
				return _Wms_logmemory;
			}else {
				return _Wms_logmemory;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS/LogMemory.
	 * @param v Value to Set.
	 */
	public void setWms_logmemory(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS/LogMemory",v);
		_Wms_logmemory=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Wms_logmemoryrecall=null;

	/**
	 * @return Returns the WMS/LogMemoryRecall.
	 */
	public Double getWms_logmemoryrecall() {
		try{
			if (_Wms_logmemoryrecall==null){
				_Wms_logmemoryrecall=getDoubleProperty("WMS/LogMemoryRecall");
				return _Wms_logmemoryrecall;
			}else {
				return _Wms_logmemoryrecall;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS/LogMemoryRecall.
	 * @param v Value to Set.
	 */
	public void setWms_logmemoryrecall(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS/LogMemoryRecall",v);
		_Wms_logmemoryrecall=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wms_digitspan=null;

	/**
	 * @return Returns the WMS/DigitSpan.
	 */
	public Integer getWms_digitspan() {
		try{
			if (_Wms_digitspan==null){
				_Wms_digitspan=getIntegerProperty("WMS/DigitSpan");
				return _Wms_digitspan;
			}else {
				return _Wms_digitspan;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS/DigitSpan.
	 * @param v Value to Set.
	 */
	public void setWms_digitspan(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS/DigitSpan",v);
		_Wms_digitspan=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wms_assocrecalleasy=null;

	/**
	 * @return Returns the WMS/AssocRecallEasy.
	 */
	public Integer getWms_assocrecalleasy() {
		try{
			if (_Wms_assocrecalleasy==null){
				_Wms_assocrecalleasy=getIntegerProperty("WMS/AssocRecallEasy");
				return _Wms_assocrecalleasy;
			}else {
				return _Wms_assocrecalleasy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS/AssocRecallEasy.
	 * @param v Value to Set.
	 */
	public void setWms_assocrecalleasy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS/AssocRecallEasy",v);
		_Wms_assocrecalleasy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wms_assocrecallhard=null;

	/**
	 * @return Returns the WMS/AssocRecallHard.
	 */
	public Integer getWms_assocrecallhard() {
		try{
			if (_Wms_assocrecallhard==null){
				_Wms_assocrecallhard=getIntegerProperty("WMS/AssocRecallHard");
				return _Wms_assocrecallhard;
			}else {
				return _Wms_assocrecallhard;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS/AssocRecallHard.
	 * @param v Value to Set.
	 */
	public void setWms_assocrecallhard(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS/AssocRecallHard",v);
		_Wms_assocrecallhard=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wms_assocrecogeasy=null;

	/**
	 * @return Returns the WMS/AssocRecogEasy.
	 */
	public Integer getWms_assocrecogeasy() {
		try{
			if (_Wms_assocrecogeasy==null){
				_Wms_assocrecogeasy=getIntegerProperty("WMS/AssocRecogEasy");
				return _Wms_assocrecogeasy;
			}else {
				return _Wms_assocrecogeasy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS/AssocRecogEasy.
	 * @param v Value to Set.
	 */
	public void setWms_assocrecogeasy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS/AssocRecogEasy",v);
		_Wms_assocrecogeasy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wms_assocrecoghard=null;

	/**
	 * @return Returns the WMS/AssocRecogHard.
	 */
	public Integer getWms_assocrecoghard() {
		try{
			if (_Wms_assocrecoghard==null){
				_Wms_assocrecoghard=getIntegerProperty("WMS/AssocRecogHard");
				return _Wms_assocrecoghard;
			}else {
				return _Wms_assocrecoghard;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS/AssocRecogHard.
	 * @param v Value to Set.
	 */
	public void setWms_assocrecoghard(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS/AssocRecogHard",v);
		_Wms_assocrecoghard=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Wms_assocmemrecall=null;

	/**
	 * @return Returns the WMS/AssocMemRecall.
	 */
	public Double getWms_assocmemrecall() {
		try{
			if (_Wms_assocmemrecall==null){
				_Wms_assocmemrecall=getDoubleProperty("WMS/AssocMemRecall");
				return _Wms_assocmemrecall;
			}else {
				return _Wms_assocmemrecall;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS/AssocMemRecall.
	 * @param v Value to Set.
	 */
	public void setWms_assocmemrecall(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS/AssocMemRecall",v);
		_Wms_assocmemrecall=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wms_mentalcontrol=null;

	/**
	 * @return Returns the WMS/MentalControl.
	 */
	public Integer getWms_mentalcontrol() {
		try{
			if (_Wms_mentalcontrol==null){
				_Wms_mentalcontrol=getIntegerProperty("WMS/MentalControl");
				return _Wms_mentalcontrol;
			}else {
				return _Wms_mentalcontrol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WMS/MentalControl.
	 * @param v Value to Set.
	 */
	public void setWms_mentalcontrol(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WMS/MentalControl",v);
		_Wms_mentalcontrol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TrailsA=null;

	/**
	 * @return Returns the Trails_A.
	 */
	public Integer getTrailsA() {
		try{
			if (_TrailsA==null){
				_TrailsA=getIntegerProperty("Trails_A");
				return _TrailsA;
			}else {
				return _TrailsA;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Trails_A.
	 * @param v Value to Set.
	 */
	public void setTrailsA(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Trails_A",v);
		_TrailsA=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TrailsB=null;

	/**
	 * @return Returns the Trails_B.
	 */
	public Integer getTrailsB() {
		try{
			if (_TrailsB==null){
				_TrailsB=getIntegerProperty("Trails_B");
				return _TrailsB;
			}else {
				return _TrailsB;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Trails_B.
	 * @param v Value to Set.
	 */
	public void setTrailsB(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Trails_B",v);
		_TrailsB=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _TrailsBc=null;

	/**
	 * @return Returns the Trails_B_C.
	 */
	public Integer getTrailsBc() {
		try{
			if (_TrailsBc==null){
				_TrailsBc=getIntegerProperty("Trails_B_C");
				return _TrailsBc;
			}else {
				return _TrailsBc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Trails_B_C.
	 * @param v Value to Set.
	 */
	public void setTrailsBc(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Trails_B_C",v);
		_TrailsBc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wais_info=null;

	/**
	 * @return Returns the WAIS/Info.
	 */
	public Integer getWais_info() {
		try{
			if (_Wais_info==null){
				_Wais_info=getIntegerProperty("WAIS/Info");
				return _Wais_info;
			}else {
				return _Wais_info;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WAIS/Info.
	 * @param v Value to Set.
	 */
	public void setWais_info(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WAIS/Info",v);
		_Wais_info=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wais_blockdesign=null;

	/**
	 * @return Returns the WAIS/BlockDesign.
	 */
	public Integer getWais_blockdesign() {
		try{
			if (_Wais_blockdesign==null){
				_Wais_blockdesign=getIntegerProperty("WAIS/BlockDesign");
				return _Wais_blockdesign;
			}else {
				return _Wais_blockdesign;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WAIS/BlockDesign.
	 * @param v Value to Set.
	 */
	public void setWais_blockdesign(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WAIS/BlockDesign",v);
		_Wais_blockdesign=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wais_digitsymbol=null;

	/**
	 * @return Returns the WAIS/DigitSymbol.
	 */
	public Integer getWais_digitsymbol() {
		try{
			if (_Wais_digitsymbol==null){
				_Wais_digitsymbol=getIntegerProperty("WAIS/DigitSymbol");
				return _Wais_digitsymbol;
			}else {
				return _Wais_digitsymbol;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WAIS/DigitSymbol.
	 * @param v Value to Set.
	 */
	public void setWais_digitsymbol(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WAIS/DigitSymbol",v);
		_Wais_digitsymbol=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bentonformcdelay=null;

	/**
	 * @return Returns the BentonFormCDelay.
	 */
	public Integer getBentonformcdelay() {
		try{
			if (_Bentonformcdelay==null){
				_Bentonformcdelay=getIntegerProperty("BentonFormCDelay");
				return _Bentonformcdelay;
			}else {
				return _Bentonformcdelay;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BentonFormCDelay.
	 * @param v Value to Set.
	 */
	public void setBentonformcdelay(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BentonFormCDelay",v);
		_Bentonformcdelay=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bentonformdcopy=null;

	/**
	 * @return Returns the BentonFormDCopy.
	 */
	public Integer getBentonformdcopy() {
		try{
			if (_Bentonformdcopy==null){
				_Bentonformdcopy=getIntegerProperty("BentonFormDCopy");
				return _Bentonformdcopy;
			}else {
				return _Bentonformdcopy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BentonFormDCopy.
	 * @param v Value to Set.
	 */
	public void setBentonformdcopy(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BentonFormDCopy",v);
		_Bentonformdcopy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bostonnaming=null;

	/**
	 * @return Returns the BostonNaming.
	 */
	public Integer getBostonnaming() {
		try{
			if (_Bostonnaming==null){
				_Bostonnaming=getIntegerProperty("BostonNaming");
				return _Bostonnaming;
			}else {
				return _Bostonnaming;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BostonNaming.
	 * @param v Value to Set.
	 */
	public void setBostonnaming(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BostonNaming",v);
		_Bostonnaming=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Crossingoff=null;

	/**
	 * @return Returns the CrossingOff.
	 */
	public Integer getCrossingoff() {
		try{
			if (_Crossingoff==null){
				_Crossingoff=getIntegerProperty("CrossingOff");
				return _Crossingoff;
			}else {
				return _Crossingoff;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CrossingOff.
	 * @param v Value to Set.
	 */
	public void setCrossingoff(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CrossingOff",v);
		_Crossingoff=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wordfluency=null;

	/**
	 * @return Returns the WordFluency.
	 */
	public Integer getWordfluency() {
		try{
			if (_Wordfluency==null){
				_Wordfluency=getIntegerProperty("WordFluency");
				return _Wordfluency;
			}else {
				return _Wordfluency;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for WordFluency.
	 * @param v Value to Set.
	 */
	public void setWordfluency(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/WordFluency",v);
		_Wordfluency=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Generalfactor=null;

	/**
	 * @return Returns the GeneralFactor.
	 */
	public Double getGeneralfactor() {
		try{
			if (_Generalfactor==null){
				_Generalfactor=getDoubleProperty("GeneralFactor");
				return _Generalfactor;
			}else {
				return _Generalfactor;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for GeneralFactor.
	 * @param v Value to Set.
	 */
	public void setGeneralfactor(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/GeneralFactor",v);
		_Generalfactor=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Kanne_temporal=null;

	/**
	 * @return Returns the Kanne/Temporal.
	 */
	public Double getKanne_temporal() {
		try{
			if (_Kanne_temporal==null){
				_Kanne_temporal=getDoubleProperty("Kanne/Temporal");
				return _Kanne_temporal;
			}else {
				return _Kanne_temporal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Kanne/Temporal.
	 * @param v Value to Set.
	 */
	public void setKanne_temporal(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Kanne/Temporal",v);
		_Kanne_temporal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Kanne_parietal=null;

	/**
	 * @return Returns the Kanne/Parietal.
	 */
	public Double getKanne_parietal() {
		try{
			if (_Kanne_parietal==null){
				_Kanne_parietal=getDoubleProperty("Kanne/Parietal");
				return _Kanne_parietal;
			}else {
				return _Kanne_parietal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Kanne/Parietal.
	 * @param v Value to Set.
	 */
	public void setKanne_parietal(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Kanne/Parietal",v);
		_Kanne_parietal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Kanne_frontal=null;

	/**
	 * @return Returns the Kanne/Frontal.
	 */
	public Double getKanne_frontal() {
		try{
			if (_Kanne_frontal==null){
				_Kanne_frontal=getDoubleProperty("Kanne/Frontal");
				return _Kanne_frontal;
			}else {
				return _Kanne_frontal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Kanne/Frontal.
	 * @param v Value to Set.
	 */
	public void setKanne_frontal(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Kanne/Frontal",v);
		_Kanne_frontal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Animal=null;

	/**
	 * @return Returns the Animal.
	 */
	public Integer getAnimal() {
		try{
			if (_Animal==null){
				_Animal=getIntegerProperty("Animal");
				return _Animal;
			}else {
				return _Animal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Animal.
	 * @param v Value to Set.
	 */
	public void setAnimal(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Animal",v);
		_Animal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaPsychometricsdata> getAllCndaPsychometricsdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPsychometricsdata> al = new ArrayList<org.nrg.xdat.om.CndaPsychometricsdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaPsychometricsdata> getCndaPsychometricsdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPsychometricsdata> al = new ArrayList<org.nrg.xdat.om.CndaPsychometricsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaPsychometricsdata> getCndaPsychometricsdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPsychometricsdata> al = new ArrayList<org.nrg.xdat.om.CndaPsychometricsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaPsychometricsdata getCndaPsychometricsdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:psychometricsData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaPsychometricsdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
