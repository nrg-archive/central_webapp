/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaAtrophynildataPeak extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CndaAtrophynildataPeakI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaAtrophynildataPeak.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:atrophyNilData_peak";

	public AutoCndaAtrophynildataPeak(ItemI item)
	{
		super(item);
	}

	public AutoCndaAtrophynildataPeak(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaAtrophynildataPeak(UserI user)
	 **/
	public AutoCndaAtrophynildataPeak(){}

	public AutoCndaAtrophynildataPeak(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:atrophyNilData_peak";
	}

	//FIELD

	private Double _Location=null;

	/**
	 * @return Returns the location.
	 */
	public Double getLocation() {
		try{
			if (_Location==null){
				_Location=getDoubleProperty("location");
				return _Location;
			}else {
				return _Location;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for location.
	 * @param v Value to Set.
	 */
	public void setLocation(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/location",v);
		_Location=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Magnitude=null;

	/**
	 * @return Returns the magnitude.
	 */
	public Double getMagnitude() {
		try{
			if (_Magnitude==null){
				_Magnitude=getDoubleProperty("magnitude");
				return _Magnitude;
			}else {
				return _Magnitude;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for magnitude.
	 * @param v Value to Set.
	 */
	public void setMagnitude(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/magnitude",v);
		_Magnitude=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _PercWhite=null;

	/**
	 * @return Returns the perc_white.
	 */
	public Double getPercWhite() {
		try{
			if (_PercWhite==null){
				_PercWhite=getDoubleProperty("perc_white");
				return _PercWhite;
			}else {
				return _PercWhite;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for perc_white.
	 * @param v Value to Set.
	 */
	public void setPercWhite(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/perc_white",v);
		_PercWhite=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Type=null;

	/**
	 * @return Returns the type.
	 */
	public String getType(){
		try{
			if (_Type==null){
				_Type=getStringProperty("type");
				return _Type;
			}else {
				return _Type;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for type.
	 * @param v Value to Set.
	 */
	public void setType(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/type",v);
		_Type=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CndaAtrophynildataPeakId=null;

	/**
	 * @return Returns the cnda_atrophyNilData_peak_id.
	 */
	public Integer getCndaAtrophynildataPeakId() {
		try{
			if (_CndaAtrophynildataPeakId==null){
				_CndaAtrophynildataPeakId=getIntegerProperty("cnda_atrophyNilData_peak_id");
				return _CndaAtrophynildataPeakId;
			}else {
				return _CndaAtrophynildataPeakId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda_atrophyNilData_peak_id.
	 * @param v Value to Set.
	 */
	public void setCndaAtrophynildataPeakId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cnda_atrophyNilData_peak_id",v);
		_CndaAtrophynildataPeakId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaAtrophynildataPeak> getAllCndaAtrophynildataPeaks(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaAtrophynildataPeak> al = new ArrayList<org.nrg.xdat.om.CndaAtrophynildataPeak>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaAtrophynildataPeak> getCndaAtrophynildataPeaksByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaAtrophynildataPeak> al = new ArrayList<org.nrg.xdat.om.CndaAtrophynildataPeak>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaAtrophynildataPeak> getCndaAtrophynildataPeaksByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaAtrophynildataPeak> al = new ArrayList<org.nrg.xdat.om.CndaAtrophynildataPeak>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaAtrophynildataPeak getCndaAtrophynildataPeaksByCndaAtrophynildataPeakId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:atrophyNilData_peak/cnda_atrophyNilData_peak_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaAtrophynildataPeak) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
