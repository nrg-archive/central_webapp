/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaCsfdata extends XnatSubjectassessordata implements org.nrg.xdat.model.CndaCsfdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaCsfdata.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:csfData";

	public AutoCndaCsfdata(ItemI item)
	{
		super(item);
	}

	public AutoCndaCsfdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaCsfdata(UserI user)
	 **/
	public AutoCndaCsfdata(){}

	public AutoCndaCsfdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:csfData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tau=null;

	/**
	 * @return Returns the tau.
	 */
	public Integer getTau() {
		try{
			if (_Tau==null){
				_Tau=getIntegerProperty("tau");
				return _Tau;
			}else {
				return _Tau;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tau.
	 * @param v Value to Set.
	 */
	public void setTau(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tau",v);
		_Tau=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ptau=null;

	/**
	 * @return Returns the ptau.
	 */
	public Integer getPtau() {
		try{
			if (_Ptau==null){
				_Ptau=getIntegerProperty("ptau");
				return _Ptau;
			}else {
				return _Ptau;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ptau.
	 * @param v Value to Set.
	 */
	public void setPtau(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ptau",v);
		_Ptau=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ab42=null;

	/**
	 * @return Returns the Ab42.
	 */
	public Integer getAb42() {
		try{
			if (_Ab42==null){
				_Ab42=getIntegerProperty("Ab42");
				return _Ab42;
			}else {
				return _Ab42;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Ab42.
	 * @param v Value to Set.
	 */
	public void setAb42(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Ab42",v);
		_Ab42=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lilly40=null;

	/**
	 * @return Returns the Lilly40.
	 */
	public Integer getLilly40() {
		try{
			if (_Lilly40==null){
				_Lilly40=getIntegerProperty("Lilly40");
				return _Lilly40;
			}else {
				return _Lilly40;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Lilly40.
	 * @param v Value to Set.
	 */
	public void setLilly40(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Lilly40",v);
		_Lilly40=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lilly42=null;

	/**
	 * @return Returns the Lilly42.
	 */
	public Integer getLilly42() {
		try{
			if (_Lilly42==null){
				_Lilly42=getIntegerProperty("Lilly42");
				return _Lilly42;
			}else {
				return _Lilly42;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Lilly42.
	 * @param v Value to Set.
	 */
	public void setLilly42(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Lilly42",v);
		_Lilly42=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaCsfdata> getAllCndaCsfdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaCsfdata> al = new ArrayList<org.nrg.xdat.om.CndaCsfdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaCsfdata> getCndaCsfdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaCsfdata> al = new ArrayList<org.nrg.xdat.om.CndaCsfdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaCsfdata> getCndaCsfdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaCsfdata> al = new ArrayList<org.nrg.xdat.om.CndaCsfdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaCsfdata getCndaCsfdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:csfData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaCsfdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
