/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaAdrcPsychometrics extends XnatSubjectassessordata implements org.nrg.xdat.model.CndaAdrcPsychometricsI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaAdrcPsychometrics.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:adrc_psychometrics";

	public AutoCndaAdrcPsychometrics(ItemI item)
	{
		super(item);
	}

	public AutoCndaAdrcPsychometrics(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaAdrcPsychometrics(UserI user)
	 **/
	public AutoCndaAdrcPsychometrics(){}

	public AutoCndaAdrcPsychometrics(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:adrc_psychometrics";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bentonrecall=null;

	/**
	 * @return Returns the BentonRecall.
	 */
	public Integer getBentonrecall() {
		try{
			if (_Bentonrecall==null){
				_Bentonrecall=getIntegerProperty("BentonRecall");
				return _Bentonrecall;
			}else {
				return _Bentonrecall;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BentonRecall.
	 * @param v Value to Set.
	 */
	public void setBentonrecall(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BentonRecall",v);
		_Bentonrecall=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bnt=null;

	/**
	 * @return Returns the BNT.
	 */
	public Integer getBnt() {
		try{
			if (_Bnt==null){
				_Bnt=getIntegerProperty("BNT");
				return _Bnt;
			}else {
				return _Bnt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for BNT.
	 * @param v Value to Set.
	 */
	public void setBnt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/BNT",v);
		_Bnt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Animals=null;

	/**
	 * @return Returns the animals.
	 */
	public Integer getAnimals() {
		try{
			if (_Animals==null){
				_Animals=getIntegerProperty("animals");
				return _Animals;
			}else {
				return _Animals;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for animals.
	 * @param v Value to Set.
	 */
	public void setAnimals(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/animals",v);
		_Animals=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Veg=null;

	/**
	 * @return Returns the veg.
	 */
	public Integer getVeg() {
		try{
			if (_Veg==null){
				_Veg=getIntegerProperty("veg");
				return _Veg;
			}else {
				return _Veg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for veg.
	 * @param v Value to Set.
	 */
	public void setVeg(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/veg",v);
		_Veg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Crossingoff=null;

	/**
	 * @return Returns the crossingOff.
	 */
	public Integer getCrossingoff() {
		try{
			if (_Crossingoff==null){
				_Crossingoff=getIntegerProperty("crossingOff");
				return _Crossingoff;
			}else {
				return _Crossingoff;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for crossingOff.
	 * @param v Value to Set.
	 */
	public void setCrossingoff(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/crossingOff",v);
		_Crossingoff=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Srtfree=null;

	/**
	 * @return Returns the SRTFree.
	 */
	public Integer getSrtfree() {
		try{
			if (_Srtfree==null){
				_Srtfree=getIntegerProperty("SRTFree");
				return _Srtfree;
			}else {
				return _Srtfree;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SRTFree.
	 * @param v Value to Set.
	 */
	public void setSrtfree(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SRTFree",v);
		_Srtfree=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tma=null;

	/**
	 * @return Returns the tma.
	 */
	public Integer getTma() {
		try{
			if (_Tma==null){
				_Tma=getIntegerProperty("tma");
				return _Tma;
			}else {
				return _Tma;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tma.
	 * @param v Value to Set.
	 */
	public void setTma(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tma",v);
		_Tma=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Tmb=null;

	/**
	 * @return Returns the tmb.
	 */
	public Integer getTmb() {
		try{
			if (_Tmb==null){
				_Tmb=getIntegerProperty("tmb");
				return _Tmb;
			}else {
				return _Tmb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tmb.
	 * @param v Value to Set.
	 */
	public void setTmb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tmb",v);
		_Tmb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Info=null;

	/**
	 * @return Returns the info.
	 */
	public Integer getInfo() {
		try{
			if (_Info==null){
				_Info=getIntegerProperty("info");
				return _Info;
			}else {
				return _Info;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for info.
	 * @param v Value to Set.
	 */
	public void setInfo(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/info",v);
		_Info=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _BlockI=null;

	/**
	 * @return Returns the block_I.
	 */
	public Integer getBlockI() {
		try{
			if (_BlockI==null){
				_BlockI=getIntegerProperty("block_I");
				return _BlockI;
			}else {
				return _BlockI;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for block_I.
	 * @param v Value to Set.
	 */
	public void setBlockI(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/block_I",v);
		_BlockI=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Digsym=null;

	/**
	 * @return Returns the digsym.
	 */
	public Integer getDigsym() {
		try{
			if (_Digsym==null){
				_Digsym=getIntegerProperty("digsym");
				return _Digsym;
			}else {
				return _Digsym;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for digsym.
	 * @param v Value to Set.
	 */
	public void setDigsym(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/digsym",v);
		_Digsym=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sim=null;

	/**
	 * @return Returns the sim.
	 */
	public Integer getSim() {
		try{
			if (_Sim==null){
				_Sim=getIntegerProperty("sim");
				return _Sim;
			}else {
				return _Sim;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sim.
	 * @param v Value to Set.
	 */
	public void setSim(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sim",v);
		_Sim=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mentcont=null;

	/**
	 * @return Returns the mentcont.
	 */
	public Integer getMentcont() {
		try{
			if (_Mentcont==null){
				_Mentcont=getIntegerProperty("mentcont");
				return _Mentcont;
			}else {
				return _Mentcont;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mentcont.
	 * @param v Value to Set.
	 */
	public void setMentcont(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mentcont",v);
		_Mentcont=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Logmemimmed=null;

	/**
	 * @return Returns the logMemImmed.
	 */
	public Double getLogmemimmed() {
		try{
			if (_Logmemimmed==null){
				_Logmemimmed=getDoubleProperty("logMemImmed");
				return _Logmemimmed;
			}else {
				return _Logmemimmed;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for logMemImmed.
	 * @param v Value to Set.
	 */
	public void setLogmemimmed(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/logMemImmed",v);
		_Logmemimmed=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Asscmem=null;

	/**
	 * @return Returns the asscMem.
	 */
	public Double getAsscmem() {
		try{
			if (_Asscmem==null){
				_Asscmem=getDoubleProperty("asscMem");
				return _Asscmem;
			}else {
				return _Asscmem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for asscMem.
	 * @param v Value to Set.
	 */
	public void setAsscmem(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/asscMem",v);
		_Asscmem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Digfor=null;

	/**
	 * @return Returns the digfor.
	 */
	public Integer getDigfor() {
		try{
			if (_Digfor==null){
				_Digfor=getIntegerProperty("digfor");
				return _Digfor;
			}else {
				return _Digfor;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for digfor.
	 * @param v Value to Set.
	 */
	public void setDigfor(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/digfor",v);
		_Digfor=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Digback=null;

	/**
	 * @return Returns the digback.
	 */
	public Integer getDigback() {
		try{
			if (_Digback==null){
				_Digback=getIntegerProperty("digback");
				return _Digback;
			}else {
				return _Digback;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for digback.
	 * @param v Value to Set.
	 */
	public void setDigback(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/digback",v);
		_Digback=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Wordflu=null;

	/**
	 * @return Returns the wordflu.
	 */
	public Integer getWordflu() {
		try{
			if (_Wordflu==null){
				_Wordflu=getIntegerProperty("wordflu");
				return _Wordflu;
			}else {
				return _Wordflu;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for wordflu.
	 * @param v Value to Set.
	 */
	public void setWordflu(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/wordflu",v);
		_Wordflu=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Gencomp=null;

	/**
	 * @return Returns the gencomp.
	 */
	public Double getGencomp() {
		try{
			if (_Gencomp==null){
				_Gencomp=getDoubleProperty("gencomp");
				return _Gencomp;
			}else {
				return _Gencomp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for gencomp.
	 * @param v Value to Set.
	 */
	public void setGencomp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/gencomp",v);
		_Gencomp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Visuospa=null;

	/**
	 * @return Returns the visuospa.
	 */
	public Double getVisuospa() {
		try{
			if (_Visuospa==null){
				_Visuospa=getDoubleProperty("visuospa");
				return _Visuospa;
			}else {
				return _Visuospa;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for visuospa.
	 * @param v Value to Set.
	 */
	public void setVisuospa(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/visuospa",v);
		_Visuospa=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Exec=null;

	/**
	 * @return Returns the exec.
	 */
	public Double getExec() {
		try{
			if (_Exec==null){
				_Exec=getDoubleProperty("exec");
				return _Exec;
			}else {
				return _Exec;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for exec.
	 * @param v Value to Set.
	 */
	public void setExec(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/exec",v);
		_Exec=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Trigrams=null;

	/**
	 * @return Returns the trigrams.
	 */
	public Integer getTrigrams() {
		try{
			if (_Trigrams==null){
				_Trigrams=getIntegerProperty("trigrams");
				return _Trigrams;
			}else {
				return _Trigrams;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for trigrams.
	 * @param v Value to Set.
	 */
	public void setTrigrams(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/trigrams",v);
		_Trigrams=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Line=null;

	/**
	 * @return Returns the line.
	 */
	public Integer getLine() {
		try{
			if (_Line==null){
				_Line=getIntegerProperty("line");
				return _Line;
			}else {
				return _Line;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for line.
	 * @param v Value to Set.
	 */
	public void setLine(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/line",v);
		_Line=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Trailb=null;

	/**
	 * @return Returns the trailb.
	 */
	public Integer getTrailb() {
		try{
			if (_Trailb==null){
				_Trailb=getIntegerProperty("trailb");
				return _Trailb;
			}else {
				return _Trailb;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for trailb.
	 * @param v Value to Set.
	 */
	public void setTrailb(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/trailb",v);
		_Trailb=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _BlockIii=null;

	/**
	 * @return Returns the block_III.
	 */
	public Integer getBlockIii() {
		try{
			if (_BlockIii==null){
				_BlockIii=getIntegerProperty("block_III");
				return _BlockIii;
			}else {
				return _BlockIii;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for block_III.
	 * @param v Value to Set.
	 */
	public void setBlockIii(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/block_III",v);
		_BlockIii=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Inform=null;

	/**
	 * @return Returns the inform.
	 */
	public Integer getInform() {
		try{
			if (_Inform==null){
				_Inform=getIntegerProperty("inform");
				return _Inform;
			}else {
				return _Inform;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for inform.
	 * @param v Value to Set.
	 */
	public void setInform(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/inform",v);
		_Inform=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lettnum=null;

	/**
	 * @return Returns the lettnum.
	 */
	public Integer getLettnum() {
		try{
			if (_Lettnum==null){
				_Lettnum=getIntegerProperty("lettnum");
				return _Lettnum;
			}else {
				return _Lettnum;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lettnum.
	 * @param v Value to Set.
	 */
	public void setLettnum(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lettnum",v);
		_Lettnum=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Logmem=null;

	/**
	 * @return Returns the logmem.
	 */
	public Integer getLogmem() {
		try{
			if (_Logmem==null){
				_Logmem=getIntegerProperty("logmem");
				return _Logmem;
			}else {
				return _Logmem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for logmem.
	 * @param v Value to Set.
	 */
	public void setLogmem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/logmem",v);
		_Logmem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Lmdelay=null;

	/**
	 * @return Returns the lmdelay.
	 */
	public Integer getLmdelay() {
		try{
			if (_Lmdelay==null){
				_Lmdelay=getIntegerProperty("lmdelay");
				return _Lmdelay;
			}else {
				return _Lmdelay;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lmdelay.
	 * @param v Value to Set.
	 */
	public void setLmdelay(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lmdelay",v);
		_Lmdelay=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pairs=null;

	/**
	 * @return Returns the pairs.
	 */
	public Integer getPairs() {
		try{
			if (_Pairs==null){
				_Pairs=getIntegerProperty("pairs");
				return _Pairs;
			}else {
				return _Pairs;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pairs.
	 * @param v Value to Set.
	 */
	public void setPairs(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pairs",v);
		_Pairs=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Spatial=null;

	/**
	 * @return Returns the spatial.
	 */
	public Integer getSpatial() {
		try{
			if (_Spatial==null){
				_Spatial=getIntegerProperty("spatial");
				return _Spatial;
			}else {
				return _Spatial;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for spatial.
	 * @param v Value to Set.
	 */
	public void setSpatial(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/spatial",v);
		_Spatial=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Logimem=null;

	/**
	 * @return Returns the logimem.
	 */
	public Integer getLogimem() {
		try{
			if (_Logimem==null){
				_Logimem=getIntegerProperty("logimem");
				return _Logimem;
			}else {
				return _Logimem;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for logimem.
	 * @param v Value to Set.
	 */
	public void setLogimem(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/logimem",v);
		_Logimem=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Memunits=null;

	/**
	 * @return Returns the memunits.
	 */
	public Integer getMemunits() {
		try{
			if (_Memunits==null){
				_Memunits=getIntegerProperty("memunits");
				return _Memunits;
			}else {
				return _Memunits;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for memunits.
	 * @param v Value to Set.
	 */
	public void setMemunits(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/memunits",v);
		_Memunits=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaAdrcPsychometrics> getAllCndaAdrcPsychometricss(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaAdrcPsychometrics> al = new ArrayList<org.nrg.xdat.om.CndaAdrcPsychometrics>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaAdrcPsychometrics> getCndaAdrcPsychometricssByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaAdrcPsychometrics> al = new ArrayList<org.nrg.xdat.om.CndaAdrcPsychometrics>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaAdrcPsychometrics> getCndaAdrcPsychometricssByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaAdrcPsychometrics> al = new ArrayList<org.nrg.xdat.om.CndaAdrcPsychometrics>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaAdrcPsychometrics getCndaAdrcPsychometricssById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:adrc_psychometrics/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaAdrcPsychometrics) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
