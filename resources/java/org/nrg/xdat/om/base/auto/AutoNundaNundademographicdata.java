/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoNundaNundademographicdata extends XnatDemographicdata implements org.nrg.xdat.model.NundaNundademographicdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoNundaNundademographicdata.class);
	public static String SCHEMA_ELEMENT_NAME="nunda:nundaDemographicData";

	public AutoNundaNundademographicdata(ItemI item)
	{
		super(item);
	}

	public AutoNundaNundademographicdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoNundaNundademographicdata(UserI user)
	 **/
	public AutoNundaNundademographicdata(){}

	public AutoNundaNundademographicdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "nunda:nundaDemographicData";
	}
	 private org.nrg.xdat.om.XnatDemographicdata _Demographicdata =null;

	/**
	 * demographicData
	 * @return org.nrg.xdat.om.XnatDemographicdata
	 */
	public org.nrg.xdat.om.XnatDemographicdata getDemographicdata() {
		try{
			if (_Demographicdata==null){
				_Demographicdata=((XnatDemographicdata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("demographicData")));
				return _Demographicdata;
			}else {
				return _Demographicdata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for demographicData.
	 * @param v Value to Set.
	 */
	public void setDemographicdata(ItemI v) throws Exception{
		_Demographicdata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/demographicData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/demographicData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * demographicData
	 * set org.nrg.xdat.model.XnatDemographicdataI
	 */
	public <A extends org.nrg.xdat.model.XnatDemographicdataI> void setDemographicdata(A item) throws Exception{
	setDemographicdata((ItemI)item);
	}

	/**
	 * Removes the demographicData.
	 * */
	public void removeDemographicdata() {
		_Demographicdata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/demographicData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Maritalstatus=null;

	/**
	 * @return Returns the maritalStatus.
	 */
	public Integer getMaritalstatus() {
		try{
			if (_Maritalstatus==null){
				_Maritalstatus=getIntegerProperty("maritalStatus");
				return _Maritalstatus;
			}else {
				return _Maritalstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for maritalStatus.
	 * @param v Value to Set.
	 */
	public void setMaritalstatus(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/maritalStatus",v);
		_Maritalstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Housingtype=null;

	/**
	 * @return Returns the housingType.
	 */
	public Integer getHousingtype() {
		try{
			if (_Housingtype==null){
				_Housingtype=getIntegerProperty("housingType");
				return _Housingtype;
			}else {
				return _Housingtype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for housingType.
	 * @param v Value to Set.
	 */
	public void setHousingtype(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/housingType",v);
		_Housingtype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Siblingsnumber=null;

	/**
	 * @return Returns the siblingsNumber.
	 */
	public Integer getSiblingsnumber() {
		try{
			if (_Siblingsnumber==null){
				_Siblingsnumber=getIntegerProperty("siblingsNumber");
				return _Siblingsnumber;
			}else {
				return _Siblingsnumber;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblingsNumber.
	 * @param v Value to Set.
	 */
	public void setSiblingsnumber(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblingsNumber",v);
		_Siblingsnumber=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Siblingage1=null;

	/**
	 * @return Returns the siblingAge1.
	 */
	public String getSiblingage1(){
		try{
			if (_Siblingage1==null){
				_Siblingage1=getStringProperty("siblingAge1");
				return _Siblingage1;
			}else {
				return _Siblingage1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblingAge1.
	 * @param v Value to Set.
	 */
	public void setSiblingage1(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblingAge1",v);
		_Siblingage1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Siblingage2=null;

	/**
	 * @return Returns the siblingAge2.
	 */
	public String getSiblingage2(){
		try{
			if (_Siblingage2==null){
				_Siblingage2=getStringProperty("siblingAge2");
				return _Siblingage2;
			}else {
				return _Siblingage2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblingAge2.
	 * @param v Value to Set.
	 */
	public void setSiblingage2(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblingAge2",v);
		_Siblingage2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Siblingage3=null;

	/**
	 * @return Returns the siblingAge3.
	 */
	public String getSiblingage3(){
		try{
			if (_Siblingage3==null){
				_Siblingage3=getStringProperty("siblingAge3");
				return _Siblingage3;
			}else {
				return _Siblingage3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblingAge3.
	 * @param v Value to Set.
	 */
	public void setSiblingage3(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblingAge3",v);
		_Siblingage3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Siblingage4=null;

	/**
	 * @return Returns the siblingAge4.
	 */
	public String getSiblingage4(){
		try{
			if (_Siblingage4==null){
				_Siblingage4=getStringProperty("siblingAge4");
				return _Siblingage4;
			}else {
				return _Siblingage4;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblingAge4.
	 * @param v Value to Set.
	 */
	public void setSiblingage4(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblingAge4",v);
		_Siblingage4=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Siblingage5=null;

	/**
	 * @return Returns the siblingAge5.
	 */
	public String getSiblingage5(){
		try{
			if (_Siblingage5==null){
				_Siblingage5=getStringProperty("siblingAge5");
				return _Siblingage5;
			}else {
				return _Siblingage5;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblingAge5.
	 * @param v Value to Set.
	 */
	public void setSiblingage5(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblingAge5",v);
		_Siblingage5=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Siblingage6=null;

	/**
	 * @return Returns the siblingAge6.
	 */
	public String getSiblingage6(){
		try{
			if (_Siblingage6==null){
				_Siblingage6=getStringProperty("siblingAge6");
				return _Siblingage6;
			}else {
				return _Siblingage6;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblingAge6.
	 * @param v Value to Set.
	 */
	public void setSiblingage6(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblingAge6",v);
		_Siblingage6=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Siblingage7=null;

	/**
	 * @return Returns the siblingAge7.
	 */
	public String getSiblingage7(){
		try{
			if (_Siblingage7==null){
				_Siblingage7=getStringProperty("siblingAge7");
				return _Siblingage7;
			}else {
				return _Siblingage7;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblingAge7.
	 * @param v Value to Set.
	 */
	public void setSiblingage7(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblingAge7",v);
		_Siblingage7=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Siblingage8=null;

	/**
	 * @return Returns the siblingAge8.
	 */
	public String getSiblingage8(){
		try{
			if (_Siblingage8==null){
				_Siblingage8=getStringProperty("siblingAge8");
				return _Siblingage8;
			}else {
				return _Siblingage8;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for siblingAge8.
	 * @param v Value to Set.
	 */
	public void setSiblingage8(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/siblingAge8",v);
		_Siblingage8=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Numberofchildren=null;

	/**
	 * @return Returns the numberOfChildren.
	 */
	public Integer getNumberofchildren() {
		try{
			if (_Numberofchildren==null){
				_Numberofchildren=getIntegerProperty("numberOfChildren");
				return _Numberofchildren;
			}else {
				return _Numberofchildren;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for numberOfChildren.
	 * @param v Value to Set.
	 */
	public void setNumberofchildren(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/numberOfChildren",v);
		_Numberofchildren=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Employmentstatus=null;

	/**
	 * @return Returns the employmentStatus.
	 */
	public Integer getEmploymentstatus() {
		try{
			if (_Employmentstatus==null){
				_Employmentstatus=getIntegerProperty("employmentStatus");
				return _Employmentstatus;
			}else {
				return _Employmentstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for employmentStatus.
	 * @param v Value to Set.
	 */
	public void setEmploymentstatus(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/employmentStatus",v);
		_Employmentstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Currentoccupation=null;

	/**
	 * @return Returns the currentOccupation.
	 */
	public String getCurrentoccupation(){
		try{
			if (_Currentoccupation==null){
				_Currentoccupation=getStringProperty("currentOccupation");
				return _Currentoccupation;
			}else {
				return _Currentoccupation;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for currentOccupation.
	 * @param v Value to Set.
	 */
	public void setCurrentoccupation(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/currentOccupation",v);
		_Currentoccupation=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Principaloccupation=null;

	/**
	 * @return Returns the principalOccupation.
	 */
	public String getPrincipaloccupation(){
		try{
			if (_Principaloccupation==null){
				_Principaloccupation=getStringProperty("principalOccupation");
				return _Principaloccupation;
			}else {
				return _Principaloccupation;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for principalOccupation.
	 * @param v Value to Set.
	 */
	public void setPrincipaloccupation(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/principalOccupation",v);
		_Principaloccupation=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Educationlevel=null;

	/**
	 * @return Returns the educationLevel.
	 */
	public Integer getEducationlevel() {
		try{
			if (_Educationlevel==null){
				_Educationlevel=getIntegerProperty("educationLevel");
				return _Educationlevel;
			}else {
				return _Educationlevel;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for educationLevel.
	 * @param v Value to Set.
	 */
	public void setEducationlevel(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/educationLevel",v);
		_Educationlevel=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yearsofschooling=null;

	/**
	 * @return Returns the yearsOfSchooling.
	 */
	public Integer getYearsofschooling() {
		try{
			if (_Yearsofschooling==null){
				_Yearsofschooling=getIntegerProperty("yearsOfSchooling");
				return _Yearsofschooling;
			}else {
				return _Yearsofschooling;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for yearsOfSchooling.
	 * @param v Value to Set.
	 */
	public void setYearsofschooling(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/yearsOfSchooling",v);
		_Yearsofschooling=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Attendspecialclasses=null;

	/**
	 * @return Returns the attendSpecialClasses.
	 */
	public Integer getAttendspecialclasses() {
		try{
			if (_Attendspecialclasses==null){
				_Attendspecialclasses=getIntegerProperty("attendSpecialClasses");
				return _Attendspecialclasses;
			}else {
				return _Attendspecialclasses;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for attendSpecialClasses.
	 * @param v Value to Set.
	 */
	public void setAttendspecialclasses(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/attendSpecialClasses",v);
		_Attendspecialclasses=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Specialclass=null;

	/**
	 * @return Returns the specialClass.
	 */
	public String getSpecialclass(){
		try{
			if (_Specialclass==null){
				_Specialclass=getStringProperty("specialClass");
				return _Specialclass;
			}else {
				return _Specialclass;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for specialClass.
	 * @param v Value to Set.
	 */
	public void setSpecialclass(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/specialClass",v);
		_Specialclass=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Educationlevelfather=null;

	/**
	 * @return Returns the educationLevelFather.
	 */
	public Integer getEducationlevelfather() {
		try{
			if (_Educationlevelfather==null){
				_Educationlevelfather=getIntegerProperty("educationLevelFather");
				return _Educationlevelfather;
			}else {
				return _Educationlevelfather;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for educationLevelFather.
	 * @param v Value to Set.
	 */
	public void setEducationlevelfather(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/educationLevelFather",v);
		_Educationlevelfather=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Schoolinglevelfather=null;

	/**
	 * @return Returns the schoolingLevelFather.
	 */
	public String getSchoolinglevelfather(){
		try{
			if (_Schoolinglevelfather==null){
				_Schoolinglevelfather=getStringProperty("schoolingLevelFather");
				return _Schoolinglevelfather;
			}else {
				return _Schoolinglevelfather;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for schoolingLevelFather.
	 * @param v Value to Set.
	 */
	public void setSchoolinglevelfather(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/schoolingLevelFather",v);
		_Schoolinglevelfather=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Educationlevelmother=null;

	/**
	 * @return Returns the educationLevelMother.
	 */
	public Integer getEducationlevelmother() {
		try{
			if (_Educationlevelmother==null){
				_Educationlevelmother=getIntegerProperty("educationLevelMother");
				return _Educationlevelmother;
			}else {
				return _Educationlevelmother;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for educationLevelMother.
	 * @param v Value to Set.
	 */
	public void setEducationlevelmother(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/educationLevelMother",v);
		_Educationlevelmother=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Schoolinglevelmother=null;

	/**
	 * @return Returns the schoolingLevelMother.
	 */
	public String getSchoolinglevelmother(){
		try{
			if (_Schoolinglevelmother==null){
				_Schoolinglevelmother=getStringProperty("schoolingLevelMother");
				return _Schoolinglevelmother;
			}else {
				return _Schoolinglevelmother;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for schoolingLevelMother.
	 * @param v Value to Set.
	 */
	public void setSchoolinglevelmother(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/schoolingLevelMother",v);
		_Schoolinglevelmother=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Sesdesc=null;

	/**
	 * @return Returns the sesDesc.
	 */
	public String getSesdesc(){
		try{
			if (_Sesdesc==null){
				_Sesdesc=getStringProperty("sesDesc");
				return _Sesdesc;
			}else {
				return _Sesdesc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sesDesc.
	 * @param v Value to Set.
	 */
	public void setSesdesc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sesDesc",v);
		_Sesdesc=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handednessexam_writing=null;

	/**
	 * @return Returns the handednessExam/writing.
	 */
	public Integer getHandednessexam_writing() {
		try{
			if (_Handednessexam_writing==null){
				_Handednessexam_writing=getIntegerProperty("handednessExam/writing");
				return _Handednessexam_writing;
			}else {
				return _Handednessexam_writing;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for handednessExam/writing.
	 * @param v Value to Set.
	 */
	public void setHandednessexam_writing(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/handednessExam/writing",v);
		_Handednessexam_writing=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handednessexam_throwing=null;

	/**
	 * @return Returns the handednessExam/throwing.
	 */
	public Integer getHandednessexam_throwing() {
		try{
			if (_Handednessexam_throwing==null){
				_Handednessexam_throwing=getIntegerProperty("handednessExam/throwing");
				return _Handednessexam_throwing;
			}else {
				return _Handednessexam_throwing;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for handednessExam/throwing.
	 * @param v Value to Set.
	 */
	public void setHandednessexam_throwing(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/handednessExam/throwing",v);
		_Handednessexam_throwing=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handednessexam_scissors=null;

	/**
	 * @return Returns the handednessExam/scissors.
	 */
	public Integer getHandednessexam_scissors() {
		try{
			if (_Handednessexam_scissors==null){
				_Handednessexam_scissors=getIntegerProperty("handednessExam/scissors");
				return _Handednessexam_scissors;
			}else {
				return _Handednessexam_scissors;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for handednessExam/scissors.
	 * @param v Value to Set.
	 */
	public void setHandednessexam_scissors(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/handednessExam/scissors",v);
		_Handednessexam_scissors=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handednessexam_toothbrush=null;

	/**
	 * @return Returns the handednessExam/toothbrush.
	 */
	public Integer getHandednessexam_toothbrush() {
		try{
			if (_Handednessexam_toothbrush==null){
				_Handednessexam_toothbrush=getIntegerProperty("handednessExam/toothbrush");
				return _Handednessexam_toothbrush;
			}else {
				return _Handednessexam_toothbrush;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for handednessExam/toothbrush.
	 * @param v Value to Set.
	 */
	public void setHandednessexam_toothbrush(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/handednessExam/toothbrush",v);
		_Handednessexam_toothbrush=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handednessexam_knife=null;

	/**
	 * @return Returns the handednessExam/knife.
	 */
	public Integer getHandednessexam_knife() {
		try{
			if (_Handednessexam_knife==null){
				_Handednessexam_knife=getIntegerProperty("handednessExam/knife");
				return _Handednessexam_knife;
			}else {
				return _Handednessexam_knife;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for handednessExam/knife.
	 * @param v Value to Set.
	 */
	public void setHandednessexam_knife(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/handednessExam/knife",v);
		_Handednessexam_knife=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handednessexam_spoon=null;

	/**
	 * @return Returns the handednessExam/spoon.
	 */
	public Integer getHandednessexam_spoon() {
		try{
			if (_Handednessexam_spoon==null){
				_Handednessexam_spoon=getIntegerProperty("handednessExam/spoon");
				return _Handednessexam_spoon;
			}else {
				return _Handednessexam_spoon;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for handednessExam/spoon.
	 * @param v Value to Set.
	 */
	public void setHandednessexam_spoon(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/handednessExam/spoon",v);
		_Handednessexam_spoon=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handednessexam_broom=null;

	/**
	 * @return Returns the handednessExam/broom.
	 */
	public Integer getHandednessexam_broom() {
		try{
			if (_Handednessexam_broom==null){
				_Handednessexam_broom=getIntegerProperty("handednessExam/broom");
				return _Handednessexam_broom;
			}else {
				return _Handednessexam_broom;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for handednessExam/broom.
	 * @param v Value to Set.
	 */
	public void setHandednessexam_broom(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/handednessExam/broom",v);
		_Handednessexam_broom=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handednessexam_match=null;

	/**
	 * @return Returns the handednessExam/match.
	 */
	public Integer getHandednessexam_match() {
		try{
			if (_Handednessexam_match==null){
				_Handednessexam_match=getIntegerProperty("handednessExam/match");
				return _Handednessexam_match;
			}else {
				return _Handednessexam_match;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for handednessExam/match.
	 * @param v Value to Set.
	 */
	public void setHandednessexam_match(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/handednessExam/match",v);
		_Handednessexam_match=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handednessexam_box=null;

	/**
	 * @return Returns the handednessExam/box.
	 */
	public Integer getHandednessexam_box() {
		try{
			if (_Handednessexam_box==null){
				_Handednessexam_box=getIntegerProperty("handednessExam/box");
				return _Handednessexam_box;
			}else {
				return _Handednessexam_box;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for handednessExam/box.
	 * @param v Value to Set.
	 */
	public void setHandednessexam_box(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/handednessExam/box",v);
		_Handednessexam_box=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handednessexam_footkick=null;

	/**
	 * @return Returns the handednessExam/footkick.
	 */
	public Integer getHandednessexam_footkick() {
		try{
			if (_Handednessexam_footkick==null){
				_Handednessexam_footkick=getIntegerProperty("handednessExam/footkick");
				return _Handednessexam_footkick;
			}else {
				return _Handednessexam_footkick;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for handednessExam/footkick.
	 * @param v Value to Set.
	 */
	public void setHandednessexam_footkick(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/handednessExam/footkick",v);
		_Handednessexam_footkick=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Handednessexam_eye=null;

	/**
	 * @return Returns the handednessExam/eye.
	 */
	public Integer getHandednessexam_eye() {
		try{
			if (_Handednessexam_eye==null){
				_Handednessexam_eye=getIntegerProperty("handednessExam/eye");
				return _Handednessexam_eye;
			}else {
				return _Handednessexam_eye;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for handednessExam/eye.
	 * @param v Value to Set.
	 */
	public void setHandednessexam_eye(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/handednessExam/eye",v);
		_Handednessexam_eye=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.NundaNundademographicdataRelationship> _Relationships_relationship =null;

	/**
	 * relationships/relationship
	 * @return Returns an List of org.nrg.xdat.om.NundaNundademographicdataRelationship
	 */
	public <A extends org.nrg.xdat.model.NundaNundademographicdataRelationshipI> List<A> getRelationships_relationship() {
		try{
			if (_Relationships_relationship==null){
				_Relationships_relationship=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("relationships/relationship"));
			}
			return (List<A>) _Relationships_relationship;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.NundaNundademographicdataRelationship>();}
	}

	/**
	 * Sets the value for relationships/relationship.
	 * @param v Value to Set.
	 */
	public void setRelationships_relationship(ItemI v) throws Exception{
		_Relationships_relationship =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/relationships/relationship",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/relationships/relationship",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * relationships/relationship
	 * Adds org.nrg.xdat.model.NundaNundademographicdataRelationshipI
	 */
	public <A extends org.nrg.xdat.model.NundaNundademographicdataRelationshipI> void addRelationships_relationship(A item) throws Exception{
	setRelationships_relationship((ItemI)item);
	}

	/**
	 * Removes the relationships/relationship of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeRelationships_relationship(int index) throws java.lang.IndexOutOfBoundsException {
		_Relationships_relationship =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/relationships/relationship",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.NundaNundademographicdata> getAllNundaNundademographicdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.NundaNundademographicdata> al = new ArrayList<org.nrg.xdat.om.NundaNundademographicdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.NundaNundademographicdata> getNundaNundademographicdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.NundaNundademographicdata> al = new ArrayList<org.nrg.xdat.om.NundaNundademographicdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.NundaNundademographicdata> getNundaNundademographicdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.NundaNundademographicdata> al = new ArrayList<org.nrg.xdat.om.NundaNundademographicdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static NundaNundademographicdata getNundaNundademographicdatasByXnatAbstractdemographicdataId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("nunda:nundaDemographicData/xnat_abstractdemographicdata_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (NundaNundademographicdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //demographicData
	        XnatDemographicdata childDemographicdata = (XnatDemographicdata)this.getDemographicdata();
	            if (childDemographicdata!=null){
	              for(ResourceFile rf: ((XnatDemographicdata)childDemographicdata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("demographicData[" + ((XnatDemographicdata)childDemographicdata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("demographicData/" + ((XnatDemographicdata)childDemographicdata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //relationships/relationship
	        for(org.nrg.xdat.model.NundaNundademographicdataRelationshipI childRelationships_relationship : this.getRelationships_relationship()){
	            if (childRelationships_relationship!=null){
	              for(ResourceFile rf: ((NundaNundademographicdataRelationship)childRelationships_relationship).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("relationships/relationship[" + ((NundaNundademographicdataRelationship)childRelationships_relationship).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("relationships/relationship/" + ((NundaNundademographicdataRelationship)childRelationships_relationship).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
