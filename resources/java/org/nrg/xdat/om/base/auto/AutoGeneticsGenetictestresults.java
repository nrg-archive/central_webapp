/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:47 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoGeneticsGenetictestresults extends XnatSubjectassessordata implements org.nrg.xdat.model.GeneticsGenetictestresultsI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoGeneticsGenetictestresults.class);
	public static String SCHEMA_ELEMENT_NAME="genetics:geneticTestResults";

	public AutoGeneticsGenetictestresults(ItemI item)
	{
		super(item);
	}

	public AutoGeneticsGenetictestresults(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoGeneticsGenetictestresults(UserI user)
	 **/
	public AutoGeneticsGenetictestresults(){}

	public AutoGeneticsGenetictestresults(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "genetics:geneticTestResults";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.GeneticsGenetictestresultsGene> _Genes_gene =null;

	/**
	 * genes/gene
	 * @return Returns an List of org.nrg.xdat.om.GeneticsGenetictestresultsGene
	 */
	public <A extends org.nrg.xdat.model.GeneticsGenetictestresultsGeneI> List<A> getGenes_gene() {
		try{
			if (_Genes_gene==null){
				_Genes_gene=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("genes/gene"));
			}
			return (List<A>) _Genes_gene;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.GeneticsGenetictestresultsGene>();}
	}

	/**
	 * Sets the value for genes/gene.
	 * @param v Value to Set.
	 */
	public void setGenes_gene(ItemI v) throws Exception{
		_Genes_gene =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/genes/gene",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/genes/gene",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * genes/gene
	 * Adds org.nrg.xdat.model.GeneticsGenetictestresultsGeneI
	 */
	public <A extends org.nrg.xdat.model.GeneticsGenetictestresultsGeneI> void addGenes_gene(A item) throws Exception{
	setGenes_gene((ItemI)item);
	}

	/**
	 * Removes the genes/gene of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeGenes_gene(int index) throws java.lang.IndexOutOfBoundsException {
		_Genes_gene =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/genes/gene",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.GeneticsGenetictestresults> getAllGeneticsGenetictestresultss(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GeneticsGenetictestresults> al = new ArrayList<org.nrg.xdat.om.GeneticsGenetictestresults>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.GeneticsGenetictestresults> getGeneticsGenetictestresultssByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GeneticsGenetictestresults> al = new ArrayList<org.nrg.xdat.om.GeneticsGenetictestresults>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.GeneticsGenetictestresults> getGeneticsGenetictestresultssByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GeneticsGenetictestresults> al = new ArrayList<org.nrg.xdat.om.GeneticsGenetictestresults>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static GeneticsGenetictestresults getGeneticsGenetictestresultssById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("genetics:geneticTestResults/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (GeneticsGenetictestresults) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //genes/gene
	        for(org.nrg.xdat.model.GeneticsGenetictestresultsGeneI childGenes_gene : this.getGenes_gene()){
	            if (childGenes_gene!=null){
	              for(ResourceFile rf: ((GeneticsGenetictestresultsGene)childGenes_gene).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("genes/gene[" + ((GeneticsGenetictestresultsGene)childGenes_gene).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("genes/gene/" + ((GeneticsGenetictestresultsGene)childGenes_gene).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
