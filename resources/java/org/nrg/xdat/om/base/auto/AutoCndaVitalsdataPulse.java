/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaVitalsdataPulse extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CndaVitalsdataPulseI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaVitalsdataPulse.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:vitalsData_pulse";

	public AutoCndaVitalsdataPulse(ItemI item)
	{
		super(item);
	}

	public AutoCndaVitalsdataPulse(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaVitalsdataPulse(UserI user)
	 **/
	public AutoCndaVitalsdataPulse(){}

	public AutoCndaVitalsdataPulse(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:vitalsData_pulse";
	}

	//FIELD

	private Integer _Pulse=null;

	/**
	 * @return Returns the pulse.
	 */
	public Integer getPulse() {
		try{
			if (_Pulse==null){
				_Pulse=getIntegerProperty("pulse");
				return _Pulse;
			}else {
				return _Pulse;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pulse.
	 * @param v Value to Set.
	 */
	public void setPulse(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pulse",v);
		_Pulse=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Condition=null;

	/**
	 * @return Returns the condition.
	 */
	public String getCondition(){
		try{
			if (_Condition==null){
				_Condition=getStringProperty("condition");
				return _Condition;
			}else {
				return _Condition;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for condition.
	 * @param v Value to Set.
	 */
	public void setCondition(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/condition",v);
		_Condition=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CndaVitalsdataPulseId=null;

	/**
	 * @return Returns the cnda_vitalsData_pulse_id.
	 */
	public Integer getCndaVitalsdataPulseId() {
		try{
			if (_CndaVitalsdataPulseId==null){
				_CndaVitalsdataPulseId=getIntegerProperty("cnda_vitalsData_pulse_id");
				return _CndaVitalsdataPulseId;
			}else {
				return _CndaVitalsdataPulseId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda_vitalsData_pulse_id.
	 * @param v Value to Set.
	 */
	public void setCndaVitalsdataPulseId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cnda_vitalsData_pulse_id",v);
		_CndaVitalsdataPulseId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaVitalsdataPulse> getAllCndaVitalsdataPulses(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaVitalsdataPulse> al = new ArrayList<org.nrg.xdat.om.CndaVitalsdataPulse>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaVitalsdataPulse> getCndaVitalsdataPulsesByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaVitalsdataPulse> al = new ArrayList<org.nrg.xdat.om.CndaVitalsdataPulse>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaVitalsdataPulse> getCndaVitalsdataPulsesByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaVitalsdataPulse> al = new ArrayList<org.nrg.xdat.om.CndaVitalsdataPulse>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaVitalsdataPulse getCndaVitalsdataPulsesByCndaVitalsdataPulseId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:vitalsData_pulse/cnda_vitalsData_pulse_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaVitalsdataPulse) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
