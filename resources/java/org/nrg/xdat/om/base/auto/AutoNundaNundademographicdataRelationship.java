/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoNundaNundademographicdataRelationship extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.NundaNundademographicdataRelationshipI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoNundaNundademographicdataRelationship.class);
	public static String SCHEMA_ELEMENT_NAME="nunda:nundaDemographicData_relationship";

	public AutoNundaNundademographicdataRelationship(ItemI item)
	{
		super(item);
	}

	public AutoNundaNundademographicdataRelationship(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoNundaNundademographicdataRelationship(UserI user)
	 **/
	public AutoNundaNundademographicdataRelationship(){}

	public AutoNundaNundademographicdataRelationship(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "nunda:nundaDemographicData_relationship";
	}

	//FIELD

	private String _Otherid=null;

	/**
	 * @return Returns the otherId.
	 */
	public String getOtherid(){
		try{
			if (_Otherid==null){
				_Otherid=getStringProperty("otherId");
				return _Otherid;
			}else {
				return _Otherid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for otherId.
	 * @param v Value to Set.
	 */
	public void setOtherid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/otherId",v);
		_Otherid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Relationshiptype=null;

	/**
	 * @return Returns the relationshipType.
	 */
	public Integer getRelationshiptype() {
		try{
			if (_Relationshiptype==null){
				_Relationshiptype=getIntegerProperty("relationshipType");
				return _Relationshiptype;
			}else {
				return _Relationshiptype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for relationshipType.
	 * @param v Value to Set.
	 */
	public void setRelationshiptype(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/relationshipType",v);
		_Relationshiptype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _NundaNundademographicdataRelationshipId=null;

	/**
	 * @return Returns the nunda_nundaDemographicData_relationship_id.
	 */
	public Integer getNundaNundademographicdataRelationshipId() {
		try{
			if (_NundaNundademographicdataRelationshipId==null){
				_NundaNundademographicdataRelationshipId=getIntegerProperty("nunda_nundaDemographicData_relationship_id");
				return _NundaNundademographicdataRelationshipId;
			}else {
				return _NundaNundademographicdataRelationshipId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for nunda_nundaDemographicData_relationship_id.
	 * @param v Value to Set.
	 */
	public void setNundaNundademographicdataRelationshipId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/nunda_nundaDemographicData_relationship_id",v);
		_NundaNundademographicdataRelationshipId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.NundaNundademographicdataRelationship> getAllNundaNundademographicdataRelationships(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.NundaNundademographicdataRelationship> al = new ArrayList<org.nrg.xdat.om.NundaNundademographicdataRelationship>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.NundaNundademographicdataRelationship> getNundaNundademographicdataRelationshipsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.NundaNundademographicdataRelationship> al = new ArrayList<org.nrg.xdat.om.NundaNundademographicdataRelationship>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.NundaNundademographicdataRelationship> getNundaNundademographicdataRelationshipsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.NundaNundademographicdataRelationship> al = new ArrayList<org.nrg.xdat.om.NundaNundademographicdataRelationship>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static NundaNundademographicdataRelationship getNundaNundademographicdataRelationshipsByNundaNundademographicdataRelationshipId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("nunda:nundaDemographicData_relationship/nunda_nundaDemographicData_relationship_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (NundaNundademographicdataRelationship) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
