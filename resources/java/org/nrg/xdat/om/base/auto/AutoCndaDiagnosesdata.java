/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaDiagnosesdata extends XnatSubjectassessordata implements org.nrg.xdat.model.CndaDiagnosesdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaDiagnosesdata.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:diagnosesData";

	public AutoCndaDiagnosesdata(ItemI item)
	{
		super(item);
	}

	public AutoCndaDiagnosesdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaDiagnosesdata(UserI user)
	 **/
	public AutoCndaDiagnosesdata(){}

	public AutoCndaDiagnosesdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:diagnosesData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Diagnoser=null;

	/**
	 * @return Returns the diagnoser.
	 */
	public String getDiagnoser(){
		try{
			if (_Diagnoser==null){
				_Diagnoser=getStringProperty("diagnoser");
				return _Diagnoser;
			}else {
				return _Diagnoser;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for diagnoser.
	 * @param v Value to Set.
	 */
	public void setDiagnoser(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/diagnoser",v);
		_Diagnoser=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ticdiagnosis_dsmiv=null;

	/**
	 * @return Returns the ticDiagnosis/DSMIV.
	 */
	public String getTicdiagnosis_dsmiv(){
		try{
			if (_Ticdiagnosis_dsmiv==null){
				_Ticdiagnosis_dsmiv=getStringProperty("ticDiagnosis/DSMIV");
				return _Ticdiagnosis_dsmiv;
			}else {
				return _Ticdiagnosis_dsmiv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ticDiagnosis/DSMIV.
	 * @param v Value to Set.
	 */
	public void setTicdiagnosis_dsmiv(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ticDiagnosis/DSMIV",v);
		_Ticdiagnosis_dsmiv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Ticdiagnosis_impairdistress=null;

	/**
	 * @return Returns the ticDiagnosis/impairDistress.
	 */
	public Boolean getTicdiagnosis_impairdistress() {
		try{
			if (_Ticdiagnosis_impairdistress==null){
				_Ticdiagnosis_impairdistress=getBooleanProperty("ticDiagnosis/impairDistress");
				return _Ticdiagnosis_impairdistress;
			}else {
				return _Ticdiagnosis_impairdistress;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ticDiagnosis/impairDistress.
	 * @param v Value to Set.
	 */
	public void setTicdiagnosis_impairdistress(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/ticDiagnosis/impairDistress",v);
		_Ticdiagnosis_impairdistress=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ticdiagnosis_tssg=null;

	/**
	 * @return Returns the ticDiagnosis/TSSG.
	 */
	public String getTicdiagnosis_tssg(){
		try{
			if (_Ticdiagnosis_tssg==null){
				_Ticdiagnosis_tssg=getStringProperty("ticDiagnosis/TSSG");
				return _Ticdiagnosis_tssg;
			}else {
				return _Ticdiagnosis_tssg;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ticDiagnosis/TSSG.
	 * @param v Value to Set.
	 */
	public void setTicdiagnosis_tssg(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ticDiagnosis/TSSG",v);
		_Ticdiagnosis_tssg=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Ticdiagnosis_tssg2=null;

	/**
	 * @return Returns the ticDiagnosis/TSSG2.
	 */
	public String getTicdiagnosis_tssg2(){
		try{
			if (_Ticdiagnosis_tssg2==null){
				_Ticdiagnosis_tssg2=getStringProperty("ticDiagnosis/TSSG2");
				return _Ticdiagnosis_tssg2;
			}else {
				return _Ticdiagnosis_tssg2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ticDiagnosis/TSSG2.
	 * @param v Value to Set.
	 */
	public void setTicdiagnosis_tssg2(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ticDiagnosis/TSSG2",v);
		_Ticdiagnosis_tssg2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Motor_firstticage=null;

	/**
	 * @return Returns the motor/firstTicAge.
	 */
	public Double getMotor_firstticage() {
		try{
			if (_Motor_firstticage==null){
				_Motor_firstticage=getDoubleProperty("motor/firstTicAge");
				return _Motor_firstticage;
			}else {
				return _Motor_firstticage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for motor/firstTicAge.
	 * @param v Value to Set.
	 */
	public void setMotor_firstticage(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/motor/firstTicAge",v);
		_Motor_firstticage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Motor_worstticage=null;

	/**
	 * @return Returns the motor/worstTicAge.
	 */
	public Double getMotor_worstticage() {
		try{
			if (_Motor_worstticage==null){
				_Motor_worstticage=getDoubleProperty("motor/worstTicAge");
				return _Motor_worstticage;
			}else {
				return _Motor_worstticage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for motor/worstTicAge.
	 * @param v Value to Set.
	 */
	public void setMotor_worstticage(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/motor/worstTicAge",v);
		_Motor_worstticage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Vocal_firstticage=null;

	/**
	 * @return Returns the vocal/firstTicAge.
	 */
	public Double getVocal_firstticage() {
		try{
			if (_Vocal_firstticage==null){
				_Vocal_firstticage=getDoubleProperty("vocal/firstTicAge");
				return _Vocal_firstticage;
			}else {
				return _Vocal_firstticage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for vocal/firstTicAge.
	 * @param v Value to Set.
	 */
	public void setVocal_firstticage(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/vocal/firstTicAge",v);
		_Vocal_firstticage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Vocal_worstticage=null;

	/**
	 * @return Returns the vocal/worstTicAge.
	 */
	public Double getVocal_worstticage() {
		try{
			if (_Vocal_worstticage==null){
				_Vocal_worstticage=getDoubleProperty("vocal/worstTicAge");
				return _Vocal_worstticage;
			}else {
				return _Vocal_worstticage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for vocal/worstTicAge.
	 * @param v Value to Set.
	 */
	public void setVocal_worstticage(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/vocal/worstTicAge",v);
		_Vocal_worstticage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Firstdiagnosedticsage=null;

	/**
	 * @return Returns the firstDiagnosedTicsAge.
	 */
	public Double getFirstdiagnosedticsage() {
		try{
			if (_Firstdiagnosedticsage==null){
				_Firstdiagnosedticsage=getDoubleProperty("firstDiagnosedTicsAge");
				return _Firstdiagnosedticsage;
			}else {
				return _Firstdiagnosedticsage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for firstDiagnosedTicsAge.
	 * @param v Value to Set.
	 */
	public void setFirstdiagnosedticsage(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/firstDiagnosedTicsAge",v);
		_Firstdiagnosedticsage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _DiagnosisAdhdDsmiv=null;

	/**
	 * @return Returns the diagnosis_ADHD_DSMIV.
	 */
	public String getDiagnosisAdhdDsmiv(){
		try{
			if (_DiagnosisAdhdDsmiv==null){
				_DiagnosisAdhdDsmiv=getStringProperty("diagnosis_ADHD_DSMIV");
				return _DiagnosisAdhdDsmiv;
			}else {
				return _DiagnosisAdhdDsmiv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for diagnosis_ADHD_DSMIV.
	 * @param v Value to Set.
	 */
	public void setDiagnosisAdhdDsmiv(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/diagnosis_ADHD_DSMIV",v);
		_DiagnosisAdhdDsmiv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _ScoreTsDci=null;

	/**
	 * @return Returns the score_TS_DCI.
	 */
	public Double getScoreTsDci() {
		try{
			if (_ScoreTsDci==null){
				_ScoreTsDci=getDoubleProperty("score_TS_DCI");
				return _ScoreTsDci;
			}else {
				return _ScoreTsDci;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for score_TS_DCI.
	 * @param v Value to Set.
	 */
	public void setScoreTsDci(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/score_TS_DCI",v);
		_ScoreTsDci=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _DiagnosisOcdDsmiv=null;

	/**
	 * @return Returns the diagnosis_OCD_DSMIV.
	 */
	public String getDiagnosisOcdDsmiv(){
		try{
			if (_DiagnosisOcdDsmiv==null){
				_DiagnosisOcdDsmiv=getStringProperty("diagnosis_OCD_DSMIV");
				return _DiagnosisOcdDsmiv;
			}else {
				return _DiagnosisOcdDsmiv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for diagnosis_OCD_DSMIV.
	 * @param v Value to Set.
	 */
	public void setDiagnosisOcdDsmiv(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/diagnosis_OCD_DSMIV",v);
		_DiagnosisOcdDsmiv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Firstobsessionage=null;

	/**
	 * @return Returns the firstObsessionAge.
	 */
	public Double getFirstobsessionage() {
		try{
			if (_Firstobsessionage==null){
				_Firstobsessionage=getDoubleProperty("firstObsessionAge");
				return _Firstobsessionage;
			}else {
				return _Firstobsessionage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for firstObsessionAge.
	 * @param v Value to Set.
	 */
	public void setFirstobsessionage(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/firstObsessionAge",v);
		_Firstobsessionage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Firstcompulsionage=null;

	/**
	 * @return Returns the firstCompulsionAge.
	 */
	public Double getFirstcompulsionage() {
		try{
			if (_Firstcompulsionage==null){
				_Firstcompulsionage=getDoubleProperty("firstCompulsionAge");
				return _Firstcompulsionage;
			}else {
				return _Firstcompulsionage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for firstCompulsionAge.
	 * @param v Value to Set.
	 */
	public void setFirstcompulsionage(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/firstCompulsionAge",v);
		_Firstcompulsionage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Worstobsessivecompulsivesymptomage=null;

	/**
	 * @return Returns the worstObsessiveCompulsiveSymptomAge.
	 */
	public Double getWorstobsessivecompulsivesymptomage() {
		try{
			if (_Worstobsessivecompulsivesymptomage==null){
				_Worstobsessivecompulsivesymptomage=getDoubleProperty("worstObsessiveCompulsiveSymptomAge");
				return _Worstobsessivecompulsivesymptomage;
			}else {
				return _Worstobsessivecompulsivesymptomage;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for worstObsessiveCompulsiveSymptomAge.
	 * @param v Value to Set.
	 */
	public void setWorstobsessivecompulsivesymptomage(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/worstObsessiveCompulsiveSymptomAge",v);
		_Worstobsessivecompulsivesymptomage=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Handedness=null;

	/**
	 * @return Returns the handedness.
	 */
	public String getHandedness(){
		try{
			if (_Handedness==null){
				_Handedness=getStringProperty("handedness");
				return _Handedness;
			}else {
				return _Handedness;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for handedness.
	 * @param v Value to Set.
	 */
	public void setHandedness(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/handedness",v);
		_Handedness=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Ineligiblefromscid=null;

	/**
	 * @return Returns the ineligibleFromSCID.
	 */
	public Boolean getIneligiblefromscid() {
		try{
			if (_Ineligiblefromscid==null){
				_Ineligiblefromscid=getBooleanProperty("ineligibleFromSCID");
				return _Ineligiblefromscid;
			}else {
				return _Ineligiblefromscid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ineligibleFromSCID.
	 * @param v Value to Set.
	 */
	public void setIneligiblefromscid(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/ineligibleFromSCID",v);
		_Ineligiblefromscid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaDiagnosesdata> getAllCndaDiagnosesdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaDiagnosesdata> al = new ArrayList<org.nrg.xdat.om.CndaDiagnosesdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaDiagnosesdata> getCndaDiagnosesdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaDiagnosesdata> al = new ArrayList<org.nrg.xdat.om.CndaDiagnosesdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaDiagnosesdata> getCndaDiagnosesdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaDiagnosesdata> al = new ArrayList<org.nrg.xdat.om.CndaDiagnosesdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaDiagnosesdata getCndaDiagnosesdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:diagnosesData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaDiagnosesdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
