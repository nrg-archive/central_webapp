/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:47 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoGeneticsGenetictestresultsGene extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.GeneticsGenetictestresultsGeneI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoGeneticsGenetictestresultsGene.class);
	public static String SCHEMA_ELEMENT_NAME="genetics:geneticTestResults_gene";

	public AutoGeneticsGenetictestresultsGene(ItemI item)
	{
		super(item);
	}

	public AutoGeneticsGenetictestresultsGene(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoGeneticsGenetictestresultsGene(UserI user)
	 **/
	public AutoGeneticsGenetictestresultsGene(){}

	public AutoGeneticsGenetictestresultsGene(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "genetics:geneticTestResults_gene";
	}

	//FIELD

	private String _Name=null;

	/**
	 * @return Returns the name.
	 */
	public String getName(){
		try{
			if (_Name==null){
				_Name=getStringProperty("name");
				return _Name;
			}else {
				return _Name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/name",v);
		_Name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Genotype=null;

	/**
	 * @return Returns the genotype.
	 */
	public String getGenotype(){
		try{
			if (_Genotype==null){
				_Genotype=getStringProperty("genotype");
				return _Genotype;
			}else {
				return _Genotype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for genotype.
	 * @param v Value to Set.
	 */
	public void setGenotype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/genotype",v);
		_Genotype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Minorpos=null;

	/**
	 * @return Returns the minorpos.
	 */
	public String getMinorpos(){
		try{
			if (_Minorpos==null){
				_Minorpos=getStringProperty("minorpos");
				return _Minorpos;
			}else {
				return _Minorpos;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for minorpos.
	 * @param v Value to Set.
	 */
	public void setMinorpos(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/minorpos",v);
		_Minorpos=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Heteropos=null;

	/**
	 * @return Returns the heteropos.
	 */
	public String getHeteropos(){
		try{
			if (_Heteropos==null){
				_Heteropos=getStringProperty("heteropos");
				return _Heteropos;
			}else {
				return _Heteropos;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for heteropos.
	 * @param v Value to Set.
	 */
	public void setHeteropos(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/heteropos",v);
		_Heteropos=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _GeneticsGenetictestresultsGeneId=null;

	/**
	 * @return Returns the genetics_geneticTestResults_gene_id.
	 */
	public Integer getGeneticsGenetictestresultsGeneId() {
		try{
			if (_GeneticsGenetictestresultsGeneId==null){
				_GeneticsGenetictestresultsGeneId=getIntegerProperty("genetics_geneticTestResults_gene_id");
				return _GeneticsGenetictestresultsGeneId;
			}else {
				return _GeneticsGenetictestresultsGeneId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for genetics_geneticTestResults_gene_id.
	 * @param v Value to Set.
	 */
	public void setGeneticsGenetictestresultsGeneId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/genetics_geneticTestResults_gene_id",v);
		_GeneticsGenetictestresultsGeneId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.GeneticsGenetictestresultsGene> getAllGeneticsGenetictestresultsGenes(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GeneticsGenetictestresultsGene> al = new ArrayList<org.nrg.xdat.om.GeneticsGenetictestresultsGene>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.GeneticsGenetictestresultsGene> getGeneticsGenetictestresultsGenesByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GeneticsGenetictestresultsGene> al = new ArrayList<org.nrg.xdat.om.GeneticsGenetictestresultsGene>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.GeneticsGenetictestresultsGene> getGeneticsGenetictestresultsGenesByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.GeneticsGenetictestresultsGene> al = new ArrayList<org.nrg.xdat.om.GeneticsGenetictestresultsGene>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static GeneticsGenetictestresultsGene getGeneticsGenetictestresultsGenesByGeneticsGenetictestresultsGeneId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("genetics:geneticTestResults_gene/genetics_geneticTestResults_gene_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (GeneticsGenetictestresultsGene) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
