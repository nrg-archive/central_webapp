/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaClinicalassessmentdataDiagnosis extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CndaClinicalassessmentdataDiagnosisI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaClinicalassessmentdataDiagnosis.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:clinicalAssessmentData_Diagnosis";

	public AutoCndaClinicalassessmentdataDiagnosis(ItemI item)
	{
		super(item);
	}

	public AutoCndaClinicalassessmentdataDiagnosis(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaClinicalassessmentdataDiagnosis(UserI user)
	 **/
	public AutoCndaClinicalassessmentdataDiagnosis(){}

	public AutoCndaClinicalassessmentdataDiagnosis(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:clinicalAssessmentData_Diagnosis";
	}

	//FIELD

	private String _Diagnosis=null;

	/**
	 * @return Returns the Diagnosis.
	 */
	public String getDiagnosis(){
		try{
			if (_Diagnosis==null){
				_Diagnosis=getStringProperty("Diagnosis");
				return _Diagnosis;
			}else {
				return _Diagnosis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for Diagnosis.
	 * @param v Value to Set.
	 */
	public void setDiagnosis(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/Diagnosis",v);
		_Diagnosis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Type=null;

	/**
	 * @return Returns the type.
	 */
	public String getType(){
		try{
			if (_Type==null){
				_Type=getStringProperty("type");
				return _Type;
			}else {
				return _Type;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for type.
	 * @param v Value to Set.
	 */
	public void setType(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/type",v);
		_Type=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Number=null;

	/**
	 * @return Returns the number.
	 */
	public Integer getNumber() {
		try{
			if (_Number==null){
				_Number=getIntegerProperty("number");
				return _Number;
			}else {
				return _Number;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for number.
	 * @param v Value to Set.
	 */
	public void setNumber(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/number",v);
		_Number=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CndaClinicalassessmentdataDiagnosisId=null;

	/**
	 * @return Returns the cnda_clinicalAssessmentData_Diagnosis_id.
	 */
	public Integer getCndaClinicalassessmentdataDiagnosisId() {
		try{
			if (_CndaClinicalassessmentdataDiagnosisId==null){
				_CndaClinicalassessmentdataDiagnosisId=getIntegerProperty("cnda_clinicalAssessmentData_Diagnosis_id");
				return _CndaClinicalassessmentdataDiagnosisId;
			}else {
				return _CndaClinicalassessmentdataDiagnosisId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda_clinicalAssessmentData_Diagnosis_id.
	 * @param v Value to Set.
	 */
	public void setCndaClinicalassessmentdataDiagnosisId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cnda_clinicalAssessmentData_Diagnosis_id",v);
		_CndaClinicalassessmentdataDiagnosisId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaClinicalassessmentdataDiagnosis> getAllCndaClinicalassessmentdataDiagnosiss(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaClinicalassessmentdataDiagnosis> al = new ArrayList<org.nrg.xdat.om.CndaClinicalassessmentdataDiagnosis>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaClinicalassessmentdataDiagnosis> getCndaClinicalassessmentdataDiagnosissByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaClinicalassessmentdataDiagnosis> al = new ArrayList<org.nrg.xdat.om.CndaClinicalassessmentdataDiagnosis>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaClinicalassessmentdataDiagnosis> getCndaClinicalassessmentdataDiagnosissByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaClinicalassessmentdataDiagnosis> al = new ArrayList<org.nrg.xdat.om.CndaClinicalassessmentdataDiagnosis>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaClinicalassessmentdataDiagnosis getCndaClinicalassessmentdataDiagnosissByCndaClinicalassessmentdataDiagnosisId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:clinicalAssessmentData_Diagnosis/cnda_clinicalAssessmentData_Diagnosis_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaClinicalassessmentdataDiagnosis) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
