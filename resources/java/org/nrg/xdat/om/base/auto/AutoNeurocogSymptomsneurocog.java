/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoNeurocogSymptomsneurocog extends XnatSubjectassessordata implements org.nrg.xdat.model.NeurocogSymptomsneurocogI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoNeurocogSymptomsneurocog.class);
	public static String SCHEMA_ELEMENT_NAME="neurocog:symptomsNeurocog";

	public AutoNeurocogSymptomsneurocog(ItemI item)
	{
		super(item);
	}

	public AutoNeurocogSymptomsneurocog(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoNeurocogSymptomsneurocog(UserI user)
	 **/
	public AutoNeurocogSymptomsneurocog(){}

	public AutoNeurocogSymptomsneurocog(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "neurocog:symptomsNeurocog";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Dfbv=null;

	/**
	 * @return Returns the dfbv.
	 */
	public Integer getDfbv() {
		try{
			if (_Dfbv==null){
				_Dfbv=getIntegerProperty("dfbv");
				return _Dfbv;
			}else {
				return _Dfbv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dfbv.
	 * @param v Value to Set.
	 */
	public void setDfbv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dfbv",v);
		_Dfbv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Yfbv=null;

	/**
	 * @return Returns the yfbv.
	 */
	public Integer getYfbv() {
		try{
			if (_Yfbv==null){
				_Yfbv=getIntegerProperty("yfbv");
				return _Yfbv;
			}else {
				return _Yfbv;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for yfbv.
	 * @param v Value to Set.
	 */
	public void setYfbv(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/yfbv",v);
		_Yfbv=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zcog2grp_zIq2grp=null;

	/**
	 * @return Returns the z_cog_2grp/z_iq_2grp.
	 */
	public String getZcog2grp_zIq2grp(){
		try{
			if (_Zcog2grp_zIq2grp==null){
				_Zcog2grp_zIq2grp=getStringProperty("z_cog_2grp/z_iq_2grp");
				return _Zcog2grp_zIq2grp;
			}else {
				return _Zcog2grp_zIq2grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_cog_2grp/z_iq_2grp.
	 * @param v Value to Set.
	 */
	public void setZcog2grp_zIq2grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_cog_2grp/z_iq_2grp",v);
		_Zcog2grp_zIq2grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zcog2grp_zWm2grp=null;

	/**
	 * @return Returns the z_cog_2grp/z_wm_2grp.
	 */
	public String getZcog2grp_zWm2grp(){
		try{
			if (_Zcog2grp_zWm2grp==null){
				_Zcog2grp_zWm2grp=getStringProperty("z_cog_2grp/z_wm_2grp");
				return _Zcog2grp_zWm2grp;
			}else {
				return _Zcog2grp_zWm2grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_cog_2grp/z_wm_2grp.
	 * @param v Value to Set.
	 */
	public void setZcog2grp_zWm2grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_cog_2grp/z_wm_2grp",v);
		_Zcog2grp_zWm2grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zcog2grp_zEm2grp=null;

	/**
	 * @return Returns the z_cog_2grp/z_em_2grp.
	 */
	public String getZcog2grp_zEm2grp(){
		try{
			if (_Zcog2grp_zEm2grp==null){
				_Zcog2grp_zEm2grp=getStringProperty("z_cog_2grp/z_em_2grp");
				return _Zcog2grp_zEm2grp;
			}else {
				return _Zcog2grp_zEm2grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_cog_2grp/z_em_2grp.
	 * @param v Value to Set.
	 */
	public void setZcog2grp_zEm2grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_cog_2grp/z_em_2grp",v);
		_Zcog2grp_zEm2grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zcog2grp_zRa2grp=null;

	/**
	 * @return Returns the z_cog_2grp/z_ra_2grp.
	 */
	public String getZcog2grp_zRa2grp(){
		try{
			if (_Zcog2grp_zRa2grp==null){
				_Zcog2grp_zRa2grp=getStringProperty("z_cog_2grp/z_ra_2grp");
				return _Zcog2grp_zRa2grp;
			}else {
				return _Zcog2grp_zRa2grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_cog_2grp/z_ra_2grp.
	 * @param v Value to Set.
	 */
	public void setZcog2grp_zRa2grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_cog_2grp/z_ra_2grp",v);
		_Zcog2grp_zRa2grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zpsy2grp_zPos2grp=null;

	/**
	 * @return Returns the z_psy_2grp/z_pos_2grp.
	 */
	public String getZpsy2grp_zPos2grp(){
		try{
			if (_Zpsy2grp_zPos2grp==null){
				_Zpsy2grp_zPos2grp=getStringProperty("z_psy_2grp/z_pos_2grp");
				return _Zpsy2grp_zPos2grp;
			}else {
				return _Zpsy2grp_zPos2grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_psy_2grp/z_pos_2grp.
	 * @param v Value to Set.
	 */
	public void setZpsy2grp_zPos2grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_psy_2grp/z_pos_2grp",v);
		_Zpsy2grp_zPos2grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zpsy2grp_zNeg2grp=null;

	/**
	 * @return Returns the z_psy_2grp/z_neg_2grp.
	 */
	public String getZpsy2grp_zNeg2grp(){
		try{
			if (_Zpsy2grp_zNeg2grp==null){
				_Zpsy2grp_zNeg2grp=getStringProperty("z_psy_2grp/z_neg_2grp");
				return _Zpsy2grp_zNeg2grp;
			}else {
				return _Zpsy2grp_zNeg2grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_psy_2grp/z_neg_2grp.
	 * @param v Value to Set.
	 */
	public void setZpsy2grp_zNeg2grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_psy_2grp/z_neg_2grp",v);
		_Zpsy2grp_zNeg2grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zpsy2grp_zDis2grp=null;

	/**
	 * @return Returns the z_psy_2grp/z_dis_2grp.
	 */
	public String getZpsy2grp_zDis2grp(){
		try{
			if (_Zpsy2grp_zDis2grp==null){
				_Zpsy2grp_zDis2grp=getStringProperty("z_psy_2grp/z_dis_2grp");
				return _Zpsy2grp_zDis2grp;
			}else {
				return _Zpsy2grp_zDis2grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_psy_2grp/z_dis_2grp.
	 * @param v Value to Set.
	 */
	public void setZpsy2grp_zDis2grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_psy_2grp/z_dis_2grp",v);
		_Zpsy2grp_zDis2grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zcog4grp_zIq4grp=null;

	/**
	 * @return Returns the z_cog_4grp/z_iq_4grp.
	 */
	public String getZcog4grp_zIq4grp(){
		try{
			if (_Zcog4grp_zIq4grp==null){
				_Zcog4grp_zIq4grp=getStringProperty("z_cog_4grp/z_iq_4grp");
				return _Zcog4grp_zIq4grp;
			}else {
				return _Zcog4grp_zIq4grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_cog_4grp/z_iq_4grp.
	 * @param v Value to Set.
	 */
	public void setZcog4grp_zIq4grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_cog_4grp/z_iq_4grp",v);
		_Zcog4grp_zIq4grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zcog4grp_zAttn4grp=null;

	/**
	 * @return Returns the z_cog_4grp/z_attn_4grp.
	 */
	public String getZcog4grp_zAttn4grp(){
		try{
			if (_Zcog4grp_zAttn4grp==null){
				_Zcog4grp_zAttn4grp=getStringProperty("z_cog_4grp/z_attn_4grp");
				return _Zcog4grp_zAttn4grp;
			}else {
				return _Zcog4grp_zAttn4grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_cog_4grp/z_attn_4grp.
	 * @param v Value to Set.
	 */
	public void setZcog4grp_zAttn4grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_cog_4grp/z_attn_4grp",v);
		_Zcog4grp_zAttn4grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zcog4grp_zWm4grp=null;

	/**
	 * @return Returns the z_cog_4grp/z_wm_4grp.
	 */
	public String getZcog4grp_zWm4grp(){
		try{
			if (_Zcog4grp_zWm4grp==null){
				_Zcog4grp_zWm4grp=getStringProperty("z_cog_4grp/z_wm_4grp");
				return _Zcog4grp_zWm4grp;
			}else {
				return _Zcog4grp_zWm4grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_cog_4grp/z_wm_4grp.
	 * @param v Value to Set.
	 */
	public void setZcog4grp_zWm4grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_cog_4grp/z_wm_4grp",v);
		_Zcog4grp_zWm4grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zcog4grp_zEm4grp=null;

	/**
	 * @return Returns the z_cog_4grp/z_em_4grp.
	 */
	public String getZcog4grp_zEm4grp(){
		try{
			if (_Zcog4grp_zEm4grp==null){
				_Zcog4grp_zEm4grp=getStringProperty("z_cog_4grp/z_em_4grp");
				return _Zcog4grp_zEm4grp;
			}else {
				return _Zcog4grp_zEm4grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_cog_4grp/z_em_4grp.
	 * @param v Value to Set.
	 */
	public void setZcog4grp_zEm4grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_cog_4grp/z_em_4grp",v);
		_Zcog4grp_zEm4grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zcog4grp_zRa4grp=null;

	/**
	 * @return Returns the z_cog_4grp/z_ra_4grp.
	 */
	public String getZcog4grp_zRa4grp(){
		try{
			if (_Zcog4grp_zRa4grp==null){
				_Zcog4grp_zRa4grp=getStringProperty("z_cog_4grp/z_ra_4grp");
				return _Zcog4grp_zRa4grp;
			}else {
				return _Zcog4grp_zRa4grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_cog_4grp/z_ra_4grp.
	 * @param v Value to Set.
	 */
	public void setZcog4grp_zRa4grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_cog_4grp/z_ra_4grp",v);
		_Zcog4grp_zRa4grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zpsy4grp_zPos4grp=null;

	/**
	 * @return Returns the z_psy_4grp/z_pos_4grp.
	 */
	public String getZpsy4grp_zPos4grp(){
		try{
			if (_Zpsy4grp_zPos4grp==null){
				_Zpsy4grp_zPos4grp=getStringProperty("z_psy_4grp/z_pos_4grp");
				return _Zpsy4grp_zPos4grp;
			}else {
				return _Zpsy4grp_zPos4grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_psy_4grp/z_pos_4grp.
	 * @param v Value to Set.
	 */
	public void setZpsy4grp_zPos4grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_psy_4grp/z_pos_4grp",v);
		_Zpsy4grp_zPos4grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zpsy4grp_zNeg4grp=null;

	/**
	 * @return Returns the z_psy_4grp/z_neg_4grp.
	 */
	public String getZpsy4grp_zNeg4grp(){
		try{
			if (_Zpsy4grp_zNeg4grp==null){
				_Zpsy4grp_zNeg4grp=getStringProperty("z_psy_4grp/z_neg_4grp");
				return _Zpsy4grp_zNeg4grp;
			}else {
				return _Zpsy4grp_zNeg4grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_psy_4grp/z_neg_4grp.
	 * @param v Value to Set.
	 */
	public void setZpsy4grp_zNeg4grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_psy_4grp/z_neg_4grp",v);
		_Zpsy4grp_zNeg4grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Zpsy4grp_zDis4grp=null;

	/**
	 * @return Returns the z_psy_4grp/z_dis_4grp.
	 */
	public String getZpsy4grp_zDis4grp(){
		try{
			if (_Zpsy4grp_zDis4grp==null){
				_Zpsy4grp_zDis4grp=getStringProperty("z_psy_4grp/z_dis_4grp");
				return _Zpsy4grp_zDis4grp;
			}else {
				return _Zpsy4grp_zDis4grp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for z_psy_4grp/z_dis_4grp.
	 * @param v Value to Set.
	 */
	public void setZpsy4grp_zDis4grp(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/z_psy_4grp/z_dis_4grp",v);
		_Zpsy4grp_zDis4grp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.NeurocogSymptomsneurocog> getAllNeurocogSymptomsneurocogs(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.NeurocogSymptomsneurocog> al = new ArrayList<org.nrg.xdat.om.NeurocogSymptomsneurocog>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.NeurocogSymptomsneurocog> getNeurocogSymptomsneurocogsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.NeurocogSymptomsneurocog> al = new ArrayList<org.nrg.xdat.om.NeurocogSymptomsneurocog>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.NeurocogSymptomsneurocog> getNeurocogSymptomsneurocogsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.NeurocogSymptomsneurocog> al = new ArrayList<org.nrg.xdat.om.NeurocogSymptomsneurocog>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static NeurocogSymptomsneurocog getNeurocogSymptomsneurocogsById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("neurocog:symptomsNeurocog/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (NeurocogSymptomsneurocog) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
