/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:47 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoPupPuptimecoursedata extends XnatPetassessordata implements org.nrg.xdat.model.PupPuptimecoursedataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoPupPuptimecoursedata.class);
	public static String SCHEMA_ELEMENT_NAME="pup:pupTimeCourseData";

	public AutoPupPuptimecoursedata(ItemI item)
	{
		super(item);
	}

	public AutoPupPuptimecoursedata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoPupPuptimecoursedata(UserI user)
	 **/
	public AutoPupPuptimecoursedata(){}

	public AutoPupPuptimecoursedata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "pup:pupTimeCourseData";
	}
	 private org.nrg.xdat.om.XnatPetassessordata _Petassessordata =null;

	/**
	 * petAssessorData
	 * @return org.nrg.xdat.om.XnatPetassessordata
	 */
	public org.nrg.xdat.om.XnatPetassessordata getPetassessordata() {
		try{
			if (_Petassessordata==null){
				_Petassessordata=((XnatPetassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("petAssessorData")));
				return _Petassessordata;
			}else {
				return _Petassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for petAssessorData.
	 * @param v Value to Set.
	 */
	public void setPetassessordata(ItemI v) throws Exception{
		_Petassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/petAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/petAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * petAssessorData
	 * set org.nrg.xdat.model.XnatPetassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatPetassessordataI> void setPetassessordata(A item) throws Exception{
	setPetassessordata((ItemI)item);
	}

	/**
	 * Removes the petAssessorData.
	 * */
	public void removePetassessordata() {
		_Petassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/petAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private String _Userfullname=null;

	/**
	 * @return Returns the userfullname.
	 */
	public String getUserfullname(){
		try{
			if (_Userfullname==null){
				_Userfullname=getStringProperty("userfullname");
				return _Userfullname;
			}else {
				return _Userfullname;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for userfullname.
	 * @param v Value to Set.
	 */
	public void setUserfullname(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/userfullname",v);
		_Userfullname=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Petlabel=null;

	/**
	 * @return Returns the petLabel.
	 */
	public String getPetlabel(){
		try{
			if (_Petlabel==null){
				_Petlabel=getStringProperty("petLabel");
				return _Petlabel;
			}else {
				return _Petlabel;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for petLabel.
	 * @param v Value to Set.
	 */
	public void setPetlabel(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/petLabel",v);
		_Petlabel=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Petscanid=null;

	/**
	 * @return Returns the petScanId.
	 */
	public String getPetscanid(){
		try{
			if (_Petscanid==null){
				_Petscanid=getStringProperty("petScanId");
				return _Petscanid;
			}else {
				return _Petscanid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for petScanId.
	 * @param v Value to Set.
	 */
	public void setPetscanid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/petScanId",v);
		_Petscanid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Proctype=null;

	/**
	 * @return Returns the procType.
	 */
	public String getProctype(){
		try{
			if (_Proctype==null){
				_Proctype=getStringProperty("procType");
				return _Proctype;
			}else {
				return _Proctype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for procType.
	 * @param v Value to Set.
	 */
	public void setProctype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/procType",v);
		_Proctype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Model=null;

	/**
	 * @return Returns the model.
	 */
	public String getModel(){
		try{
			if (_Model==null){
				_Model=getStringProperty("model");
				return _Model;
			}else {
				return _Model;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for model.
	 * @param v Value to Set.
	 */
	public void setModel(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/model",v);
		_Model=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tracer=null;

	/**
	 * @return Returns the tracer.
	 */
	public String getTracer(){
		try{
			if (_Tracer==null){
				_Tracer=getStringProperty("tracer");
				return _Tracer;
			}else {
				return _Tracer;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tracer.
	 * @param v Value to Set.
	 */
	public void setTracer(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tracer",v);
		_Tracer=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Halflife=null;

	/**
	 * @return Returns the halfLife.
	 */
	public String getHalflife(){
		try{
			if (_Halflife==null){
				_Halflife=getStringProperty("halfLife");
				return _Halflife;
			}else {
				return _Halflife;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for halfLife.
	 * @param v Value to Set.
	 */
	public void setHalflife(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/halfLife",v);
		_Halflife=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Templatetype=null;

	/**
	 * @return Returns the templateType.
	 */
	public String getTemplatetype(){
		try{
			if (_Templatetype==null){
				_Templatetype=getStringProperty("templateType");
				return _Templatetype;
			}else {
				return _Templatetype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for templateType.
	 * @param v Value to Set.
	 */
	public void setTemplatetype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/templateType",v);
		_Templatetype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Fsid=null;

	/**
	 * @return Returns the FSId.
	 */
	public String getFsid(){
		try{
			if (_Fsid==null){
				_Fsid=getStringProperty("FSId");
				return _Fsid;
			}else {
				return _Fsid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FSId.
	 * @param v Value to Set.
	 */
	public void setFsid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FSId",v);
		_Fsid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _T1=null;

	/**
	 * @return Returns the t1.
	 */
	public String getT1(){
		try{
			if (_T1==null){
				_T1=getStringProperty("t1");
				return _T1;
			}else {
				return _T1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for t1.
	 * @param v Value to Set.
	 */
	public void setT1(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/t1",v);
		_T1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Mrid=null;

	/**
	 * @return Returns the MRId.
	 */
	public String getMrid(){
		try{
			if (_Mrid==null){
				_Mrid=getStringProperty("MRId");
				return _Mrid;
			}else {
				return _Mrid;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MRId.
	 * @param v Value to Set.
	 */
	public void setMrid(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MRId",v);
		_Mrid=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mocoerror=null;

	/**
	 * @return Returns the mocoError.
	 */
	public Integer getMocoerror() {
		try{
			if (_Mocoerror==null){
				_Mocoerror=getIntegerProperty("mocoError");
				return _Mocoerror;
			}else {
				return _Mocoerror;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mocoError.
	 * @param v Value to Set.
	 */
	public void setMocoerror(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mocoError",v);
		_Mocoerror=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Regerror=null;

	/**
	 * @return Returns the regError.
	 */
	public Integer getRegerror() {
		try{
			if (_Regerror==null){
				_Regerror=getIntegerProperty("regError");
				return _Regerror;
			}else {
				return _Regerror;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for regError.
	 * @param v Value to Set.
	 */
	public void setRegerror(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/regError",v);
		_Regerror=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Suvrflag=null;

	/**
	 * @return Returns the suvrFlag.
	 */
	public Integer getSuvrflag() {
		try{
			if (_Suvrflag==null){
				_Suvrflag=getIntegerProperty("suvrFlag");
				return _Suvrflag;
			}else {
				return _Suvrflag;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for suvrFlag.
	 * @param v Value to Set.
	 */
	public void setSuvrflag(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/suvrFlag",v);
		_Suvrflag=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Refroistr=null;

	/**
	 * @return Returns the refroistr.
	 */
	public String getRefroistr(){
		try{
			if (_Refroistr==null){
				_Refroistr=getStringProperty("refroistr");
				return _Refroistr;
			}else {
				return _Refroistr;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for refroistr.
	 * @param v Value to Set.
	 */
	public void setRefroistr(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/refroistr",v);
		_Refroistr=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Delayparam=null;

	/**
	 * @return Returns the delayParam.
	 */
	public Integer getDelayparam() {
		try{
			if (_Delayparam==null){
				_Delayparam=getIntegerProperty("delayParam");
				return _Delayparam;
			}else {
				return _Delayparam;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for delayParam.
	 * @param v Value to Set.
	 */
	public void setDelayparam(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/delayParam",v);
		_Delayparam=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mst=null;

	/**
	 * @return Returns the mst.
	 */
	public Integer getMst() {
		try{
			if (_Mst==null){
				_Mst=getIntegerProperty("mst");
				return _Mst;
			}else {
				return _Mst;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mst.
	 * @param v Value to Set.
	 */
	public void setMst(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mst",v);
		_Mst=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Mdt=null;

	/**
	 * @return Returns the mdt.
	 */
	public Integer getMdt() {
		try{
			if (_Mdt==null){
				_Mdt=getIntegerProperty("mdt");
				return _Mdt;
			}else {
				return _Mdt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mdt.
	 * @param v Value to Set.
	 */
	public void setMdt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mdt",v);
		_Mdt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Tbl=null;

	/**
	 * @return Returns the tbl.
	 */
	public String getTbl(){
		try{
			if (_Tbl==null){
				_Tbl=getStringProperty("tbl");
				return _Tbl;
			}else {
				return _Tbl;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for tbl.
	 * @param v Value to Set.
	 */
	public void setTbl(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/tbl",v);
		_Tbl=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rbf=null;

	/**
	 * @return Returns the rbf.
	 */
	public String getRbf(){
		try{
			if (_Rbf==null){
				_Rbf=getStringProperty("rbf");
				return _Rbf;
			}else {
				return _Rbf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rbf.
	 * @param v Value to Set.
	 */
	public void setRbf(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rbf",v);
		_Rbf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Mbf=null;

	/**
	 * @return Returns the mbf.
	 */
	public String getMbf(){
		try{
			if (_Mbf==null){
				_Mbf=getStringProperty("mbf");
				return _Mbf;
			}else {
				return _Mbf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for mbf.
	 * @param v Value to Set.
	 */
	public void setMbf(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/mbf",v);
		_Mbf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Sf=null;

	/**
	 * @return Returns the sf.
	 */
	public Integer getSf() {
		try{
			if (_Sf==null){
				_Sf=getIntegerProperty("sf");
				return _Sf;
			}else {
				return _Sf;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for sf.
	 * @param v Value to Set.
	 */
	public void setSf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/sf",v);
		_Sf=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ef=null;

	/**
	 * @return Returns the ef.
	 */
	public Integer getEf() {
		try{
			if (_Ef==null){
				_Ef=getIntegerProperty("ef");
				return _Ef;
			}else {
				return _Ef;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ef.
	 * @param v Value to Set.
	 */
	public void setEf(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ef",v);
		_Ef=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Filter=null;

	/**
	 * @return Returns the filter.
	 */
	public Integer getFilter() {
		try{
			if (_Filter==null){
				_Filter=getIntegerProperty("filter");
				return _Filter;
			}else {
				return _Filter;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for filter.
	 * @param v Value to Set.
	 */
	public void setFilter(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/filter",v);
		_Filter=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Filterxy=null;

	/**
	 * @return Returns the filterxy.
	 */
	public Double getFilterxy() {
		try{
			if (_Filterxy==null){
				_Filterxy=getDoubleProperty("filterxy");
				return _Filterxy;
			}else {
				return _Filterxy;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for filterxy.
	 * @param v Value to Set.
	 */
	public void setFilterxy(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/filterxy",v);
		_Filterxy=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Filterz=null;

	/**
	 * @return Returns the filterz.
	 */
	public Double getFilterz() {
		try{
			if (_Filterz==null){
				_Filterz=getDoubleProperty("filterz");
				return _Filterz;
			}else {
				return _Filterz;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for filterz.
	 * @param v Value to Set.
	 */
	public void setFilterz(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/filterz",v);
		_Filterz=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Fwhm=null;

	/**
	 * @return Returns the fwhm.
	 */
	public Double getFwhm() {
		try{
			if (_Fwhm==null){
				_Fwhm=getDoubleProperty("fwhm");
				return _Fwhm;
			}else {
				return _Fwhm;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for fwhm.
	 * @param v Value to Set.
	 */
	public void setFwhm(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/fwhm",v);
		_Fwhm=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Pvc2cflag=null;

	/**
	 * @return Returns the pvc2cflag.
	 */
	public Integer getPvc2cflag() {
		try{
			if (_Pvc2cflag==null){
				_Pvc2cflag=getIntegerProperty("pvc2cflag");
				return _Pvc2cflag;
			}else {
				return _Pvc2cflag;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pvc2cflag.
	 * @param v Value to Set.
	 */
	public void setPvc2cflag(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pvc2cflag",v);
		_Pvc2cflag=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rsfflag=null;

	/**
	 * @return Returns the rsfflag.
	 */
	public Integer getRsfflag() {
		try{
			if (_Rsfflag==null){
				_Rsfflag=getIntegerProperty("rsfflag");
				return _Rsfflag;
			}else {
				return _Rsfflag;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rsfflag.
	 * @param v Value to Set.
	 */
	public void setRsfflag(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rsfflag",v);
		_Rsfflag=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pupstatus=null;

	/**
	 * @return Returns the pupStatus.
	 */
	public String getPupstatus(){
		try{
			if (_Pupstatus==null){
				_Pupstatus=getStringProperty("pupStatus");
				return _Pupstatus;
			}else {
				return _Pupstatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pupStatus.
	 * @param v Value to Set.
	 */
	public void setPupstatus(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pupStatus",v);
		_Pupstatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi> _Rois_roi =null;

	/**
	 * rois/roi
	 * @return Returns an List of org.nrg.xdat.om.PupPuptimecoursedataRoi
	 */
	public <A extends org.nrg.xdat.model.PupPuptimecoursedataRoiI> List<A> getRois_roi() {
		try{
			if (_Rois_roi==null){
				_Rois_roi=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("rois/roi"));
			}
			return (List<A>) _Rois_roi;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.PupPuptimecoursedataRoi>();}
	}

	/**
	 * Sets the value for rois/roi.
	 * @param v Value to Set.
	 */
	public void setRois_roi(ItemI v) throws Exception{
		_Rois_roi =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/rois/roi",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/rois/roi",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * rois/roi
	 * Adds org.nrg.xdat.model.PupPuptimecoursedataRoiI
	 */
	public <A extends org.nrg.xdat.model.PupPuptimecoursedataRoiI> void addRois_roi(A item) throws Exception{
	setRois_roi((ItemI)item);
	}

	/**
	 * Removes the rois/roi of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeRois_roi(int index) throws java.lang.IndexOutOfBoundsException {
		_Rois_roi =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/rois/roi",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.PupPuptimecoursedata> getAllPupPuptimecoursedatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PupPuptimecoursedata> al = new ArrayList<org.nrg.xdat.om.PupPuptimecoursedata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.PupPuptimecoursedata> getPupPuptimecoursedatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PupPuptimecoursedata> al = new ArrayList<org.nrg.xdat.om.PupPuptimecoursedata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.PupPuptimecoursedata> getPupPuptimecoursedatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.PupPuptimecoursedata> al = new ArrayList<org.nrg.xdat.om.PupPuptimecoursedata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static PupPuptimecoursedata getPupPuptimecoursedatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("pup:pupTimeCourseData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (PupPuptimecoursedata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //petAssessorData
	        XnatPetassessordata childPetassessordata = (XnatPetassessordata)this.getPetassessordata();
	            if (childPetassessordata!=null){
	              for(ResourceFile rf: ((XnatPetassessordata)childPetassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("petAssessorData[" + ((XnatPetassessordata)childPetassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("petAssessorData/" + ((XnatPetassessordata)childPetassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //rois/roi
	        for(org.nrg.xdat.model.PupPuptimecoursedataRoiI childRois_roi : this.getRois_roi()){
	            if (childRois_roi!=null){
	              for(ResourceFile rf: ((PupPuptimecoursedataRoi)childRois_roi).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("rois/roi[" + ((PupPuptimecoursedataRoi)childRois_roi).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("rois/roi/" + ((PupPuptimecoursedataRoi)childRois_roi).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
