/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaModifiedscheltenspvregion extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CndaModifiedscheltenspvregionI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaModifiedscheltenspvregion.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:modifiedScheltensPvRegion";

	public AutoCndaModifiedscheltenspvregion(ItemI item)
	{
		super(item);
	}

	public AutoCndaModifiedscheltenspvregion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaModifiedscheltenspvregion(UserI user)
	 **/
	public AutoCndaModifiedscheltenspvregion(){}

	public AutoCndaModifiedscheltenspvregion(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:modifiedScheltensPvRegion";
	}

	//FIELD

	private Integer _Left=null;

	/**
	 * @return Returns the left.
	 */
	public Integer getLeft() {
		try{
			if (_Left==null){
				_Left=getIntegerProperty("left");
				return _Left;
			}else {
				return _Left;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for left.
	 * @param v Value to Set.
	 */
	public void setLeft(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/left",v);
		_Left=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Right=null;

	/**
	 * @return Returns the right.
	 */
	public Integer getRight() {
		try{
			if (_Right==null){
				_Right=getIntegerProperty("right");
				return _Right;
			}else {
				return _Right;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for right.
	 * @param v Value to Set.
	 */
	public void setRight(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/right",v);
		_Right=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Scheltens=null;

	/**
	 * @return Returns the scheltens.
	 */
	public Integer getScheltens() {
		try{
			if (_Scheltens==null){
				_Scheltens=getIntegerProperty("scheltens");
				return _Scheltens;
			}else {
				return _Scheltens;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for scheltens.
	 * @param v Value to Set.
	 */
	public void setScheltens(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/scheltens",v);
		_Scheltens=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CndaModifiedscheltenspvregionId=null;

	/**
	 * @return Returns the cnda_modifiedScheltensPvRegion_id.
	 */
	public Integer getCndaModifiedscheltenspvregionId() {
		try{
			if (_CndaModifiedscheltenspvregionId==null){
				_CndaModifiedscheltenspvregionId=getIntegerProperty("cnda_modifiedScheltensPvRegion_id");
				return _CndaModifiedscheltenspvregionId;
			}else {
				return _CndaModifiedscheltenspvregionId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda_modifiedScheltensPvRegion_id.
	 * @param v Value to Set.
	 */
	public void setCndaModifiedscheltenspvregionId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cnda_modifiedScheltensPvRegion_id",v);
		_CndaModifiedscheltenspvregionId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaModifiedscheltenspvregion> getAllCndaModifiedscheltenspvregions(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaModifiedscheltenspvregion> al = new ArrayList<org.nrg.xdat.om.CndaModifiedscheltenspvregion>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaModifiedscheltenspvregion> getCndaModifiedscheltenspvregionsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaModifiedscheltenspvregion> al = new ArrayList<org.nrg.xdat.om.CndaModifiedscheltenspvregion>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaModifiedscheltenspvregion> getCndaModifiedscheltenspvregionsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaModifiedscheltenspvregion> al = new ArrayList<org.nrg.xdat.om.CndaModifiedscheltenspvregion>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaModifiedscheltenspvregion getCndaModifiedscheltenspvregionsByCndaModifiedscheltenspvregionId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:modifiedScheltensPvRegion/cnda_modifiedScheltensPvRegion_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaModifiedscheltenspvregion) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
