/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaPettimecoursedataDurationBp extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CndaPettimecoursedataDurationBpI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaPettimecoursedataDurationBp.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:petTimeCourseData_duration_bp";

	public AutoCndaPettimecoursedataDurationBp(ItemI item)
	{
		super(item);
	}

	public AutoCndaPettimecoursedataDurationBp(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaPettimecoursedataDurationBp(UserI user)
	 **/
	public AutoCndaPettimecoursedataDurationBp(){}

	public AutoCndaPettimecoursedataDurationBp(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:petTimeCourseData_duration_bp";
	}

	//FIELD

	private Double _Bp=null;

	/**
	 * @return Returns the bp.
	 */
	public Double getBp() {
		try{
			if (_Bp==null){
				_Bp=getDoubleProperty("bp");
				return _Bp;
			}else {
				return _Bp;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for bp.
	 * @param v Value to Set.
	 */
	public void setBp(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/bp",v);
		_Bp=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Name=null;

	/**
	 * @return Returns the name.
	 */
	public String getName(){
		try{
			if (_Name==null){
				_Name=getStringProperty("name");
				return _Name;
			}else {
				return _Name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/name",v);
		_Name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Correl=null;

	/**
	 * @return Returns the correl.
	 */
	public Double getCorrel() {
		try{
			if (_Correl==null){
				_Correl=getDoubleProperty("correl");
				return _Correl;
			}else {
				return _Correl;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for correl.
	 * @param v Value to Set.
	 */
	public void setCorrel(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/correl",v);
		_Correl=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CndaPettimecoursedataDurationBpId=null;

	/**
	 * @return Returns the cnda_petTimeCourseData_duration_bp_id.
	 */
	public Integer getCndaPettimecoursedataDurationBpId() {
		try{
			if (_CndaPettimecoursedataDurationBpId==null){
				_CndaPettimecoursedataDurationBpId=getIntegerProperty("cnda_petTimeCourseData_duration_bp_id");
				return _CndaPettimecoursedataDurationBpId;
			}else {
				return _CndaPettimecoursedataDurationBpId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda_petTimeCourseData_duration_bp_id.
	 * @param v Value to Set.
	 */
	public void setCndaPettimecoursedataDurationBpId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cnda_petTimeCourseData_duration_bp_id",v);
		_CndaPettimecoursedataDurationBpId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDurationBp> getAllCndaPettimecoursedataDurationBps(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDurationBp> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDurationBp>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDurationBp> getCndaPettimecoursedataDurationBpsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDurationBp> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDurationBp>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDurationBp> getCndaPettimecoursedataDurationBpsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDurationBp> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataDurationBp>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaPettimecoursedataDurationBp getCndaPettimecoursedataDurationBpsByCndaPettimecoursedataDurationBpId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:petTimeCourseData_duration_bp/cnda_petTimeCourseData_duration_bp_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaPettimecoursedataDurationBp) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
