/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:46 CST 2017
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCndaPettimecoursedataRegionActivity extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.CndaPettimecoursedataRegionActivityI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCndaPettimecoursedataRegionActivity.class);
	public static String SCHEMA_ELEMENT_NAME="cnda:petTimeCourseData_region_activity";

	public AutoCndaPettimecoursedataRegionActivity(ItemI item)
	{
		super(item);
	}

	public AutoCndaPettimecoursedataRegionActivity(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCndaPettimecoursedataRegionActivity(UserI user)
	 **/
	public AutoCndaPettimecoursedataRegionActivity(){}

	public AutoCndaPettimecoursedataRegionActivity(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "cnda:petTimeCourseData_region_activity";
	}

	//FIELD

	private Double _Activity=null;

	/**
	 * @return Returns the activity.
	 */
	public Double getActivity() {
		try{
			if (_Activity==null){
				_Activity=getDoubleProperty("activity");
				return _Activity;
			}else {
				return _Activity;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for activity.
	 * @param v Value to Set.
	 */
	public void setActivity(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/activity",v);
		_Activity=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Time=null;

	/**
	 * @return Returns the time.
	 */
	public Double getTime() {
		try{
			if (_Time==null){
				_Time=getDoubleProperty("time");
				return _Time;
			}else {
				return _Time;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for time.
	 * @param v Value to Set.
	 */
	public void setTime(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/time",v);
		_Time=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _CndaPettimecoursedataRegionActivityId=null;

	/**
	 * @return Returns the cnda_petTimeCourseData_region_activity_id.
	 */
	public Integer getCndaPettimecoursedataRegionActivityId() {
		try{
			if (_CndaPettimecoursedataRegionActivityId==null){
				_CndaPettimecoursedataRegionActivityId=getIntegerProperty("cnda_petTimeCourseData_region_activity_id");
				return _CndaPettimecoursedataRegionActivityId;
			}else {
				return _CndaPettimecoursedataRegionActivityId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for cnda_petTimeCourseData_region_activity_id.
	 * @param v Value to Set.
	 */
	public void setCndaPettimecoursedataRegionActivityId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/cnda_petTimeCourseData_region_activity_id",v);
		_CndaPettimecoursedataRegionActivityId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegionActivity> getAllCndaPettimecoursedataRegionActivitys(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegionActivity> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegionActivity>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegionActivity> getCndaPettimecoursedataRegionActivitysByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegionActivity> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegionActivity>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegionActivity> getCndaPettimecoursedataRegionActivitysByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegionActivity> al = new ArrayList<org.nrg.xdat.om.CndaPettimecoursedataRegionActivity>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CndaPettimecoursedataRegionActivity getCndaPettimecoursedataRegionActivitysByCndaPettimecoursedataRegionActivityId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("cnda:petTimeCourseData_region_activity/cnda_petTimeCourseData_region_activity_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CndaPettimecoursedataRegionActivity) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
