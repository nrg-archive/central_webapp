/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:49 CST 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseSapssansSymptomssapssans extends AutoSapssansSymptomssapssans {

	public BaseSapssansSymptomssapssans(ItemI item)
	{
		super(item);
	}

	public BaseSapssansSymptomssapssans(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseSapssansSymptomssapssans(UserI user)
	 **/
	public BaseSapssansSymptomssapssans()
	{}

	public BaseSapssansSymptomssapssans(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
