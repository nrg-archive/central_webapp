/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:48 CST 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseNeurocogSymptomsneurocog extends AutoNeurocogSymptomsneurocog {

	public BaseNeurocogSymptomsneurocog(ItemI item)
	{
		super(item);
	}

	public BaseNeurocogSymptomsneurocog(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseNeurocogSymptomsneurocog(UserI user)
	 **/
	public BaseNeurocogSymptomsneurocog()
	{}

	public BaseNeurocogSymptomsneurocog(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
