/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:49 CST 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaVitalsdataBloodpressure extends AutoCndaVitalsdataBloodpressure {

	public BaseCndaVitalsdataBloodpressure(ItemI item)
	{
		super(item);
	}

	public BaseCndaVitalsdataBloodpressure(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaVitalsdataBloodpressure(UserI user)
	 **/
	public BaseCndaVitalsdataBloodpressure()
	{}

	public BaseCndaVitalsdataBloodpressure(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
