/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:49 CST 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseFsLongfsdataHemisphere extends AutoFsLongfsdataHemisphere {

	public BaseFsLongfsdataHemisphere(ItemI item)
	{
		super(item);
	}

	public BaseFsLongfsdataHemisphere(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsLongfsdataHemisphere(UserI user)
	 **/
	public BaseFsLongfsdataHemisphere()
	{}

	public BaseFsLongfsdataHemisphere(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
