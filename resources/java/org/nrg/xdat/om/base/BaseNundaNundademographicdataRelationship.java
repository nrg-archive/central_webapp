/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:48 CST 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseNundaNundademographicdataRelationship extends AutoNundaNundademographicdataRelationship {

	public BaseNundaNundademographicdataRelationship(ItemI item)
	{
		super(item);
	}

	public BaseNundaNundademographicdataRelationship(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseNundaNundademographicdataRelationship(UserI user)
	 **/
	public BaseNundaNundademographicdataRelationship()
	{}

	public BaseNundaNundademographicdataRelationship(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
