/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:49 CST 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseFsLongfsdataRegion extends AutoFsLongfsdataRegion {

	public BaseFsLongfsdataRegion(ItemI item)
	{
		super(item);
	}

	public BaseFsLongfsdataRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsLongfsdataRegion(UserI user)
	 **/
	public BaseFsLongfsdataRegion()
	{}

	public BaseFsLongfsdataRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
