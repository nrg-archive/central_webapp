/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:48 CST 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaVolumetryregioninfo extends AutoCndaVolumetryregioninfo {

	public BaseCndaVolumetryregioninfo(ItemI item)
	{
		super(item);
	}

	public BaseCndaVolumetryregioninfo(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaVolumetryregioninfo(UserI user)
	 **/
	public BaseCndaVolumetryregioninfo()
	{}

	public BaseCndaVolumetryregioninfo(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
