/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:48 CST 2017
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaClinicalassessmentdata extends AutoCndaClinicalassessmentdata {

	public BaseCndaClinicalassessmentdata(ItemI item)
	{
		super(item);
	}

	public BaseCndaClinicalassessmentdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaClinicalassessmentdata(UserI user)
	 **/
	public BaseCndaClinicalassessmentdata()
	{}

	public BaseCndaClinicalassessmentdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
