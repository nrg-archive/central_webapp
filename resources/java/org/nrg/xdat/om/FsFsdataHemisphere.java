/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:49 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class FsFsdataHemisphere extends BaseFsFsdataHemisphere {

	public FsFsdataHemisphere(ItemI item)
	{
		super(item);
	}

	public FsFsdataHemisphere(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsFsdataHemisphere(UserI user)
	 **/
	public FsFsdataHemisphere()
	{}

	public FsFsdataHemisphere(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
