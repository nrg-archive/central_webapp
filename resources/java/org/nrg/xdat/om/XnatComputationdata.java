/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:12:49 CST 2017
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatComputationdata extends BaseXnatComputationdata {

	public XnatComputationdata(ItemI item)
	{
		super(item);
	}

	public XnatComputationdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatComputationdata(UserI user)
	 **/
	public XnatComputationdata()
	{}

	public XnatComputationdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
