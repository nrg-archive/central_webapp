/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_petTimeCourseData_duration(){
this.xsiType="cnda:petTimeCourseData_duration";

	this.getSchemaElementName=function(){
		return "petTimeCourseData_duration";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:petTimeCourseData_duration";
	}
	this.Bp =new Array();

	function getBp() {
		return this.Bp;
	}
	this.getBp=getBp;


	function addBp(v){
		this.Bp.push(v);
	}
	this.addBp=addBp;

	this.Span=null;


	function getSpan() {
		return this.Span;
	}
	this.getSpan=getSpan;


	function setSpan(v){
		this.Span=v;
	}
	this.setSpan=setSpan;

	this.CndaPettimecoursedataDurationId=null;


	function getCndaPettimecoursedataDurationId() {
		return this.CndaPettimecoursedataDurationId;
	}
	this.getCndaPettimecoursedataDurationId=getCndaPettimecoursedataDurationId;


	function setCndaPettimecoursedataDurationId(v){
		this.CndaPettimecoursedataDurationId=v;
	}
	this.setCndaPettimecoursedataDurationId=setCndaPettimecoursedataDurationId;

	this.durations_duration_cnda_petTime_id_fk=null;


	this.getdurations_duration_cnda_petTime_id=function() {
		return this.durations_duration_cnda_petTime_id_fk;
	}


	this.setdurations_duration_cnda_petTime_id=function(v){
		this.durations_duration_cnda_petTime_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="bp"){
				return this.Bp ;
			} else 
			if(xmlPath.startsWith("bp")){
				xmlPath=xmlPath.substring(2);
				if(xmlPath=="")return this.Bp ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Bp.length;whereCount++){

					var tempValue=this.Bp[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Bp[whereCount]);

					}

				}
				}else{

				whereArray=this.Bp;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="span"){
				return this.Span ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="cnda_petTimeCourseData_duration_id"){
				return this.CndaPettimecoursedataDurationId ;
			} else 
			if(xmlPath=="durations_duration_cnda_petTime_id"){
				return this.durations_duration_cnda_petTime_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="bp"){
				this.Bp=value;
			} else 
			if(xmlPath.startsWith("bp")){
				xmlPath=xmlPath.substring(2);
				if(xmlPath=="")return this.Bp ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Bp.length;whereCount++){

					var tempValue=this.Bp[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Bp[whereCount]);

					}

				}
				}else{

				whereArray=this.Bp;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("cnda:petTimeCourseData_duration_bp");//omUtils.js
					}
					this.addBp(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="span"){
				this.Span=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="cnda_petTimeCourseData_duration_id"){
				this.CndaPettimecoursedataDurationId=value;
			} else 
			if(xmlPath=="durations_duration_cnda_petTime_id"){
				this.durations_duration_cnda_petTime_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="bp"){
			this.addBp(v);
		}
		else{
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="bp"){
			return "http://nrg.wustl.edu/cnda:petTimeCourseData_duration_bp";
		}
		else{
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="bp"){
			return "field_NO_CHILD";
		}else if (xmlPath=="span"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:petTimeCourseData_duration";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:petTimeCourseData_duration>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.CndaPettimecoursedataDurationId!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_petTimeCourseData_duration_id=\"" + this.CndaPettimecoursedataDurationId + "\"";
			}
			if(this.durations_duration_cnda_petTime_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="durations_duration_cnda_petTime_id=\"" + this.durations_duration_cnda_petTime_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Span!=null)
			attTxt+=" span=\"" +this.Span +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		for(var BpCOUNT=0;BpCOUNT<this.Bp.length;BpCOUNT++){
			xmlTxt +="\n<cnda:bp";
			xmlTxt +=this.Bp[BpCOUNT].getXMLAtts();
			if(this.Bp[BpCOUNT].xsiType!="cnda:petTimeCourseData_duration_bp"){
				xmlTxt+=" xsi:type=\"" + this.Bp[BpCOUNT].xsiType + "\"";
			}
			if (this.Bp[BpCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Bp[BpCOUNT].getXMLBody(preventComments);
					xmlTxt+="</cnda:bp>";
			}else {xmlTxt+="/>";}
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.CndaPettimecoursedataDurationId!=null) return true;
			if (this.durations_duration_cnda_petTime_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if(this.Bp.length>0) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
