/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_manualVolumetryRegion_slice(){
this.xsiType="cnda:manualVolumetryRegion_slice";

	this.getSchemaElementName=function(){
		return "manualVolumetryRegion_slice";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:manualVolumetryRegion_slice";
	}

	this.Num=null;


	function getNum() {
		return this.Num;
	}
	this.getNum=getNum;


	function setNum(v){
		this.Num=v;
	}
	this.setNum=setNum;

	this.Voxels=null;


	function getVoxels() {
		return this.Voxels;
	}
	this.getVoxels=getVoxels;


	function setVoxels(v){
		this.Voxels=v;
	}
	this.setVoxels=setVoxels;

	this.Area=null;


	function getArea() {
		return this.Area;
	}
	this.getArea=getArea;


	function setArea(v){
		this.Area=v;
	}
	this.setArea=setArea;

	this.Volume=null;


	function getVolume() {
		return this.Volume;
	}
	this.getVolume=getVolume;


	function setVolume(v){
		this.Volume=v;
	}
	this.setVolume=setVolume;

	this.CndaManualvolumetryregionSliceId=null;


	function getCndaManualvolumetryregionSliceId() {
		return this.CndaManualvolumetryregionSliceId;
	}
	this.getCndaManualvolumetryregionSliceId=getCndaManualvolumetryregionSliceId;


	function setCndaManualvolumetryregionSliceId(v){
		this.CndaManualvolumetryregionSliceId=v;
	}
	this.setCndaManualvolumetryregionSliceId=setCndaManualvolumetryregionSliceId;

	this.cnda_manualVolumetryRegion_xnat_volumetricregion_id_fk=null;


	this.getcnda_manualVolumetryRegion_xnat_volumetricregion_id=function() {
		return this.cnda_manualVolumetryRegion_xnat_volumetricregion_id_fk;
	}


	this.setcnda_manualVolumetryRegion_xnat_volumetricregion_id=function(v){
		this.cnda_manualVolumetryRegion_xnat_volumetricregion_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="num"){
				return this.Num ;
			} else 
			if(xmlPath=="voxels"){
				return this.Voxels ;
			} else 
			if(xmlPath=="area"){
				return this.Area ;
			} else 
			if(xmlPath=="volume"){
				return this.Volume ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="cnda_manualVolumetryRegion_slice_id"){
				return this.CndaManualvolumetryregionSliceId ;
			} else 
			if(xmlPath=="cnda_manualVolumetryRegion_xnat_volumetricregion_id"){
				return this.cnda_manualVolumetryRegion_xnat_volumetricregion_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="num"){
				this.Num=value;
			} else 
			if(xmlPath=="voxels"){
				this.Voxels=value;
			} else 
			if(xmlPath=="area"){
				this.Area=value;
			} else 
			if(xmlPath=="volume"){
				this.Volume=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="cnda_manualVolumetryRegion_slice_id"){
				this.CndaManualvolumetryregionSliceId=value;
			} else 
			if(xmlPath=="cnda_manualVolumetryRegion_xnat_volumetricregion_id"){
				this.cnda_manualVolumetryRegion_xnat_volumetricregion_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="num"){
			return "field_data";
		}else if (xmlPath=="voxels"){
			return "field_data";
		}else if (xmlPath=="area"){
			return "field_data";
		}else if (xmlPath=="volume"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:manualVolumetryRegion_slice";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:manualVolumetryRegion_slice>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.CndaManualvolumetryregionSliceId!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_manualVolumetryRegion_slice_id=\"" + this.CndaManualvolumetryregionSliceId + "\"";
			}
			if(this.cnda_manualVolumetryRegion_xnat_volumetricregion_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_manualVolumetryRegion_xnat_volumetricregion_id=\"" + this.cnda_manualVolumetryRegion_xnat_volumetricregion_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Num!=null)
			attTxt+=" num=\"" +this.Num +"\"";
		else attTxt+=" num=\"\"";//REQUIRED FIELD

		if (this.Voxels!=null)
			attTxt+=" voxels=\"" +this.Voxels +"\"";
		else attTxt+=" voxels=\"\"";//REQUIRED FIELD

		if (this.Area!=null)
			attTxt+=" area=\"" +this.Area +"\"";
		//NOT REQUIRED FIELD

		if (this.Volume!=null)
			attTxt+=" volume=\"" +this.Volume +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.CndaManualvolumetryregionSliceId!=null) return true;
			if (this.cnda_manualVolumetryRegion_xnat_volumetricregion_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if(this.hasXMLComments())return true;
		return false;
	}
}
