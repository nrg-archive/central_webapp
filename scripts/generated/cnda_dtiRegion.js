/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_dtiRegion(){
this.xsiType="cnda:dtiRegion";

	this.getSchemaElementName=function(){
		return "dtiRegion";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:dtiRegion";
	}
this.extension=dynamicJSLoad('xnat_volumetricRegion','generated/xnat_volumetricRegion.js');

	this.Manisotropy=null;


	function getManisotropy() {
		return this.Manisotropy;
	}
	this.getManisotropy=getManisotropy;


	function setManisotropy(v){
		this.Manisotropy=v;
	}
	this.setManisotropy=setManisotropy;

	this.Mdiffusivity=null;


	function getMdiffusivity() {
		return this.Mdiffusivity;
	}
	this.getMdiffusivity=getMdiffusivity;


	function setMdiffusivity(v){
		this.Mdiffusivity=v;
	}
	this.setMdiffusivity=setMdiffusivity;

	this.Imagefile=null;


	function getImagefile() {
		return this.Imagefile;
	}
	this.getImagefile=getImagefile;


	function setImagefile(v){
		this.Imagefile=v;
	}
	this.setImagefile=setImagefile;

	this.Statsfile=null;


	function getStatsfile() {
		return this.Statsfile;
	}
	this.getStatsfile=getStatsfile;


	function setStatsfile(v){
		this.Statsfile=v;
	}
	this.setStatsfile=setStatsfile;

	this.regions_region_cnda_dtiData_id_fk=null;


	this.getregions_region_cnda_dtiData_id=function() {
		return this.regions_region_cnda_dtiData_id_fk;
	}


	this.setregions_region_cnda_dtiData_id=function(v){
		this.regions_region_cnda_dtiData_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="volumetricRegion"){
				return this.Volumetricregion ;
			} else 
			if(xmlPath.startsWith("volumetricRegion")){
				xmlPath=xmlPath.substring(16);
				if(xmlPath=="")return this.Volumetricregion ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Volumetricregion!=undefined)return this.Volumetricregion.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="mAnisotropy"){
				return this.Manisotropy ;
			} else 
			if(xmlPath=="mDiffusivity"){
				return this.Mdiffusivity ;
			} else 
			if(xmlPath=="imageFile"){
				return this.Imagefile ;
			} else 
			if(xmlPath=="statsFile"){
				return this.Statsfile ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="regions_region_cnda_dtiData_id"){
				return this.regions_region_cnda_dtiData_id_fk ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="volumetricRegion"){
				this.Volumetricregion=value;
			} else 
			if(xmlPath.startsWith("volumetricRegion")){
				xmlPath=xmlPath.substring(16);
				if(xmlPath=="")return this.Volumetricregion ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Volumetricregion!=undefined){
					this.Volumetricregion.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Volumetricregion= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Volumetricregion= instanciateObject("xnat:volumetricRegion");//omUtils.js
						}
						if(options && options.where)this.Volumetricregion.setProperty(options.where.field,options.where.value);
						this.Volumetricregion.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="mAnisotropy"){
				this.Manisotropy=value;
			} else 
			if(xmlPath=="mDiffusivity"){
				this.Mdiffusivity=value;
			} else 
			if(xmlPath=="imageFile"){
				this.Imagefile=value;
			} else 
			if(xmlPath=="statsFile"){
				this.Statsfile=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="regions_region_cnda_dtiData_id"){
				this.regions_region_cnda_dtiData_id_fk=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="mAnisotropy"){
			return "field_data";
		}else if (xmlPath=="mDiffusivity"){
			return "field_data";
		}else if (xmlPath=="imageFile"){
			return "field_data";
		}else if (xmlPath=="statsFile"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:dtiRegion";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:dtiRegion>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.regions_region_cnda_dtiData_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="regions_region_cnda_dtiData_id=\"" + this.regions_region_cnda_dtiData_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		if (this.Imagefile!=null)
			attTxt+=" imageFile=\"" +this.Imagefile +"\"";
		//NOT REQUIRED FIELD

		if (this.Statsfile!=null)
			attTxt+=" statsFile=\"" +this.Statsfile +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Manisotropy!=null){
			xmlTxt+="\n<cnda:mAnisotropy";
			xmlTxt+=">";
			xmlTxt+=this.Manisotropy;
			xmlTxt+="</cnda:mAnisotropy>";
		}
		if (this.Mdiffusivity!=null){
			xmlTxt+="\n<cnda:mDiffusivity";
			xmlTxt+=">";
			xmlTxt+=this.Mdiffusivity;
			xmlTxt+="</cnda:mDiffusivity>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.regions_region_cnda_dtiData_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Manisotropy!=null) return true;
		if (this.Mdiffusivity!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
