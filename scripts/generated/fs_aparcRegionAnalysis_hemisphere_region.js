/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:49 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function fs_aparcRegionAnalysis_hemisphere_region(){
this.xsiType="fs:aparcRegionAnalysis_hemisphere_region";

	this.getSchemaElementName=function(){
		return "aparcRegionAnalysis_hemisphere_region";
	}

	this.getFullSchemaElementName=function(){
		return "fs:aparcRegionAnalysis_hemisphere_region";
	}

	this.Numvert=null;


	function getNumvert() {
		return this.Numvert;
	}
	this.getNumvert=getNumvert;


	function setNumvert(v){
		this.Numvert=v;
	}
	this.setNumvert=setNumvert;

	this.Surfarea=null;


	function getSurfarea() {
		return this.Surfarea;
	}
	this.getSurfarea=getSurfarea;


	function setSurfarea(v){
		this.Surfarea=v;
	}
	this.setSurfarea=setSurfarea;

	this.Grayvol=null;


	function getGrayvol() {
		return this.Grayvol;
	}
	this.getGrayvol=getGrayvol;


	function setGrayvol(v){
		this.Grayvol=v;
	}
	this.setGrayvol=setGrayvol;

	this.Thickavg=null;


	function getThickavg() {
		return this.Thickavg;
	}
	this.getThickavg=getThickavg;


	function setThickavg(v){
		this.Thickavg=v;
	}
	this.setThickavg=setThickavg;

	this.Thickstd=null;


	function getThickstd() {
		return this.Thickstd;
	}
	this.getThickstd=getThickstd;


	function setThickstd(v){
		this.Thickstd=v;
	}
	this.setThickstd=setThickstd;

	this.Meancurv=null;


	function getMeancurv() {
		return this.Meancurv;
	}
	this.getMeancurv=getMeancurv;


	function setMeancurv(v){
		this.Meancurv=v;
	}
	this.setMeancurv=setMeancurv;

	this.Gauscurv=null;


	function getGauscurv() {
		return this.Gauscurv;
	}
	this.getGauscurv=getGauscurv;


	function setGauscurv(v){
		this.Gauscurv=v;
	}
	this.setGauscurv=setGauscurv;

	this.Foldind=null;


	function getFoldind() {
		return this.Foldind;
	}
	this.getFoldind=getFoldind;


	function setFoldind(v){
		this.Foldind=v;
	}
	this.setFoldind=setFoldind;

	this.Curvind=null;


	function getCurvind() {
		return this.Curvind;
	}
	this.getCurvind=getCurvind;


	function setCurvind(v){
		this.Curvind=v;
	}
	this.setCurvind=setCurvind;

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.Hemisphere=null;


	function getHemisphere() {
		return this.Hemisphere;
	}
	this.getHemisphere=getHemisphere;


	function setHemisphere(v){
		this.Hemisphere=v;
	}
	this.setHemisphere=setHemisphere;

	this.FsAparcregionanalysisHemisphereRegionId=null;


	function getFsAparcregionanalysisHemisphereRegionId() {
		return this.FsAparcregionanalysisHemisphereRegionId;
	}
	this.getFsAparcregionanalysisHemisphereRegionId=getFsAparcregionanalysisHemisphereRegionId;


	function setFsAparcregionanalysisHemisphereRegionId(v){
		this.FsAparcregionanalysisHemisphereRegionId=v;
	}
	this.setFsAparcregionanalysisHemisphereRegionId=setFsAparcregionanalysisHemisphereRegionId;

	this.regions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe_fk=null;


	this.getregions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe=function() {
		return this.regions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe_fk;
	}


	this.setregions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe=function(v){
		this.regions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="NumVert"){
				return this.Numvert ;
			} else 
			if(xmlPath=="SurfArea"){
				return this.Surfarea ;
			} else 
			if(xmlPath=="GrayVol"){
				return this.Grayvol ;
			} else 
			if(xmlPath=="ThickAvg"){
				return this.Thickavg ;
			} else 
			if(xmlPath=="ThickStd"){
				return this.Thickstd ;
			} else 
			if(xmlPath=="MeanCurv"){
				return this.Meancurv ;
			} else 
			if(xmlPath=="GausCurv"){
				return this.Gauscurv ;
			} else 
			if(xmlPath=="FoldInd"){
				return this.Foldind ;
			} else 
			if(xmlPath=="CurvInd"){
				return this.Curvind ;
			} else 
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="hemisphere"){
				return this.Hemisphere ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="fs_aparcRegionAnalysis_hemisphere_region_id"){
				return this.FsAparcregionanalysisHemisphereRegionId ;
			} else 
			if(xmlPath=="regions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe"){
				return this.regions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="NumVert"){
				this.Numvert=value;
			} else 
			if(xmlPath=="SurfArea"){
				this.Surfarea=value;
			} else 
			if(xmlPath=="GrayVol"){
				this.Grayvol=value;
			} else 
			if(xmlPath=="ThickAvg"){
				this.Thickavg=value;
			} else 
			if(xmlPath=="ThickStd"){
				this.Thickstd=value;
			} else 
			if(xmlPath=="MeanCurv"){
				this.Meancurv=value;
			} else 
			if(xmlPath=="GausCurv"){
				this.Gauscurv=value;
			} else 
			if(xmlPath=="FoldInd"){
				this.Foldind=value;
			} else 
			if(xmlPath=="CurvInd"){
				this.Curvind=value;
			} else 
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="hemisphere"){
				this.Hemisphere=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="fs_aparcRegionAnalysis_hemisphere_region_id"){
				this.FsAparcregionanalysisHemisphereRegionId=value;
			} else 
			if(xmlPath=="regions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe"){
				this.regions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="NumVert"){
			return "field_data";
		}else if (xmlPath=="SurfArea"){
			return "field_data";
		}else if (xmlPath=="GrayVol"){
			return "field_data";
		}else if (xmlPath=="ThickAvg"){
			return "field_data";
		}else if (xmlPath=="ThickStd"){
			return "field_data";
		}else if (xmlPath=="MeanCurv"){
			return "field_data";
		}else if (xmlPath=="GausCurv"){
			return "field_data";
		}else if (xmlPath=="FoldInd"){
			return "field_data";
		}else if (xmlPath=="CurvInd"){
			return "field_data";
		}else if (xmlPath=="name"){
			return "field_data";
		}else if (xmlPath=="hemisphere"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<fs:aparcRegionAnalysis_hemisphere_region";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</fs:aparcRegionAnalysis_hemisphere_region>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.FsAparcregionanalysisHemisphereRegionId!=null){
				if(hiddenCount++>0)str+=",";
				str+="fs_aparcRegionAnalysis_hemisphere_region_id=\"" + this.FsAparcregionanalysisHemisphereRegionId + "\"";
			}
			if(this.regions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="regions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe=\"" + this.regions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Name!=null)
			attTxt+=" name=\"" +this.Name +"\"";
		else attTxt+=" name=\"\"";//REQUIRED FIELD

		if (this.Hemisphere!=null)
			attTxt+=" hemisphere=\"" +this.Hemisphere +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Numvert!=null){
			xmlTxt+="\n<fs:NumVert";
			xmlTxt+=">";
			xmlTxt+=this.Numvert;
			xmlTxt+="</fs:NumVert>";
		}
		if (this.Surfarea!=null){
			xmlTxt+="\n<fs:SurfArea";
			xmlTxt+=">";
			xmlTxt+=this.Surfarea;
			xmlTxt+="</fs:SurfArea>";
		}
		if (this.Grayvol!=null){
			xmlTxt+="\n<fs:GrayVol";
			xmlTxt+=">";
			xmlTxt+=this.Grayvol;
			xmlTxt+="</fs:GrayVol>";
		}
		if (this.Thickavg!=null){
			xmlTxt+="\n<fs:ThickAvg";
			xmlTxt+=">";
			xmlTxt+=this.Thickavg;
			xmlTxt+="</fs:ThickAvg>";
		}
		if (this.Thickstd!=null){
			xmlTxt+="\n<fs:ThickStd";
			xmlTxt+=">";
			xmlTxt+=this.Thickstd;
			xmlTxt+="</fs:ThickStd>";
		}
		if (this.Meancurv!=null){
			xmlTxt+="\n<fs:MeanCurv";
			xmlTxt+=">";
			xmlTxt+=this.Meancurv;
			xmlTxt+="</fs:MeanCurv>";
		}
		if (this.Gauscurv!=null){
			xmlTxt+="\n<fs:GausCurv";
			xmlTxt+=">";
			xmlTxt+=this.Gauscurv;
			xmlTxt+="</fs:GausCurv>";
		}
		if (this.Foldind!=null){
			xmlTxt+="\n<fs:FoldInd";
			xmlTxt+=">";
			xmlTxt+=this.Foldind;
			xmlTxt+="</fs:FoldInd>";
		}
		if (this.Curvind!=null){
			xmlTxt+="\n<fs:CurvInd";
			xmlTxt+=">";
			xmlTxt+=this.Curvind;
			xmlTxt+="</fs:CurvInd>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.FsAparcregionanalysisHemisphereRegionId!=null) return true;
			if (this.regions_region_fs_aparcRegionAn_fs_aparcregionanalysis_hemisphe_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Numvert!=null) return true;
		if (this.Surfarea!=null) return true;
		if (this.Grayvol!=null) return true;
		if (this.Thickavg!=null) return true;
		if (this.Thickstd!=null) return true;
		if (this.Meancurv!=null) return true;
		if (this.Gauscurv!=null) return true;
		if (this.Foldind!=null) return true;
		if (this.Curvind!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
