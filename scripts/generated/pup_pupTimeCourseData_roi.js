/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:49 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function pup_pupTimeCourseData_roi(){
this.xsiType="pup:pupTimeCourseData_roi";

	this.getSchemaElementName=function(){
		return "pupTimeCourseData_roi";
	}

	this.getFullSchemaElementName=function(){
		return "pup:pupTimeCourseData_roi";
	}

	this.Nvox=null;


	function getNvox() {
		return this.Nvox;
	}
	this.getNvox=getNvox;


	function setNvox(v){
		this.Nvox=v;
	}
	this.setNvox=setNvox;

	this.Bp=null;


	function getBp() {
		return this.Bp;
	}
	this.getBp=getBp;


	function setBp(v){
		this.Bp=v;
	}
	this.setBp=setBp;

	this.R2=null;


	function getR2() {
		return this.R2;
	}
	this.getR2=getR2;


	function setR2(v){
		this.R2=v;
	}
	this.setR2=setR2;

	this.Intc=null;


	function getIntc() {
		return this.Intc;
	}
	this.getIntc=getIntc;


	function setIntc(v){
		this.Intc=v;
	}
	this.setIntc=setIntc;

	this.Suvr=null;


	function getSuvr() {
		return this.Suvr;
	}
	this.getSuvr=getSuvr;


	function setSuvr(v){
		this.Suvr=v;
	}
	this.setSuvr=setSuvr;

	this.BpRsf=null;


	function getBpRsf() {
		return this.BpRsf;
	}
	this.getBpRsf=getBpRsf;


	function setBpRsf(v){
		this.BpRsf=v;
	}
	this.setBpRsf=setBpRsf;

	this.R2Rsf=null;


	function getR2Rsf() {
		return this.R2Rsf;
	}
	this.getR2Rsf=getR2Rsf;


	function setR2Rsf(v){
		this.R2Rsf=v;
	}
	this.setR2Rsf=setR2Rsf;

	this.IntcRsf=null;


	function getIntcRsf() {
		return this.IntcRsf;
	}
	this.getIntcRsf=getIntcRsf;


	function setIntcRsf(v){
		this.IntcRsf=v;
	}
	this.setIntcRsf=setIntcRsf;

	this.SuvrRsf=null;


	function getSuvrRsf() {
		return this.SuvrRsf;
	}
	this.getSuvrRsf=getSuvrRsf;


	function setSuvrRsf(v){
		this.SuvrRsf=v;
	}
	this.setSuvrRsf=setSuvrRsf;

	this.BpPvc2c=null;


	function getBpPvc2c() {
		return this.BpPvc2c;
	}
	this.getBpPvc2c=getBpPvc2c;


	function setBpPvc2c(v){
		this.BpPvc2c=v;
	}
	this.setBpPvc2c=setBpPvc2c;

	this.R2Pvc2c=null;


	function getR2Pvc2c() {
		return this.R2Pvc2c;
	}
	this.getR2Pvc2c=getR2Pvc2c;


	function setR2Pvc2c(v){
		this.R2Pvc2c=v;
	}
	this.setR2Pvc2c=setR2Pvc2c;

	this.IntcPvc2c=null;


	function getIntcPvc2c() {
		return this.IntcPvc2c;
	}
	this.getIntcPvc2c=getIntcPvc2c;


	function setIntcPvc2c(v){
		this.IntcPvc2c=v;
	}
	this.setIntcPvc2c=setIntcPvc2c;

	this.SuvrPvc2c=null;


	function getSuvrPvc2c() {
		return this.SuvrPvc2c;
	}
	this.getSuvrPvc2c=getSuvrPvc2c;


	function setSuvrPvc2c(v){
		this.SuvrPvc2c=v;
	}
	this.setSuvrPvc2c=setSuvrPvc2c;

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.PupPuptimecoursedataRoiId=null;


	function getPupPuptimecoursedataRoiId() {
		return this.PupPuptimecoursedataRoiId;
	}
	this.getPupPuptimecoursedataRoiId=getPupPuptimecoursedataRoiId;


	function setPupPuptimecoursedataRoiId(v){
		this.PupPuptimecoursedataRoiId=v;
	}
	this.setPupPuptimecoursedataRoiId=setPupPuptimecoursedataRoiId;

	this.rois_roi_pup_pupTimeCourseData_id_fk=null;


	this.getrois_roi_pup_pupTimeCourseData_id=function() {
		return this.rois_roi_pup_pupTimeCourseData_id_fk;
	}


	this.setrois_roi_pup_pupTimeCourseData_id=function(v){
		this.rois_roi_pup_pupTimeCourseData_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="NVox"){
				return this.Nvox ;
			} else 
			if(xmlPath=="BP"){
				return this.Bp ;
			} else 
			if(xmlPath=="R2"){
				return this.R2 ;
			} else 
			if(xmlPath=="INTC"){
				return this.Intc ;
			} else 
			if(xmlPath=="SUVR"){
				return this.Suvr ;
			} else 
			if(xmlPath=="BP_RSF"){
				return this.BpRsf ;
			} else 
			if(xmlPath=="R2_RSF"){
				return this.R2Rsf ;
			} else 
			if(xmlPath=="INTC_RSF"){
				return this.IntcRsf ;
			} else 
			if(xmlPath=="SUVR_RSF"){
				return this.SuvrRsf ;
			} else 
			if(xmlPath=="BP_PVC2C"){
				return this.BpPvc2c ;
			} else 
			if(xmlPath=="R2_PVC2C"){
				return this.R2Pvc2c ;
			} else 
			if(xmlPath=="INTC_PVC2C"){
				return this.IntcPvc2c ;
			} else 
			if(xmlPath=="SUVR_PVC2C"){
				return this.SuvrPvc2c ;
			} else 
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="pup_pupTimeCourseData_roi_id"){
				return this.PupPuptimecoursedataRoiId ;
			} else 
			if(xmlPath=="rois_roi_pup_pupTimeCourseData_id"){
				return this.rois_roi_pup_pupTimeCourseData_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="NVox"){
				this.Nvox=value;
			} else 
			if(xmlPath=="BP"){
				this.Bp=value;
			} else 
			if(xmlPath=="R2"){
				this.R2=value;
			} else 
			if(xmlPath=="INTC"){
				this.Intc=value;
			} else 
			if(xmlPath=="SUVR"){
				this.Suvr=value;
			} else 
			if(xmlPath=="BP_RSF"){
				this.BpRsf=value;
			} else 
			if(xmlPath=="R2_RSF"){
				this.R2Rsf=value;
			} else 
			if(xmlPath=="INTC_RSF"){
				this.IntcRsf=value;
			} else 
			if(xmlPath=="SUVR_RSF"){
				this.SuvrRsf=value;
			} else 
			if(xmlPath=="BP_PVC2C"){
				this.BpPvc2c=value;
			} else 
			if(xmlPath=="R2_PVC2C"){
				this.R2Pvc2c=value;
			} else 
			if(xmlPath=="INTC_PVC2C"){
				this.IntcPvc2c=value;
			} else 
			if(xmlPath=="SUVR_PVC2C"){
				this.SuvrPvc2c=value;
			} else 
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="pup_pupTimeCourseData_roi_id"){
				this.PupPuptimecoursedataRoiId=value;
			} else 
			if(xmlPath=="rois_roi_pup_pupTimeCourseData_id"){
				this.rois_roi_pup_pupTimeCourseData_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="NVox"){
			return "field_data";
		}else if (xmlPath=="BP"){
			return "field_data";
		}else if (xmlPath=="R2"){
			return "field_data";
		}else if (xmlPath=="INTC"){
			return "field_data";
		}else if (xmlPath=="SUVR"){
			return "field_data";
		}else if (xmlPath=="BP_RSF"){
			return "field_data";
		}else if (xmlPath=="R2_RSF"){
			return "field_data";
		}else if (xmlPath=="INTC_RSF"){
			return "field_data";
		}else if (xmlPath=="SUVR_RSF"){
			return "field_data";
		}else if (xmlPath=="BP_PVC2C"){
			return "field_data";
		}else if (xmlPath=="R2_PVC2C"){
			return "field_data";
		}else if (xmlPath=="INTC_PVC2C"){
			return "field_data";
		}else if (xmlPath=="SUVR_PVC2C"){
			return "field_data";
		}else if (xmlPath=="name"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<pup:pupTimeCourseData_roi";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</pup:pupTimeCourseData_roi>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.PupPuptimecoursedataRoiId!=null){
				if(hiddenCount++>0)str+=",";
				str+="pup_pupTimeCourseData_roi_id=\"" + this.PupPuptimecoursedataRoiId + "\"";
			}
			if(this.rois_roi_pup_pupTimeCourseData_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="rois_roi_pup_pupTimeCourseData_id=\"" + this.rois_roi_pup_pupTimeCourseData_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Name!=null)
			attTxt+=" name=\"" +this.Name +"\"";
		else attTxt+=" name=\"\"";//REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Nvox!=null){
			xmlTxt+="\n<pup:NVox";
			xmlTxt+=">";
			xmlTxt+=this.Nvox;
			xmlTxt+="</pup:NVox>";
		}
		if (this.Bp!=null){
			xmlTxt+="\n<pup:BP";
			xmlTxt+=">";
			xmlTxt+=this.Bp;
			xmlTxt+="</pup:BP>";
		}
		if (this.R2!=null){
			xmlTxt+="\n<pup:R2";
			xmlTxt+=">";
			xmlTxt+=this.R2;
			xmlTxt+="</pup:R2>";
		}
		if (this.Intc!=null){
			xmlTxt+="\n<pup:INTC";
			xmlTxt+=">";
			xmlTxt+=this.Intc;
			xmlTxt+="</pup:INTC>";
		}
		if (this.Suvr!=null){
			xmlTxt+="\n<pup:SUVR";
			xmlTxt+=">";
			xmlTxt+=this.Suvr;
			xmlTxt+="</pup:SUVR>";
		}
		if (this.BpRsf!=null){
			xmlTxt+="\n<pup:BP_RSF";
			xmlTxt+=">";
			xmlTxt+=this.BpRsf;
			xmlTxt+="</pup:BP_RSF>";
		}
		if (this.R2Rsf!=null){
			xmlTxt+="\n<pup:R2_RSF";
			xmlTxt+=">";
			xmlTxt+=this.R2Rsf;
			xmlTxt+="</pup:R2_RSF>";
		}
		if (this.IntcRsf!=null){
			xmlTxt+="\n<pup:INTC_RSF";
			xmlTxt+=">";
			xmlTxt+=this.IntcRsf;
			xmlTxt+="</pup:INTC_RSF>";
		}
		if (this.SuvrRsf!=null){
			xmlTxt+="\n<pup:SUVR_RSF";
			xmlTxt+=">";
			xmlTxt+=this.SuvrRsf;
			xmlTxt+="</pup:SUVR_RSF>";
		}
		if (this.BpPvc2c!=null){
			xmlTxt+="\n<pup:BP_PVC2C";
			xmlTxt+=">";
			xmlTxt+=this.BpPvc2c;
			xmlTxt+="</pup:BP_PVC2C>";
		}
		if (this.R2Pvc2c!=null){
			xmlTxt+="\n<pup:R2_PVC2C";
			xmlTxt+=">";
			xmlTxt+=this.R2Pvc2c;
			xmlTxt+="</pup:R2_PVC2C>";
		}
		if (this.IntcPvc2c!=null){
			xmlTxt+="\n<pup:INTC_PVC2C";
			xmlTxt+=">";
			xmlTxt+=this.IntcPvc2c;
			xmlTxt+="</pup:INTC_PVC2C>";
		}
		if (this.SuvrPvc2c!=null){
			xmlTxt+="\n<pup:SUVR_PVC2C";
			xmlTxt+=">";
			xmlTxt+=this.SuvrPvc2c;
			xmlTxt+="</pup:SUVR_PVC2C>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.PupPuptimecoursedataRoiId!=null) return true;
			if (this.rois_roi_pup_pupTimeCourseData_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Nvox!=null) return true;
		if (this.Bp!=null) return true;
		if (this.R2!=null) return true;
		if (this.Intc!=null) return true;
		if (this.Suvr!=null) return true;
		if (this.BpRsf!=null) return true;
		if (this.R2Rsf!=null) return true;
		if (this.IntcRsf!=null) return true;
		if (this.SuvrRsf!=null) return true;
		if (this.BpPvc2c!=null) return true;
		if (this.R2Pvc2c!=null) return true;
		if (this.IntcPvc2c!=null) return true;
		if (this.SuvrPvc2c!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
