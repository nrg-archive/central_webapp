/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_vitalsData_pulse(){
this.xsiType="cnda:vitalsData_pulse";

	this.getSchemaElementName=function(){
		return "vitalsData_pulse";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:vitalsData_pulse";
	}

	this.Pulse=null;


	function getPulse() {
		return this.Pulse;
	}
	this.getPulse=getPulse;


	function setPulse(v){
		this.Pulse=v;
	}
	this.setPulse=setPulse;

	this.Condition=null;


	function getCondition() {
		return this.Condition;
	}
	this.getCondition=getCondition;


	function setCondition(v){
		this.Condition=v;
	}
	this.setCondition=setCondition;

	this.CndaVitalsdataPulseId=null;


	function getCndaVitalsdataPulseId() {
		return this.CndaVitalsdataPulseId;
	}
	this.getCndaVitalsdataPulseId=getCndaVitalsdataPulseId;


	function setCndaVitalsdataPulseId(v){
		this.CndaVitalsdataPulseId=v;
	}
	this.setCndaVitalsdataPulseId=setCndaVitalsdataPulseId;

	this.cnda_vitalsData_id_fk=null;


	this.getcnda_vitalsData_id=function() {
		return this.cnda_vitalsData_id_fk;
	}


	this.setcnda_vitalsData_id=function(v){
		this.cnda_vitalsData_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="pulse"){
				return this.Pulse ;
			} else 
			if(xmlPath=="condition"){
				return this.Condition ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="cnda_vitalsData_pulse_id"){
				return this.CndaVitalsdataPulseId ;
			} else 
			if(xmlPath=="cnda_vitalsData_id"){
				return this.cnda_vitalsData_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="pulse"){
				this.Pulse=value;
			} else 
			if(xmlPath=="condition"){
				this.Condition=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="cnda_vitalsData_pulse_id"){
				this.CndaVitalsdataPulseId=value;
			} else 
			if(xmlPath=="cnda_vitalsData_id"){
				this.cnda_vitalsData_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="pulse"){
			return "field_data";
		}else if (xmlPath=="condition"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:vitalsData_pulse";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:vitalsData_pulse>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.CndaVitalsdataPulseId!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_vitalsData_pulse_id=\"" + this.CndaVitalsdataPulseId + "\"";
			}
			if(this.cnda_vitalsData_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_vitalsData_id=\"" + this.cnda_vitalsData_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Condition!=null)
			attTxt+=" condition=\"" +this.Condition +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Pulse!=null){
			xmlTxt+=this.Pulse;
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.CndaVitalsdataPulseId!=null) return true;
			if (this.cnda_vitalsData_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Pulse!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
