/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_modifiedScheltensPvRegion(){
this.xsiType="cnda:modifiedScheltensPvRegion";

	this.getSchemaElementName=function(){
		return "modifiedScheltensPvRegion";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:modifiedScheltensPvRegion";
	}

	this.Left=null;


	function getLeft() {
		return this.Left;
	}
	this.getLeft=getLeft;


	function setLeft(v){
		this.Left=v;
	}
	this.setLeft=setLeft;

	this.Right=null;


	function getRight() {
		return this.Right;
	}
	this.getRight=getRight;


	function setRight(v){
		this.Right=v;
	}
	this.setRight=setRight;

	this.Scheltens=null;


	function getScheltens() {
		return this.Scheltens;
	}
	this.getScheltens=getScheltens;


	function setScheltens(v){
		this.Scheltens=v;
	}
	this.setScheltens=setScheltens;

	this.CndaModifiedscheltenspvregionId=null;


	function getCndaModifiedscheltenspvregionId() {
		return this.CndaModifiedscheltenspvregionId;
	}
	this.getCndaModifiedscheltenspvregionId=getCndaModifiedscheltenspvregionId;


	function setCndaModifiedscheltenspvregionId(v){
		this.CndaModifiedscheltenspvregionId=v;
	}
	this.setCndaModifiedscheltenspvregionId=setCndaModifiedscheltenspvregionId;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="left"){
				return this.Left ;
			} else 
			if(xmlPath=="right"){
				return this.Right ;
			} else 
			if(xmlPath=="scheltens"){
				return this.Scheltens ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="cnda_modifiedScheltensPvRegion_id"){
				return this.CndaModifiedscheltenspvregionId ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="left"){
				this.Left=value;
			} else 
			if(xmlPath=="right"){
				this.Right=value;
			} else 
			if(xmlPath=="scheltens"){
				this.Scheltens=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="cnda_modifiedScheltensPvRegion_id"){
				this.CndaModifiedscheltenspvregionId=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="left"){
			return "field_data";
		}else if (xmlPath=="right"){
			return "field_data";
		}else if (xmlPath=="scheltens"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:modifiedScheltensPvRegion";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:modifiedScheltensPvRegion>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.CndaModifiedscheltenspvregionId!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_modifiedScheltensPvRegion_id=\"" + this.CndaModifiedscheltenspvregionId + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Scheltens!=null)
			attTxt+=" scheltens=\"" +this.Scheltens +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Left!=null){
			xmlTxt+="\n<cnda:left";
			xmlTxt+=">";
			xmlTxt+=this.Left;
			xmlTxt+="</cnda:left>";
		}
		else{
			xmlTxt+="\n<cnda:left";
			xmlTxt+="/>";
		}

		if (this.Right!=null){
			xmlTxt+="\n<cnda:right";
			xmlTxt+=">";
			xmlTxt+=this.Right;
			xmlTxt+="</cnda:right>";
		}
		else{
			xmlTxt+="\n<cnda:right";
			xmlTxt+="/>";
		}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.CndaModifiedscheltenspvregionId!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Left!=null) return true;
		return true;//REQUIRED left
	}
}
