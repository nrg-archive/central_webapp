/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:49 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function fs_longFSData_timepoint(){
this.xsiType="fs:longFSData_timepoint";

	this.getSchemaElementName=function(){
		return "longFSData_timepoint";
	}

	this.getFullSchemaElementName=function(){
		return "fs:longFSData_timepoint";
	}

	this.Imagesessionid=null;


	function getImagesessionid() {
		return this.Imagesessionid;
	}
	this.getImagesessionid=getImagesessionid;


	function setImagesessionid(v){
		this.Imagesessionid=v;
	}
	this.setImagesessionid=setImagesessionid;

	this.Label=null;


	function getLabel() {
		return this.Label;
	}
	this.getLabel=getLabel;


	function setLabel(v){
		this.Label=v;
	}
	this.setLabel=setLabel;

	this.VisitId=null;


	function getVisitId() {
		return this.VisitId;
	}
	this.getVisitId=getVisitId;


	function setVisitId(v){
		this.VisitId=v;
	}
	this.setVisitId=setVisitId;

	this.Project=null;


	function getProject() {
		return this.Project;
	}
	this.getProject=getProject;


	function setProject(v){
		this.Project=v;
	}
	this.setProject=setProject;

	this.FsLongfsdataTimepointId=null;


	function getFsLongfsdataTimepointId() {
		return this.FsLongfsdataTimepointId;
	}
	this.getFsLongfsdataTimepointId=getFsLongfsdataTimepointId;


	function setFsLongfsdataTimepointId(v){
		this.FsLongfsdataTimepointId=v;
	}
	this.setFsLongfsdataTimepointId=setFsLongfsdataTimepointId;

	this.timepoints_timepoint_fs_longFSD_id_fk=null;


	this.gettimepoints_timepoint_fs_longFSD_id=function() {
		return this.timepoints_timepoint_fs_longFSD_id_fk;
	}


	this.settimepoints_timepoint_fs_longFSD_id=function(v){
		this.timepoints_timepoint_fs_longFSD_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="imageSessionID"){
				return this.Imagesessionid ;
			} else 
			if(xmlPath=="label"){
				return this.Label ;
			} else 
			if(xmlPath=="visit_id"){
				return this.VisitId ;
			} else 
			if(xmlPath=="project"){
				return this.Project ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="fs_longFSData_timepoint_id"){
				return this.FsLongfsdataTimepointId ;
			} else 
			if(xmlPath=="timepoints_timepoint_fs_longFSD_id"){
				return this.timepoints_timepoint_fs_longFSD_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="imageSessionID"){
				this.Imagesessionid=value;
			} else 
			if(xmlPath=="label"){
				this.Label=value;
			} else 
			if(xmlPath=="visit_id"){
				this.VisitId=value;
			} else 
			if(xmlPath=="project"){
				this.Project=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="fs_longFSData_timepoint_id"){
				this.FsLongfsdataTimepointId=value;
			} else 
			if(xmlPath=="timepoints_timepoint_fs_longFSD_id"){
				this.timepoints_timepoint_fs_longFSD_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="imageSessionID"){
			return "field_data";
		}else if (xmlPath=="label"){
			return "field_data";
		}else if (xmlPath=="visit_id"){
			return "field_data";
		}else if (xmlPath=="project"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<fs:longFSData_timepoint";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</fs:longFSData_timepoint>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.FsLongfsdataTimepointId!=null){
				if(hiddenCount++>0)str+=",";
				str+="fs_longFSData_timepoint_id=\"" + this.FsLongfsdataTimepointId + "\"";
			}
			if(this.timepoints_timepoint_fs_longFSD_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="timepoints_timepoint_fs_longFSD_id=\"" + this.timepoints_timepoint_fs_longFSD_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Imagesessionid!=null)
			attTxt+=" imageSessionID=\"" +this.Imagesessionid +"\"";
		else attTxt+=" imageSessionID=\"\"";//REQUIRED FIELD

		if (this.Label!=null)
			attTxt+=" label=\"" +this.Label +"\"";
		//NOT REQUIRED FIELD

		if (this.VisitId!=null)
			attTxt+=" visit_id=\"" +this.VisitId +"\"";
		//NOT REQUIRED FIELD

		if (this.Project!=null)
			attTxt+=" project=\"" +this.Project +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.FsLongfsdataTimepointId!=null) return true;
			if (this.timepoints_timepoint_fs_longFSD_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if(this.hasXMLComments())return true;
		return false;
	}
}
