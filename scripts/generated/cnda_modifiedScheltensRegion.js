/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_modifiedScheltensRegion(){
this.xsiType="cnda:modifiedScheltensRegion";

	this.getSchemaElementName=function(){
		return "modifiedScheltensRegion";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:modifiedScheltensRegion";
	}

	this.Left_lesions_small=null;


	function getLeft_lesions_small() {
		return this.Left_lesions_small;
	}
	this.getLeft_lesions_small=getLeft_lesions_small;


	function setLeft_lesions_small(v){
		this.Left_lesions_small=v;
	}
	this.setLeft_lesions_small=setLeft_lesions_small;

	this.Left_lesions_medium=null;


	function getLeft_lesions_medium() {
		return this.Left_lesions_medium;
	}
	this.getLeft_lesions_medium=getLeft_lesions_medium;


	function setLeft_lesions_medium(v){
		this.Left_lesions_medium=v;
	}
	this.setLeft_lesions_medium=setLeft_lesions_medium;

	this.Left_lesions_large=null;


	function getLeft_lesions_large() {
		return this.Left_lesions_large;
	}
	this.getLeft_lesions_large=getLeft_lesions_large;


	function setLeft_lesions_large(v){
		this.Left_lesions_large=v;
	}
	this.setLeft_lesions_large=setLeft_lesions_large;

	this.Left_infarcts_medium=null;


	function getLeft_infarcts_medium() {
		return this.Left_infarcts_medium;
	}
	this.getLeft_infarcts_medium=getLeft_infarcts_medium;


	function setLeft_infarcts_medium(v){
		this.Left_infarcts_medium=v;
	}
	this.setLeft_infarcts_medium=setLeft_infarcts_medium;

	this.Left_infarcts_large=null;


	function getLeft_infarcts_large() {
		return this.Left_infarcts_large;
	}
	this.getLeft_infarcts_large=getLeft_infarcts_large;


	function setLeft_infarcts_large(v){
		this.Left_infarcts_large=v;
	}
	this.setLeft_infarcts_large=setLeft_infarcts_large;

	this.Left_volume=null;


	function getLeft_volume() {
		return this.Left_volume;
	}
	this.getLeft_volume=getLeft_volume;


	function setLeft_volume(v){
		this.Left_volume=v;
	}
	this.setLeft_volume=setLeft_volume;

	this.Left_confluent=null;


	function getLeft_confluent() {
		return this.Left_confluent;
	}
	this.getLeft_confluent=getLeft_confluent;


	function setLeft_confluent(v){
		this.Left_confluent=v;
	}
	this.setLeft_confluent=setLeft_confluent;


	this.isLeft_confluent=function(defaultValue) {
		if(this.Left_confluent==null)return defaultValue;
		if(this.Left_confluent=="1" || this.Left_confluent==true)return true;
		return false;
	}

	this.Right_lesions_small=null;


	function getRight_lesions_small() {
		return this.Right_lesions_small;
	}
	this.getRight_lesions_small=getRight_lesions_small;


	function setRight_lesions_small(v){
		this.Right_lesions_small=v;
	}
	this.setRight_lesions_small=setRight_lesions_small;

	this.Right_lesions_medium=null;


	function getRight_lesions_medium() {
		return this.Right_lesions_medium;
	}
	this.getRight_lesions_medium=getRight_lesions_medium;


	function setRight_lesions_medium(v){
		this.Right_lesions_medium=v;
	}
	this.setRight_lesions_medium=setRight_lesions_medium;

	this.Right_lesions_large=null;


	function getRight_lesions_large() {
		return this.Right_lesions_large;
	}
	this.getRight_lesions_large=getRight_lesions_large;


	function setRight_lesions_large(v){
		this.Right_lesions_large=v;
	}
	this.setRight_lesions_large=setRight_lesions_large;

	this.Right_infarcts_medium=null;


	function getRight_infarcts_medium() {
		return this.Right_infarcts_medium;
	}
	this.getRight_infarcts_medium=getRight_infarcts_medium;


	function setRight_infarcts_medium(v){
		this.Right_infarcts_medium=v;
	}
	this.setRight_infarcts_medium=setRight_infarcts_medium;

	this.Right_infarcts_large=null;


	function getRight_infarcts_large() {
		return this.Right_infarcts_large;
	}
	this.getRight_infarcts_large=getRight_infarcts_large;


	function setRight_infarcts_large(v){
		this.Right_infarcts_large=v;
	}
	this.setRight_infarcts_large=setRight_infarcts_large;

	this.Right_volume=null;


	function getRight_volume() {
		return this.Right_volume;
	}
	this.getRight_volume=getRight_volume;


	function setRight_volume(v){
		this.Right_volume=v;
	}
	this.setRight_volume=setRight_volume;

	this.Right_confluent=null;


	function getRight_confluent() {
		return this.Right_confluent;
	}
	this.getRight_confluent=getRight_confluent;


	function setRight_confluent(v){
		this.Right_confluent=v;
	}
	this.setRight_confluent=setRight_confluent;


	this.isRight_confluent=function(defaultValue) {
		if(this.Right_confluent==null)return defaultValue;
		if(this.Right_confluent=="1" || this.Right_confluent==true)return true;
		return false;
	}

	this.Scheltens=null;


	function getScheltens() {
		return this.Scheltens;
	}
	this.getScheltens=getScheltens;


	function setScheltens(v){
		this.Scheltens=v;
	}
	this.setScheltens=setScheltens;

	this.CndaModifiedscheltensregionId=null;


	function getCndaModifiedscheltensregionId() {
		return this.CndaModifiedscheltensregionId;
	}
	this.getCndaModifiedscheltensregionId=getCndaModifiedscheltensregionId;


	function setCndaModifiedscheltensregionId(v){
		this.CndaModifiedscheltensregionId=v;
	}
	this.setCndaModifiedscheltensregionId=setCndaModifiedscheltensregionId;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="left/lesions/small"){
				return this.Left_lesions_small ;
			} else 
			if(xmlPath=="left/lesions/medium"){
				return this.Left_lesions_medium ;
			} else 
			if(xmlPath=="left/lesions/large"){
				return this.Left_lesions_large ;
			} else 
			if(xmlPath=="left/infarcts/medium"){
				return this.Left_infarcts_medium ;
			} else 
			if(xmlPath=="left/infarcts/large"){
				return this.Left_infarcts_large ;
			} else 
			if(xmlPath=="left/volume"){
				return this.Left_volume ;
			} else 
			if(xmlPath=="left/confluent"){
				return this.Left_confluent ;
			} else 
			if(xmlPath=="right/lesions/small"){
				return this.Right_lesions_small ;
			} else 
			if(xmlPath=="right/lesions/medium"){
				return this.Right_lesions_medium ;
			} else 
			if(xmlPath=="right/lesions/large"){
				return this.Right_lesions_large ;
			} else 
			if(xmlPath=="right/infarcts/medium"){
				return this.Right_infarcts_medium ;
			} else 
			if(xmlPath=="right/infarcts/large"){
				return this.Right_infarcts_large ;
			} else 
			if(xmlPath=="right/volume"){
				return this.Right_volume ;
			} else 
			if(xmlPath=="right/confluent"){
				return this.Right_confluent ;
			} else 
			if(xmlPath=="scheltens"){
				return this.Scheltens ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="cnda_modifiedScheltensRegion_id"){
				return this.CndaModifiedscheltensregionId ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="left/lesions/small"){
				this.Left_lesions_small=value;
			} else 
			if(xmlPath=="left/lesions/medium"){
				this.Left_lesions_medium=value;
			} else 
			if(xmlPath=="left/lesions/large"){
				this.Left_lesions_large=value;
			} else 
			if(xmlPath=="left/infarcts/medium"){
				this.Left_infarcts_medium=value;
			} else 
			if(xmlPath=="left/infarcts/large"){
				this.Left_infarcts_large=value;
			} else 
			if(xmlPath=="left/volume"){
				this.Left_volume=value;
			} else 
			if(xmlPath=="left/confluent"){
				this.Left_confluent=value;
			} else 
			if(xmlPath=="right/lesions/small"){
				this.Right_lesions_small=value;
			} else 
			if(xmlPath=="right/lesions/medium"){
				this.Right_lesions_medium=value;
			} else 
			if(xmlPath=="right/lesions/large"){
				this.Right_lesions_large=value;
			} else 
			if(xmlPath=="right/infarcts/medium"){
				this.Right_infarcts_medium=value;
			} else 
			if(xmlPath=="right/infarcts/large"){
				this.Right_infarcts_large=value;
			} else 
			if(xmlPath=="right/volume"){
				this.Right_volume=value;
			} else 
			if(xmlPath=="right/confluent"){
				this.Right_confluent=value;
			} else 
			if(xmlPath=="scheltens"){
				this.Scheltens=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="cnda_modifiedScheltensRegion_id"){
				this.CndaModifiedscheltensregionId=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="left/lesions/small"){
			return "field_data";
		}else if (xmlPath=="left/lesions/medium"){
			return "field_data";
		}else if (xmlPath=="left/lesions/large"){
			return "field_data";
		}else if (xmlPath=="left/infarcts/medium"){
			return "field_data";
		}else if (xmlPath=="left/infarcts/large"){
			return "field_data";
		}else if (xmlPath=="left/volume"){
			return "field_data";
		}else if (xmlPath=="left/confluent"){
			return "field_data";
		}else if (xmlPath=="right/lesions/small"){
			return "field_data";
		}else if (xmlPath=="right/lesions/medium"){
			return "field_data";
		}else if (xmlPath=="right/lesions/large"){
			return "field_data";
		}else if (xmlPath=="right/infarcts/medium"){
			return "field_data";
		}else if (xmlPath=="right/infarcts/large"){
			return "field_data";
		}else if (xmlPath=="right/volume"){
			return "field_data";
		}else if (xmlPath=="right/confluent"){
			return "field_data";
		}else if (xmlPath=="scheltens"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:modifiedScheltensRegion";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:modifiedScheltensRegion>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.CndaModifiedscheltensregionId!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_modifiedScheltensRegion_id=\"" + this.CndaModifiedscheltensregionId + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Scheltens!=null)
			attTxt+=" scheltens=\"" +this.Scheltens +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		var LeftATT = ""
		if (this.Left_volume!=null)
			LeftATT+=" volume=\"" + this.Left_volume + "\"";
		if (this.Left_confluent!=null)
			LeftATT+=" confluent=\"" + this.Left_confluent + "\"";
			var child0=0;
			var att0=0;
			if(this.Left_lesions_small!=null)
			child0++;
			if(this.Left_lesions_large!=null)
			child0++;
			if(this.Left_volume!=null)
			att0++;
			if(this.Left_infarcts_medium!=null)
			child0++;
			if(this.Left_lesions_medium!=null)
			child0++;
			if(this.Left_confluent!=null)
			att0++;
			if(this.Left_infarcts_large!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<cnda:left";
				xmlTxt+=LeftATT;
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child1=0;
			var att1=0;
			if(this.Left_lesions_large!=null)
			child1++;
			if(this.Left_lesions_small!=null)
			child1++;
			if(this.Left_lesions_medium!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<cnda:lesions";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Left_lesions_small!=null){
			xmlTxt+="\n<cnda:small";
			xmlTxt+=">";
			xmlTxt+=this.Left_lesions_small;
			xmlTxt+="</cnda:small>";
		}
		else{
			xmlTxt+="\n<cnda:small";
			xmlTxt+="/>";
		}

		if (this.Left_lesions_medium!=null){
			xmlTxt+="\n<cnda:medium";
			xmlTxt+=">";
			xmlTxt+=this.Left_lesions_medium;
			xmlTxt+="</cnda:medium>";
		}
		else{
			xmlTxt+="\n<cnda:medium";
			xmlTxt+="/>";
		}

		if (this.Left_lesions_large!=null){
			xmlTxt+="\n<cnda:large";
			xmlTxt+=">";
			xmlTxt+=this.Left_lesions_large;
			xmlTxt+="</cnda:large>";
		}
		else{
			xmlTxt+="\n<cnda:large";
			xmlTxt+="/>";
		}

				xmlTxt+="\n</cnda:lesions>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Left_infarcts_medium!=null)
			child2++;
			if(this.Left_infarcts_large!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<cnda:infarcts";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Left_infarcts_medium!=null){
			xmlTxt+="\n<cnda:medium";
			xmlTxt+=">";
			xmlTxt+=this.Left_infarcts_medium;
			xmlTxt+="</cnda:medium>";
		}
		else{
			xmlTxt+="\n<cnda:medium";
			xmlTxt+="/>";
		}

		if (this.Left_infarcts_large!=null){
			xmlTxt+="\n<cnda:large";
			xmlTxt+=">";
			xmlTxt+=this.Left_infarcts_large;
			xmlTxt+="</cnda:large>";
		}
		else{
			xmlTxt+="\n<cnda:large";
			xmlTxt+="/>";
		}

				xmlTxt+="\n</cnda:infarcts>";
			}
			}

				xmlTxt+="\n</cnda:left>";
			}
			}

		var RightATT = ""
		if (this.Right_volume!=null)
			RightATT+=" volume=\"" + this.Right_volume + "\"";
		if (this.Right_confluent!=null)
			RightATT+=" confluent=\"" + this.Right_confluent + "\"";
			var child3=0;
			var att3=0;
			if(this.Right_lesions_medium!=null)
			child3++;
			if(this.Right_infarcts_large!=null)
			child3++;
			if(this.Right_confluent!=null)
			att3++;
			if(this.Right_volume!=null)
			att3++;
			if(this.Right_infarcts_medium!=null)
			child3++;
			if(this.Right_lesions_small!=null)
			child3++;
			if(this.Right_lesions_large!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<cnda:right";
				xmlTxt+=RightATT;
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child4=0;
			var att4=0;
			if(this.Right_lesions_medium!=null)
			child4++;
			if(this.Right_lesions_large!=null)
			child4++;
			if(this.Right_lesions_small!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<cnda:lesions";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Right_lesions_small!=null){
			xmlTxt+="\n<cnda:small";
			xmlTxt+=">";
			xmlTxt+=this.Right_lesions_small;
			xmlTxt+="</cnda:small>";
		}
		else{
			xmlTxt+="\n<cnda:small";
			xmlTxt+="/>";
		}

		if (this.Right_lesions_medium!=null){
			xmlTxt+="\n<cnda:medium";
			xmlTxt+=">";
			xmlTxt+=this.Right_lesions_medium;
			xmlTxt+="</cnda:medium>";
		}
		else{
			xmlTxt+="\n<cnda:medium";
			xmlTxt+="/>";
		}

		if (this.Right_lesions_large!=null){
			xmlTxt+="\n<cnda:large";
			xmlTxt+=">";
			xmlTxt+=this.Right_lesions_large;
			xmlTxt+="</cnda:large>";
		}
		else{
			xmlTxt+="\n<cnda:large";
			xmlTxt+="/>";
		}

				xmlTxt+="\n</cnda:lesions>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Right_infarcts_large!=null)
			child5++;
			if(this.Right_infarcts_medium!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<cnda:infarcts";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Right_infarcts_medium!=null){
			xmlTxt+="\n<cnda:medium";
			xmlTxt+=">";
			xmlTxt+=this.Right_infarcts_medium;
			xmlTxt+="</cnda:medium>";
		}
		else{
			xmlTxt+="\n<cnda:medium";
			xmlTxt+="/>";
		}

		if (this.Right_infarcts_large!=null){
			xmlTxt+="\n<cnda:large";
			xmlTxt+=">";
			xmlTxt+=this.Right_infarcts_large;
			xmlTxt+="</cnda:large>";
		}
		else{
			xmlTxt+="\n<cnda:large";
			xmlTxt+="/>";
		}

				xmlTxt+="\n</cnda:infarcts>";
			}
			}

				xmlTxt+="\n</cnda:right>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.CndaModifiedscheltensregionId!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Left_volume!=null)
			return true;
		if (this.Left_confluent!=null)
			return true;
			if(this.Left_lesions_small!=null) return true;
			if(this.Left_lesions_large!=null) return true;
			if(this.Left_volume!=null) return true;
			if(this.Left_infarcts_medium!=null) return true;
			if(this.Left_lesions_medium!=null) return true;
			if(this.Left_confluent!=null) return true;
			if(this.Left_infarcts_large!=null) return true;
		if (this.Right_volume!=null)
			return true;
		if (this.Right_confluent!=null)
			return true;
			if(this.Right_lesions_medium!=null) return true;
			if(this.Right_infarcts_large!=null) return true;
			if(this.Right_confluent!=null) return true;
			if(this.Right_volume!=null) return true;
			if(this.Right_infarcts_medium!=null) return true;
			if(this.Right_lesions_small!=null) return true;
			if(this.Right_lesions_large!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
