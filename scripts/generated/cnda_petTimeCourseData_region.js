/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_petTimeCourseData_region(){
this.xsiType="cnda:petTimeCourseData_region";

	this.getSchemaElementName=function(){
		return "petTimeCourseData_region";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:petTimeCourseData_region";
	}
	this.Timeseries_activity =new Array();

	function getTimeseries_activity() {
		return this.Timeseries_activity;
	}
	this.getTimeseries_activity=getTimeseries_activity;


	function addTimeseries_activity(v){
		this.Timeseries_activity.push(v);
	}
	this.addTimeseries_activity=addTimeseries_activity;

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.Hemisphere=null;


	function getHemisphere() {
		return this.Hemisphere;
	}
	this.getHemisphere=getHemisphere;


	function setHemisphere(v){
		this.Hemisphere=v;
	}
	this.setHemisphere=setHemisphere;

	this.CndaPettimecoursedataRegionId=null;


	function getCndaPettimecoursedataRegionId() {
		return this.CndaPettimecoursedataRegionId;
	}
	this.getCndaPettimecoursedataRegionId=getCndaPettimecoursedataRegionId;


	function setCndaPettimecoursedataRegionId(v){
		this.CndaPettimecoursedataRegionId=v;
	}
	this.setCndaPettimecoursedataRegionId=setCndaPettimecoursedataRegionId;

	this.regions_region_cnda_petTimeCour_id_fk=null;


	this.getregions_region_cnda_petTimeCour_id=function() {
		return this.regions_region_cnda_petTimeCour_id_fk;
	}


	this.setregions_region_cnda_petTimeCour_id=function(v){
		this.regions_region_cnda_petTimeCour_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="timeSeries/activity"){
				return this.Timeseries_activity ;
			} else 
			if(xmlPath.startsWith("timeSeries/activity")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Timeseries_activity ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Timeseries_activity.length;whereCount++){

					var tempValue=this.Timeseries_activity[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Timeseries_activity[whereCount]);

					}

				}
				}else{

				whereArray=this.Timeseries_activity;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="hemisphere"){
				return this.Hemisphere ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="cnda_petTimeCourseData_region_id"){
				return this.CndaPettimecoursedataRegionId ;
			} else 
			if(xmlPath=="regions_region_cnda_petTimeCour_id"){
				return this.regions_region_cnda_petTimeCour_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="timeSeries/activity"){
				this.Timeseries_activity=value;
			} else 
			if(xmlPath.startsWith("timeSeries/activity")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Timeseries_activity ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Timeseries_activity.length;whereCount++){

					var tempValue=this.Timeseries_activity[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Timeseries_activity[whereCount]);

					}

				}
				}else{

				whereArray=this.Timeseries_activity;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("cnda:petTimeCourseData_region_activity");//omUtils.js
					}
					this.addTimeseries_activity(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="hemisphere"){
				this.Hemisphere=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="cnda_petTimeCourseData_region_id"){
				this.CndaPettimecoursedataRegionId=value;
			} else 
			if(xmlPath=="regions_region_cnda_petTimeCour_id"){
				this.regions_region_cnda_petTimeCour_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="timeSeries/activity"){
			this.addTimeseries_activity(v);
		}
		else{
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="timeSeries/activity"){
			return "http://nrg.wustl.edu/cnda:petTimeCourseData_region_activity";
		}
		else{
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="timeSeries/activity"){
			return "field_NO_CHILD";
		}else if (xmlPath=="name"){
			return "field_data";
		}else if (xmlPath=="hemisphere"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:petTimeCourseData_region";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:petTimeCourseData_region>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.CndaPettimecoursedataRegionId!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_petTimeCourseData_region_id=\"" + this.CndaPettimecoursedataRegionId + "\"";
			}
			if(this.regions_region_cnda_petTimeCour_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="regions_region_cnda_petTimeCour_id=\"" + this.regions_region_cnda_petTimeCour_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Name!=null)
			attTxt+=" name=\"" +this.Name +"\"";
		else attTxt+=" name=\"\"";//REQUIRED FIELD

		if (this.Hemisphere!=null)
			attTxt+=" hemisphere=\"" +this.Hemisphere +"\"";
		else attTxt+=" hemisphere=\"\"";//REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
			var child0=0;
			var att0=0;
			child0+=this.Timeseries_activity.length;
			if(child0>0 || att0>0){
				xmlTxt+="\n<cnda:timeSeries";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Timeseries_activityCOUNT=0;Timeseries_activityCOUNT<this.Timeseries_activity.length;Timeseries_activityCOUNT++){
			xmlTxt +="\n<cnda:activity";
			xmlTxt +=this.Timeseries_activity[Timeseries_activityCOUNT].getXMLAtts();
			if(this.Timeseries_activity[Timeseries_activityCOUNT].xsiType!="cnda:petTimeCourseData_region_activity"){
				xmlTxt+=" xsi:type=\"" + this.Timeseries_activity[Timeseries_activityCOUNT].xsiType + "\"";
			}
			if (this.Timeseries_activity[Timeseries_activityCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Timeseries_activity[Timeseries_activityCOUNT].getXMLBody(preventComments);
					xmlTxt+="</cnda:activity>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</cnda:timeSeries>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.CndaPettimecoursedataRegionId!=null) return true;
			if (this.regions_region_cnda_petTimeCour_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
			if(this.Timeseries_activity.length>0)return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
