/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_psychometricsData(){
this.xsiType="cnda:psychometricsData";

	this.getSchemaElementName=function(){
		return "psychometricsData";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:psychometricsData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Wms_logmemory=null;


	function getWms_logmemory() {
		return this.Wms_logmemory;
	}
	this.getWms_logmemory=getWms_logmemory;


	function setWms_logmemory(v){
		this.Wms_logmemory=v;
	}
	this.setWms_logmemory=setWms_logmemory;

	this.Wms_logmemoryrecall=null;


	function getWms_logmemoryrecall() {
		return this.Wms_logmemoryrecall;
	}
	this.getWms_logmemoryrecall=getWms_logmemoryrecall;


	function setWms_logmemoryrecall(v){
		this.Wms_logmemoryrecall=v;
	}
	this.setWms_logmemoryrecall=setWms_logmemoryrecall;

	this.Wms_digitspan=null;


	function getWms_digitspan() {
		return this.Wms_digitspan;
	}
	this.getWms_digitspan=getWms_digitspan;


	function setWms_digitspan(v){
		this.Wms_digitspan=v;
	}
	this.setWms_digitspan=setWms_digitspan;

	this.Wms_assocrecalleasy=null;


	function getWms_assocrecalleasy() {
		return this.Wms_assocrecalleasy;
	}
	this.getWms_assocrecalleasy=getWms_assocrecalleasy;


	function setWms_assocrecalleasy(v){
		this.Wms_assocrecalleasy=v;
	}
	this.setWms_assocrecalleasy=setWms_assocrecalleasy;

	this.Wms_assocrecallhard=null;


	function getWms_assocrecallhard() {
		return this.Wms_assocrecallhard;
	}
	this.getWms_assocrecallhard=getWms_assocrecallhard;


	function setWms_assocrecallhard(v){
		this.Wms_assocrecallhard=v;
	}
	this.setWms_assocrecallhard=setWms_assocrecallhard;

	this.Wms_assocrecogeasy=null;


	function getWms_assocrecogeasy() {
		return this.Wms_assocrecogeasy;
	}
	this.getWms_assocrecogeasy=getWms_assocrecogeasy;


	function setWms_assocrecogeasy(v){
		this.Wms_assocrecogeasy=v;
	}
	this.setWms_assocrecogeasy=setWms_assocrecogeasy;

	this.Wms_assocrecoghard=null;


	function getWms_assocrecoghard() {
		return this.Wms_assocrecoghard;
	}
	this.getWms_assocrecoghard=getWms_assocrecoghard;


	function setWms_assocrecoghard(v){
		this.Wms_assocrecoghard=v;
	}
	this.setWms_assocrecoghard=setWms_assocrecoghard;

	this.Wms_assocmemrecall=null;


	function getWms_assocmemrecall() {
		return this.Wms_assocmemrecall;
	}
	this.getWms_assocmemrecall=getWms_assocmemrecall;


	function setWms_assocmemrecall(v){
		this.Wms_assocmemrecall=v;
	}
	this.setWms_assocmemrecall=setWms_assocmemrecall;

	this.Wms_mentalcontrol=null;


	function getWms_mentalcontrol() {
		return this.Wms_mentalcontrol;
	}
	this.getWms_mentalcontrol=getWms_mentalcontrol;


	function setWms_mentalcontrol(v){
		this.Wms_mentalcontrol=v;
	}
	this.setWms_mentalcontrol=setWms_mentalcontrol;

	this.TrailsA=null;


	function getTrailsA() {
		return this.TrailsA;
	}
	this.getTrailsA=getTrailsA;


	function setTrailsA(v){
		this.TrailsA=v;
	}
	this.setTrailsA=setTrailsA;

	this.TrailsB=null;


	function getTrailsB() {
		return this.TrailsB;
	}
	this.getTrailsB=getTrailsB;


	function setTrailsB(v){
		this.TrailsB=v;
	}
	this.setTrailsB=setTrailsB;

	this.TrailsBc=null;


	function getTrailsBc() {
		return this.TrailsBc;
	}
	this.getTrailsBc=getTrailsBc;


	function setTrailsBc(v){
		this.TrailsBc=v;
	}
	this.setTrailsBc=setTrailsBc;

	this.Wais_info=null;


	function getWais_info() {
		return this.Wais_info;
	}
	this.getWais_info=getWais_info;


	function setWais_info(v){
		this.Wais_info=v;
	}
	this.setWais_info=setWais_info;

	this.Wais_blockdesign=null;


	function getWais_blockdesign() {
		return this.Wais_blockdesign;
	}
	this.getWais_blockdesign=getWais_blockdesign;


	function setWais_blockdesign(v){
		this.Wais_blockdesign=v;
	}
	this.setWais_blockdesign=setWais_blockdesign;

	this.Wais_digitsymbol=null;


	function getWais_digitsymbol() {
		return this.Wais_digitsymbol;
	}
	this.getWais_digitsymbol=getWais_digitsymbol;


	function setWais_digitsymbol(v){
		this.Wais_digitsymbol=v;
	}
	this.setWais_digitsymbol=setWais_digitsymbol;

	this.Bentonformcdelay=null;


	function getBentonformcdelay() {
		return this.Bentonformcdelay;
	}
	this.getBentonformcdelay=getBentonformcdelay;


	function setBentonformcdelay(v){
		this.Bentonformcdelay=v;
	}
	this.setBentonformcdelay=setBentonformcdelay;

	this.Bentonformdcopy=null;


	function getBentonformdcopy() {
		return this.Bentonformdcopy;
	}
	this.getBentonformdcopy=getBentonformdcopy;


	function setBentonformdcopy(v){
		this.Bentonformdcopy=v;
	}
	this.setBentonformdcopy=setBentonformdcopy;

	this.Bostonnaming=null;


	function getBostonnaming() {
		return this.Bostonnaming;
	}
	this.getBostonnaming=getBostonnaming;


	function setBostonnaming(v){
		this.Bostonnaming=v;
	}
	this.setBostonnaming=setBostonnaming;

	this.Crossingoff=null;


	function getCrossingoff() {
		return this.Crossingoff;
	}
	this.getCrossingoff=getCrossingoff;


	function setCrossingoff(v){
		this.Crossingoff=v;
	}
	this.setCrossingoff=setCrossingoff;

	this.Wordfluency=null;


	function getWordfluency() {
		return this.Wordfluency;
	}
	this.getWordfluency=getWordfluency;


	function setWordfluency(v){
		this.Wordfluency=v;
	}
	this.setWordfluency=setWordfluency;

	this.Generalfactor=null;


	function getGeneralfactor() {
		return this.Generalfactor;
	}
	this.getGeneralfactor=getGeneralfactor;


	function setGeneralfactor(v){
		this.Generalfactor=v;
	}
	this.setGeneralfactor=setGeneralfactor;

	this.Kanne_temporal=null;


	function getKanne_temporal() {
		return this.Kanne_temporal;
	}
	this.getKanne_temporal=getKanne_temporal;


	function setKanne_temporal(v){
		this.Kanne_temporal=v;
	}
	this.setKanne_temporal=setKanne_temporal;

	this.Kanne_parietal=null;


	function getKanne_parietal() {
		return this.Kanne_parietal;
	}
	this.getKanne_parietal=getKanne_parietal;


	function setKanne_parietal(v){
		this.Kanne_parietal=v;
	}
	this.setKanne_parietal=setKanne_parietal;

	this.Kanne_frontal=null;


	function getKanne_frontal() {
		return this.Kanne_frontal;
	}
	this.getKanne_frontal=getKanne_frontal;


	function setKanne_frontal(v){
		this.Kanne_frontal=v;
	}
	this.setKanne_frontal=setKanne_frontal;

	this.Animal=null;


	function getAnimal() {
		return this.Animal;
	}
	this.getAnimal=getAnimal;


	function setAnimal(v){
		this.Animal=v;
	}
	this.setAnimal=setAnimal;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="WMS/LogMemory"){
				return this.Wms_logmemory ;
			} else 
			if(xmlPath=="WMS/LogMemoryRecall"){
				return this.Wms_logmemoryrecall ;
			} else 
			if(xmlPath=="WMS/DigitSpan"){
				return this.Wms_digitspan ;
			} else 
			if(xmlPath=="WMS/AssocRecallEasy"){
				return this.Wms_assocrecalleasy ;
			} else 
			if(xmlPath=="WMS/AssocRecallHard"){
				return this.Wms_assocrecallhard ;
			} else 
			if(xmlPath=="WMS/AssocRecogEasy"){
				return this.Wms_assocrecogeasy ;
			} else 
			if(xmlPath=="WMS/AssocRecogHard"){
				return this.Wms_assocrecoghard ;
			} else 
			if(xmlPath=="WMS/AssocMemRecall"){
				return this.Wms_assocmemrecall ;
			} else 
			if(xmlPath=="WMS/MentalControl"){
				return this.Wms_mentalcontrol ;
			} else 
			if(xmlPath=="Trails_A"){
				return this.TrailsA ;
			} else 
			if(xmlPath=="Trails_B"){
				return this.TrailsB ;
			} else 
			if(xmlPath=="Trails_B_C"){
				return this.TrailsBc ;
			} else 
			if(xmlPath=="WAIS/Info"){
				return this.Wais_info ;
			} else 
			if(xmlPath=="WAIS/BlockDesign"){
				return this.Wais_blockdesign ;
			} else 
			if(xmlPath=="WAIS/DigitSymbol"){
				return this.Wais_digitsymbol ;
			} else 
			if(xmlPath=="BentonFormCDelay"){
				return this.Bentonformcdelay ;
			} else 
			if(xmlPath=="BentonFormDCopy"){
				return this.Bentonformdcopy ;
			} else 
			if(xmlPath=="BostonNaming"){
				return this.Bostonnaming ;
			} else 
			if(xmlPath=="CrossingOff"){
				return this.Crossingoff ;
			} else 
			if(xmlPath=="WordFluency"){
				return this.Wordfluency ;
			} else 
			if(xmlPath=="GeneralFactor"){
				return this.Generalfactor ;
			} else 
			if(xmlPath=="Kanne/Temporal"){
				return this.Kanne_temporal ;
			} else 
			if(xmlPath=="Kanne/Parietal"){
				return this.Kanne_parietal ;
			} else 
			if(xmlPath=="Kanne/Frontal"){
				return this.Kanne_frontal ;
			} else 
			if(xmlPath=="Animal"){
				return this.Animal ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="WMS/LogMemory"){
				this.Wms_logmemory=value;
			} else 
			if(xmlPath=="WMS/LogMemoryRecall"){
				this.Wms_logmemoryrecall=value;
			} else 
			if(xmlPath=="WMS/DigitSpan"){
				this.Wms_digitspan=value;
			} else 
			if(xmlPath=="WMS/AssocRecallEasy"){
				this.Wms_assocrecalleasy=value;
			} else 
			if(xmlPath=="WMS/AssocRecallHard"){
				this.Wms_assocrecallhard=value;
			} else 
			if(xmlPath=="WMS/AssocRecogEasy"){
				this.Wms_assocrecogeasy=value;
			} else 
			if(xmlPath=="WMS/AssocRecogHard"){
				this.Wms_assocrecoghard=value;
			} else 
			if(xmlPath=="WMS/AssocMemRecall"){
				this.Wms_assocmemrecall=value;
			} else 
			if(xmlPath=="WMS/MentalControl"){
				this.Wms_mentalcontrol=value;
			} else 
			if(xmlPath=="Trails_A"){
				this.TrailsA=value;
			} else 
			if(xmlPath=="Trails_B"){
				this.TrailsB=value;
			} else 
			if(xmlPath=="Trails_B_C"){
				this.TrailsBc=value;
			} else 
			if(xmlPath=="WAIS/Info"){
				this.Wais_info=value;
			} else 
			if(xmlPath=="WAIS/BlockDesign"){
				this.Wais_blockdesign=value;
			} else 
			if(xmlPath=="WAIS/DigitSymbol"){
				this.Wais_digitsymbol=value;
			} else 
			if(xmlPath=="BentonFormCDelay"){
				this.Bentonformcdelay=value;
			} else 
			if(xmlPath=="BentonFormDCopy"){
				this.Bentonformdcopy=value;
			} else 
			if(xmlPath=="BostonNaming"){
				this.Bostonnaming=value;
			} else 
			if(xmlPath=="CrossingOff"){
				this.Crossingoff=value;
			} else 
			if(xmlPath=="WordFluency"){
				this.Wordfluency=value;
			} else 
			if(xmlPath=="GeneralFactor"){
				this.Generalfactor=value;
			} else 
			if(xmlPath=="Kanne/Temporal"){
				this.Kanne_temporal=value;
			} else 
			if(xmlPath=="Kanne/Parietal"){
				this.Kanne_parietal=value;
			} else 
			if(xmlPath=="Kanne/Frontal"){
				this.Kanne_frontal=value;
			} else 
			if(xmlPath=="Animal"){
				this.Animal=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="WMS/LogMemory"){
			return "field_data";
		}else if (xmlPath=="WMS/LogMemoryRecall"){
			return "field_data";
		}else if (xmlPath=="WMS/DigitSpan"){
			return "field_data";
		}else if (xmlPath=="WMS/AssocRecallEasy"){
			return "field_data";
		}else if (xmlPath=="WMS/AssocRecallHard"){
			return "field_data";
		}else if (xmlPath=="WMS/AssocRecogEasy"){
			return "field_data";
		}else if (xmlPath=="WMS/AssocRecogHard"){
			return "field_data";
		}else if (xmlPath=="WMS/AssocMemRecall"){
			return "field_data";
		}else if (xmlPath=="WMS/MentalControl"){
			return "field_data";
		}else if (xmlPath=="Trails_A"){
			return "field_data";
		}else if (xmlPath=="Trails_B"){
			return "field_data";
		}else if (xmlPath=="Trails_B_C"){
			return "field_data";
		}else if (xmlPath=="WAIS/Info"){
			return "field_data";
		}else if (xmlPath=="WAIS/BlockDesign"){
			return "field_data";
		}else if (xmlPath=="WAIS/DigitSymbol"){
			return "field_data";
		}else if (xmlPath=="BentonFormCDelay"){
			return "field_data";
		}else if (xmlPath=="BentonFormDCopy"){
			return "field_data";
		}else if (xmlPath=="BostonNaming"){
			return "field_data";
		}else if (xmlPath=="CrossingOff"){
			return "field_data";
		}else if (xmlPath=="WordFluency"){
			return "field_data";
		}else if (xmlPath=="GeneralFactor"){
			return "field_data";
		}else if (xmlPath=="Kanne/Temporal"){
			return "field_data";
		}else if (xmlPath=="Kanne/Parietal"){
			return "field_data";
		}else if (xmlPath=="Kanne/Frontal"){
			return "field_data";
		}else if (xmlPath=="Animal"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:Psychometrics";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:Psychometrics>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			if(this.Wms_digitspan!=null)
			child0++;
			if(this.Wms_mentalcontrol!=null)
			child0++;
			if(this.Wms_assocrecoghard!=null)
			child0++;
			if(this.Wms_assocrecallhard!=null)
			child0++;
			if(this.Wms_assocmemrecall!=null)
			child0++;
			if(this.Wms_assocrecogeasy!=null)
			child0++;
			if(this.Wms_logmemory!=null)
			child0++;
			if(this.Wms_assocrecalleasy!=null)
			child0++;
			if(this.Wms_logmemoryrecall!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<cnda:WMS";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Wms_logmemory!=null){
			xmlTxt+="\n<cnda:LogMemory";
			xmlTxt+=">";
			xmlTxt+=this.Wms_logmemory;
			xmlTxt+="</cnda:LogMemory>";
		}
		if (this.Wms_logmemoryrecall!=null){
			xmlTxt+="\n<cnda:LogMemoryRecall";
			xmlTxt+=">";
			xmlTxt+=this.Wms_logmemoryrecall;
			xmlTxt+="</cnda:LogMemoryRecall>";
		}
		if (this.Wms_digitspan!=null){
			xmlTxt+="\n<cnda:DigitSpan";
			xmlTxt+=">";
			xmlTxt+=this.Wms_digitspan;
			xmlTxt+="</cnda:DigitSpan>";
		}
		if (this.Wms_assocrecalleasy!=null){
			xmlTxt+="\n<cnda:AssocRecallEasy";
			xmlTxt+=">";
			xmlTxt+=this.Wms_assocrecalleasy;
			xmlTxt+="</cnda:AssocRecallEasy>";
		}
		if (this.Wms_assocrecallhard!=null){
			xmlTxt+="\n<cnda:AssocRecallHard";
			xmlTxt+=">";
			xmlTxt+=this.Wms_assocrecallhard;
			xmlTxt+="</cnda:AssocRecallHard>";
		}
		if (this.Wms_assocrecogeasy!=null){
			xmlTxt+="\n<cnda:AssocRecogEasy";
			xmlTxt+=">";
			xmlTxt+=this.Wms_assocrecogeasy;
			xmlTxt+="</cnda:AssocRecogEasy>";
		}
		if (this.Wms_assocrecoghard!=null){
			xmlTxt+="\n<cnda:AssocRecogHard";
			xmlTxt+=">";
			xmlTxt+=this.Wms_assocrecoghard;
			xmlTxt+="</cnda:AssocRecogHard>";
		}
		if (this.Wms_assocmemrecall!=null){
			xmlTxt+="\n<cnda:AssocMemRecall";
			xmlTxt+=">";
			xmlTxt+=this.Wms_assocmemrecall;
			xmlTxt+="</cnda:AssocMemRecall>";
		}
		if (this.Wms_mentalcontrol!=null){
			xmlTxt+="\n<cnda:MentalControl";
			xmlTxt+=">";
			xmlTxt+=this.Wms_mentalcontrol;
			xmlTxt+="</cnda:MentalControl>";
		}
				xmlTxt+="\n</cnda:WMS>";
			}
			}

		if (this.TrailsA!=null){
			xmlTxt+="\n<cnda:Trails_A";
			xmlTxt+=">";
			xmlTxt+=this.TrailsA;
			xmlTxt+="</cnda:Trails_A>";
		}
		if (this.TrailsB!=null){
			xmlTxt+="\n<cnda:Trails_B";
			xmlTxt+=">";
			xmlTxt+=this.TrailsB;
			xmlTxt+="</cnda:Trails_B>";
		}
		if (this.TrailsBc!=null){
			xmlTxt+="\n<cnda:Trails_B_C";
			xmlTxt+=">";
			xmlTxt+=this.TrailsBc;
			xmlTxt+="</cnda:Trails_B_C>";
		}
			var child1=0;
			var att1=0;
			if(this.Wais_blockdesign!=null)
			child1++;
			if(this.Wais_digitsymbol!=null)
			child1++;
			if(this.Wais_info!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<cnda:WAIS";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Wais_info!=null){
			xmlTxt+="\n<cnda:Info";
			xmlTxt+=">";
			xmlTxt+=this.Wais_info;
			xmlTxt+="</cnda:Info>";
		}
		if (this.Wais_blockdesign!=null){
			xmlTxt+="\n<cnda:BlockDesign";
			xmlTxt+=">";
			xmlTxt+=this.Wais_blockdesign;
			xmlTxt+="</cnda:BlockDesign>";
		}
		if (this.Wais_digitsymbol!=null){
			xmlTxt+="\n<cnda:DigitSymbol";
			xmlTxt+=">";
			xmlTxt+=this.Wais_digitsymbol;
			xmlTxt+="</cnda:DigitSymbol>";
		}
				xmlTxt+="\n</cnda:WAIS>";
			}
			}

		if (this.Bentonformcdelay!=null){
			xmlTxt+="\n<cnda:BentonFormCDelay";
			xmlTxt+=">";
			xmlTxt+=this.Bentonformcdelay;
			xmlTxt+="</cnda:BentonFormCDelay>";
		}
		if (this.Bentonformdcopy!=null){
			xmlTxt+="\n<cnda:BentonFormDCopy";
			xmlTxt+=">";
			xmlTxt+=this.Bentonformdcopy;
			xmlTxt+="</cnda:BentonFormDCopy>";
		}
		if (this.Bostonnaming!=null){
			xmlTxt+="\n<cnda:BostonNaming";
			xmlTxt+=">";
			xmlTxt+=this.Bostonnaming;
			xmlTxt+="</cnda:BostonNaming>";
		}
		if (this.Crossingoff!=null){
			xmlTxt+="\n<cnda:CrossingOff";
			xmlTxt+=">";
			xmlTxt+=this.Crossingoff;
			xmlTxt+="</cnda:CrossingOff>";
		}
		if (this.Wordfluency!=null){
			xmlTxt+="\n<cnda:WordFluency";
			xmlTxt+=">";
			xmlTxt+=this.Wordfluency;
			xmlTxt+="</cnda:WordFluency>";
		}
		if (this.Generalfactor!=null){
			xmlTxt+="\n<cnda:GeneralFactor";
			xmlTxt+=">";
			xmlTxt+=this.Generalfactor;
			xmlTxt+="</cnda:GeneralFactor>";
		}
			var child2=0;
			var att2=0;
			if(this.Kanne_frontal!=null)
			child2++;
			if(this.Kanne_parietal!=null)
			child2++;
			if(this.Kanne_temporal!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<cnda:Kanne";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Kanne_temporal!=null){
			xmlTxt+="\n<cnda:Temporal";
			xmlTxt+=">";
			xmlTxt+=this.Kanne_temporal;
			xmlTxt+="</cnda:Temporal>";
		}
		if (this.Kanne_parietal!=null){
			xmlTxt+="\n<cnda:Parietal";
			xmlTxt+=">";
			xmlTxt+=this.Kanne_parietal;
			xmlTxt+="</cnda:Parietal>";
		}
		if (this.Kanne_frontal!=null){
			xmlTxt+="\n<cnda:Frontal";
			xmlTxt+=">";
			xmlTxt+=this.Kanne_frontal;
			xmlTxt+="</cnda:Frontal>";
		}
				xmlTxt+="\n</cnda:Kanne>";
			}
			}

		if (this.Animal!=null){
			xmlTxt+="\n<cnda:Animal";
			xmlTxt+=">";
			xmlTxt+=this.Animal;
			xmlTxt+="</cnda:Animal>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.Wms_digitspan!=null) return true;
			if(this.Wms_mentalcontrol!=null) return true;
			if(this.Wms_assocrecoghard!=null) return true;
			if(this.Wms_assocrecallhard!=null) return true;
			if(this.Wms_assocmemrecall!=null) return true;
			if(this.Wms_assocrecogeasy!=null) return true;
			if(this.Wms_logmemory!=null) return true;
			if(this.Wms_assocrecalleasy!=null) return true;
			if(this.Wms_logmemoryrecall!=null) return true;
		if (this.TrailsA!=null) return true;
		if (this.TrailsB!=null) return true;
		if (this.TrailsBc!=null) return true;
			if(this.Wais_blockdesign!=null) return true;
			if(this.Wais_digitsymbol!=null) return true;
			if(this.Wais_info!=null) return true;
		if (this.Bentonformcdelay!=null) return true;
		if (this.Bentonformdcopy!=null) return true;
		if (this.Bostonnaming!=null) return true;
		if (this.Crossingoff!=null) return true;
		if (this.Wordfluency!=null) return true;
		if (this.Generalfactor!=null) return true;
			if(this.Kanne_frontal!=null) return true;
			if(this.Kanne_parietal!=null) return true;
			if(this.Kanne_temporal!=null) return true;
		if (this.Animal!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
