/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:49 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function sapssans_symptomsSAPSSANS(){
this.xsiType="sapssans:symptomsSAPSSANS";

	this.getSchemaElementName=function(){
		return "symptomsSAPSSANS";
	}

	this.getFullSchemaElementName=function(){
		return "sapssans:symptomsSAPSSANS";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Saps_saps1=null;


	function getSaps_saps1() {
		return this.Saps_saps1;
	}
	this.getSaps_saps1=getSaps_saps1;


	function setSaps_saps1(v){
		this.Saps_saps1=v;
	}
	this.setSaps_saps1=setSaps_saps1;

	this.Saps_saps2=null;


	function getSaps_saps2() {
		return this.Saps_saps2;
	}
	this.getSaps_saps2=getSaps_saps2;


	function setSaps_saps2(v){
		this.Saps_saps2=v;
	}
	this.setSaps_saps2=setSaps_saps2;

	this.Saps_saps3=null;


	function getSaps_saps3() {
		return this.Saps_saps3;
	}
	this.getSaps_saps3=getSaps_saps3;


	function setSaps_saps3(v){
		this.Saps_saps3=v;
	}
	this.setSaps_saps3=setSaps_saps3;

	this.Saps_saps4=null;


	function getSaps_saps4() {
		return this.Saps_saps4;
	}
	this.getSaps_saps4=getSaps_saps4;


	function setSaps_saps4(v){
		this.Saps_saps4=v;
	}
	this.setSaps_saps4=setSaps_saps4;

	this.Saps_saps5=null;


	function getSaps_saps5() {
		return this.Saps_saps5;
	}
	this.getSaps_saps5=getSaps_saps5;


	function setSaps_saps5(v){
		this.Saps_saps5=v;
	}
	this.setSaps_saps5=setSaps_saps5;

	this.Saps_saps6=null;


	function getSaps_saps6() {
		return this.Saps_saps6;
	}
	this.getSaps_saps6=getSaps_saps6;


	function setSaps_saps6(v){
		this.Saps_saps6=v;
	}
	this.setSaps_saps6=setSaps_saps6;

	this.Saps_saps7=null;


	function getSaps_saps7() {
		return this.Saps_saps7;
	}
	this.getSaps_saps7=getSaps_saps7;


	function setSaps_saps7(v){
		this.Saps_saps7=v;
	}
	this.setSaps_saps7=setSaps_saps7;

	this.Saps_saps8=null;


	function getSaps_saps8() {
		return this.Saps_saps8;
	}
	this.getSaps_saps8=getSaps_saps8;


	function setSaps_saps8(v){
		this.Saps_saps8=v;
	}
	this.setSaps_saps8=setSaps_saps8;

	this.Saps_saps9=null;


	function getSaps_saps9() {
		return this.Saps_saps9;
	}
	this.getSaps_saps9=getSaps_saps9;


	function setSaps_saps9(v){
		this.Saps_saps9=v;
	}
	this.setSaps_saps9=setSaps_saps9;

	this.Saps_saps10=null;


	function getSaps_saps10() {
		return this.Saps_saps10;
	}
	this.getSaps_saps10=getSaps_saps10;


	function setSaps_saps10(v){
		this.Saps_saps10=v;
	}
	this.setSaps_saps10=setSaps_saps10;

	this.Saps_saps11=null;


	function getSaps_saps11() {
		return this.Saps_saps11;
	}
	this.getSaps_saps11=getSaps_saps11;


	function setSaps_saps11(v){
		this.Saps_saps11=v;
	}
	this.setSaps_saps11=setSaps_saps11;

	this.Saps_saps12=null;


	function getSaps_saps12() {
		return this.Saps_saps12;
	}
	this.getSaps_saps12=getSaps_saps12;


	function setSaps_saps12(v){
		this.Saps_saps12=v;
	}
	this.setSaps_saps12=setSaps_saps12;

	this.Saps_saps13=null;


	function getSaps_saps13() {
		return this.Saps_saps13;
	}
	this.getSaps_saps13=getSaps_saps13;


	function setSaps_saps13(v){
		this.Saps_saps13=v;
	}
	this.setSaps_saps13=setSaps_saps13;

	this.Saps_saps14=null;


	function getSaps_saps14() {
		return this.Saps_saps14;
	}
	this.getSaps_saps14=getSaps_saps14;


	function setSaps_saps14(v){
		this.Saps_saps14=v;
	}
	this.setSaps_saps14=setSaps_saps14;

	this.Saps_saps15=null;


	function getSaps_saps15() {
		return this.Saps_saps15;
	}
	this.getSaps_saps15=getSaps_saps15;


	function setSaps_saps15(v){
		this.Saps_saps15=v;
	}
	this.setSaps_saps15=setSaps_saps15;

	this.Saps_saps16=null;


	function getSaps_saps16() {
		return this.Saps_saps16;
	}
	this.getSaps_saps16=getSaps_saps16;


	function setSaps_saps16(v){
		this.Saps_saps16=v;
	}
	this.setSaps_saps16=setSaps_saps16;

	this.Saps_saps17=null;


	function getSaps_saps17() {
		return this.Saps_saps17;
	}
	this.getSaps_saps17=getSaps_saps17;


	function setSaps_saps17(v){
		this.Saps_saps17=v;
	}
	this.setSaps_saps17=setSaps_saps17;

	this.Saps_saps18=null;


	function getSaps_saps18() {
		return this.Saps_saps18;
	}
	this.getSaps_saps18=getSaps_saps18;


	function setSaps_saps18(v){
		this.Saps_saps18=v;
	}
	this.setSaps_saps18=setSaps_saps18;

	this.Saps_saps19=null;


	function getSaps_saps19() {
		return this.Saps_saps19;
	}
	this.getSaps_saps19=getSaps_saps19;


	function setSaps_saps19(v){
		this.Saps_saps19=v;
	}
	this.setSaps_saps19=setSaps_saps19;

	this.Saps_saps20=null;


	function getSaps_saps20() {
		return this.Saps_saps20;
	}
	this.getSaps_saps20=getSaps_saps20;


	function setSaps_saps20(v){
		this.Saps_saps20=v;
	}
	this.setSaps_saps20=setSaps_saps20;

	this.Saps_saps21=null;


	function getSaps_saps21() {
		return this.Saps_saps21;
	}
	this.getSaps_saps21=getSaps_saps21;


	function setSaps_saps21(v){
		this.Saps_saps21=v;
	}
	this.setSaps_saps21=setSaps_saps21;

	this.Saps_saps22=null;


	function getSaps_saps22() {
		return this.Saps_saps22;
	}
	this.getSaps_saps22=getSaps_saps22;


	function setSaps_saps22(v){
		this.Saps_saps22=v;
	}
	this.setSaps_saps22=setSaps_saps22;

	this.Saps_saps23=null;


	function getSaps_saps23() {
		return this.Saps_saps23;
	}
	this.getSaps_saps23=getSaps_saps23;


	function setSaps_saps23(v){
		this.Saps_saps23=v;
	}
	this.setSaps_saps23=setSaps_saps23;

	this.Saps_saps24=null;


	function getSaps_saps24() {
		return this.Saps_saps24;
	}
	this.getSaps_saps24=getSaps_saps24;


	function setSaps_saps24(v){
		this.Saps_saps24=v;
	}
	this.setSaps_saps24=setSaps_saps24;

	this.Saps_saps25=null;


	function getSaps_saps25() {
		return this.Saps_saps25;
	}
	this.getSaps_saps25=getSaps_saps25;


	function setSaps_saps25(v){
		this.Saps_saps25=v;
	}
	this.setSaps_saps25=setSaps_saps25;

	this.Saps_saps26=null;


	function getSaps_saps26() {
		return this.Saps_saps26;
	}
	this.getSaps_saps26=getSaps_saps26;


	function setSaps_saps26(v){
		this.Saps_saps26=v;
	}
	this.setSaps_saps26=setSaps_saps26;

	this.Saps_saps27=null;


	function getSaps_saps27() {
		return this.Saps_saps27;
	}
	this.getSaps_saps27=getSaps_saps27;


	function setSaps_saps27(v){
		this.Saps_saps27=v;
	}
	this.setSaps_saps27=setSaps_saps27;

	this.Saps_saps28=null;


	function getSaps_saps28() {
		return this.Saps_saps28;
	}
	this.getSaps_saps28=getSaps_saps28;


	function setSaps_saps28(v){
		this.Saps_saps28=v;
	}
	this.setSaps_saps28=setSaps_saps28;

	this.Saps_saps29=null;


	function getSaps_saps29() {
		return this.Saps_saps29;
	}
	this.getSaps_saps29=getSaps_saps29;


	function setSaps_saps29(v){
		this.Saps_saps29=v;
	}
	this.setSaps_saps29=setSaps_saps29;

	this.Saps_saps30=null;


	function getSaps_saps30() {
		return this.Saps_saps30;
	}
	this.getSaps_saps30=getSaps_saps30;


	function setSaps_saps30(v){
		this.Saps_saps30=v;
	}
	this.setSaps_saps30=setSaps_saps30;

	this.Saps_saps31=null;


	function getSaps_saps31() {
		return this.Saps_saps31;
	}
	this.getSaps_saps31=getSaps_saps31;


	function setSaps_saps31(v){
		this.Saps_saps31=v;
	}
	this.setSaps_saps31=setSaps_saps31;

	this.Saps_saps32=null;


	function getSaps_saps32() {
		return this.Saps_saps32;
	}
	this.getSaps_saps32=getSaps_saps32;


	function setSaps_saps32(v){
		this.Saps_saps32=v;
	}
	this.setSaps_saps32=setSaps_saps32;

	this.Saps_saps33=null;


	function getSaps_saps33() {
		return this.Saps_saps33;
	}
	this.getSaps_saps33=getSaps_saps33;


	function setSaps_saps33(v){
		this.Saps_saps33=v;
	}
	this.setSaps_saps33=setSaps_saps33;

	this.Saps_saps34=null;


	function getSaps_saps34() {
		return this.Saps_saps34;
	}
	this.getSaps_saps34=getSaps_saps34;


	function setSaps_saps34(v){
		this.Saps_saps34=v;
	}
	this.setSaps_saps34=setSaps_saps34;

	this.Sans_sans1=null;


	function getSans_sans1() {
		return this.Sans_sans1;
	}
	this.getSans_sans1=getSans_sans1;


	function setSans_sans1(v){
		this.Sans_sans1=v;
	}
	this.setSans_sans1=setSans_sans1;

	this.Sans_sans2=null;


	function getSans_sans2() {
		return this.Sans_sans2;
	}
	this.getSans_sans2=getSans_sans2;


	function setSans_sans2(v){
		this.Sans_sans2=v;
	}
	this.setSans_sans2=setSans_sans2;

	this.Sans_sans3=null;


	function getSans_sans3() {
		return this.Sans_sans3;
	}
	this.getSans_sans3=getSans_sans3;


	function setSans_sans3(v){
		this.Sans_sans3=v;
	}
	this.setSans_sans3=setSans_sans3;

	this.Sans_sans4=null;


	function getSans_sans4() {
		return this.Sans_sans4;
	}
	this.getSans_sans4=getSans_sans4;


	function setSans_sans4(v){
		this.Sans_sans4=v;
	}
	this.setSans_sans4=setSans_sans4;

	this.Sans_sans5=null;


	function getSans_sans5() {
		return this.Sans_sans5;
	}
	this.getSans_sans5=getSans_sans5;


	function setSans_sans5(v){
		this.Sans_sans5=v;
	}
	this.setSans_sans5=setSans_sans5;

	this.Sans_sans6=null;


	function getSans_sans6() {
		return this.Sans_sans6;
	}
	this.getSans_sans6=getSans_sans6;


	function setSans_sans6(v){
		this.Sans_sans6=v;
	}
	this.setSans_sans6=setSans_sans6;

	this.Sans_sans7=null;


	function getSans_sans7() {
		return this.Sans_sans7;
	}
	this.getSans_sans7=getSans_sans7;


	function setSans_sans7(v){
		this.Sans_sans7=v;
	}
	this.setSans_sans7=setSans_sans7;

	this.Sans_sans8=null;


	function getSans_sans8() {
		return this.Sans_sans8;
	}
	this.getSans_sans8=getSans_sans8;


	function setSans_sans8(v){
		this.Sans_sans8=v;
	}
	this.setSans_sans8=setSans_sans8;

	this.Sans_sans9=null;


	function getSans_sans9() {
		return this.Sans_sans9;
	}
	this.getSans_sans9=getSans_sans9;


	function setSans_sans9(v){
		this.Sans_sans9=v;
	}
	this.setSans_sans9=setSans_sans9;

	this.Sans_sans10=null;


	function getSans_sans10() {
		return this.Sans_sans10;
	}
	this.getSans_sans10=getSans_sans10;


	function setSans_sans10(v){
		this.Sans_sans10=v;
	}
	this.setSans_sans10=setSans_sans10;

	this.Sans_sans11=null;


	function getSans_sans11() {
		return this.Sans_sans11;
	}
	this.getSans_sans11=getSans_sans11;


	function setSans_sans11(v){
		this.Sans_sans11=v;
	}
	this.setSans_sans11=setSans_sans11;

	this.Sans_sans12=null;


	function getSans_sans12() {
		return this.Sans_sans12;
	}
	this.getSans_sans12=getSans_sans12;


	function setSans_sans12(v){
		this.Sans_sans12=v;
	}
	this.setSans_sans12=setSans_sans12;

	this.Sans_sans13=null;


	function getSans_sans13() {
		return this.Sans_sans13;
	}
	this.getSans_sans13=getSans_sans13;


	function setSans_sans13(v){
		this.Sans_sans13=v;
	}
	this.setSans_sans13=setSans_sans13;

	this.Sans_sans14=null;


	function getSans_sans14() {
		return this.Sans_sans14;
	}
	this.getSans_sans14=getSans_sans14;


	function setSans_sans14(v){
		this.Sans_sans14=v;
	}
	this.setSans_sans14=setSans_sans14;

	this.Sans_sans15=null;


	function getSans_sans15() {
		return this.Sans_sans15;
	}
	this.getSans_sans15=getSans_sans15;


	function setSans_sans15(v){
		this.Sans_sans15=v;
	}
	this.setSans_sans15=setSans_sans15;

	this.Sans_sans16=null;


	function getSans_sans16() {
		return this.Sans_sans16;
	}
	this.getSans_sans16=getSans_sans16;


	function setSans_sans16(v){
		this.Sans_sans16=v;
	}
	this.setSans_sans16=setSans_sans16;

	this.Sans_sans17=null;


	function getSans_sans17() {
		return this.Sans_sans17;
	}
	this.getSans_sans17=getSans_sans17;


	function setSans_sans17(v){
		this.Sans_sans17=v;
	}
	this.setSans_sans17=setSans_sans17;

	this.Sans_sans18=null;


	function getSans_sans18() {
		return this.Sans_sans18;
	}
	this.getSans_sans18=getSans_sans18;


	function setSans_sans18(v){
		this.Sans_sans18=v;
	}
	this.setSans_sans18=setSans_sans18;

	this.Sans_sans19=null;


	function getSans_sans19() {
		return this.Sans_sans19;
	}
	this.getSans_sans19=getSans_sans19;


	function setSans_sans19(v){
		this.Sans_sans19=v;
	}
	this.setSans_sans19=setSans_sans19;

	this.Sans_sans20=null;


	function getSans_sans20() {
		return this.Sans_sans20;
	}
	this.getSans_sans20=getSans_sans20;


	function setSans_sans20(v){
		this.Sans_sans20=v;
	}
	this.setSans_sans20=setSans_sans20;

	this.Sans_sans21=null;


	function getSans_sans21() {
		return this.Sans_sans21;
	}
	this.getSans_sans21=getSans_sans21;


	function setSans_sans21(v){
		this.Sans_sans21=v;
	}
	this.setSans_sans21=setSans_sans21;

	this.Sans_sans22=null;


	function getSans_sans22() {
		return this.Sans_sans22;
	}
	this.getSans_sans22=getSans_sans22;


	function setSans_sans22(v){
		this.Sans_sans22=v;
	}
	this.setSans_sans22=setSans_sans22;

	this.Sans_sans23=null;


	function getSans_sans23() {
		return this.Sans_sans23;
	}
	this.getSans_sans23=getSans_sans23;


	function setSans_sans23(v){
		this.Sans_sans23=v;
	}
	this.setSans_sans23=setSans_sans23;

	this.Sans_sans24=null;


	function getSans_sans24() {
		return this.Sans_sans24;
	}
	this.getSans_sans24=getSans_sans24;


	function setSans_sans24(v){
		this.Sans_sans24=v;
	}
	this.setSans_sans24=setSans_sans24;

	this.Sans_sans25=null;


	function getSans_sans25() {
		return this.Sans_sans25;
	}
	this.getSans_sans25=getSans_sans25;


	function setSans_sans25(v){
		this.Sans_sans25=v;
	}
	this.setSans_sans25=setSans_sans25;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="saps/saps1"){
				return this.Saps_saps1 ;
			} else 
			if(xmlPath=="saps/saps2"){
				return this.Saps_saps2 ;
			} else 
			if(xmlPath=="saps/saps3"){
				return this.Saps_saps3 ;
			} else 
			if(xmlPath=="saps/saps4"){
				return this.Saps_saps4 ;
			} else 
			if(xmlPath=="saps/saps5"){
				return this.Saps_saps5 ;
			} else 
			if(xmlPath=="saps/saps6"){
				return this.Saps_saps6 ;
			} else 
			if(xmlPath=="saps/saps7"){
				return this.Saps_saps7 ;
			} else 
			if(xmlPath=="saps/saps8"){
				return this.Saps_saps8 ;
			} else 
			if(xmlPath=="saps/saps9"){
				return this.Saps_saps9 ;
			} else 
			if(xmlPath=="saps/saps10"){
				return this.Saps_saps10 ;
			} else 
			if(xmlPath=="saps/saps11"){
				return this.Saps_saps11 ;
			} else 
			if(xmlPath=="saps/saps12"){
				return this.Saps_saps12 ;
			} else 
			if(xmlPath=="saps/saps13"){
				return this.Saps_saps13 ;
			} else 
			if(xmlPath=="saps/saps14"){
				return this.Saps_saps14 ;
			} else 
			if(xmlPath=="saps/saps15"){
				return this.Saps_saps15 ;
			} else 
			if(xmlPath=="saps/saps16"){
				return this.Saps_saps16 ;
			} else 
			if(xmlPath=="saps/saps17"){
				return this.Saps_saps17 ;
			} else 
			if(xmlPath=="saps/saps18"){
				return this.Saps_saps18 ;
			} else 
			if(xmlPath=="saps/saps19"){
				return this.Saps_saps19 ;
			} else 
			if(xmlPath=="saps/saps20"){
				return this.Saps_saps20 ;
			} else 
			if(xmlPath=="saps/saps21"){
				return this.Saps_saps21 ;
			} else 
			if(xmlPath=="saps/saps22"){
				return this.Saps_saps22 ;
			} else 
			if(xmlPath=="saps/saps23"){
				return this.Saps_saps23 ;
			} else 
			if(xmlPath=="saps/saps24"){
				return this.Saps_saps24 ;
			} else 
			if(xmlPath=="saps/saps25"){
				return this.Saps_saps25 ;
			} else 
			if(xmlPath=="saps/saps26"){
				return this.Saps_saps26 ;
			} else 
			if(xmlPath=="saps/saps27"){
				return this.Saps_saps27 ;
			} else 
			if(xmlPath=="saps/saps28"){
				return this.Saps_saps28 ;
			} else 
			if(xmlPath=="saps/saps29"){
				return this.Saps_saps29 ;
			} else 
			if(xmlPath=="saps/saps30"){
				return this.Saps_saps30 ;
			} else 
			if(xmlPath=="saps/saps31"){
				return this.Saps_saps31 ;
			} else 
			if(xmlPath=="saps/saps32"){
				return this.Saps_saps32 ;
			} else 
			if(xmlPath=="saps/saps33"){
				return this.Saps_saps33 ;
			} else 
			if(xmlPath=="saps/saps34"){
				return this.Saps_saps34 ;
			} else 
			if(xmlPath=="sans/sans1"){
				return this.Sans_sans1 ;
			} else 
			if(xmlPath=="sans/sans2"){
				return this.Sans_sans2 ;
			} else 
			if(xmlPath=="sans/sans3"){
				return this.Sans_sans3 ;
			} else 
			if(xmlPath=="sans/sans4"){
				return this.Sans_sans4 ;
			} else 
			if(xmlPath=="sans/sans5"){
				return this.Sans_sans5 ;
			} else 
			if(xmlPath=="sans/sans6"){
				return this.Sans_sans6 ;
			} else 
			if(xmlPath=="sans/sans7"){
				return this.Sans_sans7 ;
			} else 
			if(xmlPath=="sans/sans8"){
				return this.Sans_sans8 ;
			} else 
			if(xmlPath=="sans/sans9"){
				return this.Sans_sans9 ;
			} else 
			if(xmlPath=="sans/sans10"){
				return this.Sans_sans10 ;
			} else 
			if(xmlPath=="sans/sans11"){
				return this.Sans_sans11 ;
			} else 
			if(xmlPath=="sans/sans12"){
				return this.Sans_sans12 ;
			} else 
			if(xmlPath=="sans/sans13"){
				return this.Sans_sans13 ;
			} else 
			if(xmlPath=="sans/sans14"){
				return this.Sans_sans14 ;
			} else 
			if(xmlPath=="sans/sans15"){
				return this.Sans_sans15 ;
			} else 
			if(xmlPath=="sans/sans16"){
				return this.Sans_sans16 ;
			} else 
			if(xmlPath=="sans/sans17"){
				return this.Sans_sans17 ;
			} else 
			if(xmlPath=="sans/sans18"){
				return this.Sans_sans18 ;
			} else 
			if(xmlPath=="sans/sans19"){
				return this.Sans_sans19 ;
			} else 
			if(xmlPath=="sans/sans20"){
				return this.Sans_sans20 ;
			} else 
			if(xmlPath=="sans/sans21"){
				return this.Sans_sans21 ;
			} else 
			if(xmlPath=="sans/sans22"){
				return this.Sans_sans22 ;
			} else 
			if(xmlPath=="sans/sans23"){
				return this.Sans_sans23 ;
			} else 
			if(xmlPath=="sans/sans24"){
				return this.Sans_sans24 ;
			} else 
			if(xmlPath=="sans/sans25"){
				return this.Sans_sans25 ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="saps/saps1"){
				this.Saps_saps1=value;
			} else 
			if(xmlPath=="saps/saps2"){
				this.Saps_saps2=value;
			} else 
			if(xmlPath=="saps/saps3"){
				this.Saps_saps3=value;
			} else 
			if(xmlPath=="saps/saps4"){
				this.Saps_saps4=value;
			} else 
			if(xmlPath=="saps/saps5"){
				this.Saps_saps5=value;
			} else 
			if(xmlPath=="saps/saps6"){
				this.Saps_saps6=value;
			} else 
			if(xmlPath=="saps/saps7"){
				this.Saps_saps7=value;
			} else 
			if(xmlPath=="saps/saps8"){
				this.Saps_saps8=value;
			} else 
			if(xmlPath=="saps/saps9"){
				this.Saps_saps9=value;
			} else 
			if(xmlPath=="saps/saps10"){
				this.Saps_saps10=value;
			} else 
			if(xmlPath=="saps/saps11"){
				this.Saps_saps11=value;
			} else 
			if(xmlPath=="saps/saps12"){
				this.Saps_saps12=value;
			} else 
			if(xmlPath=="saps/saps13"){
				this.Saps_saps13=value;
			} else 
			if(xmlPath=="saps/saps14"){
				this.Saps_saps14=value;
			} else 
			if(xmlPath=="saps/saps15"){
				this.Saps_saps15=value;
			} else 
			if(xmlPath=="saps/saps16"){
				this.Saps_saps16=value;
			} else 
			if(xmlPath=="saps/saps17"){
				this.Saps_saps17=value;
			} else 
			if(xmlPath=="saps/saps18"){
				this.Saps_saps18=value;
			} else 
			if(xmlPath=="saps/saps19"){
				this.Saps_saps19=value;
			} else 
			if(xmlPath=="saps/saps20"){
				this.Saps_saps20=value;
			} else 
			if(xmlPath=="saps/saps21"){
				this.Saps_saps21=value;
			} else 
			if(xmlPath=="saps/saps22"){
				this.Saps_saps22=value;
			} else 
			if(xmlPath=="saps/saps23"){
				this.Saps_saps23=value;
			} else 
			if(xmlPath=="saps/saps24"){
				this.Saps_saps24=value;
			} else 
			if(xmlPath=="saps/saps25"){
				this.Saps_saps25=value;
			} else 
			if(xmlPath=="saps/saps26"){
				this.Saps_saps26=value;
			} else 
			if(xmlPath=="saps/saps27"){
				this.Saps_saps27=value;
			} else 
			if(xmlPath=="saps/saps28"){
				this.Saps_saps28=value;
			} else 
			if(xmlPath=="saps/saps29"){
				this.Saps_saps29=value;
			} else 
			if(xmlPath=="saps/saps30"){
				this.Saps_saps30=value;
			} else 
			if(xmlPath=="saps/saps31"){
				this.Saps_saps31=value;
			} else 
			if(xmlPath=="saps/saps32"){
				this.Saps_saps32=value;
			} else 
			if(xmlPath=="saps/saps33"){
				this.Saps_saps33=value;
			} else 
			if(xmlPath=="saps/saps34"){
				this.Saps_saps34=value;
			} else 
			if(xmlPath=="sans/sans1"){
				this.Sans_sans1=value;
			} else 
			if(xmlPath=="sans/sans2"){
				this.Sans_sans2=value;
			} else 
			if(xmlPath=="sans/sans3"){
				this.Sans_sans3=value;
			} else 
			if(xmlPath=="sans/sans4"){
				this.Sans_sans4=value;
			} else 
			if(xmlPath=="sans/sans5"){
				this.Sans_sans5=value;
			} else 
			if(xmlPath=="sans/sans6"){
				this.Sans_sans6=value;
			} else 
			if(xmlPath=="sans/sans7"){
				this.Sans_sans7=value;
			} else 
			if(xmlPath=="sans/sans8"){
				this.Sans_sans8=value;
			} else 
			if(xmlPath=="sans/sans9"){
				this.Sans_sans9=value;
			} else 
			if(xmlPath=="sans/sans10"){
				this.Sans_sans10=value;
			} else 
			if(xmlPath=="sans/sans11"){
				this.Sans_sans11=value;
			} else 
			if(xmlPath=="sans/sans12"){
				this.Sans_sans12=value;
			} else 
			if(xmlPath=="sans/sans13"){
				this.Sans_sans13=value;
			} else 
			if(xmlPath=="sans/sans14"){
				this.Sans_sans14=value;
			} else 
			if(xmlPath=="sans/sans15"){
				this.Sans_sans15=value;
			} else 
			if(xmlPath=="sans/sans16"){
				this.Sans_sans16=value;
			} else 
			if(xmlPath=="sans/sans17"){
				this.Sans_sans17=value;
			} else 
			if(xmlPath=="sans/sans18"){
				this.Sans_sans18=value;
			} else 
			if(xmlPath=="sans/sans19"){
				this.Sans_sans19=value;
			} else 
			if(xmlPath=="sans/sans20"){
				this.Sans_sans20=value;
			} else 
			if(xmlPath=="sans/sans21"){
				this.Sans_sans21=value;
			} else 
			if(xmlPath=="sans/sans22"){
				this.Sans_sans22=value;
			} else 
			if(xmlPath=="sans/sans23"){
				this.Sans_sans23=value;
			} else 
			if(xmlPath=="sans/sans24"){
				this.Sans_sans24=value;
			} else 
			if(xmlPath=="sans/sans25"){
				this.Sans_sans25=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="saps/saps1"){
			return "field_data";
		}else if (xmlPath=="saps/saps2"){
			return "field_data";
		}else if (xmlPath=="saps/saps3"){
			return "field_data";
		}else if (xmlPath=="saps/saps4"){
			return "field_data";
		}else if (xmlPath=="saps/saps5"){
			return "field_data";
		}else if (xmlPath=="saps/saps6"){
			return "field_data";
		}else if (xmlPath=="saps/saps7"){
			return "field_data";
		}else if (xmlPath=="saps/saps8"){
			return "field_data";
		}else if (xmlPath=="saps/saps9"){
			return "field_data";
		}else if (xmlPath=="saps/saps10"){
			return "field_data";
		}else if (xmlPath=="saps/saps11"){
			return "field_data";
		}else if (xmlPath=="saps/saps12"){
			return "field_data";
		}else if (xmlPath=="saps/saps13"){
			return "field_data";
		}else if (xmlPath=="saps/saps14"){
			return "field_data";
		}else if (xmlPath=="saps/saps15"){
			return "field_data";
		}else if (xmlPath=="saps/saps16"){
			return "field_data";
		}else if (xmlPath=="saps/saps17"){
			return "field_data";
		}else if (xmlPath=="saps/saps18"){
			return "field_data";
		}else if (xmlPath=="saps/saps19"){
			return "field_data";
		}else if (xmlPath=="saps/saps20"){
			return "field_data";
		}else if (xmlPath=="saps/saps21"){
			return "field_data";
		}else if (xmlPath=="saps/saps22"){
			return "field_data";
		}else if (xmlPath=="saps/saps23"){
			return "field_data";
		}else if (xmlPath=="saps/saps24"){
			return "field_data";
		}else if (xmlPath=="saps/saps25"){
			return "field_data";
		}else if (xmlPath=="saps/saps26"){
			return "field_data";
		}else if (xmlPath=="saps/saps27"){
			return "field_data";
		}else if (xmlPath=="saps/saps28"){
			return "field_data";
		}else if (xmlPath=="saps/saps29"){
			return "field_data";
		}else if (xmlPath=="saps/saps30"){
			return "field_data";
		}else if (xmlPath=="saps/saps31"){
			return "field_data";
		}else if (xmlPath=="saps/saps32"){
			return "field_data";
		}else if (xmlPath=="saps/saps33"){
			return "field_data";
		}else if (xmlPath=="saps/saps34"){
			return "field_data";
		}else if (xmlPath=="sans/sans1"){
			return "field_data";
		}else if (xmlPath=="sans/sans2"){
			return "field_data";
		}else if (xmlPath=="sans/sans3"){
			return "field_data";
		}else if (xmlPath=="sans/sans4"){
			return "field_data";
		}else if (xmlPath=="sans/sans5"){
			return "field_data";
		}else if (xmlPath=="sans/sans6"){
			return "field_data";
		}else if (xmlPath=="sans/sans7"){
			return "field_data";
		}else if (xmlPath=="sans/sans8"){
			return "field_data";
		}else if (xmlPath=="sans/sans9"){
			return "field_data";
		}else if (xmlPath=="sans/sans10"){
			return "field_data";
		}else if (xmlPath=="sans/sans11"){
			return "field_data";
		}else if (xmlPath=="sans/sans12"){
			return "field_data";
		}else if (xmlPath=="sans/sans13"){
			return "field_data";
		}else if (xmlPath=="sans/sans14"){
			return "field_data";
		}else if (xmlPath=="sans/sans15"){
			return "field_data";
		}else if (xmlPath=="sans/sans16"){
			return "field_data";
		}else if (xmlPath=="sans/sans17"){
			return "field_data";
		}else if (xmlPath=="sans/sans18"){
			return "field_data";
		}else if (xmlPath=="sans/sans19"){
			return "field_data";
		}else if (xmlPath=="sans/sans20"){
			return "field_data";
		}else if (xmlPath=="sans/sans21"){
			return "field_data";
		}else if (xmlPath=="sans/sans22"){
			return "field_data";
		}else if (xmlPath=="sans/sans23"){
			return "field_data";
		}else if (xmlPath=="sans/sans24"){
			return "field_data";
		}else if (xmlPath=="sans/sans25"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<sapssans:symptomsSAPSSANS";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</sapssans:symptomsSAPSSANS>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			if(this.Saps_saps8!=null)
			child0++;
			if(this.Saps_saps7!=null)
			child0++;
			if(this.Saps_saps6!=null)
			child0++;
			if(this.Saps_saps5!=null)
			child0++;
			if(this.Saps_saps29!=null)
			child0++;
			if(this.Saps_saps4!=null)
			child0++;
			if(this.Saps_saps28!=null)
			child0++;
			if(this.Saps_saps3!=null)
			child0++;
			if(this.Saps_saps27!=null)
			child0++;
			if(this.Saps_saps2!=null)
			child0++;
			if(this.Saps_saps26!=null)
			child0++;
			if(this.Saps_saps1!=null)
			child0++;
			if(this.Saps_saps25!=null)
			child0++;
			if(this.Saps_saps24!=null)
			child0++;
			if(this.Saps_saps23!=null)
			child0++;
			if(this.Saps_saps22!=null)
			child0++;
			if(this.Saps_saps21!=null)
			child0++;
			if(this.Saps_saps20!=null)
			child0++;
			if(this.Saps_saps34!=null)
			child0++;
			if(this.Saps_saps33!=null)
			child0++;
			if(this.Saps_saps32!=null)
			child0++;
			if(this.Saps_saps31!=null)
			child0++;
			if(this.Saps_saps30!=null)
			child0++;
			if(this.Saps_saps19!=null)
			child0++;
			if(this.Saps_saps18!=null)
			child0++;
			if(this.Saps_saps17!=null)
			child0++;
			if(this.Saps_saps16!=null)
			child0++;
			if(this.Saps_saps15!=null)
			child0++;
			if(this.Saps_saps14!=null)
			child0++;
			if(this.Saps_saps13!=null)
			child0++;
			if(this.Saps_saps12!=null)
			child0++;
			if(this.Saps_saps11!=null)
			child0++;
			if(this.Saps_saps10!=null)
			child0++;
			if(this.Saps_saps9!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<sapssans:saps";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Saps_saps1!=null){
			xmlTxt+="\n<sapssans:saps1";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps1.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps1>";
		}
		if (this.Saps_saps2!=null){
			xmlTxt+="\n<sapssans:saps2";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps2.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps2>";
		}
		if (this.Saps_saps3!=null){
			xmlTxt+="\n<sapssans:saps3";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps3.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps3>";
		}
		if (this.Saps_saps4!=null){
			xmlTxt+="\n<sapssans:saps4";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps4.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps4>";
		}
		if (this.Saps_saps5!=null){
			xmlTxt+="\n<sapssans:saps5";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps5.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps5>";
		}
		if (this.Saps_saps6!=null){
			xmlTxt+="\n<sapssans:saps6";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps6.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps6>";
		}
		if (this.Saps_saps7!=null){
			xmlTxt+="\n<sapssans:saps7";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps7.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps7>";
		}
		if (this.Saps_saps8!=null){
			xmlTxt+="\n<sapssans:saps8";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps8.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps8>";
		}
		if (this.Saps_saps9!=null){
			xmlTxt+="\n<sapssans:saps9";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps9.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps9>";
		}
		if (this.Saps_saps10!=null){
			xmlTxt+="\n<sapssans:saps10";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps10.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps10>";
		}
		if (this.Saps_saps11!=null){
			xmlTxt+="\n<sapssans:saps11";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps11.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps11>";
		}
		if (this.Saps_saps12!=null){
			xmlTxt+="\n<sapssans:saps12";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps12.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps12>";
		}
		if (this.Saps_saps13!=null){
			xmlTxt+="\n<sapssans:saps13";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps13.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps13>";
		}
		if (this.Saps_saps14!=null){
			xmlTxt+="\n<sapssans:saps14";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps14.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps14>";
		}
		if (this.Saps_saps15!=null){
			xmlTxt+="\n<sapssans:saps15";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps15.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps15>";
		}
		if (this.Saps_saps16!=null){
			xmlTxt+="\n<sapssans:saps16";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps16.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps16>";
		}
		if (this.Saps_saps17!=null){
			xmlTxt+="\n<sapssans:saps17";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps17.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps17>";
		}
		if (this.Saps_saps18!=null){
			xmlTxt+="\n<sapssans:saps18";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps18.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps18>";
		}
		if (this.Saps_saps19!=null){
			xmlTxt+="\n<sapssans:saps19";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps19.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps19>";
		}
		if (this.Saps_saps20!=null){
			xmlTxt+="\n<sapssans:saps20";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps20.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps20>";
		}
		if (this.Saps_saps21!=null){
			xmlTxt+="\n<sapssans:saps21";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps21.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps21>";
		}
		if (this.Saps_saps22!=null){
			xmlTxt+="\n<sapssans:saps22";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps22.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps22>";
		}
		if (this.Saps_saps23!=null){
			xmlTxt+="\n<sapssans:saps23";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps23.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps23>";
		}
		if (this.Saps_saps24!=null){
			xmlTxt+="\n<sapssans:saps24";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps24.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps24>";
		}
		if (this.Saps_saps25!=null){
			xmlTxt+="\n<sapssans:saps25";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps25.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps25>";
		}
		if (this.Saps_saps26!=null){
			xmlTxt+="\n<sapssans:saps26";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps26.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps26>";
		}
		if (this.Saps_saps27!=null){
			xmlTxt+="\n<sapssans:saps27";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps27.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps27>";
		}
		if (this.Saps_saps28!=null){
			xmlTxt+="\n<sapssans:saps28";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps28.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps28>";
		}
		if (this.Saps_saps29!=null){
			xmlTxt+="\n<sapssans:saps29";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps29.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps29>";
		}
		if (this.Saps_saps30!=null){
			xmlTxt+="\n<sapssans:saps30";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps30.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps30>";
		}
		if (this.Saps_saps31!=null){
			xmlTxt+="\n<sapssans:saps31";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps31.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps31>";
		}
		if (this.Saps_saps32!=null){
			xmlTxt+="\n<sapssans:saps32";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps32.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps32>";
		}
		if (this.Saps_saps33!=null){
			xmlTxt+="\n<sapssans:saps33";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps33.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps33>";
		}
		if (this.Saps_saps34!=null){
			xmlTxt+="\n<sapssans:saps34";
			xmlTxt+=">";
			xmlTxt+=this.Saps_saps34.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:saps34>";
		}
				xmlTxt+="\n</sapssans:saps>";
			}
			}

			var child1=0;
			var att1=0;
			if(this.Sans_sans20!=null)
			child1++;
			if(this.Sans_sans4!=null)
			child1++;
			if(this.Sans_sans3!=null)
			child1++;
			if(this.Sans_sans2!=null)
			child1++;
			if(this.Sans_sans1!=null)
			child1++;
			if(this.Sans_sans19!=null)
			child1++;
			if(this.Sans_sans18!=null)
			child1++;
			if(this.Sans_sans17!=null)
			child1++;
			if(this.Sans_sans16!=null)
			child1++;
			if(this.Sans_sans15!=null)
			child1++;
			if(this.Sans_sans14!=null)
			child1++;
			if(this.Sans_sans13!=null)
			child1++;
			if(this.Sans_sans12!=null)
			child1++;
			if(this.Sans_sans11!=null)
			child1++;
			if(this.Sans_sans10!=null)
			child1++;
			if(this.Sans_sans25!=null)
			child1++;
			if(this.Sans_sans9!=null)
			child1++;
			if(this.Sans_sans24!=null)
			child1++;
			if(this.Sans_sans8!=null)
			child1++;
			if(this.Sans_sans23!=null)
			child1++;
			if(this.Sans_sans7!=null)
			child1++;
			if(this.Sans_sans22!=null)
			child1++;
			if(this.Sans_sans6!=null)
			child1++;
			if(this.Sans_sans21!=null)
			child1++;
			if(this.Sans_sans5!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<sapssans:sans";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Sans_sans1!=null){
			xmlTxt+="\n<sapssans:sans1";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans1.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans1>";
		}
		if (this.Sans_sans2!=null){
			xmlTxt+="\n<sapssans:sans2";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans2.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans2>";
		}
		if (this.Sans_sans3!=null){
			xmlTxt+="\n<sapssans:sans3";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans3.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans3>";
		}
		if (this.Sans_sans4!=null){
			xmlTxt+="\n<sapssans:sans4";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans4.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans4>";
		}
		if (this.Sans_sans5!=null){
			xmlTxt+="\n<sapssans:sans5";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans5.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans5>";
		}
		if (this.Sans_sans6!=null){
			xmlTxt+="\n<sapssans:sans6";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans6.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans6>";
		}
		if (this.Sans_sans7!=null){
			xmlTxt+="\n<sapssans:sans7";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans7.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans7>";
		}
		if (this.Sans_sans8!=null){
			xmlTxt+="\n<sapssans:sans8";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans8.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans8>";
		}
		if (this.Sans_sans9!=null){
			xmlTxt+="\n<sapssans:sans9";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans9.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans9>";
		}
		if (this.Sans_sans10!=null){
			xmlTxt+="\n<sapssans:sans10";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans10.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans10>";
		}
		if (this.Sans_sans11!=null){
			xmlTxt+="\n<sapssans:sans11";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans11.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans11>";
		}
		if (this.Sans_sans12!=null){
			xmlTxt+="\n<sapssans:sans12";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans12.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans12>";
		}
		if (this.Sans_sans13!=null){
			xmlTxt+="\n<sapssans:sans13";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans13.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans13>";
		}
		if (this.Sans_sans14!=null){
			xmlTxt+="\n<sapssans:sans14";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans14.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans14>";
		}
		if (this.Sans_sans15!=null){
			xmlTxt+="\n<sapssans:sans15";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans15.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans15>";
		}
		if (this.Sans_sans16!=null){
			xmlTxt+="\n<sapssans:sans16";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans16.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans16>";
		}
		if (this.Sans_sans17!=null){
			xmlTxt+="\n<sapssans:sans17";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans17.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans17>";
		}
		if (this.Sans_sans18!=null){
			xmlTxt+="\n<sapssans:sans18";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans18.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans18>";
		}
		if (this.Sans_sans19!=null){
			xmlTxt+="\n<sapssans:sans19";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans19.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans19>";
		}
		if (this.Sans_sans20!=null){
			xmlTxt+="\n<sapssans:sans20";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans20.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans20>";
		}
		if (this.Sans_sans21!=null){
			xmlTxt+="\n<sapssans:sans21";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans21.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans21>";
		}
		if (this.Sans_sans22!=null){
			xmlTxt+="\n<sapssans:sans22";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans22.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans22>";
		}
		if (this.Sans_sans23!=null){
			xmlTxt+="\n<sapssans:sans23";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans23.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans23>";
		}
		if (this.Sans_sans24!=null){
			xmlTxt+="\n<sapssans:sans24";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans24.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans24>";
		}
		if (this.Sans_sans25!=null){
			xmlTxt+="\n<sapssans:sans25";
			xmlTxt+=">";
			xmlTxt+=this.Sans_sans25.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sapssans:sans25>";
		}
				xmlTxt+="\n</sapssans:sans>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.Saps_saps8!=null) return true;
			if(this.Saps_saps7!=null) return true;
			if(this.Saps_saps6!=null) return true;
			if(this.Saps_saps5!=null) return true;
			if(this.Saps_saps29!=null) return true;
			if(this.Saps_saps4!=null) return true;
			if(this.Saps_saps28!=null) return true;
			if(this.Saps_saps3!=null) return true;
			if(this.Saps_saps27!=null) return true;
			if(this.Saps_saps2!=null) return true;
			if(this.Saps_saps26!=null) return true;
			if(this.Saps_saps1!=null) return true;
			if(this.Saps_saps25!=null) return true;
			if(this.Saps_saps24!=null) return true;
			if(this.Saps_saps23!=null) return true;
			if(this.Saps_saps22!=null) return true;
			if(this.Saps_saps21!=null) return true;
			if(this.Saps_saps20!=null) return true;
			if(this.Saps_saps34!=null) return true;
			if(this.Saps_saps33!=null) return true;
			if(this.Saps_saps32!=null) return true;
			if(this.Saps_saps31!=null) return true;
			if(this.Saps_saps30!=null) return true;
			if(this.Saps_saps19!=null) return true;
			if(this.Saps_saps18!=null) return true;
			if(this.Saps_saps17!=null) return true;
			if(this.Saps_saps16!=null) return true;
			if(this.Saps_saps15!=null) return true;
			if(this.Saps_saps14!=null) return true;
			if(this.Saps_saps13!=null) return true;
			if(this.Saps_saps12!=null) return true;
			if(this.Saps_saps11!=null) return true;
			if(this.Saps_saps10!=null) return true;
			if(this.Saps_saps9!=null) return true;
			if(this.Sans_sans20!=null) return true;
			if(this.Sans_sans4!=null) return true;
			if(this.Sans_sans3!=null) return true;
			if(this.Sans_sans2!=null) return true;
			if(this.Sans_sans1!=null) return true;
			if(this.Sans_sans19!=null) return true;
			if(this.Sans_sans18!=null) return true;
			if(this.Sans_sans17!=null) return true;
			if(this.Sans_sans16!=null) return true;
			if(this.Sans_sans15!=null) return true;
			if(this.Sans_sans14!=null) return true;
			if(this.Sans_sans13!=null) return true;
			if(this.Sans_sans12!=null) return true;
			if(this.Sans_sans11!=null) return true;
			if(this.Sans_sans10!=null) return true;
			if(this.Sans_sans25!=null) return true;
			if(this.Sans_sans9!=null) return true;
			if(this.Sans_sans24!=null) return true;
			if(this.Sans_sans8!=null) return true;
			if(this.Sans_sans23!=null) return true;
			if(this.Sans_sans7!=null) return true;
			if(this.Sans_sans22!=null) return true;
			if(this.Sans_sans6!=null) return true;
			if(this.Sans_sans21!=null) return true;
			if(this.Sans_sans5!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
