/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:49 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function genetics_geneticTestResults_gene(){
this.xsiType="genetics:geneticTestResults_gene";

	this.getSchemaElementName=function(){
		return "geneticTestResults_gene";
	}

	this.getFullSchemaElementName=function(){
		return "genetics:geneticTestResults_gene";
	}

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.Genotype=null;


	function getGenotype() {
		return this.Genotype;
	}
	this.getGenotype=getGenotype;


	function setGenotype(v){
		this.Genotype=v;
	}
	this.setGenotype=setGenotype;

	this.Minorpos=null;


	function getMinorpos() {
		return this.Minorpos;
	}
	this.getMinorpos=getMinorpos;


	function setMinorpos(v){
		this.Minorpos=v;
	}
	this.setMinorpos=setMinorpos;

	this.Heteropos=null;


	function getHeteropos() {
		return this.Heteropos;
	}
	this.getHeteropos=getHeteropos;


	function setHeteropos(v){
		this.Heteropos=v;
	}
	this.setHeteropos=setHeteropos;

	this.GeneticsGenetictestresultsGeneId=null;


	function getGeneticsGenetictestresultsGeneId() {
		return this.GeneticsGenetictestresultsGeneId;
	}
	this.getGeneticsGenetictestresultsGeneId=getGeneticsGenetictestresultsGeneId;


	function setGeneticsGenetictestresultsGeneId(v){
		this.GeneticsGenetictestresultsGeneId=v;
	}
	this.setGeneticsGenetictestresultsGeneId=setGeneticsGenetictestresultsGeneId;

	this.genes_gene_genetics_geneticTest_id_fk=null;


	this.getgenes_gene_genetics_geneticTest_id=function() {
		return this.genes_gene_genetics_geneticTest_id_fk;
	}


	this.setgenes_gene_genetics_geneticTest_id=function(v){
		this.genes_gene_genetics_geneticTest_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="genotype"){
				return this.Genotype ;
			} else 
			if(xmlPath=="minorpos"){
				return this.Minorpos ;
			} else 
			if(xmlPath=="heteropos"){
				return this.Heteropos ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="genetics_geneticTestResults_gene_id"){
				return this.GeneticsGenetictestresultsGeneId ;
			} else 
			if(xmlPath=="genes_gene_genetics_geneticTest_id"){
				return this.genes_gene_genetics_geneticTest_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="genotype"){
				this.Genotype=value;
			} else 
			if(xmlPath=="minorpos"){
				this.Minorpos=value;
			} else 
			if(xmlPath=="heteropos"){
				this.Heteropos=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="genetics_geneticTestResults_gene_id"){
				this.GeneticsGenetictestresultsGeneId=value;
			} else 
			if(xmlPath=="genes_gene_genetics_geneticTest_id"){
				this.genes_gene_genetics_geneticTest_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="name"){
			return "field_data";
		}else if (xmlPath=="genotype"){
			return "field_data";
		}else if (xmlPath=="minorpos"){
			return "field_data";
		}else if (xmlPath=="heteropos"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<genetics:geneticTestResults_gene";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</genetics:geneticTestResults_gene>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.GeneticsGenetictestresultsGeneId!=null){
				if(hiddenCount++>0)str+=",";
				str+="genetics_geneticTestResults_gene_id=\"" + this.GeneticsGenetictestresultsGeneId + "\"";
			}
			if(this.genes_gene_genetics_geneticTest_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="genes_gene_genetics_geneticTest_id=\"" + this.genes_gene_genetics_geneticTest_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Name!=null){
			xmlTxt+="\n<genetics:name";
			xmlTxt+=">";
			xmlTxt+=this.Name.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</genetics:name>";
		}
		if (this.Genotype!=null){
			xmlTxt+="\n<genetics:genotype";
			xmlTxt+=">";
			xmlTxt+=this.Genotype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</genetics:genotype>";
		}
		if (this.Minorpos!=null){
			xmlTxt+="\n<genetics:minorpos";
			xmlTxt+=">";
			xmlTxt+=this.Minorpos.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</genetics:minorpos>";
		}
		if (this.Heteropos!=null){
			xmlTxt+="\n<genetics:heteropos";
			xmlTxt+=">";
			xmlTxt+=this.Heteropos.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</genetics:heteropos>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.GeneticsGenetictestresultsGeneId!=null) return true;
			if (this.genes_gene_genetics_geneticTest_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Name!=null) return true;
		if (this.Genotype!=null) return true;
		if (this.Minorpos!=null) return true;
		if (this.Heteropos!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
