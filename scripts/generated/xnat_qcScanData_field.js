/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:49 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function xnat_qcScanData_field(){
this.xsiType="xnat:qcScanData_field";

	this.getSchemaElementName=function(){
		return "qcScanData_field";
	}

	this.getFullSchemaElementName=function(){
		return "xnat:qcScanData_field";
	}

	this.Field=null;


	function getField() {
		return this.Field;
	}
	this.getField=getField;


	function setField(v){
		this.Field=v;
	}
	this.setField=setField;

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.XnatQcscandataFieldId=null;


	function getXnatQcscandataFieldId() {
		return this.XnatQcscandataFieldId;
	}
	this.getXnatQcscandataFieldId=getXnatQcscandataFieldId;


	function setXnatQcscandataFieldId(v){
		this.XnatQcscandataFieldId=v;
	}
	this.setXnatQcscandataFieldId=setXnatQcscandataFieldId;

	this.fields_field_xnat_qcScanData_xnat_qcscandata_id_fk=null;


	this.getfields_field_xnat_qcScanData_xnat_qcscandata_id=function() {
		return this.fields_field_xnat_qcScanData_xnat_qcscandata_id_fk;
	}


	this.setfields_field_xnat_qcScanData_xnat_qcscandata_id=function(v){
		this.fields_field_xnat_qcScanData_xnat_qcscandata_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="field"){
				return this.Field ;
			} else 
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="xnat_qcScanData_field_id"){
				return this.XnatQcscandataFieldId ;
			} else 
			if(xmlPath=="fields_field_xnat_qcScanData_xnat_qcscandata_id"){
				return this.fields_field_xnat_qcScanData_xnat_qcscandata_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="field"){
				this.Field=value;
			} else 
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="xnat_qcScanData_field_id"){
				this.XnatQcscandataFieldId=value;
			} else 
			if(xmlPath=="fields_field_xnat_qcScanData_xnat_qcscandata_id"){
				this.fields_field_xnat_qcScanData_xnat_qcscandata_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="field"){
			return "field_data";
		}else if (xmlPath=="name"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<xnat:qcScanData_field";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</xnat:qcScanData_field>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.XnatQcscandataFieldId!=null){
				if(hiddenCount++>0)str+=",";
				str+="xnat_qcScanData_field_id=\"" + this.XnatQcscandataFieldId + "\"";
			}
			if(this.fields_field_xnat_qcScanData_xnat_qcscandata_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="fields_field_xnat_qcScanData_xnat_qcscandata_id=\"" + this.fields_field_xnat_qcScanData_xnat_qcscandata_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Name!=null)
			attTxt+=" name=\"" +this.Name +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Field!=null){
			xmlTxt+=this.Field.replace(/>/g,"&gt;").replace(/</g,"&lt;");
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.XnatQcscandataFieldId!=null) return true;
			if (this.fields_field_xnat_qcScanData_xnat_qcscandata_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Field!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
