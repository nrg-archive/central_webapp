/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_cndaSubjectMetadata(){
this.xsiType="cnda:cndaSubjectMetadata";

	this.getSchemaElementName=function(){
		return "cndaSubjectMetadata";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:cndaSubjectMetadata";
	}
this.extension=dynamicJSLoad('xnat_subjectMetadata','generated/xnat_subjectMetadata.js');

	this.Map=null;


	function getMap() {
		return this.Map;
	}
	this.getMap=getMap;


	function setMap(v){
		this.Map=v;
	}
	this.setMap=setMap;

	this.LabId=null;


	function getLabId() {
		return this.LabId;
	}
	this.getLabId=getLabId;


	function setLabId(v){
		this.LabId=v;
	}
	this.setLabId=setLabId;

	this.Pib=null;


	function getPib() {
		return this.Pib;
	}
	this.getPib=getPib;


	function setPib(v){
		this.Pib=v;
	}
	this.setPib=setPib;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectMetadata"){
				return this.Subjectmetadata ;
			} else 
			if(xmlPath.startsWith("subjectMetadata")){
				xmlPath=xmlPath.substring(15);
				if(xmlPath=="")return this.Subjectmetadata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectmetadata!=undefined)return this.Subjectmetadata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="map"){
				return this.Map ;
			} else 
			if(xmlPath=="lab_id"){
				return this.LabId ;
			} else 
			if(xmlPath=="pib"){
				return this.Pib ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectMetadata"){
				this.Subjectmetadata=value;
			} else 
			if(xmlPath.startsWith("subjectMetadata")){
				xmlPath=xmlPath.substring(15);
				if(xmlPath=="")return this.Subjectmetadata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectmetadata!=undefined){
					this.Subjectmetadata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectmetadata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectmetadata= instanciateObject("xnat:subjectMetadata");//omUtils.js
						}
						if(options && options.where)this.Subjectmetadata.setProperty(options.where.field,options.where.value);
						this.Subjectmetadata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="map"){
				this.Map=value;
			} else 
			if(xmlPath=="lab_id"){
				this.LabId=value;
			} else 
			if(xmlPath=="pib"){
				this.Pib=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="map"){
			return "field_data";
		}else if (xmlPath=="lab_id"){
			return "field_data";
		}else if (xmlPath=="pib"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:cndaSubjectMetadata";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:cndaSubjectMetadata>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Map!=null){
			xmlTxt+="\n<cnda:map";
			xmlTxt+=">";
			xmlTxt+=this.Map.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:map>";
		}
		if (this.LabId!=null){
			xmlTxt+="\n<cnda:lab_id";
			xmlTxt+=">";
			xmlTxt+=this.LabId.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:lab_id>";
		}
		if (this.Pib!=null){
			xmlTxt+="\n<cnda:pib";
			xmlTxt+=">";
			xmlTxt+=this.Pib.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:pib>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Map!=null) return true;
		if (this.LabId!=null) return true;
		if (this.Pib!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
