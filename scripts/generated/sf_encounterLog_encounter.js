/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:49 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function sf_encounterLog_encounter(){
this.xsiType="sf:encounterLog_encounter";

	this.getSchemaElementName=function(){
		return "encounterLog_encounter";
	}

	this.getFullSchemaElementName=function(){
		return "sf:encounterLog_encounter";
	}

	this.Date=null;


	function getDate() {
		return this.Date;
	}
	this.getDate=getDate;


	function setDate(v){
		this.Date=v;
	}
	this.setDate=setDate;

	this.Time=null;


	function getTime() {
		return this.Time;
	}
	this.getTime=getTime;


	function setTime(v){
		this.Time=v;
	}
	this.setTime=setTime;

	this.EncounterType=null;


	function getEncounterType() {
		return this.EncounterType;
	}
	this.getEncounterType=getEncounterType;


	function setEncounterType(v){
		this.EncounterType=v;
	}
	this.setEncounterType=setEncounterType;

	this.ProjectTimepoint=null;


	function getProjectTimepoint() {
		return this.ProjectTimepoint;
	}
	this.getProjectTimepoint=getProjectTimepoint;


	function setProjectTimepoint(v){
		this.ProjectTimepoint=v;
	}
	this.setProjectTimepoint=setProjectTimepoint;

	this.SubjectStatus=null;


	function getSubjectStatus() {
		return this.SubjectStatus;
	}
	this.getSubjectStatus=getSubjectStatus;


	function setSubjectStatus(v){
		this.SubjectStatus=v;
	}
	this.setSubjectStatus=setSubjectStatus;

	this.Comment=null;


	function getComment() {
		return this.Comment;
	}
	this.getComment=getComment;


	function setComment(v){
		this.Comment=v;
	}
	this.setComment=setComment;

	this.EncounterNumber=null;


	function getEncounterNumber() {
		return this.EncounterNumber;
	}
	this.getEncounterNumber=getEncounterNumber;


	function setEncounterNumber(v){
		this.EncounterNumber=v;
	}
	this.setEncounterNumber=setEncounterNumber;

	this.SfEncounterlogEncounterId=null;


	function getSfEncounterlogEncounterId() {
		return this.SfEncounterlogEncounterId;
	}
	this.getSfEncounterlogEncounterId=getSfEncounterlogEncounterId;


	function setSfEncounterlogEncounterId(v){
		this.SfEncounterlogEncounterId=v;
	}
	this.setSfEncounterlogEncounterId=setSfEncounterlogEncounterId;

	this.encounters_encounter_sf_encount_id_fk=null;


	this.getencounters_encounter_sf_encount_id=function() {
		return this.encounters_encounter_sf_encount_id_fk;
	}


	this.setencounters_encounter_sf_encount_id=function(v){
		this.encounters_encounter_sf_encount_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="date"){
				return this.Date ;
			} else 
			if(xmlPath=="time"){
				return this.Time ;
			} else 
			if(xmlPath=="encounter_type"){
				return this.EncounterType ;
			} else 
			if(xmlPath=="project_timepoint"){
				return this.ProjectTimepoint ;
			} else 
			if(xmlPath=="subject_status"){
				return this.SubjectStatus ;
			} else 
			if(xmlPath=="comment"){
				return this.Comment ;
			} else 
			if(xmlPath=="encounter_number"){
				return this.EncounterNumber ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="sf_encounterLog_encounter_id"){
				return this.SfEncounterlogEncounterId ;
			} else 
			if(xmlPath=="encounters_encounter_sf_encount_id"){
				return this.encounters_encounter_sf_encount_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="date"){
				this.Date=value;
			} else 
			if(xmlPath=="time"){
				this.Time=value;
			} else 
			if(xmlPath=="encounter_type"){
				this.EncounterType=value;
			} else 
			if(xmlPath=="project_timepoint"){
				this.ProjectTimepoint=value;
			} else 
			if(xmlPath=="subject_status"){
				this.SubjectStatus=value;
			} else 
			if(xmlPath=="comment"){
				this.Comment=value;
			} else 
			if(xmlPath=="encounter_number"){
				this.EncounterNumber=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="sf_encounterLog_encounter_id"){
				this.SfEncounterlogEncounterId=value;
			} else 
			if(xmlPath=="encounters_encounter_sf_encount_id"){
				this.encounters_encounter_sf_encount_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="date"){
			return "field_data";
		}else if (xmlPath=="time"){
			return "field_data";
		}else if (xmlPath=="encounter_type"){
			return "field_data";
		}else if (xmlPath=="project_timepoint"){
			return "field_data";
		}else if (xmlPath=="subject_status"){
			return "field_data";
		}else if (xmlPath=="comment"){
			return "field_LONG_DATA";
		}else if (xmlPath=="encounter_number"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<sf:encounterLog_encounter";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</sf:encounterLog_encounter>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.SfEncounterlogEncounterId!=null){
				if(hiddenCount++>0)str+=",";
				str+="sf_encounterLog_encounter_id=\"" + this.SfEncounterlogEncounterId + "\"";
			}
			if(this.encounters_encounter_sf_encount_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="encounters_encounter_sf_encount_id=\"" + this.encounters_encounter_sf_encount_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.EncounterNumber!=null)
			attTxt+=" encounter_number=\"" +this.EncounterNumber +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Date!=null){
			xmlTxt+="\n<sf:date";
			xmlTxt+=">";
			xmlTxt+=this.Date;
			xmlTxt+="</sf:date>";
		}
		if (this.Time!=null){
			xmlTxt+="\n<sf:time";
			xmlTxt+=">";
			xmlTxt+=this.Time;
			xmlTxt+="</sf:time>";
		}
		if (this.EncounterType!=null){
			xmlTxt+="\n<sf:encounter_type";
			xmlTxt+=">";
			xmlTxt+=this.EncounterType.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:encounter_type>";
		}
		if (this.ProjectTimepoint!=null){
			xmlTxt+="\n<sf:project_timepoint";
			xmlTxt+=">";
			xmlTxt+=this.ProjectTimepoint.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:project_timepoint>";
		}
		if (this.SubjectStatus!=null){
			xmlTxt+="\n<sf:subject_status";
			xmlTxt+=">";
			xmlTxt+=this.SubjectStatus.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:subject_status>";
		}
		if (this.Comment!=null){
			xmlTxt+="\n<sf:comment";
			xmlTxt+=">";
			xmlTxt+=this.Comment.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</sf:comment>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.SfEncounterlogEncounterId!=null) return true;
			if (this.encounters_encounter_sf_encount_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Date!=null) return true;
		if (this.Time!=null) return true;
		if (this.EncounterType!=null) return true;
		if (this.ProjectTimepoint!=null) return true;
		if (this.SubjectStatus!=null) return true;
		if (this.Comment!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
