/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_modifiedScheltensData(){
this.xsiType="cnda:modifiedScheltensData";

	this.getSchemaElementName=function(){
		return "modifiedScheltensData";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:modifiedScheltensData";
	}
this.extension=dynamicJSLoad('xnat_mrAssessorData','generated/xnat_mrAssessorData.js');
	this.Assessment_regions_deepWhiteMatter_frontal =null;
	function getAssessment_regions_deepWhiteMatter_frontal() {
		return this.Assessment_regions_deepWhiteMatter_frontal;
	}
	this.getAssessment_regions_deepWhiteMatter_frontal=getAssessment_regions_deepWhiteMatter_frontal;


	function setAssessment_regions_deepWhiteMatter_frontal(v){
		this.Assessment_regions_deepWhiteMatter_frontal =v;
	}
	this.setAssessment_regions_deepWhiteMatter_frontal=setAssessment_regions_deepWhiteMatter_frontal;

	this.Assessment_regions_deepWhiteMatter_frontal_AssessmentRegionsDeepWhiteMatterFrontalCndaModifiedschel=null;


	function getAssessment_regions_deepWhiteMatter_frontal_AssessmentRegionsDeepWhiteMatterFrontalCndaModifiedschel(){
		return this.Assessment_regions_deepWhiteMatter_frontal_AssessmentRegionsDeepWhiteMatterFrontalCndaModifiedschel;
	}
	this.getAssessment_regions_deepWhiteMatter_frontal_AssessmentRegionsDeepWhiteMatterFrontalCndaModifiedschel=getAssessment_regions_deepWhiteMatter_frontal_AssessmentRegionsDeepWhiteMatterFrontalCndaModifiedschel;


	function setAssessment_regions_deepWhiteMatter_frontal_AssessmentRegionsDeepWhiteMatterFrontalCndaModifiedschel(v){
		this.Assessment_regions_deepWhiteMatter_frontal_AssessmentRegionsDeepWhiteMatterFrontalCndaModifiedschel=v;
	}
	this.setAssessment_regions_deepWhiteMatter_frontal_AssessmentRegionsDeepWhiteMatterFrontalCndaModifiedschel=setAssessment_regions_deepWhiteMatter_frontal_AssessmentRegionsDeepWhiteMatterFrontalCndaModifiedschel;
	this.Assessment_regions_deepWhiteMatter_parietal =null;
	function getAssessment_regions_deepWhiteMatter_parietal() {
		return this.Assessment_regions_deepWhiteMatter_parietal;
	}
	this.getAssessment_regions_deepWhiteMatter_parietal=getAssessment_regions_deepWhiteMatter_parietal;


	function setAssessment_regions_deepWhiteMatter_parietal(v){
		this.Assessment_regions_deepWhiteMatter_parietal =v;
	}
	this.setAssessment_regions_deepWhiteMatter_parietal=setAssessment_regions_deepWhiteMatter_parietal;

	this.Assessment_regions_deepWhiteMatter_parietal_AssessmentRegionsDeepWhiteMatterParietalCndaModifiedsche=null;


	function getAssessment_regions_deepWhiteMatter_parietal_AssessmentRegionsDeepWhiteMatterParietalCndaModifiedsche(){
		return this.Assessment_regions_deepWhiteMatter_parietal_AssessmentRegionsDeepWhiteMatterParietalCndaModifiedsche;
	}
	this.getAssessment_regions_deepWhiteMatter_parietal_AssessmentRegionsDeepWhiteMatterParietalCndaModifiedsche=getAssessment_regions_deepWhiteMatter_parietal_AssessmentRegionsDeepWhiteMatterParietalCndaModifiedsche;


	function setAssessment_regions_deepWhiteMatter_parietal_AssessmentRegionsDeepWhiteMatterParietalCndaModifiedsche(v){
		this.Assessment_regions_deepWhiteMatter_parietal_AssessmentRegionsDeepWhiteMatterParietalCndaModifiedsche=v;
	}
	this.setAssessment_regions_deepWhiteMatter_parietal_AssessmentRegionsDeepWhiteMatterParietalCndaModifiedsche=setAssessment_regions_deepWhiteMatter_parietal_AssessmentRegionsDeepWhiteMatterParietalCndaModifiedsche;
	this.Assessment_regions_deepWhiteMatter_occipital =null;
	function getAssessment_regions_deepWhiteMatter_occipital() {
		return this.Assessment_regions_deepWhiteMatter_occipital;
	}
	this.getAssessment_regions_deepWhiteMatter_occipital=getAssessment_regions_deepWhiteMatter_occipital;


	function setAssessment_regions_deepWhiteMatter_occipital(v){
		this.Assessment_regions_deepWhiteMatter_occipital =v;
	}
	this.setAssessment_regions_deepWhiteMatter_occipital=setAssessment_regions_deepWhiteMatter_occipital;

	this.Assessment_regions_deepWhiteMatter_occipital_AssessmentRegionsDeepWhiteMatterOccipitalCndaModifiedsch=null;


	function getAssessment_regions_deepWhiteMatter_occipital_AssessmentRegionsDeepWhiteMatterOccipitalCndaModifiedsch(){
		return this.Assessment_regions_deepWhiteMatter_occipital_AssessmentRegionsDeepWhiteMatterOccipitalCndaModifiedsch;
	}
	this.getAssessment_regions_deepWhiteMatter_occipital_AssessmentRegionsDeepWhiteMatterOccipitalCndaModifiedsch=getAssessment_regions_deepWhiteMatter_occipital_AssessmentRegionsDeepWhiteMatterOccipitalCndaModifiedsch;


	function setAssessment_regions_deepWhiteMatter_occipital_AssessmentRegionsDeepWhiteMatterOccipitalCndaModifiedsch(v){
		this.Assessment_regions_deepWhiteMatter_occipital_AssessmentRegionsDeepWhiteMatterOccipitalCndaModifiedsch=v;
	}
	this.setAssessment_regions_deepWhiteMatter_occipital_AssessmentRegionsDeepWhiteMatterOccipitalCndaModifiedsch=setAssessment_regions_deepWhiteMatter_occipital_AssessmentRegionsDeepWhiteMatterOccipitalCndaModifiedsch;
	this.Assessment_regions_deepWhiteMatter_temporal =null;
	function getAssessment_regions_deepWhiteMatter_temporal() {
		return this.Assessment_regions_deepWhiteMatter_temporal;
	}
	this.getAssessment_regions_deepWhiteMatter_temporal=getAssessment_regions_deepWhiteMatter_temporal;


	function setAssessment_regions_deepWhiteMatter_temporal(v){
		this.Assessment_regions_deepWhiteMatter_temporal =v;
	}
	this.setAssessment_regions_deepWhiteMatter_temporal=setAssessment_regions_deepWhiteMatter_temporal;

	this.Assessment_regions_deepWhiteMatter_temporal_AssessmentRegionsDeepWhiteMatterTemporalCndaModifiedsche=null;


	function getAssessment_regions_deepWhiteMatter_temporal_AssessmentRegionsDeepWhiteMatterTemporalCndaModifiedsche(){
		return this.Assessment_regions_deepWhiteMatter_temporal_AssessmentRegionsDeepWhiteMatterTemporalCndaModifiedsche;
	}
	this.getAssessment_regions_deepWhiteMatter_temporal_AssessmentRegionsDeepWhiteMatterTemporalCndaModifiedsche=getAssessment_regions_deepWhiteMatter_temporal_AssessmentRegionsDeepWhiteMatterTemporalCndaModifiedsche;


	function setAssessment_regions_deepWhiteMatter_temporal_AssessmentRegionsDeepWhiteMatterTemporalCndaModifiedsche(v){
		this.Assessment_regions_deepWhiteMatter_temporal_AssessmentRegionsDeepWhiteMatterTemporalCndaModifiedsche=v;
	}
	this.setAssessment_regions_deepWhiteMatter_temporal_AssessmentRegionsDeepWhiteMatterTemporalCndaModifiedsche=setAssessment_regions_deepWhiteMatter_temporal_AssessmentRegionsDeepWhiteMatterTemporalCndaModifiedsche;
	this.Assessment_regions_periventricular_frontalHorns =null;
	function getAssessment_regions_periventricular_frontalHorns() {
		return this.Assessment_regions_periventricular_frontalHorns;
	}
	this.getAssessment_regions_periventricular_frontalHorns=getAssessment_regions_periventricular_frontalHorns;


	function setAssessment_regions_periventricular_frontalHorns(v){
		this.Assessment_regions_periventricular_frontalHorns =v;
	}
	this.setAssessment_regions_periventricular_frontalHorns=setAssessment_regions_periventricular_frontalHorns;

	this.Assessment_regions_periventricular_frontalHorns_AssessmentRegionsPeriventricularFrontalHornsCndaModifieds=null;


	function getAssessment_regions_periventricular_frontalHorns_AssessmentRegionsPeriventricularFrontalHornsCndaModifieds(){
		return this.Assessment_regions_periventricular_frontalHorns_AssessmentRegionsPeriventricularFrontalHornsCndaModifieds;
	}
	this.getAssessment_regions_periventricular_frontalHorns_AssessmentRegionsPeriventricularFrontalHornsCndaModifieds=getAssessment_regions_periventricular_frontalHorns_AssessmentRegionsPeriventricularFrontalHornsCndaModifieds;


	function setAssessment_regions_periventricular_frontalHorns_AssessmentRegionsPeriventricularFrontalHornsCndaModifieds(v){
		this.Assessment_regions_periventricular_frontalHorns_AssessmentRegionsPeriventricularFrontalHornsCndaModifieds=v;
	}
	this.setAssessment_regions_periventricular_frontalHorns_AssessmentRegionsPeriventricularFrontalHornsCndaModifieds=setAssessment_regions_periventricular_frontalHorns_AssessmentRegionsPeriventricularFrontalHornsCndaModifieds;
	this.Assessment_regions_periventricular_posteriorHorns =null;
	function getAssessment_regions_periventricular_posteriorHorns() {
		return this.Assessment_regions_periventricular_posteriorHorns;
	}
	this.getAssessment_regions_periventricular_posteriorHorns=getAssessment_regions_periventricular_posteriorHorns;


	function setAssessment_regions_periventricular_posteriorHorns(v){
		this.Assessment_regions_periventricular_posteriorHorns =v;
	}
	this.setAssessment_regions_periventricular_posteriorHorns=setAssessment_regions_periventricular_posteriorHorns;

	this.Assessment_regions_periventricular_posteriorHorns_AssessmentRegionsPeriventricularPosteriorHornsCndaModifie=null;


	function getAssessment_regions_periventricular_posteriorHorns_AssessmentRegionsPeriventricularPosteriorHornsCndaModifie(){
		return this.Assessment_regions_periventricular_posteriorHorns_AssessmentRegionsPeriventricularPosteriorHornsCndaModifie;
	}
	this.getAssessment_regions_periventricular_posteriorHorns_AssessmentRegionsPeriventricularPosteriorHornsCndaModifie=getAssessment_regions_periventricular_posteriorHorns_AssessmentRegionsPeriventricularPosteriorHornsCndaModifie;


	function setAssessment_regions_periventricular_posteriorHorns_AssessmentRegionsPeriventricularPosteriorHornsCndaModifie(v){
		this.Assessment_regions_periventricular_posteriorHorns_AssessmentRegionsPeriventricularPosteriorHornsCndaModifie=v;
	}
	this.setAssessment_regions_periventricular_posteriorHorns_AssessmentRegionsPeriventricularPosteriorHornsCndaModifie=setAssessment_regions_periventricular_posteriorHorns_AssessmentRegionsPeriventricularPosteriorHornsCndaModifie;
	this.Assessment_regions_periventricular_ventricularBody =null;
	function getAssessment_regions_periventricular_ventricularBody() {
		return this.Assessment_regions_periventricular_ventricularBody;
	}
	this.getAssessment_regions_periventricular_ventricularBody=getAssessment_regions_periventricular_ventricularBody;


	function setAssessment_regions_periventricular_ventricularBody(v){
		this.Assessment_regions_periventricular_ventricularBody =v;
	}
	this.setAssessment_regions_periventricular_ventricularBody=setAssessment_regions_periventricular_ventricularBody;

	this.Assessment_regions_periventricular_ventricularBody_AssessmentRegionsPeriventricularVentricularBodyCndaModifi=null;


	function getAssessment_regions_periventricular_ventricularBody_AssessmentRegionsPeriventricularVentricularBodyCndaModifi(){
		return this.Assessment_regions_periventricular_ventricularBody_AssessmentRegionsPeriventricularVentricularBodyCndaModifi;
	}
	this.getAssessment_regions_periventricular_ventricularBody_AssessmentRegionsPeriventricularVentricularBodyCndaModifi=getAssessment_regions_periventricular_ventricularBody_AssessmentRegionsPeriventricularVentricularBodyCndaModifi;


	function setAssessment_regions_periventricular_ventricularBody_AssessmentRegionsPeriventricularVentricularBodyCndaModifi(v){
		this.Assessment_regions_periventricular_ventricularBody_AssessmentRegionsPeriventricularVentricularBodyCndaModifi=v;
	}
	this.setAssessment_regions_periventricular_ventricularBody_AssessmentRegionsPeriventricularVentricularBodyCndaModifi=setAssessment_regions_periventricular_ventricularBody_AssessmentRegionsPeriventricularVentricularBodyCndaModifi;
	this.Assessment_regions_basalGanglia_internalCapsule =null;
	function getAssessment_regions_basalGanglia_internalCapsule() {
		return this.Assessment_regions_basalGanglia_internalCapsule;
	}
	this.getAssessment_regions_basalGanglia_internalCapsule=getAssessment_regions_basalGanglia_internalCapsule;


	function setAssessment_regions_basalGanglia_internalCapsule(v){
		this.Assessment_regions_basalGanglia_internalCapsule =v;
	}
	this.setAssessment_regions_basalGanglia_internalCapsule=setAssessment_regions_basalGanglia_internalCapsule;

	this.Assessment_regions_basalGanglia_internalCapsule_AssessmentRegionsBasalGangliaInternalCapsuleCndaModified=null;


	function getAssessment_regions_basalGanglia_internalCapsule_AssessmentRegionsBasalGangliaInternalCapsuleCndaModified(){
		return this.Assessment_regions_basalGanglia_internalCapsule_AssessmentRegionsBasalGangliaInternalCapsuleCndaModified;
	}
	this.getAssessment_regions_basalGanglia_internalCapsule_AssessmentRegionsBasalGangliaInternalCapsuleCndaModified=getAssessment_regions_basalGanglia_internalCapsule_AssessmentRegionsBasalGangliaInternalCapsuleCndaModified;


	function setAssessment_regions_basalGanglia_internalCapsule_AssessmentRegionsBasalGangliaInternalCapsuleCndaModified(v){
		this.Assessment_regions_basalGanglia_internalCapsule_AssessmentRegionsBasalGangliaInternalCapsuleCndaModified=v;
	}
	this.setAssessment_regions_basalGanglia_internalCapsule_AssessmentRegionsBasalGangliaInternalCapsuleCndaModified=setAssessment_regions_basalGanglia_internalCapsule_AssessmentRegionsBasalGangliaInternalCapsuleCndaModified;
	this.Assessment_regions_basalGanglia_caudate =null;
	function getAssessment_regions_basalGanglia_caudate() {
		return this.Assessment_regions_basalGanglia_caudate;
	}
	this.getAssessment_regions_basalGanglia_caudate=getAssessment_regions_basalGanglia_caudate;


	function setAssessment_regions_basalGanglia_caudate(v){
		this.Assessment_regions_basalGanglia_caudate =v;
	}
	this.setAssessment_regions_basalGanglia_caudate=setAssessment_regions_basalGanglia_caudate;

	this.Assessment_regions_basalGanglia_caudate_AssessmentRegionsBasalGangliaCaudateCndaModifiedscheltens=null;


	function getAssessment_regions_basalGanglia_caudate_AssessmentRegionsBasalGangliaCaudateCndaModifiedscheltens(){
		return this.Assessment_regions_basalGanglia_caudate_AssessmentRegionsBasalGangliaCaudateCndaModifiedscheltens;
	}
	this.getAssessment_regions_basalGanglia_caudate_AssessmentRegionsBasalGangliaCaudateCndaModifiedscheltens=getAssessment_regions_basalGanglia_caudate_AssessmentRegionsBasalGangliaCaudateCndaModifiedscheltens;


	function setAssessment_regions_basalGanglia_caudate_AssessmentRegionsBasalGangliaCaudateCndaModifiedscheltens(v){
		this.Assessment_regions_basalGanglia_caudate_AssessmentRegionsBasalGangliaCaudateCndaModifiedscheltens=v;
	}
	this.setAssessment_regions_basalGanglia_caudate_AssessmentRegionsBasalGangliaCaudateCndaModifiedscheltens=setAssessment_regions_basalGanglia_caudate_AssessmentRegionsBasalGangliaCaudateCndaModifiedscheltens;
	this.Assessment_regions_basalGanglia_putamen =null;
	function getAssessment_regions_basalGanglia_putamen() {
		return this.Assessment_regions_basalGanglia_putamen;
	}
	this.getAssessment_regions_basalGanglia_putamen=getAssessment_regions_basalGanglia_putamen;


	function setAssessment_regions_basalGanglia_putamen(v){
		this.Assessment_regions_basalGanglia_putamen =v;
	}
	this.setAssessment_regions_basalGanglia_putamen=setAssessment_regions_basalGanglia_putamen;

	this.Assessment_regions_basalGanglia_putamen_AssessmentRegionsBasalGangliaPutamenCndaModifiedscheltens=null;


	function getAssessment_regions_basalGanglia_putamen_AssessmentRegionsBasalGangliaPutamenCndaModifiedscheltens(){
		return this.Assessment_regions_basalGanglia_putamen_AssessmentRegionsBasalGangliaPutamenCndaModifiedscheltens;
	}
	this.getAssessment_regions_basalGanglia_putamen_AssessmentRegionsBasalGangliaPutamenCndaModifiedscheltens=getAssessment_regions_basalGanglia_putamen_AssessmentRegionsBasalGangliaPutamenCndaModifiedscheltens;


	function setAssessment_regions_basalGanglia_putamen_AssessmentRegionsBasalGangliaPutamenCndaModifiedscheltens(v){
		this.Assessment_regions_basalGanglia_putamen_AssessmentRegionsBasalGangliaPutamenCndaModifiedscheltens=v;
	}
	this.setAssessment_regions_basalGanglia_putamen_AssessmentRegionsBasalGangliaPutamenCndaModifiedscheltens=setAssessment_regions_basalGanglia_putamen_AssessmentRegionsBasalGangliaPutamenCndaModifiedscheltens;
	this.Assessment_regions_basalGanglia_globusPallidus =null;
	function getAssessment_regions_basalGanglia_globusPallidus() {
		return this.Assessment_regions_basalGanglia_globusPallidus;
	}
	this.getAssessment_regions_basalGanglia_globusPallidus=getAssessment_regions_basalGanglia_globusPallidus;


	function setAssessment_regions_basalGanglia_globusPallidus(v){
		this.Assessment_regions_basalGanglia_globusPallidus =v;
	}
	this.setAssessment_regions_basalGanglia_globusPallidus=setAssessment_regions_basalGanglia_globusPallidus;

	this.Assessment_regions_basalGanglia_globusPallidus_AssessmentRegionsBasalGangliaGlobusPallidusCndaModifieds=null;


	function getAssessment_regions_basalGanglia_globusPallidus_AssessmentRegionsBasalGangliaGlobusPallidusCndaModifieds(){
		return this.Assessment_regions_basalGanglia_globusPallidus_AssessmentRegionsBasalGangliaGlobusPallidusCndaModifieds;
	}
	this.getAssessment_regions_basalGanglia_globusPallidus_AssessmentRegionsBasalGangliaGlobusPallidusCndaModifieds=getAssessment_regions_basalGanglia_globusPallidus_AssessmentRegionsBasalGangliaGlobusPallidusCndaModifieds;


	function setAssessment_regions_basalGanglia_globusPallidus_AssessmentRegionsBasalGangliaGlobusPallidusCndaModifieds(v){
		this.Assessment_regions_basalGanglia_globusPallidus_AssessmentRegionsBasalGangliaGlobusPallidusCndaModifieds=v;
	}
	this.setAssessment_regions_basalGanglia_globusPallidus_AssessmentRegionsBasalGangliaGlobusPallidusCndaModifieds=setAssessment_regions_basalGanglia_globusPallidus_AssessmentRegionsBasalGangliaGlobusPallidusCndaModifieds;
	this.Assessment_regions_basalGanglia_thalamus =null;
	function getAssessment_regions_basalGanglia_thalamus() {
		return this.Assessment_regions_basalGanglia_thalamus;
	}
	this.getAssessment_regions_basalGanglia_thalamus=getAssessment_regions_basalGanglia_thalamus;


	function setAssessment_regions_basalGanglia_thalamus(v){
		this.Assessment_regions_basalGanglia_thalamus =v;
	}
	this.setAssessment_regions_basalGanglia_thalamus=setAssessment_regions_basalGanglia_thalamus;

	this.Assessment_regions_basalGanglia_thalamus_AssessmentRegionsBasalGangliaThalamusCndaModifiedschelten=null;


	function getAssessment_regions_basalGanglia_thalamus_AssessmentRegionsBasalGangliaThalamusCndaModifiedschelten(){
		return this.Assessment_regions_basalGanglia_thalamus_AssessmentRegionsBasalGangliaThalamusCndaModifiedschelten;
	}
	this.getAssessment_regions_basalGanglia_thalamus_AssessmentRegionsBasalGangliaThalamusCndaModifiedschelten=getAssessment_regions_basalGanglia_thalamus_AssessmentRegionsBasalGangliaThalamusCndaModifiedschelten;


	function setAssessment_regions_basalGanglia_thalamus_AssessmentRegionsBasalGangliaThalamusCndaModifiedschelten(v){
		this.Assessment_regions_basalGanglia_thalamus_AssessmentRegionsBasalGangliaThalamusCndaModifiedschelten=v;
	}
	this.setAssessment_regions_basalGanglia_thalamus_AssessmentRegionsBasalGangliaThalamusCndaModifiedschelten=setAssessment_regions_basalGanglia_thalamus_AssessmentRegionsBasalGangliaThalamusCndaModifiedschelten;

	this.Assessment_virchowRobin_present=null;


	function getAssessment_virchowRobin_present() {
		return this.Assessment_virchowRobin_present;
	}
	this.getAssessment_virchowRobin_present=getAssessment_virchowRobin_present;


	function setAssessment_virchowRobin_present(v){
		this.Assessment_virchowRobin_present=v;
	}
	this.setAssessment_virchowRobin_present=setAssessment_virchowRobin_present;


	this.isAssessment_virchowRobin_present=function(defaultValue) {
		if(this.Assessment_virchowRobin_present==null)return defaultValue;
		if(this.Assessment_virchowRobin_present=="1" || this.Assessment_virchowRobin_present==true)return true;
		return false;
	}

	this.Assessment_virchowRobin_location=null;


	function getAssessment_virchowRobin_location() {
		return this.Assessment_virchowRobin_location;
	}
	this.getAssessment_virchowRobin_location=getAssessment_virchowRobin_location;


	function setAssessment_virchowRobin_location(v){
		this.Assessment_virchowRobin_location=v;
	}
	this.setAssessment_virchowRobin_location=setAssessment_virchowRobin_location;

	this.Assessment_virchowRobin_potentialContribution=null;


	function getAssessment_virchowRobin_potentialContribution() {
		return this.Assessment_virchowRobin_potentialContribution;
	}
	this.getAssessment_virchowRobin_potentialContribution=getAssessment_virchowRobin_potentialContribution;


	function setAssessment_virchowRobin_potentialContribution(v){
		this.Assessment_virchowRobin_potentialContribution=v;
	}
	this.setAssessment_virchowRobin_potentialContribution=setAssessment_virchowRobin_potentialContribution;


	this.isAssessment_virchowRobin_potentialContribution=function(defaultValue) {
		if(this.Assessment_virchowRobin_potentialContribution==null)return defaultValue;
		if(this.Assessment_virchowRobin_potentialContribution=="1" || this.Assessment_virchowRobin_potentialContribution==true)return true;
		return false;
	}

	this.Assessment_virchowRobin_contributionLocation=null;


	function getAssessment_virchowRobin_contributionLocation() {
		return this.Assessment_virchowRobin_contributionLocation;
	}
	this.getAssessment_virchowRobin_contributionLocation=getAssessment_virchowRobin_contributionLocation;


	function setAssessment_virchowRobin_contributionLocation(v){
		this.Assessment_virchowRobin_contributionLocation=v;
	}
	this.setAssessment_virchowRobin_contributionLocation=setAssessment_virchowRobin_contributionLocation;

	this.Assessment_confluent=null;


	function getAssessment_confluent() {
		return this.Assessment_confluent;
	}
	this.getAssessment_confluent=getAssessment_confluent;


	function setAssessment_confluent(v){
		this.Assessment_confluent=v;
	}
	this.setAssessment_confluent=setAssessment_confluent;


	this.isAssessment_confluent=function(defaultValue) {
		if(this.Assessment_confluent==null)return defaultValue;
		if(this.Assessment_confluent=="1" || this.Assessment_confluent==true)return true;
		return false;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="mrAssessorData"){
				return this.Mrassessordata ;
			} else 
			if(xmlPath.startsWith("mrAssessorData")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Mrassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Mrassessordata!=undefined)return this.Mrassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/regions/deep_white_matter/frontal"){
				return this.Assessment_regions_deepWhiteMatter_frontal ;
			} else 
			if(xmlPath.startsWith("assessment/regions/deep_white_matter/frontal")){
				xmlPath=xmlPath.substring(44);
				if(xmlPath=="")return this.Assessment_regions_deepWhiteMatter_frontal ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_deepWhiteMatter_frontal!=undefined)return this.Assessment_regions_deepWhiteMatter_frontal.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/regions/deep_white_matter/parietal"){
				return this.Assessment_regions_deepWhiteMatter_parietal ;
			} else 
			if(xmlPath.startsWith("assessment/regions/deep_white_matter/parietal")){
				xmlPath=xmlPath.substring(45);
				if(xmlPath=="")return this.Assessment_regions_deepWhiteMatter_parietal ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_deepWhiteMatter_parietal!=undefined)return this.Assessment_regions_deepWhiteMatter_parietal.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/regions/deep_white_matter/occipital"){
				return this.Assessment_regions_deepWhiteMatter_occipital ;
			} else 
			if(xmlPath.startsWith("assessment/regions/deep_white_matter/occipital")){
				xmlPath=xmlPath.substring(46);
				if(xmlPath=="")return this.Assessment_regions_deepWhiteMatter_occipital ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_deepWhiteMatter_occipital!=undefined)return this.Assessment_regions_deepWhiteMatter_occipital.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/regions/deep_white_matter/temporal"){
				return this.Assessment_regions_deepWhiteMatter_temporal ;
			} else 
			if(xmlPath.startsWith("assessment/regions/deep_white_matter/temporal")){
				xmlPath=xmlPath.substring(45);
				if(xmlPath=="")return this.Assessment_regions_deepWhiteMatter_temporal ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_deepWhiteMatter_temporal!=undefined)return this.Assessment_regions_deepWhiteMatter_temporal.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/regions/periventricular/frontal_horns"){
				return this.Assessment_regions_periventricular_frontalHorns ;
			} else 
			if(xmlPath.startsWith("assessment/regions/periventricular/frontal_horns")){
				xmlPath=xmlPath.substring(48);
				if(xmlPath=="")return this.Assessment_regions_periventricular_frontalHorns ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_periventricular_frontalHorns!=undefined)return this.Assessment_regions_periventricular_frontalHorns.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/regions/periventricular/posterior_horns"){
				return this.Assessment_regions_periventricular_posteriorHorns ;
			} else 
			if(xmlPath.startsWith("assessment/regions/periventricular/posterior_horns")){
				xmlPath=xmlPath.substring(50);
				if(xmlPath=="")return this.Assessment_regions_periventricular_posteriorHorns ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_periventricular_posteriorHorns!=undefined)return this.Assessment_regions_periventricular_posteriorHorns.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/regions/periventricular/ventricular_body"){
				return this.Assessment_regions_periventricular_ventricularBody ;
			} else 
			if(xmlPath.startsWith("assessment/regions/periventricular/ventricular_body")){
				xmlPath=xmlPath.substring(51);
				if(xmlPath=="")return this.Assessment_regions_periventricular_ventricularBody ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_periventricular_ventricularBody!=undefined)return this.Assessment_regions_periventricular_ventricularBody.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/regions/basal_ganglia/internal_capsule"){
				return this.Assessment_regions_basalGanglia_internalCapsule ;
			} else 
			if(xmlPath.startsWith("assessment/regions/basal_ganglia/internal_capsule")){
				xmlPath=xmlPath.substring(49);
				if(xmlPath=="")return this.Assessment_regions_basalGanglia_internalCapsule ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_basalGanglia_internalCapsule!=undefined)return this.Assessment_regions_basalGanglia_internalCapsule.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/regions/basal_ganglia/caudate"){
				return this.Assessment_regions_basalGanglia_caudate ;
			} else 
			if(xmlPath.startsWith("assessment/regions/basal_ganglia/caudate")){
				xmlPath=xmlPath.substring(40);
				if(xmlPath=="")return this.Assessment_regions_basalGanglia_caudate ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_basalGanglia_caudate!=undefined)return this.Assessment_regions_basalGanglia_caudate.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/regions/basal_ganglia/putamen"){
				return this.Assessment_regions_basalGanglia_putamen ;
			} else 
			if(xmlPath.startsWith("assessment/regions/basal_ganglia/putamen")){
				xmlPath=xmlPath.substring(40);
				if(xmlPath=="")return this.Assessment_regions_basalGanglia_putamen ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_basalGanglia_putamen!=undefined)return this.Assessment_regions_basalGanglia_putamen.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/regions/basal_ganglia/globus_pallidus"){
				return this.Assessment_regions_basalGanglia_globusPallidus ;
			} else 
			if(xmlPath.startsWith("assessment/regions/basal_ganglia/globus_pallidus")){
				xmlPath=xmlPath.substring(48);
				if(xmlPath=="")return this.Assessment_regions_basalGanglia_globusPallidus ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_basalGanglia_globusPallidus!=undefined)return this.Assessment_regions_basalGanglia_globusPallidus.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/regions/basal_ganglia/thalamus"){
				return this.Assessment_regions_basalGanglia_thalamus ;
			} else 
			if(xmlPath.startsWith("assessment/regions/basal_ganglia/thalamus")){
				xmlPath=xmlPath.substring(41);
				if(xmlPath=="")return this.Assessment_regions_basalGanglia_thalamus ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_basalGanglia_thalamus!=undefined)return this.Assessment_regions_basalGanglia_thalamus.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="assessment/virchow_robin/present"){
				return this.Assessment_virchowRobin_present ;
			} else 
			if(xmlPath=="assessment/virchow_robin/location"){
				return this.Assessment_virchowRobin_location ;
			} else 
			if(xmlPath=="assessment/virchow_robin/potential_contribution"){
				return this.Assessment_virchowRobin_potentialContribution ;
			} else 
			if(xmlPath=="assessment/virchow_robin/contribution_location"){
				return this.Assessment_virchowRobin_contributionLocation ;
			} else 
			if(xmlPath=="assessment/confluent"){
				return this.Assessment_confluent ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="mrAssessorData"){
				this.Mrassessordata=value;
			} else 
			if(xmlPath.startsWith("mrAssessorData")){
				xmlPath=xmlPath.substring(14);
				if(xmlPath=="")return this.Mrassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Mrassessordata!=undefined){
					this.Mrassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Mrassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Mrassessordata= instanciateObject("xnat:mrAssessorData");//omUtils.js
						}
						if(options && options.where)this.Mrassessordata.setProperty(options.where.field,options.where.value);
						this.Mrassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/regions/deep_white_matter/frontal"){
				this.Assessment_regions_deepWhiteMatter_frontal=value;
			} else 
			if(xmlPath.startsWith("assessment/regions/deep_white_matter/frontal")){
				xmlPath=xmlPath.substring(44);
				if(xmlPath=="")return this.Assessment_regions_deepWhiteMatter_frontal ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_deepWhiteMatter_frontal!=undefined){
					this.Assessment_regions_deepWhiteMatter_frontal.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Assessment_regions_deepWhiteMatter_frontal= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Assessment_regions_deepWhiteMatter_frontal= instanciateObject("cnda:modifiedScheltensRegion");//omUtils.js
						}
						if(options && options.where)this.Assessment_regions_deepWhiteMatter_frontal.setProperty(options.where.field,options.where.value);
						this.Assessment_regions_deepWhiteMatter_frontal.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/regions/deep_white_matter/parietal"){
				this.Assessment_regions_deepWhiteMatter_parietal=value;
			} else 
			if(xmlPath.startsWith("assessment/regions/deep_white_matter/parietal")){
				xmlPath=xmlPath.substring(45);
				if(xmlPath=="")return this.Assessment_regions_deepWhiteMatter_parietal ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_deepWhiteMatter_parietal!=undefined){
					this.Assessment_regions_deepWhiteMatter_parietal.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Assessment_regions_deepWhiteMatter_parietal= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Assessment_regions_deepWhiteMatter_parietal= instanciateObject("cnda:modifiedScheltensRegion");//omUtils.js
						}
						if(options && options.where)this.Assessment_regions_deepWhiteMatter_parietal.setProperty(options.where.field,options.where.value);
						this.Assessment_regions_deepWhiteMatter_parietal.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/regions/deep_white_matter/occipital"){
				this.Assessment_regions_deepWhiteMatter_occipital=value;
			} else 
			if(xmlPath.startsWith("assessment/regions/deep_white_matter/occipital")){
				xmlPath=xmlPath.substring(46);
				if(xmlPath=="")return this.Assessment_regions_deepWhiteMatter_occipital ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_deepWhiteMatter_occipital!=undefined){
					this.Assessment_regions_deepWhiteMatter_occipital.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Assessment_regions_deepWhiteMatter_occipital= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Assessment_regions_deepWhiteMatter_occipital= instanciateObject("cnda:modifiedScheltensRegion");//omUtils.js
						}
						if(options && options.where)this.Assessment_regions_deepWhiteMatter_occipital.setProperty(options.where.field,options.where.value);
						this.Assessment_regions_deepWhiteMatter_occipital.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/regions/deep_white_matter/temporal"){
				this.Assessment_regions_deepWhiteMatter_temporal=value;
			} else 
			if(xmlPath.startsWith("assessment/regions/deep_white_matter/temporal")){
				xmlPath=xmlPath.substring(45);
				if(xmlPath=="")return this.Assessment_regions_deepWhiteMatter_temporal ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_deepWhiteMatter_temporal!=undefined){
					this.Assessment_regions_deepWhiteMatter_temporal.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Assessment_regions_deepWhiteMatter_temporal= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Assessment_regions_deepWhiteMatter_temporal= instanciateObject("cnda:modifiedScheltensRegion");//omUtils.js
						}
						if(options && options.where)this.Assessment_regions_deepWhiteMatter_temporal.setProperty(options.where.field,options.where.value);
						this.Assessment_regions_deepWhiteMatter_temporal.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/regions/periventricular/frontal_horns"){
				this.Assessment_regions_periventricular_frontalHorns=value;
			} else 
			if(xmlPath.startsWith("assessment/regions/periventricular/frontal_horns")){
				xmlPath=xmlPath.substring(48);
				if(xmlPath=="")return this.Assessment_regions_periventricular_frontalHorns ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_periventricular_frontalHorns!=undefined){
					this.Assessment_regions_periventricular_frontalHorns.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Assessment_regions_periventricular_frontalHorns= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Assessment_regions_periventricular_frontalHorns= instanciateObject("cnda:modifiedScheltensPvRegion");//omUtils.js
						}
						if(options && options.where)this.Assessment_regions_periventricular_frontalHorns.setProperty(options.where.field,options.where.value);
						this.Assessment_regions_periventricular_frontalHorns.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/regions/periventricular/posterior_horns"){
				this.Assessment_regions_periventricular_posteriorHorns=value;
			} else 
			if(xmlPath.startsWith("assessment/regions/periventricular/posterior_horns")){
				xmlPath=xmlPath.substring(50);
				if(xmlPath=="")return this.Assessment_regions_periventricular_posteriorHorns ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_periventricular_posteriorHorns!=undefined){
					this.Assessment_regions_periventricular_posteriorHorns.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Assessment_regions_periventricular_posteriorHorns= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Assessment_regions_periventricular_posteriorHorns= instanciateObject("cnda:modifiedScheltensPvRegion");//omUtils.js
						}
						if(options && options.where)this.Assessment_regions_periventricular_posteriorHorns.setProperty(options.where.field,options.where.value);
						this.Assessment_regions_periventricular_posteriorHorns.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/regions/periventricular/ventricular_body"){
				this.Assessment_regions_periventricular_ventricularBody=value;
			} else 
			if(xmlPath.startsWith("assessment/regions/periventricular/ventricular_body")){
				xmlPath=xmlPath.substring(51);
				if(xmlPath=="")return this.Assessment_regions_periventricular_ventricularBody ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_periventricular_ventricularBody!=undefined){
					this.Assessment_regions_periventricular_ventricularBody.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Assessment_regions_periventricular_ventricularBody= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Assessment_regions_periventricular_ventricularBody= instanciateObject("cnda:modifiedScheltensPvRegion");//omUtils.js
						}
						if(options && options.where)this.Assessment_regions_periventricular_ventricularBody.setProperty(options.where.field,options.where.value);
						this.Assessment_regions_periventricular_ventricularBody.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/regions/basal_ganglia/internal_capsule"){
				this.Assessment_regions_basalGanglia_internalCapsule=value;
			} else 
			if(xmlPath.startsWith("assessment/regions/basal_ganglia/internal_capsule")){
				xmlPath=xmlPath.substring(49);
				if(xmlPath=="")return this.Assessment_regions_basalGanglia_internalCapsule ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_basalGanglia_internalCapsule!=undefined){
					this.Assessment_regions_basalGanglia_internalCapsule.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Assessment_regions_basalGanglia_internalCapsule= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Assessment_regions_basalGanglia_internalCapsule= instanciateObject("cnda:modifiedScheltensRegion");//omUtils.js
						}
						if(options && options.where)this.Assessment_regions_basalGanglia_internalCapsule.setProperty(options.where.field,options.where.value);
						this.Assessment_regions_basalGanglia_internalCapsule.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/regions/basal_ganglia/caudate"){
				this.Assessment_regions_basalGanglia_caudate=value;
			} else 
			if(xmlPath.startsWith("assessment/regions/basal_ganglia/caudate")){
				xmlPath=xmlPath.substring(40);
				if(xmlPath=="")return this.Assessment_regions_basalGanglia_caudate ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_basalGanglia_caudate!=undefined){
					this.Assessment_regions_basalGanglia_caudate.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Assessment_regions_basalGanglia_caudate= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Assessment_regions_basalGanglia_caudate= instanciateObject("cnda:modifiedScheltensRegion");//omUtils.js
						}
						if(options && options.where)this.Assessment_regions_basalGanglia_caudate.setProperty(options.where.field,options.where.value);
						this.Assessment_regions_basalGanglia_caudate.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/regions/basal_ganglia/putamen"){
				this.Assessment_regions_basalGanglia_putamen=value;
			} else 
			if(xmlPath.startsWith("assessment/regions/basal_ganglia/putamen")){
				xmlPath=xmlPath.substring(40);
				if(xmlPath=="")return this.Assessment_regions_basalGanglia_putamen ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_basalGanglia_putamen!=undefined){
					this.Assessment_regions_basalGanglia_putamen.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Assessment_regions_basalGanglia_putamen= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Assessment_regions_basalGanglia_putamen= instanciateObject("cnda:modifiedScheltensRegion");//omUtils.js
						}
						if(options && options.where)this.Assessment_regions_basalGanglia_putamen.setProperty(options.where.field,options.where.value);
						this.Assessment_regions_basalGanglia_putamen.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/regions/basal_ganglia/globus_pallidus"){
				this.Assessment_regions_basalGanglia_globusPallidus=value;
			} else 
			if(xmlPath.startsWith("assessment/regions/basal_ganglia/globus_pallidus")){
				xmlPath=xmlPath.substring(48);
				if(xmlPath=="")return this.Assessment_regions_basalGanglia_globusPallidus ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_basalGanglia_globusPallidus!=undefined){
					this.Assessment_regions_basalGanglia_globusPallidus.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Assessment_regions_basalGanglia_globusPallidus= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Assessment_regions_basalGanglia_globusPallidus= instanciateObject("cnda:modifiedScheltensRegion");//omUtils.js
						}
						if(options && options.where)this.Assessment_regions_basalGanglia_globusPallidus.setProperty(options.where.field,options.where.value);
						this.Assessment_regions_basalGanglia_globusPallidus.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/regions/basal_ganglia/thalamus"){
				this.Assessment_regions_basalGanglia_thalamus=value;
			} else 
			if(xmlPath.startsWith("assessment/regions/basal_ganglia/thalamus")){
				xmlPath=xmlPath.substring(41);
				if(xmlPath=="")return this.Assessment_regions_basalGanglia_thalamus ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Assessment_regions_basalGanglia_thalamus!=undefined){
					this.Assessment_regions_basalGanglia_thalamus.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Assessment_regions_basalGanglia_thalamus= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Assessment_regions_basalGanglia_thalamus= instanciateObject("cnda:modifiedScheltensRegion");//omUtils.js
						}
						if(options && options.where)this.Assessment_regions_basalGanglia_thalamus.setProperty(options.where.field,options.where.value);
						this.Assessment_regions_basalGanglia_thalamus.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="assessment/virchow_robin/present"){
				this.Assessment_virchowRobin_present=value;
			} else 
			if(xmlPath=="assessment/virchow_robin/location"){
				this.Assessment_virchowRobin_location=value;
			} else 
			if(xmlPath=="assessment/virchow_robin/potential_contribution"){
				this.Assessment_virchowRobin_potentialContribution=value;
			} else 
			if(xmlPath=="assessment/virchow_robin/contribution_location"){
				this.Assessment_virchowRobin_contributionLocation=value;
			} else 
			if(xmlPath=="assessment/confluent"){
				this.Assessment_confluent=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="assessment/regions/deep_white_matter/frontal"){
			this.setAssessment_regions_deepWhiteMatter_frontal(v);
		}else if (xmlPath=="assessment/regions/deep_white_matter/parietal"){
			this.setAssessment_regions_deepWhiteMatter_parietal(v);
		}else if (xmlPath=="assessment/regions/deep_white_matter/occipital"){
			this.setAssessment_regions_deepWhiteMatter_occipital(v);
		}else if (xmlPath=="assessment/regions/deep_white_matter/temporal"){
			this.setAssessment_regions_deepWhiteMatter_temporal(v);
		}else if (xmlPath=="assessment/regions/periventricular/frontal_horns"){
			this.setAssessment_regions_periventricular_frontalHorns(v);
		}else if (xmlPath=="assessment/regions/periventricular/posterior_horns"){
			this.setAssessment_regions_periventricular_posteriorHorns(v);
		}else if (xmlPath=="assessment/regions/periventricular/ventricular_body"){
			this.setAssessment_regions_periventricular_ventricularBody(v);
		}else if (xmlPath=="assessment/regions/basal_ganglia/internal_capsule"){
			this.setAssessment_regions_basalGanglia_internalCapsule(v);
		}else if (xmlPath=="assessment/regions/basal_ganglia/caudate"){
			this.setAssessment_regions_basalGanglia_caudate(v);
		}else if (xmlPath=="assessment/regions/basal_ganglia/putamen"){
			this.setAssessment_regions_basalGanglia_putamen(v);
		}else if (xmlPath=="assessment/regions/basal_ganglia/globus_pallidus"){
			this.setAssessment_regions_basalGanglia_globusPallidus(v);
		}else if (xmlPath=="assessment/regions/basal_ganglia/thalamus"){
			this.setAssessment_regions_basalGanglia_thalamus(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="assessment/regions/deep_white_matter/frontal"){
			return "http://nrg.wustl.edu/cnda:modifiedScheltensRegion";
		}else if (xmlPath=="assessment/regions/deep_white_matter/parietal"){
			return "http://nrg.wustl.edu/cnda:modifiedScheltensRegion";
		}else if (xmlPath=="assessment/regions/deep_white_matter/occipital"){
			return "http://nrg.wustl.edu/cnda:modifiedScheltensRegion";
		}else if (xmlPath=="assessment/regions/deep_white_matter/temporal"){
			return "http://nrg.wustl.edu/cnda:modifiedScheltensRegion";
		}else if (xmlPath=="assessment/regions/periventricular/frontal_horns"){
			return "http://nrg.wustl.edu/cnda:modifiedScheltensPvRegion";
		}else if (xmlPath=="assessment/regions/periventricular/posterior_horns"){
			return "http://nrg.wustl.edu/cnda:modifiedScheltensPvRegion";
		}else if (xmlPath=="assessment/regions/periventricular/ventricular_body"){
			return "http://nrg.wustl.edu/cnda:modifiedScheltensPvRegion";
		}else if (xmlPath=="assessment/regions/basal_ganglia/internal_capsule"){
			return "http://nrg.wustl.edu/cnda:modifiedScheltensRegion";
		}else if (xmlPath=="assessment/regions/basal_ganglia/caudate"){
			return "http://nrg.wustl.edu/cnda:modifiedScheltensRegion";
		}else if (xmlPath=="assessment/regions/basal_ganglia/putamen"){
			return "http://nrg.wustl.edu/cnda:modifiedScheltensRegion";
		}else if (xmlPath=="assessment/regions/basal_ganglia/globus_pallidus"){
			return "http://nrg.wustl.edu/cnda:modifiedScheltensRegion";
		}else if (xmlPath=="assessment/regions/basal_ganglia/thalamus"){
			return "http://nrg.wustl.edu/cnda:modifiedScheltensRegion";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="assessment/regions/deep_white_matter/frontal"){
			return "field_single_reference";
		}else if (xmlPath=="assessment/regions/deep_white_matter/parietal"){
			return "field_single_reference";
		}else if (xmlPath=="assessment/regions/deep_white_matter/occipital"){
			return "field_single_reference";
		}else if (xmlPath=="assessment/regions/deep_white_matter/temporal"){
			return "field_single_reference";
		}else if (xmlPath=="assessment/regions/periventricular/frontal_horns"){
			return "field_single_reference";
		}else if (xmlPath=="assessment/regions/periventricular/posterior_horns"){
			return "field_single_reference";
		}else if (xmlPath=="assessment/regions/periventricular/ventricular_body"){
			return "field_single_reference";
		}else if (xmlPath=="assessment/regions/basal_ganglia/internal_capsule"){
			return "field_single_reference";
		}else if (xmlPath=="assessment/regions/basal_ganglia/caudate"){
			return "field_single_reference";
		}else if (xmlPath=="assessment/regions/basal_ganglia/putamen"){
			return "field_single_reference";
		}else if (xmlPath=="assessment/regions/basal_ganglia/globus_pallidus"){
			return "field_single_reference";
		}else if (xmlPath=="assessment/regions/basal_ganglia/thalamus"){
			return "field_single_reference";
		}else if (xmlPath=="assessment/virchow_robin/present"){
			return "field_data";
		}else if (xmlPath=="assessment/virchow_robin/location"){
			return "field_data";
		}else if (xmlPath=="assessment/virchow_robin/potential_contribution"){
			return "field_data";
		}else if (xmlPath=="assessment/virchow_robin/contribution_location"){
			return "field_data";
		}else if (xmlPath=="assessment/confluent"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:ModifiedScheltens";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:ModifiedScheltens>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		var AssessmentATT = ""
		if (this.Assessment_confluent!=null)
			AssessmentATT+=" confluent=\"" + this.Assessment_confluent + "\"";
			var child0=0;
			var att0=0;
			if(this.Assessment_confluent!=null)
			att0++;
			if(this.Assessment_regions_basalGanglia_putamen!=null)
			child0++;
			if(this.Assessment_regions_periventricular_posteriorHorns!=null)
			child0++;
			if(this.Assessment_regions_deepWhiteMatter_temporal!=null)
			child0++;
			if(this.Assessment_regions_basalGanglia_internalCapsule!=null)
			child0++;
			if(this.Assessment_regions_deepWhiteMatter_occipital!=null)
			child0++;
			if(this.Assessment_regions_periventricular_frontalHorns!=null)
			child0++;
			if(this.Assessment_virchowRobin_present!=null)
			child0++;
			if(this.Assessment_virchowRobin_potentialContribution!=null)
			child0++;
			if(this.Assessment_regions_basalGanglia_caudate!=null)
			child0++;
			if(this.Assessment_regions_deepWhiteMatter_frontal!=null)
			child0++;
			if(this.Assessment_regions_basalGanglia_thalamus!=null)
			child0++;
			if(this.Assessment_virchowRobin_contributionLocation!=null)
			child0++;
			if(this.Assessment_regions_periventricular_ventricularBody!=null)
			child0++;
			if(this.Assessment_virchowRobin_location!=null)
			child0++;
			if(this.Assessment_regions_basalGanglia_globusPallidus!=null)
			child0++;
			if(this.Assessment_regions_deepWhiteMatter_parietal!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<cnda:assessment";
				xmlTxt+=AssessmentATT;
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child1=0;
			var att1=0;
			if(this.Assessment_regions_basalGanglia_putamen!=null)
			child1++;
			if(this.Assessment_regions_periventricular_posteriorHorns!=null)
			child1++;
			if(this.Assessment_regions_deepWhiteMatter_temporal!=null)
			child1++;
			if(this.Assessment_regions_basalGanglia_internalCapsule!=null)
			child1++;
			if(this.Assessment_regions_deepWhiteMatter_occipital!=null)
			child1++;
			if(this.Assessment_regions_periventricular_frontalHorns!=null)
			child1++;
			if(this.Assessment_regions_basalGanglia_caudate!=null)
			child1++;
			if(this.Assessment_regions_deepWhiteMatter_frontal!=null)
			child1++;
			if(this.Assessment_regions_basalGanglia_thalamus!=null)
			child1++;
			if(this.Assessment_regions_periventricular_ventricularBody!=null)
			child1++;
			if(this.Assessment_regions_basalGanglia_globusPallidus!=null)
			child1++;
			if(this.Assessment_regions_deepWhiteMatter_parietal!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<cnda:regions";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child2=0;
			var att2=0;
			if(this.Assessment_regions_deepWhiteMatter_parietal!=null)
			child2++;
			if(this.Assessment_regions_deepWhiteMatter_temporal!=null)
			child2++;
			if(this.Assessment_regions_deepWhiteMatter_frontal!=null)
			child2++;
			if(this.Assessment_regions_deepWhiteMatter_occipital!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<cnda:deep_white_matter";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Assessment_regions_deepWhiteMatter_frontal!=null){
			xmlTxt+="\n<cnda:frontal";
			xmlTxt+=this.Assessment_regions_deepWhiteMatter_frontal.getXMLAtts();
			if(this.Assessment_regions_deepWhiteMatter_frontal.xsiType!="cnda:modifiedScheltensRegion"){
				xmlTxt+=" xsi:type=\"" + this.Assessment_regions_deepWhiteMatter_frontal.xsiType + "\"";
			}
			if (this.Assessment_regions_deepWhiteMatter_frontal.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Assessment_regions_deepWhiteMatter_frontal.getXMLBody(preventComments);
				xmlTxt+="</cnda:frontal>";
			}else {xmlTxt+="/>";}
		}
		else{
			xmlTxt+="\n<cnda:frontal/>";//REQUIRED
		}
		if (this.Assessment_regions_deepWhiteMatter_parietal!=null){
			xmlTxt+="\n<cnda:parietal";
			xmlTxt+=this.Assessment_regions_deepWhiteMatter_parietal.getXMLAtts();
			if(this.Assessment_regions_deepWhiteMatter_parietal.xsiType!="cnda:modifiedScheltensRegion"){
				xmlTxt+=" xsi:type=\"" + this.Assessment_regions_deepWhiteMatter_parietal.xsiType + "\"";
			}
			if (this.Assessment_regions_deepWhiteMatter_parietal.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Assessment_regions_deepWhiteMatter_parietal.getXMLBody(preventComments);
				xmlTxt+="</cnda:parietal>";
			}else {xmlTxt+="/>";}
		}
		else{
			xmlTxt+="\n<cnda:parietal/>";//REQUIRED
		}
		if (this.Assessment_regions_deepWhiteMatter_occipital!=null){
			xmlTxt+="\n<cnda:occipital";
			xmlTxt+=this.Assessment_regions_deepWhiteMatter_occipital.getXMLAtts();
			if(this.Assessment_regions_deepWhiteMatter_occipital.xsiType!="cnda:modifiedScheltensRegion"){
				xmlTxt+=" xsi:type=\"" + this.Assessment_regions_deepWhiteMatter_occipital.xsiType + "\"";
			}
			if (this.Assessment_regions_deepWhiteMatter_occipital.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Assessment_regions_deepWhiteMatter_occipital.getXMLBody(preventComments);
				xmlTxt+="</cnda:occipital>";
			}else {xmlTxt+="/>";}
		}
		else{
			xmlTxt+="\n<cnda:occipital/>";//REQUIRED
		}
		if (this.Assessment_regions_deepWhiteMatter_temporal!=null){
			xmlTxt+="\n<cnda:temporal";
			xmlTxt+=this.Assessment_regions_deepWhiteMatter_temporal.getXMLAtts();
			if(this.Assessment_regions_deepWhiteMatter_temporal.xsiType!="cnda:modifiedScheltensRegion"){
				xmlTxt+=" xsi:type=\"" + this.Assessment_regions_deepWhiteMatter_temporal.xsiType + "\"";
			}
			if (this.Assessment_regions_deepWhiteMatter_temporal.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Assessment_regions_deepWhiteMatter_temporal.getXMLBody(preventComments);
				xmlTxt+="</cnda:temporal>";
			}else {xmlTxt+="/>";}
		}
		else{
			xmlTxt+="\n<cnda:temporal/>";//REQUIRED
		}
				xmlTxt+="\n</cnda:deep_white_matter>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.Assessment_regions_periventricular_ventricularBody!=null)
			child3++;
			if(this.Assessment_regions_periventricular_frontalHorns!=null)
			child3++;
			if(this.Assessment_regions_periventricular_posteriorHorns!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<cnda:periventricular";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Assessment_regions_periventricular_frontalHorns!=null){
			xmlTxt+="\n<cnda:frontal_horns";
			xmlTxt+=this.Assessment_regions_periventricular_frontalHorns.getXMLAtts();
			if(this.Assessment_regions_periventricular_frontalHorns.xsiType!="cnda:modifiedScheltensPvRegion"){
				xmlTxt+=" xsi:type=\"" + this.Assessment_regions_periventricular_frontalHorns.xsiType + "\"";
			}
			if (this.Assessment_regions_periventricular_frontalHorns.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Assessment_regions_periventricular_frontalHorns.getXMLBody(preventComments);
				xmlTxt+="</cnda:frontal_horns>";
			}else {xmlTxt+="/>";}
		}
		else{
			xmlTxt+="\n<cnda:frontal_horns/>";//REQUIRED
		}
		if (this.Assessment_regions_periventricular_posteriorHorns!=null){
			xmlTxt+="\n<cnda:posterior_horns";
			xmlTxt+=this.Assessment_regions_periventricular_posteriorHorns.getXMLAtts();
			if(this.Assessment_regions_periventricular_posteriorHorns.xsiType!="cnda:modifiedScheltensPvRegion"){
				xmlTxt+=" xsi:type=\"" + this.Assessment_regions_periventricular_posteriorHorns.xsiType + "\"";
			}
			if (this.Assessment_regions_periventricular_posteriorHorns.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Assessment_regions_periventricular_posteriorHorns.getXMLBody(preventComments);
				xmlTxt+="</cnda:posterior_horns>";
			}else {xmlTxt+="/>";}
		}
		else{
			xmlTxt+="\n<cnda:posterior_horns/>";//REQUIRED
		}
		if (this.Assessment_regions_periventricular_ventricularBody!=null){
			xmlTxt+="\n<cnda:ventricular_body";
			xmlTxt+=this.Assessment_regions_periventricular_ventricularBody.getXMLAtts();
			if(this.Assessment_regions_periventricular_ventricularBody.xsiType!="cnda:modifiedScheltensPvRegion"){
				xmlTxt+=" xsi:type=\"" + this.Assessment_regions_periventricular_ventricularBody.xsiType + "\"";
			}
			if (this.Assessment_regions_periventricular_ventricularBody.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Assessment_regions_periventricular_ventricularBody.getXMLBody(preventComments);
				xmlTxt+="</cnda:ventricular_body>";
			}else {xmlTxt+="/>";}
		}
		else{
			xmlTxt+="\n<cnda:ventricular_body/>";//REQUIRED
		}
				xmlTxt+="\n</cnda:periventricular>";
			}
			}

			var child4=0;
			var att4=0;
			if(this.Assessment_regions_basalGanglia_internalCapsule!=null)
			child4++;
			if(this.Assessment_regions_basalGanglia_thalamus!=null)
			child4++;
			if(this.Assessment_regions_basalGanglia_globusPallidus!=null)
			child4++;
			if(this.Assessment_regions_basalGanglia_putamen!=null)
			child4++;
			if(this.Assessment_regions_basalGanglia_caudate!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<cnda:basal_ganglia";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Assessment_regions_basalGanglia_internalCapsule!=null){
			xmlTxt+="\n<cnda:internal_capsule";
			xmlTxt+=this.Assessment_regions_basalGanglia_internalCapsule.getXMLAtts();
			if(this.Assessment_regions_basalGanglia_internalCapsule.xsiType!="cnda:modifiedScheltensRegion"){
				xmlTxt+=" xsi:type=\"" + this.Assessment_regions_basalGanglia_internalCapsule.xsiType + "\"";
			}
			if (this.Assessment_regions_basalGanglia_internalCapsule.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Assessment_regions_basalGanglia_internalCapsule.getXMLBody(preventComments);
				xmlTxt+="</cnda:internal_capsule>";
			}else {xmlTxt+="/>";}
		}
		else{
			xmlTxt+="\n<cnda:internal_capsule/>";//REQUIRED
		}
		if (this.Assessment_regions_basalGanglia_caudate!=null){
			xmlTxt+="\n<cnda:caudate";
			xmlTxt+=this.Assessment_regions_basalGanglia_caudate.getXMLAtts();
			if(this.Assessment_regions_basalGanglia_caudate.xsiType!="cnda:modifiedScheltensRegion"){
				xmlTxt+=" xsi:type=\"" + this.Assessment_regions_basalGanglia_caudate.xsiType + "\"";
			}
			if (this.Assessment_regions_basalGanglia_caudate.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Assessment_regions_basalGanglia_caudate.getXMLBody(preventComments);
				xmlTxt+="</cnda:caudate>";
			}else {xmlTxt+="/>";}
		}
		else{
			xmlTxt+="\n<cnda:caudate/>";//REQUIRED
		}
		if (this.Assessment_regions_basalGanglia_putamen!=null){
			xmlTxt+="\n<cnda:putamen";
			xmlTxt+=this.Assessment_regions_basalGanglia_putamen.getXMLAtts();
			if(this.Assessment_regions_basalGanglia_putamen.xsiType!="cnda:modifiedScheltensRegion"){
				xmlTxt+=" xsi:type=\"" + this.Assessment_regions_basalGanglia_putamen.xsiType + "\"";
			}
			if (this.Assessment_regions_basalGanglia_putamen.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Assessment_regions_basalGanglia_putamen.getXMLBody(preventComments);
				xmlTxt+="</cnda:putamen>";
			}else {xmlTxt+="/>";}
		}
		else{
			xmlTxt+="\n<cnda:putamen/>";//REQUIRED
		}
		if (this.Assessment_regions_basalGanglia_globusPallidus!=null){
			xmlTxt+="\n<cnda:globus_pallidus";
			xmlTxt+=this.Assessment_regions_basalGanglia_globusPallidus.getXMLAtts();
			if(this.Assessment_regions_basalGanglia_globusPallidus.xsiType!="cnda:modifiedScheltensRegion"){
				xmlTxt+=" xsi:type=\"" + this.Assessment_regions_basalGanglia_globusPallidus.xsiType + "\"";
			}
			if (this.Assessment_regions_basalGanglia_globusPallidus.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Assessment_regions_basalGanglia_globusPallidus.getXMLBody(preventComments);
				xmlTxt+="</cnda:globus_pallidus>";
			}else {xmlTxt+="/>";}
		}
		else{
			xmlTxt+="\n<cnda:globus_pallidus/>";//REQUIRED
		}
		if (this.Assessment_regions_basalGanglia_thalamus!=null){
			xmlTxt+="\n<cnda:thalamus";
			xmlTxt+=this.Assessment_regions_basalGanglia_thalamus.getXMLAtts();
			if(this.Assessment_regions_basalGanglia_thalamus.xsiType!="cnda:modifiedScheltensRegion"){
				xmlTxt+=" xsi:type=\"" + this.Assessment_regions_basalGanglia_thalamus.xsiType + "\"";
			}
			if (this.Assessment_regions_basalGanglia_thalamus.hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Assessment_regions_basalGanglia_thalamus.getXMLBody(preventComments);
				xmlTxt+="</cnda:thalamus>";
			}else {xmlTxt+="/>";}
		}
		else{
			xmlTxt+="\n<cnda:thalamus/>";//REQUIRED
		}
				xmlTxt+="\n</cnda:basal_ganglia>";
			}
			}

				xmlTxt+="\n</cnda:regions>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.Assessment_virchowRobin_present!=null)
			child5++;
			if(this.Assessment_virchowRobin_contributionLocation!=null)
			child5++;
			if(this.Assessment_virchowRobin_location!=null)
			child5++;
			if(this.Assessment_virchowRobin_potentialContribution!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<cnda:virchow_robin";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Assessment_virchowRobin_present!=null){
			xmlTxt+="\n<cnda:present";
			xmlTxt+=">";
			xmlTxt+=this.Assessment_virchowRobin_present;
			xmlTxt+="</cnda:present>";
		}
		else{
			xmlTxt+="\n<cnda:present";
			xmlTxt+="/>";
		}

		if (this.Assessment_virchowRobin_location!=null){
			xmlTxt+="\n<cnda:location";
			xmlTxt+=">";
			xmlTxt+=this.Assessment_virchowRobin_location.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:location>";
		}
		if (this.Assessment_virchowRobin_potentialContribution!=null){
			xmlTxt+="\n<cnda:potential_contribution";
			xmlTxt+=">";
			xmlTxt+=this.Assessment_virchowRobin_potentialContribution;
			xmlTxt+="</cnda:potential_contribution>";
		}
		else{
			xmlTxt+="\n<cnda:potential_contribution";
			xmlTxt+="/>";
		}

		if (this.Assessment_virchowRobin_contributionLocation!=null){
			xmlTxt+="\n<cnda:contribution_location";
			xmlTxt+=">";
			xmlTxt+=this.Assessment_virchowRobin_contributionLocation.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:contribution_location>";
		}
				xmlTxt+="\n</cnda:virchow_robin>";
			}
			}

				xmlTxt+="\n</cnda:assessment>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Assessment_confluent!=null)
			return true;
			if(this.Assessment_confluent!=null) return true;
			if(this.Assessment_regions_basalGanglia_putamen!=null) return true;
			if(this.Assessment_regions_periventricular_posteriorHorns!=null) return true;
			if(this.Assessment_regions_deepWhiteMatter_temporal!=null) return true;
			if(this.Assessment_regions_basalGanglia_internalCapsule!=null) return true;
			if(this.Assessment_regions_deepWhiteMatter_occipital!=null) return true;
			if(this.Assessment_regions_periventricular_frontalHorns!=null) return true;
			if(this.Assessment_virchowRobin_present!=null) return true;
			if(this.Assessment_virchowRobin_potentialContribution!=null) return true;
			if(this.Assessment_regions_basalGanglia_caudate!=null) return true;
			if(this.Assessment_regions_deepWhiteMatter_frontal!=null) return true;
			if(this.Assessment_regions_basalGanglia_thalamus!=null) return true;
			if(this.Assessment_virchowRobin_contributionLocation!=null) return true;
			if(this.Assessment_regions_periventricular_ventricularBody!=null) return true;
			if(this.Assessment_virchowRobin_location!=null) return true;
			if(this.Assessment_regions_basalGanglia_globusPallidus!=null) return true;
			if(this.Assessment_regions_deepWhiteMatter_parietal!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
