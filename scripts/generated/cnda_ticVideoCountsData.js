/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_ticVideoCountsData(){
this.xsiType="cnda:ticVideoCountsData";

	this.getSchemaElementName=function(){
		return "ticVideoCountsData";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:ticVideoCountsData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Tape=null;


	function getTape() {
		return this.Tape;
	}
	this.getTape=getTape;


	function setTape(v){
		this.Tape=v;
	}
	this.setTape=setTape;

	this.Tapetime=null;


	function getTapetime() {
		return this.Tapetime;
	}
	this.getTapetime=getTapetime;


	function setTapetime(v){
		this.Tapetime=v;
	}
	this.setTapetime=setTapetime;

	this.Tapeinfo=null;


	function getTapeinfo() {
		return this.Tapeinfo;
	}
	this.getTapeinfo=getTapeinfo;


	function setTapeinfo(v){
		this.Tapeinfo=v;
	}
	this.setTapeinfo=setTapeinfo;

	this.Boldnumber=null;


	function getBoldnumber() {
		return this.Boldnumber;
	}
	this.getBoldnumber=getBoldnumber;


	function setBoldnumber(v){
		this.Boldnumber=v;
	}
	this.setBoldnumber=setBoldnumber;

	this.Drug=null;


	function getDrug() {
		return this.Drug;
	}
	this.getDrug=getDrug;


	function setDrug(v){
		this.Drug=v;
	}
	this.setDrug=setDrug;

	this.Examinerinroom=null;


	function getExaminerinroom() {
		return this.Examinerinroom;
	}
	this.getExaminerinroom=getExaminerinroom;


	function setExaminerinroom(v){
		this.Examinerinroom=v;
	}
	this.setExaminerinroom=setExaminerinroom;


	this.isExaminerinroom=function(defaultValue) {
		if(this.Examinerinroom==null)return defaultValue;
		if(this.Examinerinroom=="1" || this.Examinerinroom==true)return true;
		return false;
	}

	this.Durationheadshoulders=null;


	function getDurationheadshoulders() {
		return this.Durationheadshoulders;
	}
	this.getDurationheadshoulders=getDurationheadshoulders;


	function setDurationheadshoulders(v){
		this.Durationheadshoulders=v;
	}
	this.setDurationheadshoulders=setDurationheadshoulders;

	this.Durationfullbody=null;


	function getDurationfullbody() {
		return this.Durationfullbody;
	}
	this.getDurationfullbody=getDurationfullbody;


	function setDurationfullbody(v){
		this.Durationfullbody=v;
	}
	this.setDurationfullbody=setDurationfullbody;

	this.Rater=null;


	function getRater() {
		return this.Rater;
	}
	this.getRater=getRater;


	function setRater(v){
		this.Rater=v;
	}
	this.setRater=setRater;

	this.Eyes=null;


	function getEyes() {
		return this.Eyes;
	}
	this.getEyes=getEyes;


	function setEyes(v){
		this.Eyes=v;
	}
	this.setEyes=setEyes;


	this.isEyes=function(defaultValue) {
		if(this.Eyes==null)return defaultValue;
		if(this.Eyes=="1" || this.Eyes==true)return true;
		return false;
	}

	this.Nose=null;


	function getNose() {
		return this.Nose;
	}
	this.getNose=getNose;


	function setNose(v){
		this.Nose=v;
	}
	this.setNose=setNose;


	this.isNose=function(defaultValue) {
		if(this.Nose==null)return defaultValue;
		if(this.Nose=="1" || this.Nose==true)return true;
		return false;
	}

	this.Mouth=null;


	function getMouth() {
		return this.Mouth;
	}
	this.getMouth=getMouth;


	function setMouth(v){
		this.Mouth=v;
	}
	this.setMouth=setMouth;


	this.isMouth=function(defaultValue) {
		if(this.Mouth==null)return defaultValue;
		if(this.Mouth=="1" || this.Mouth==true)return true;
		return false;
	}

	this.Neck=null;


	function getNeck() {
		return this.Neck;
	}
	this.getNeck=getNeck;


	function setNeck(v){
		this.Neck=v;
	}
	this.setNeck=setNeck;


	this.isNeck=function(defaultValue) {
		if(this.Neck==null)return defaultValue;
		if(this.Neck=="1" || this.Neck==true)return true;
		return false;
	}

	this.Shoulders=null;


	function getShoulders() {
		return this.Shoulders;
	}
	this.getShoulders=getShoulders;


	function setShoulders(v){
		this.Shoulders=v;
	}
	this.setShoulders=setShoulders;


	this.isShoulders=function(defaultValue) {
		if(this.Shoulders==null)return defaultValue;
		if(this.Shoulders=="1" || this.Shoulders==true)return true;
		return false;
	}

	this.Arms=null;


	function getArms() {
		return this.Arms;
	}
	this.getArms=getArms;


	function setArms(v){
		this.Arms=v;
	}
	this.setArms=setArms;


	this.isArms=function(defaultValue) {
		if(this.Arms==null)return defaultValue;
		if(this.Arms=="1" || this.Arms==true)return true;
		return false;
	}

	this.Hands=null;


	function getHands() {
		return this.Hands;
	}
	this.getHands=getHands;


	function setHands(v){
		this.Hands=v;
	}
	this.setHands=setHands;


	this.isHands=function(defaultValue) {
		if(this.Hands==null)return defaultValue;
		if(this.Hands=="1" || this.Hands==true)return true;
		return false;
	}

	this.Trunk=null;


	function getTrunk() {
		return this.Trunk;
	}
	this.getTrunk=getTrunk;


	function setTrunk(v){
		this.Trunk=v;
	}
	this.setTrunk=setTrunk;


	this.isTrunk=function(defaultValue) {
		if(this.Trunk==null)return defaultValue;
		if(this.Trunk=="1" || this.Trunk==true)return true;
		return false;
	}

	this.Pelvis=null;


	function getPelvis() {
		return this.Pelvis;
	}
	this.getPelvis=getPelvis;


	function setPelvis(v){
		this.Pelvis=v;
	}
	this.setPelvis=setPelvis;


	this.isPelvis=function(defaultValue) {
		if(this.Pelvis==null)return defaultValue;
		if(this.Pelvis=="1" || this.Pelvis==true)return true;
		return false;
	}

	this.Legs=null;


	function getLegs() {
		return this.Legs;
	}
	this.getLegs=getLegs;


	function setLegs(v){
		this.Legs=v;
	}
	this.setLegs=setLegs;


	this.isLegs=function(defaultValue) {
		if(this.Legs==null)return defaultValue;
		if(this.Legs=="1" || this.Legs==true)return true;
		return false;
	}

	this.Feet=null;


	function getFeet() {
		return this.Feet;
	}
	this.getFeet=getFeet;


	function setFeet(v){
		this.Feet=v;
	}
	this.setFeet=setFeet;


	this.isFeet=function(defaultValue) {
		if(this.Feet==null)return defaultValue;
		if(this.Feet=="1" || this.Feet==true)return true;
		return false;
	}

	this.Eyecount=null;


	function getEyecount() {
		return this.Eyecount;
	}
	this.getEyecount=getEyecount;


	function setEyecount(v){
		this.Eyecount=v;
	}
	this.setEyecount=setEyecount;

	this.Fnscount=null;


	function getFnscount() {
		return this.Fnscount;
	}
	this.getFnscount=getFnscount;


	function setFnscount(v){
		this.Fnscount=v;
	}
	this.setFnscount=setFnscount;

	this.Vocalizations=null;


	function getVocalizations() {
		return this.Vocalizations;
	}
	this.getVocalizations=getVocalizations;


	function setVocalizations(v){
		this.Vocalizations=v;
	}
	this.setVocalizations=setVocalizations;

	this.Bodycount=null;


	function getBodycount() {
		return this.Bodycount;
	}
	this.getBodycount=getBodycount;


	function setBodycount(v){
		this.Bodycount=v;
	}
	this.setBodycount=setBodycount;

	this.Worstmotorseverity=null;


	function getWorstmotorseverity() {
		return this.Worstmotorseverity;
	}
	this.getWorstmotorseverity=getWorstmotorseverity;


	function setWorstmotorseverity(v){
		this.Worstmotorseverity=v;
	}
	this.setWorstmotorseverity=setWorstmotorseverity;

	this.Worstvocalseverity=null;


	function getWorstvocalseverity() {
		return this.Worstvocalseverity;
	}
	this.getWorstvocalseverity=getWorstvocalseverity;


	function setWorstvocalseverity(v){
		this.Worstvocalseverity=v;
	}
	this.setWorstvocalseverity=setWorstvocalseverity;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="tape"){
				return this.Tape ;
			} else 
			if(xmlPath=="tapeTime"){
				return this.Tapetime ;
			} else 
			if(xmlPath=="tapeInfo"){
				return this.Tapeinfo ;
			} else 
			if(xmlPath=="boldNumber"){
				return this.Boldnumber ;
			} else 
			if(xmlPath=="drug"){
				return this.Drug ;
			} else 
			if(xmlPath=="examinerInRoom"){
				return this.Examinerinroom ;
			} else 
			if(xmlPath=="durationHeadShoulders"){
				return this.Durationheadshoulders ;
			} else 
			if(xmlPath=="durationFullBody"){
				return this.Durationfullbody ;
			} else 
			if(xmlPath=="rater"){
				return this.Rater ;
			} else 
			if(xmlPath=="eyes"){
				return this.Eyes ;
			} else 
			if(xmlPath=="nose"){
				return this.Nose ;
			} else 
			if(xmlPath=="mouth"){
				return this.Mouth ;
			} else 
			if(xmlPath=="neck"){
				return this.Neck ;
			} else 
			if(xmlPath=="shoulders"){
				return this.Shoulders ;
			} else 
			if(xmlPath=="arms"){
				return this.Arms ;
			} else 
			if(xmlPath=="hands"){
				return this.Hands ;
			} else 
			if(xmlPath=="trunk"){
				return this.Trunk ;
			} else 
			if(xmlPath=="pelvis"){
				return this.Pelvis ;
			} else 
			if(xmlPath=="legs"){
				return this.Legs ;
			} else 
			if(xmlPath=="feet"){
				return this.Feet ;
			} else 
			if(xmlPath=="eyeCount"){
				return this.Eyecount ;
			} else 
			if(xmlPath=="fnsCount"){
				return this.Fnscount ;
			} else 
			if(xmlPath=="vocalizations"){
				return this.Vocalizations ;
			} else 
			if(xmlPath=="bodyCount"){
				return this.Bodycount ;
			} else 
			if(xmlPath=="worstMotorSeverity"){
				return this.Worstmotorseverity ;
			} else 
			if(xmlPath=="worstVocalSeverity"){
				return this.Worstvocalseverity ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="tape"){
				this.Tape=value;
			} else 
			if(xmlPath=="tapeTime"){
				this.Tapetime=value;
			} else 
			if(xmlPath=="tapeInfo"){
				this.Tapeinfo=value;
			} else 
			if(xmlPath=="boldNumber"){
				this.Boldnumber=value;
			} else 
			if(xmlPath=="drug"){
				this.Drug=value;
			} else 
			if(xmlPath=="examinerInRoom"){
				this.Examinerinroom=value;
			} else 
			if(xmlPath=="durationHeadShoulders"){
				this.Durationheadshoulders=value;
			} else 
			if(xmlPath=="durationFullBody"){
				this.Durationfullbody=value;
			} else 
			if(xmlPath=="rater"){
				this.Rater=value;
			} else 
			if(xmlPath=="eyes"){
				this.Eyes=value;
			} else 
			if(xmlPath=="nose"){
				this.Nose=value;
			} else 
			if(xmlPath=="mouth"){
				this.Mouth=value;
			} else 
			if(xmlPath=="neck"){
				this.Neck=value;
			} else 
			if(xmlPath=="shoulders"){
				this.Shoulders=value;
			} else 
			if(xmlPath=="arms"){
				this.Arms=value;
			} else 
			if(xmlPath=="hands"){
				this.Hands=value;
			} else 
			if(xmlPath=="trunk"){
				this.Trunk=value;
			} else 
			if(xmlPath=="pelvis"){
				this.Pelvis=value;
			} else 
			if(xmlPath=="legs"){
				this.Legs=value;
			} else 
			if(xmlPath=="feet"){
				this.Feet=value;
			} else 
			if(xmlPath=="eyeCount"){
				this.Eyecount=value;
			} else 
			if(xmlPath=="fnsCount"){
				this.Fnscount=value;
			} else 
			if(xmlPath=="vocalizations"){
				this.Vocalizations=value;
			} else 
			if(xmlPath=="bodyCount"){
				this.Bodycount=value;
			} else 
			if(xmlPath=="worstMotorSeverity"){
				this.Worstmotorseverity=value;
			} else 
			if(xmlPath=="worstVocalSeverity"){
				this.Worstvocalseverity=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="tape"){
			return "field_data";
		}else if (xmlPath=="tapeTime"){
			return "field_data";
		}else if (xmlPath=="tapeInfo"){
			return "field_data";
		}else if (xmlPath=="boldNumber"){
			return "field_data";
		}else if (xmlPath=="drug"){
			return "field_data";
		}else if (xmlPath=="examinerInRoom"){
			return "field_data";
		}else if (xmlPath=="durationHeadShoulders"){
			return "field_data";
		}else if (xmlPath=="durationFullBody"){
			return "field_data";
		}else if (xmlPath=="rater"){
			return "field_data";
		}else if (xmlPath=="eyes"){
			return "field_data";
		}else if (xmlPath=="nose"){
			return "field_data";
		}else if (xmlPath=="mouth"){
			return "field_data";
		}else if (xmlPath=="neck"){
			return "field_data";
		}else if (xmlPath=="shoulders"){
			return "field_data";
		}else if (xmlPath=="arms"){
			return "field_data";
		}else if (xmlPath=="hands"){
			return "field_data";
		}else if (xmlPath=="trunk"){
			return "field_data";
		}else if (xmlPath=="pelvis"){
			return "field_data";
		}else if (xmlPath=="legs"){
			return "field_data";
		}else if (xmlPath=="feet"){
			return "field_data";
		}else if (xmlPath=="eyeCount"){
			return "field_data";
		}else if (xmlPath=="fnsCount"){
			return "field_data";
		}else if (xmlPath=="vocalizations"){
			return "field_data";
		}else if (xmlPath=="bodyCount"){
			return "field_data";
		}else if (xmlPath=="worstMotorSeverity"){
			return "field_data";
		}else if (xmlPath=="worstVocalSeverity"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:TicVideoCounts";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:TicVideoCounts>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Tape!=null){
			xmlTxt+="\n<cnda:tape";
			xmlTxt+=">";
			xmlTxt+=this.Tape.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:tape>";
		}
		if (this.Tapetime!=null){
			xmlTxt+="\n<cnda:tapeTime";
			xmlTxt+=">";
			xmlTxt+=this.Tapetime.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:tapeTime>";
		}
		if (this.Tapeinfo!=null){
			xmlTxt+="\n<cnda:tapeInfo";
			xmlTxt+=">";
			xmlTxt+=this.Tapeinfo.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:tapeInfo>";
		}
		if (this.Boldnumber!=null){
			xmlTxt+="\n<cnda:boldNumber";
			xmlTxt+=">";
			xmlTxt+=this.Boldnumber.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:boldNumber>";
		}
		if (this.Drug!=null){
			xmlTxt+="\n<cnda:drug";
			xmlTxt+=">";
			xmlTxt+=this.Drug.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:drug>";
		}
		if (this.Examinerinroom!=null){
			xmlTxt+="\n<cnda:examinerInRoom";
			xmlTxt+=">";
			xmlTxt+=this.Examinerinroom;
			xmlTxt+="</cnda:examinerInRoom>";
		}
		if (this.Durationheadshoulders!=null){
			xmlTxt+="\n<cnda:durationHeadShoulders";
			xmlTxt+=">";
			xmlTxt+=this.Durationheadshoulders.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:durationHeadShoulders>";
		}
		if (this.Durationfullbody!=null){
			xmlTxt+="\n<cnda:durationFullBody";
			xmlTxt+=">";
			xmlTxt+=this.Durationfullbody.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:durationFullBody>";
		}
		if (this.Rater!=null){
			xmlTxt+="\n<cnda:rater";
			xmlTxt+=">";
			xmlTxt+=this.Rater.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:rater>";
		}
		if (this.Eyes!=null){
			xmlTxt+="\n<cnda:eyes";
			xmlTxt+=">";
			xmlTxt+=this.Eyes;
			xmlTxt+="</cnda:eyes>";
		}
		if (this.Nose!=null){
			xmlTxt+="\n<cnda:nose";
			xmlTxt+=">";
			xmlTxt+=this.Nose;
			xmlTxt+="</cnda:nose>";
		}
		if (this.Mouth!=null){
			xmlTxt+="\n<cnda:mouth";
			xmlTxt+=">";
			xmlTxt+=this.Mouth;
			xmlTxt+="</cnda:mouth>";
		}
		if (this.Neck!=null){
			xmlTxt+="\n<cnda:neck";
			xmlTxt+=">";
			xmlTxt+=this.Neck;
			xmlTxt+="</cnda:neck>";
		}
		if (this.Shoulders!=null){
			xmlTxt+="\n<cnda:shoulders";
			xmlTxt+=">";
			xmlTxt+=this.Shoulders;
			xmlTxt+="</cnda:shoulders>";
		}
		if (this.Arms!=null){
			xmlTxt+="\n<cnda:arms";
			xmlTxt+=">";
			xmlTxt+=this.Arms;
			xmlTxt+="</cnda:arms>";
		}
		if (this.Hands!=null){
			xmlTxt+="\n<cnda:hands";
			xmlTxt+=">";
			xmlTxt+=this.Hands;
			xmlTxt+="</cnda:hands>";
		}
		if (this.Trunk!=null){
			xmlTxt+="\n<cnda:trunk";
			xmlTxt+=">";
			xmlTxt+=this.Trunk;
			xmlTxt+="</cnda:trunk>";
		}
		if (this.Pelvis!=null){
			xmlTxt+="\n<cnda:pelvis";
			xmlTxt+=">";
			xmlTxt+=this.Pelvis;
			xmlTxt+="</cnda:pelvis>";
		}
		if (this.Legs!=null){
			xmlTxt+="\n<cnda:legs";
			xmlTxt+=">";
			xmlTxt+=this.Legs;
			xmlTxt+="</cnda:legs>";
		}
		if (this.Feet!=null){
			xmlTxt+="\n<cnda:feet";
			xmlTxt+=">";
			xmlTxt+=this.Feet;
			xmlTxt+="</cnda:feet>";
		}
		if (this.Eyecount!=null){
			xmlTxt+="\n<cnda:eyeCount";
			xmlTxt+=">";
			xmlTxt+=this.Eyecount;
			xmlTxt+="</cnda:eyeCount>";
		}
		if (this.Fnscount!=null){
			xmlTxt+="\n<cnda:fnsCount";
			xmlTxt+=">";
			xmlTxt+=this.Fnscount;
			xmlTxt+="</cnda:fnsCount>";
		}
		if (this.Vocalizations!=null){
			xmlTxt+="\n<cnda:vocalizations";
			xmlTxt+=">";
			xmlTxt+=this.Vocalizations;
			xmlTxt+="</cnda:vocalizations>";
		}
		if (this.Bodycount!=null){
			xmlTxt+="\n<cnda:bodyCount";
			xmlTxt+=">";
			xmlTxt+=this.Bodycount;
			xmlTxt+="</cnda:bodyCount>";
		}
		if (this.Worstmotorseverity!=null){
			xmlTxt+="\n<cnda:worstMotorSeverity";
			xmlTxt+=">";
			xmlTxt+=this.Worstmotorseverity;
			xmlTxt+="</cnda:worstMotorSeverity>";
		}
		if (this.Worstvocalseverity!=null){
			xmlTxt+="\n<cnda:worstVocalSeverity";
			xmlTxt+=">";
			xmlTxt+=this.Worstvocalseverity;
			xmlTxt+="</cnda:worstVocalSeverity>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Tape!=null) return true;
		if (this.Tapetime!=null) return true;
		if (this.Tapeinfo!=null) return true;
		if (this.Boldnumber!=null) return true;
		if (this.Drug!=null) return true;
		if (this.Examinerinroom!=null) return true;
		if (this.Durationheadshoulders!=null) return true;
		if (this.Durationfullbody!=null) return true;
		if (this.Rater!=null) return true;
		if (this.Eyes!=null) return true;
		if (this.Nose!=null) return true;
		if (this.Mouth!=null) return true;
		if (this.Neck!=null) return true;
		if (this.Shoulders!=null) return true;
		if (this.Arms!=null) return true;
		if (this.Hands!=null) return true;
		if (this.Trunk!=null) return true;
		if (this.Pelvis!=null) return true;
		if (this.Legs!=null) return true;
		if (this.Feet!=null) return true;
		if (this.Eyecount!=null) return true;
		if (this.Fnscount!=null) return true;
		if (this.Vocalizations!=null) return true;
		if (this.Bodycount!=null) return true;
		if (this.Worstmotorseverity!=null) return true;
		if (this.Worstvocalseverity!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
