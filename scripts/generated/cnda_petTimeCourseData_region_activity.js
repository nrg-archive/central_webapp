/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_petTimeCourseData_region_activity(){
this.xsiType="cnda:petTimeCourseData_region_activity";

	this.getSchemaElementName=function(){
		return "petTimeCourseData_region_activity";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:petTimeCourseData_region_activity";
	}

	this.Activity=null;


	function getActivity() {
		return this.Activity;
	}
	this.getActivity=getActivity;


	function setActivity(v){
		this.Activity=v;
	}
	this.setActivity=setActivity;

	this.Time=null;


	function getTime() {
		return this.Time;
	}
	this.getTime=getTime;


	function setTime(v){
		this.Time=v;
	}
	this.setTime=setTime;

	this.CndaPettimecoursedataRegionActivityId=null;


	function getCndaPettimecoursedataRegionActivityId() {
		return this.CndaPettimecoursedataRegionActivityId;
	}
	this.getCndaPettimecoursedataRegionActivityId=getCndaPettimecoursedataRegionActivityId;


	function setCndaPettimecoursedataRegionActivityId(v){
		this.CndaPettimecoursedataRegionActivityId=v;
	}
	this.setCndaPettimecoursedataRegionActivityId=setCndaPettimecoursedataRegionActivityId;

	this.timeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i_fk=null;


	this.gettimeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i=function() {
		return this.timeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i_fk;
	}


	this.settimeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i=function(v){
		this.timeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="activity"){
				return this.Activity ;
			} else 
			if(xmlPath=="time"){
				return this.Time ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="cnda_petTimeCourseData_region_activity_id"){
				return this.CndaPettimecoursedataRegionActivityId ;
			} else 
			if(xmlPath=="timeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i"){
				return this.timeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="activity"){
				this.Activity=value;
			} else 
			if(xmlPath=="time"){
				this.Time=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="cnda_petTimeCourseData_region_activity_id"){
				this.CndaPettimecoursedataRegionActivityId=value;
			} else 
			if(xmlPath=="timeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i"){
				this.timeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="activity"){
			return "field_data";
		}else if (xmlPath=="time"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:petTimeCourseData_region_activity";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:petTimeCourseData_region_activity>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.CndaPettimecoursedataRegionActivityId!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_petTimeCourseData_region_activity_id=\"" + this.CndaPettimecoursedataRegionActivityId + "\"";
			}
			if(this.timeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="timeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i=\"" + this.timeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Time!=null)
			attTxt+=" time=\"" +this.Time +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Activity!=null){
			xmlTxt+=this.Activity;
		}
		else{
			xmlTxt+="\n<cnda:activity";
			xmlTxt+="/>";
		}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.CndaPettimecoursedataRegionActivityId!=null) return true;
			if (this.timeseries_activity_cnda_petTim_cnda_pettimecoursedata_region_i_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Activity!=null) return true;
		return true;//REQUIRED activity
	}
}
