/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_diagnosesData(){
this.xsiType="cnda:diagnosesData";

	this.getSchemaElementName=function(){
		return "diagnosesData";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:diagnosesData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Diagnoser=null;


	function getDiagnoser() {
		return this.Diagnoser;
	}
	this.getDiagnoser=getDiagnoser;


	function setDiagnoser(v){
		this.Diagnoser=v;
	}
	this.setDiagnoser=setDiagnoser;

	this.Ticdiagnosis_dsmiv=null;


	function getTicdiagnosis_dsmiv() {
		return this.Ticdiagnosis_dsmiv;
	}
	this.getTicdiagnosis_dsmiv=getTicdiagnosis_dsmiv;


	function setTicdiagnosis_dsmiv(v){
		this.Ticdiagnosis_dsmiv=v;
	}
	this.setTicdiagnosis_dsmiv=setTicdiagnosis_dsmiv;

	this.Ticdiagnosis_impairdistress=null;


	function getTicdiagnosis_impairdistress() {
		return this.Ticdiagnosis_impairdistress;
	}
	this.getTicdiagnosis_impairdistress=getTicdiagnosis_impairdistress;


	function setTicdiagnosis_impairdistress(v){
		this.Ticdiagnosis_impairdistress=v;
	}
	this.setTicdiagnosis_impairdistress=setTicdiagnosis_impairdistress;


	this.isTicdiagnosis_impairdistress=function(defaultValue) {
		if(this.Ticdiagnosis_impairdistress==null)return defaultValue;
		if(this.Ticdiagnosis_impairdistress=="1" || this.Ticdiagnosis_impairdistress==true)return true;
		return false;
	}

	this.Ticdiagnosis_tssg=null;


	function getTicdiagnosis_tssg() {
		return this.Ticdiagnosis_tssg;
	}
	this.getTicdiagnosis_tssg=getTicdiagnosis_tssg;


	function setTicdiagnosis_tssg(v){
		this.Ticdiagnosis_tssg=v;
	}
	this.setTicdiagnosis_tssg=setTicdiagnosis_tssg;

	this.Ticdiagnosis_tssg2=null;


	function getTicdiagnosis_tssg2() {
		return this.Ticdiagnosis_tssg2;
	}
	this.getTicdiagnosis_tssg2=getTicdiagnosis_tssg2;


	function setTicdiagnosis_tssg2(v){
		this.Ticdiagnosis_tssg2=v;
	}
	this.setTicdiagnosis_tssg2=setTicdiagnosis_tssg2;

	this.Motor_firstticage=null;


	function getMotor_firstticage() {
		return this.Motor_firstticage;
	}
	this.getMotor_firstticage=getMotor_firstticage;


	function setMotor_firstticage(v){
		this.Motor_firstticage=v;
	}
	this.setMotor_firstticage=setMotor_firstticage;

	this.Motor_worstticage=null;


	function getMotor_worstticage() {
		return this.Motor_worstticage;
	}
	this.getMotor_worstticage=getMotor_worstticage;


	function setMotor_worstticage(v){
		this.Motor_worstticage=v;
	}
	this.setMotor_worstticage=setMotor_worstticage;

	this.Vocal_firstticage=null;


	function getVocal_firstticage() {
		return this.Vocal_firstticage;
	}
	this.getVocal_firstticage=getVocal_firstticage;


	function setVocal_firstticage(v){
		this.Vocal_firstticage=v;
	}
	this.setVocal_firstticage=setVocal_firstticage;

	this.Vocal_worstticage=null;


	function getVocal_worstticage() {
		return this.Vocal_worstticage;
	}
	this.getVocal_worstticage=getVocal_worstticage;


	function setVocal_worstticage(v){
		this.Vocal_worstticage=v;
	}
	this.setVocal_worstticage=setVocal_worstticage;

	this.Firstdiagnosedticsage=null;


	function getFirstdiagnosedticsage() {
		return this.Firstdiagnosedticsage;
	}
	this.getFirstdiagnosedticsage=getFirstdiagnosedticsage;


	function setFirstdiagnosedticsage(v){
		this.Firstdiagnosedticsage=v;
	}
	this.setFirstdiagnosedticsage=setFirstdiagnosedticsage;

	this.DiagnosisAdhdDsmiv=null;


	function getDiagnosisAdhdDsmiv() {
		return this.DiagnosisAdhdDsmiv;
	}
	this.getDiagnosisAdhdDsmiv=getDiagnosisAdhdDsmiv;


	function setDiagnosisAdhdDsmiv(v){
		this.DiagnosisAdhdDsmiv=v;
	}
	this.setDiagnosisAdhdDsmiv=setDiagnosisAdhdDsmiv;

	this.ScoreTsDci=null;


	function getScoreTsDci() {
		return this.ScoreTsDci;
	}
	this.getScoreTsDci=getScoreTsDci;


	function setScoreTsDci(v){
		this.ScoreTsDci=v;
	}
	this.setScoreTsDci=setScoreTsDci;

	this.DiagnosisOcdDsmiv=null;


	function getDiagnosisOcdDsmiv() {
		return this.DiagnosisOcdDsmiv;
	}
	this.getDiagnosisOcdDsmiv=getDiagnosisOcdDsmiv;


	function setDiagnosisOcdDsmiv(v){
		this.DiagnosisOcdDsmiv=v;
	}
	this.setDiagnosisOcdDsmiv=setDiagnosisOcdDsmiv;

	this.Firstobsessionage=null;


	function getFirstobsessionage() {
		return this.Firstobsessionage;
	}
	this.getFirstobsessionage=getFirstobsessionage;


	function setFirstobsessionage(v){
		this.Firstobsessionage=v;
	}
	this.setFirstobsessionage=setFirstobsessionage;

	this.Firstcompulsionage=null;


	function getFirstcompulsionage() {
		return this.Firstcompulsionage;
	}
	this.getFirstcompulsionage=getFirstcompulsionage;


	function setFirstcompulsionage(v){
		this.Firstcompulsionage=v;
	}
	this.setFirstcompulsionage=setFirstcompulsionage;

	this.Worstobsessivecompulsivesymptomage=null;


	function getWorstobsessivecompulsivesymptomage() {
		return this.Worstobsessivecompulsivesymptomage;
	}
	this.getWorstobsessivecompulsivesymptomage=getWorstobsessivecompulsivesymptomage;


	function setWorstobsessivecompulsivesymptomage(v){
		this.Worstobsessivecompulsivesymptomage=v;
	}
	this.setWorstobsessivecompulsivesymptomage=setWorstobsessivecompulsivesymptomage;

	this.Handedness=null;


	function getHandedness() {
		return this.Handedness;
	}
	this.getHandedness=getHandedness;


	function setHandedness(v){
		this.Handedness=v;
	}
	this.setHandedness=setHandedness;

	this.Ineligiblefromscid=null;


	function getIneligiblefromscid() {
		return this.Ineligiblefromscid;
	}
	this.getIneligiblefromscid=getIneligiblefromscid;


	function setIneligiblefromscid(v){
		this.Ineligiblefromscid=v;
	}
	this.setIneligiblefromscid=setIneligiblefromscid;


	this.isIneligiblefromscid=function(defaultValue) {
		if(this.Ineligiblefromscid==null)return defaultValue;
		if(this.Ineligiblefromscid=="1" || this.Ineligiblefromscid==true)return true;
		return false;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="diagnoser"){
				return this.Diagnoser ;
			} else 
			if(xmlPath=="ticDiagnosis/DSMIV"){
				return this.Ticdiagnosis_dsmiv ;
			} else 
			if(xmlPath=="ticDiagnosis/impairDistress"){
				return this.Ticdiagnosis_impairdistress ;
			} else 
			if(xmlPath=="ticDiagnosis/TSSG"){
				return this.Ticdiagnosis_tssg ;
			} else 
			if(xmlPath=="ticDiagnosis/TSSG2"){
				return this.Ticdiagnosis_tssg2 ;
			} else 
			if(xmlPath=="motor/firstTicAge"){
				return this.Motor_firstticage ;
			} else 
			if(xmlPath=="motor/worstTicAge"){
				return this.Motor_worstticage ;
			} else 
			if(xmlPath=="vocal/firstTicAge"){
				return this.Vocal_firstticage ;
			} else 
			if(xmlPath=="vocal/worstTicAge"){
				return this.Vocal_worstticage ;
			} else 
			if(xmlPath=="firstDiagnosedTicsAge"){
				return this.Firstdiagnosedticsage ;
			} else 
			if(xmlPath=="diagnosis_ADHD_DSMIV"){
				return this.DiagnosisAdhdDsmiv ;
			} else 
			if(xmlPath=="score_TS_DCI"){
				return this.ScoreTsDci ;
			} else 
			if(xmlPath=="diagnosis_OCD_DSMIV"){
				return this.DiagnosisOcdDsmiv ;
			} else 
			if(xmlPath=="firstObsessionAge"){
				return this.Firstobsessionage ;
			} else 
			if(xmlPath=="firstCompulsionAge"){
				return this.Firstcompulsionage ;
			} else 
			if(xmlPath=="worstObsessiveCompulsiveSymptomAge"){
				return this.Worstobsessivecompulsivesymptomage ;
			} else 
			if(xmlPath=="handedness"){
				return this.Handedness ;
			} else 
			if(xmlPath=="ineligibleFromSCID"){
				return this.Ineligiblefromscid ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="diagnoser"){
				this.Diagnoser=value;
			} else 
			if(xmlPath=="ticDiagnosis/DSMIV"){
				this.Ticdiagnosis_dsmiv=value;
			} else 
			if(xmlPath=="ticDiagnosis/impairDistress"){
				this.Ticdiagnosis_impairdistress=value;
			} else 
			if(xmlPath=="ticDiagnosis/TSSG"){
				this.Ticdiagnosis_tssg=value;
			} else 
			if(xmlPath=="ticDiagnosis/TSSG2"){
				this.Ticdiagnosis_tssg2=value;
			} else 
			if(xmlPath=="motor/firstTicAge"){
				this.Motor_firstticage=value;
			} else 
			if(xmlPath=="motor/worstTicAge"){
				this.Motor_worstticage=value;
			} else 
			if(xmlPath=="vocal/firstTicAge"){
				this.Vocal_firstticage=value;
			} else 
			if(xmlPath=="vocal/worstTicAge"){
				this.Vocal_worstticage=value;
			} else 
			if(xmlPath=="firstDiagnosedTicsAge"){
				this.Firstdiagnosedticsage=value;
			} else 
			if(xmlPath=="diagnosis_ADHD_DSMIV"){
				this.DiagnosisAdhdDsmiv=value;
			} else 
			if(xmlPath=="score_TS_DCI"){
				this.ScoreTsDci=value;
			} else 
			if(xmlPath=="diagnosis_OCD_DSMIV"){
				this.DiagnosisOcdDsmiv=value;
			} else 
			if(xmlPath=="firstObsessionAge"){
				this.Firstobsessionage=value;
			} else 
			if(xmlPath=="firstCompulsionAge"){
				this.Firstcompulsionage=value;
			} else 
			if(xmlPath=="worstObsessiveCompulsiveSymptomAge"){
				this.Worstobsessivecompulsivesymptomage=value;
			} else 
			if(xmlPath=="handedness"){
				this.Handedness=value;
			} else 
			if(xmlPath=="ineligibleFromSCID"){
				this.Ineligiblefromscid=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="diagnoser"){
			return "field_data";
		}else if (xmlPath=="ticDiagnosis/DSMIV"){
			return "field_data";
		}else if (xmlPath=="ticDiagnosis/impairDistress"){
			return "field_data";
		}else if (xmlPath=="ticDiagnosis/TSSG"){
			return "field_data";
		}else if (xmlPath=="ticDiagnosis/TSSG2"){
			return "field_data";
		}else if (xmlPath=="motor/firstTicAge"){
			return "field_data";
		}else if (xmlPath=="motor/worstTicAge"){
			return "field_data";
		}else if (xmlPath=="vocal/firstTicAge"){
			return "field_data";
		}else if (xmlPath=="vocal/worstTicAge"){
			return "field_data";
		}else if (xmlPath=="firstDiagnosedTicsAge"){
			return "field_data";
		}else if (xmlPath=="diagnosis_ADHD_DSMIV"){
			return "field_data";
		}else if (xmlPath=="score_TS_DCI"){
			return "field_data";
		}else if (xmlPath=="diagnosis_OCD_DSMIV"){
			return "field_data";
		}else if (xmlPath=="firstObsessionAge"){
			return "field_data";
		}else if (xmlPath=="firstCompulsionAge"){
			return "field_data";
		}else if (xmlPath=="worstObsessiveCompulsiveSymptomAge"){
			return "field_data";
		}else if (xmlPath=="handedness"){
			return "field_data";
		}else if (xmlPath=="ineligibleFromSCID"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:Diagnoses";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:Diagnoses>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Diagnoser!=null){
			xmlTxt+="\n<cnda:diagnoser";
			xmlTxt+=">";
			xmlTxt+=this.Diagnoser.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:diagnoser>";
		}
			var child0=0;
			var att0=0;
			if(this.Ticdiagnosis_tssg2!=null)
			child0++;
			if(this.Ticdiagnosis_impairdistress!=null)
			child0++;
			if(this.Ticdiagnosis_tssg!=null)
			child0++;
			if(this.Ticdiagnosis_dsmiv!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<cnda:ticDiagnosis";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Ticdiagnosis_dsmiv!=null){
			xmlTxt+="\n<cnda:DSMIV";
			xmlTxt+=">";
			xmlTxt+=this.Ticdiagnosis_dsmiv.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:DSMIV>";
		}
		if (this.Ticdiagnosis_impairdistress!=null){
			xmlTxt+="\n<cnda:impairDistress";
			xmlTxt+=">";
			xmlTxt+=this.Ticdiagnosis_impairdistress;
			xmlTxt+="</cnda:impairDistress>";
		}
		if (this.Ticdiagnosis_tssg!=null){
			xmlTxt+="\n<cnda:TSSG";
			xmlTxt+=">";
			xmlTxt+=this.Ticdiagnosis_tssg.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:TSSG>";
		}
		if (this.Ticdiagnosis_tssg2!=null){
			xmlTxt+="\n<cnda:TSSG2";
			xmlTxt+=">";
			xmlTxt+=this.Ticdiagnosis_tssg2.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:TSSG2>";
		}
				xmlTxt+="\n</cnda:ticDiagnosis>";
			}
			}

			var child1=0;
			var att1=0;
			if(this.Motor_firstticage!=null)
			child1++;
			if(this.Motor_worstticage!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<cnda:motor";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Motor_firstticage!=null){
			xmlTxt+="\n<cnda:firstTicAge";
			xmlTxt+=">";
			xmlTxt+=this.Motor_firstticage;
			xmlTxt+="</cnda:firstTicAge>";
		}
		if (this.Motor_worstticage!=null){
			xmlTxt+="\n<cnda:worstTicAge";
			xmlTxt+=">";
			xmlTxt+=this.Motor_worstticage;
			xmlTxt+="</cnda:worstTicAge>";
		}
				xmlTxt+="\n</cnda:motor>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Vocal_firstticage!=null)
			child2++;
			if(this.Vocal_worstticage!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<cnda:vocal";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Vocal_firstticage!=null){
			xmlTxt+="\n<cnda:firstTicAge";
			xmlTxt+=">";
			xmlTxt+=this.Vocal_firstticage;
			xmlTxt+="</cnda:firstTicAge>";
		}
		if (this.Vocal_worstticage!=null){
			xmlTxt+="\n<cnda:worstTicAge";
			xmlTxt+=">";
			xmlTxt+=this.Vocal_worstticage;
			xmlTxt+="</cnda:worstTicAge>";
		}
				xmlTxt+="\n</cnda:vocal>";
			}
			}

		if (this.Firstdiagnosedticsage!=null){
			xmlTxt+="\n<cnda:firstDiagnosedTicsAge";
			xmlTxt+=">";
			xmlTxt+=this.Firstdiagnosedticsage;
			xmlTxt+="</cnda:firstDiagnosedTicsAge>";
		}
		if (this.DiagnosisAdhdDsmiv!=null){
			xmlTxt+="\n<cnda:diagnosis_ADHD_DSMIV";
			xmlTxt+=">";
			xmlTxt+=this.DiagnosisAdhdDsmiv.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:diagnosis_ADHD_DSMIV>";
		}
		if (this.ScoreTsDci!=null){
			xmlTxt+="\n<cnda:score_TS_DCI";
			xmlTxt+=">";
			xmlTxt+=this.ScoreTsDci;
			xmlTxt+="</cnda:score_TS_DCI>";
		}
		if (this.DiagnosisOcdDsmiv!=null){
			xmlTxt+="\n<cnda:diagnosis_OCD_DSMIV";
			xmlTxt+=">";
			xmlTxt+=this.DiagnosisOcdDsmiv.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:diagnosis_OCD_DSMIV>";
		}
		if (this.Firstobsessionage!=null){
			xmlTxt+="\n<cnda:firstObsessionAge";
			xmlTxt+=">";
			xmlTxt+=this.Firstobsessionage;
			xmlTxt+="</cnda:firstObsessionAge>";
		}
		if (this.Firstcompulsionage!=null){
			xmlTxt+="\n<cnda:firstCompulsionAge";
			xmlTxt+=">";
			xmlTxt+=this.Firstcompulsionage;
			xmlTxt+="</cnda:firstCompulsionAge>";
		}
		if (this.Worstobsessivecompulsivesymptomage!=null){
			xmlTxt+="\n<cnda:worstObsessiveCompulsiveSymptomAge";
			xmlTxt+=">";
			xmlTxt+=this.Worstobsessivecompulsivesymptomage;
			xmlTxt+="</cnda:worstObsessiveCompulsiveSymptomAge>";
		}
		if (this.Handedness!=null){
			xmlTxt+="\n<cnda:handedness";
			xmlTxt+=">";
			xmlTxt+=this.Handedness.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:handedness>";
		}
		if (this.Ineligiblefromscid!=null){
			xmlTxt+="\n<cnda:ineligibleFromSCID";
			xmlTxt+=">";
			xmlTxt+=this.Ineligiblefromscid;
			xmlTxt+="</cnda:ineligibleFromSCID>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Diagnoser!=null) return true;
			if(this.Ticdiagnosis_tssg2!=null) return true;
			if(this.Ticdiagnosis_impairdistress!=null) return true;
			if(this.Ticdiagnosis_tssg!=null) return true;
			if(this.Ticdiagnosis_dsmiv!=null) return true;
			if(this.Motor_firstticage!=null) return true;
			if(this.Motor_worstticage!=null) return true;
			if(this.Vocal_firstticage!=null) return true;
			if(this.Vocal_worstticage!=null) return true;
		if (this.Firstdiagnosedticsage!=null) return true;
		if (this.DiagnosisAdhdDsmiv!=null) return true;
		if (this.ScoreTsDci!=null) return true;
		if (this.DiagnosisOcdDsmiv!=null) return true;
		if (this.Firstobsessionage!=null) return true;
		if (this.Firstcompulsionage!=null) return true;
		if (this.Worstobsessivecompulsivesymptomage!=null) return true;
		if (this.Handedness!=null) return true;
		if (this.Ineligiblefromscid!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
