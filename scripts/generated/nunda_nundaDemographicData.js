/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function nunda_nundaDemographicData(){
this.xsiType="nunda:nundaDemographicData";

	this.getSchemaElementName=function(){
		return "nundaDemographicData";
	}

	this.getFullSchemaElementName=function(){
		return "nunda:nundaDemographicData";
	}
this.extension=dynamicJSLoad('xnat_demographicData','generated/xnat_demographicData.js');

	this.Maritalstatus=null;


	function getMaritalstatus() {
		return this.Maritalstatus;
	}
	this.getMaritalstatus=getMaritalstatus;


	function setMaritalstatus(v){
		this.Maritalstatus=v;
	}
	this.setMaritalstatus=setMaritalstatus;

	this.Housingtype=null;


	function getHousingtype() {
		return this.Housingtype;
	}
	this.getHousingtype=getHousingtype;


	function setHousingtype(v){
		this.Housingtype=v;
	}
	this.setHousingtype=setHousingtype;

	this.Siblingsnumber=null;


	function getSiblingsnumber() {
		return this.Siblingsnumber;
	}
	this.getSiblingsnumber=getSiblingsnumber;


	function setSiblingsnumber(v){
		this.Siblingsnumber=v;
	}
	this.setSiblingsnumber=setSiblingsnumber;

	this.Siblingage1=null;


	function getSiblingage1() {
		return this.Siblingage1;
	}
	this.getSiblingage1=getSiblingage1;


	function setSiblingage1(v){
		this.Siblingage1=v;
	}
	this.setSiblingage1=setSiblingage1;

	this.Siblingage2=null;


	function getSiblingage2() {
		return this.Siblingage2;
	}
	this.getSiblingage2=getSiblingage2;


	function setSiblingage2(v){
		this.Siblingage2=v;
	}
	this.setSiblingage2=setSiblingage2;

	this.Siblingage3=null;


	function getSiblingage3() {
		return this.Siblingage3;
	}
	this.getSiblingage3=getSiblingage3;


	function setSiblingage3(v){
		this.Siblingage3=v;
	}
	this.setSiblingage3=setSiblingage3;

	this.Siblingage4=null;


	function getSiblingage4() {
		return this.Siblingage4;
	}
	this.getSiblingage4=getSiblingage4;


	function setSiblingage4(v){
		this.Siblingage4=v;
	}
	this.setSiblingage4=setSiblingage4;

	this.Siblingage5=null;


	function getSiblingage5() {
		return this.Siblingage5;
	}
	this.getSiblingage5=getSiblingage5;


	function setSiblingage5(v){
		this.Siblingage5=v;
	}
	this.setSiblingage5=setSiblingage5;

	this.Siblingage6=null;


	function getSiblingage6() {
		return this.Siblingage6;
	}
	this.getSiblingage6=getSiblingage6;


	function setSiblingage6(v){
		this.Siblingage6=v;
	}
	this.setSiblingage6=setSiblingage6;

	this.Siblingage7=null;


	function getSiblingage7() {
		return this.Siblingage7;
	}
	this.getSiblingage7=getSiblingage7;


	function setSiblingage7(v){
		this.Siblingage7=v;
	}
	this.setSiblingage7=setSiblingage7;

	this.Siblingage8=null;


	function getSiblingage8() {
		return this.Siblingage8;
	}
	this.getSiblingage8=getSiblingage8;


	function setSiblingage8(v){
		this.Siblingage8=v;
	}
	this.setSiblingage8=setSiblingage8;

	this.Numberofchildren=null;


	function getNumberofchildren() {
		return this.Numberofchildren;
	}
	this.getNumberofchildren=getNumberofchildren;


	function setNumberofchildren(v){
		this.Numberofchildren=v;
	}
	this.setNumberofchildren=setNumberofchildren;

	this.Employmentstatus=null;


	function getEmploymentstatus() {
		return this.Employmentstatus;
	}
	this.getEmploymentstatus=getEmploymentstatus;


	function setEmploymentstatus(v){
		this.Employmentstatus=v;
	}
	this.setEmploymentstatus=setEmploymentstatus;

	this.Currentoccupation=null;


	function getCurrentoccupation() {
		return this.Currentoccupation;
	}
	this.getCurrentoccupation=getCurrentoccupation;


	function setCurrentoccupation(v){
		this.Currentoccupation=v;
	}
	this.setCurrentoccupation=setCurrentoccupation;

	this.Principaloccupation=null;


	function getPrincipaloccupation() {
		return this.Principaloccupation;
	}
	this.getPrincipaloccupation=getPrincipaloccupation;


	function setPrincipaloccupation(v){
		this.Principaloccupation=v;
	}
	this.setPrincipaloccupation=setPrincipaloccupation;

	this.Educationlevel=null;


	function getEducationlevel() {
		return this.Educationlevel;
	}
	this.getEducationlevel=getEducationlevel;


	function setEducationlevel(v){
		this.Educationlevel=v;
	}
	this.setEducationlevel=setEducationlevel;

	this.Yearsofschooling=null;


	function getYearsofschooling() {
		return this.Yearsofschooling;
	}
	this.getYearsofschooling=getYearsofschooling;


	function setYearsofschooling(v){
		this.Yearsofschooling=v;
	}
	this.setYearsofschooling=setYearsofschooling;

	this.Attendspecialclasses=null;


	function getAttendspecialclasses() {
		return this.Attendspecialclasses;
	}
	this.getAttendspecialclasses=getAttendspecialclasses;


	function setAttendspecialclasses(v){
		this.Attendspecialclasses=v;
	}
	this.setAttendspecialclasses=setAttendspecialclasses;

	this.Specialclass=null;


	function getSpecialclass() {
		return this.Specialclass;
	}
	this.getSpecialclass=getSpecialclass;


	function setSpecialclass(v){
		this.Specialclass=v;
	}
	this.setSpecialclass=setSpecialclass;

	this.Educationlevelfather=null;


	function getEducationlevelfather() {
		return this.Educationlevelfather;
	}
	this.getEducationlevelfather=getEducationlevelfather;


	function setEducationlevelfather(v){
		this.Educationlevelfather=v;
	}
	this.setEducationlevelfather=setEducationlevelfather;

	this.Schoolinglevelfather=null;


	function getSchoolinglevelfather() {
		return this.Schoolinglevelfather;
	}
	this.getSchoolinglevelfather=getSchoolinglevelfather;


	function setSchoolinglevelfather(v){
		this.Schoolinglevelfather=v;
	}
	this.setSchoolinglevelfather=setSchoolinglevelfather;

	this.Educationlevelmother=null;


	function getEducationlevelmother() {
		return this.Educationlevelmother;
	}
	this.getEducationlevelmother=getEducationlevelmother;


	function setEducationlevelmother(v){
		this.Educationlevelmother=v;
	}
	this.setEducationlevelmother=setEducationlevelmother;

	this.Schoolinglevelmother=null;


	function getSchoolinglevelmother() {
		return this.Schoolinglevelmother;
	}
	this.getSchoolinglevelmother=getSchoolinglevelmother;


	function setSchoolinglevelmother(v){
		this.Schoolinglevelmother=v;
	}
	this.setSchoolinglevelmother=setSchoolinglevelmother;

	this.Sesdesc=null;


	function getSesdesc() {
		return this.Sesdesc;
	}
	this.getSesdesc=getSesdesc;


	function setSesdesc(v){
		this.Sesdesc=v;
	}
	this.setSesdesc=setSesdesc;

	this.Handednessexam_writing=null;


	function getHandednessexam_writing() {
		return this.Handednessexam_writing;
	}
	this.getHandednessexam_writing=getHandednessexam_writing;


	function setHandednessexam_writing(v){
		this.Handednessexam_writing=v;
	}
	this.setHandednessexam_writing=setHandednessexam_writing;

	this.Handednessexam_throwing=null;


	function getHandednessexam_throwing() {
		return this.Handednessexam_throwing;
	}
	this.getHandednessexam_throwing=getHandednessexam_throwing;


	function setHandednessexam_throwing(v){
		this.Handednessexam_throwing=v;
	}
	this.setHandednessexam_throwing=setHandednessexam_throwing;

	this.Handednessexam_scissors=null;


	function getHandednessexam_scissors() {
		return this.Handednessexam_scissors;
	}
	this.getHandednessexam_scissors=getHandednessexam_scissors;


	function setHandednessexam_scissors(v){
		this.Handednessexam_scissors=v;
	}
	this.setHandednessexam_scissors=setHandednessexam_scissors;

	this.Handednessexam_toothbrush=null;


	function getHandednessexam_toothbrush() {
		return this.Handednessexam_toothbrush;
	}
	this.getHandednessexam_toothbrush=getHandednessexam_toothbrush;


	function setHandednessexam_toothbrush(v){
		this.Handednessexam_toothbrush=v;
	}
	this.setHandednessexam_toothbrush=setHandednessexam_toothbrush;

	this.Handednessexam_knife=null;


	function getHandednessexam_knife() {
		return this.Handednessexam_knife;
	}
	this.getHandednessexam_knife=getHandednessexam_knife;


	function setHandednessexam_knife(v){
		this.Handednessexam_knife=v;
	}
	this.setHandednessexam_knife=setHandednessexam_knife;

	this.Handednessexam_spoon=null;


	function getHandednessexam_spoon() {
		return this.Handednessexam_spoon;
	}
	this.getHandednessexam_spoon=getHandednessexam_spoon;


	function setHandednessexam_spoon(v){
		this.Handednessexam_spoon=v;
	}
	this.setHandednessexam_spoon=setHandednessexam_spoon;

	this.Handednessexam_broom=null;


	function getHandednessexam_broom() {
		return this.Handednessexam_broom;
	}
	this.getHandednessexam_broom=getHandednessexam_broom;


	function setHandednessexam_broom(v){
		this.Handednessexam_broom=v;
	}
	this.setHandednessexam_broom=setHandednessexam_broom;

	this.Handednessexam_match=null;


	function getHandednessexam_match() {
		return this.Handednessexam_match;
	}
	this.getHandednessexam_match=getHandednessexam_match;


	function setHandednessexam_match(v){
		this.Handednessexam_match=v;
	}
	this.setHandednessexam_match=setHandednessexam_match;

	this.Handednessexam_box=null;


	function getHandednessexam_box() {
		return this.Handednessexam_box;
	}
	this.getHandednessexam_box=getHandednessexam_box;


	function setHandednessexam_box(v){
		this.Handednessexam_box=v;
	}
	this.setHandednessexam_box=setHandednessexam_box;

	this.Handednessexam_footkick=null;


	function getHandednessexam_footkick() {
		return this.Handednessexam_footkick;
	}
	this.getHandednessexam_footkick=getHandednessexam_footkick;


	function setHandednessexam_footkick(v){
		this.Handednessexam_footkick=v;
	}
	this.setHandednessexam_footkick=setHandednessexam_footkick;

	this.Handednessexam_eye=null;


	function getHandednessexam_eye() {
		return this.Handednessexam_eye;
	}
	this.getHandednessexam_eye=getHandednessexam_eye;


	function setHandednessexam_eye(v){
		this.Handednessexam_eye=v;
	}
	this.setHandednessexam_eye=setHandednessexam_eye;
	this.Relationships_relationship =new Array();

	function getRelationships_relationship() {
		return this.Relationships_relationship;
	}
	this.getRelationships_relationship=getRelationships_relationship;


	function addRelationships_relationship(v){
		this.Relationships_relationship.push(v);
	}
	this.addRelationships_relationship=addRelationships_relationship;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="demographicData"){
				return this.Demographicdata ;
			} else 
			if(xmlPath.startsWith("demographicData")){
				xmlPath=xmlPath.substring(15);
				if(xmlPath=="")return this.Demographicdata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Demographicdata!=undefined)return this.Demographicdata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="maritalStatus"){
				return this.Maritalstatus ;
			} else 
			if(xmlPath=="housingType"){
				return this.Housingtype ;
			} else 
			if(xmlPath=="siblingsNumber"){
				return this.Siblingsnumber ;
			} else 
			if(xmlPath=="siblingAge1"){
				return this.Siblingage1 ;
			} else 
			if(xmlPath=="siblingAge2"){
				return this.Siblingage2 ;
			} else 
			if(xmlPath=="siblingAge3"){
				return this.Siblingage3 ;
			} else 
			if(xmlPath=="siblingAge4"){
				return this.Siblingage4 ;
			} else 
			if(xmlPath=="siblingAge5"){
				return this.Siblingage5 ;
			} else 
			if(xmlPath=="siblingAge6"){
				return this.Siblingage6 ;
			} else 
			if(xmlPath=="siblingAge7"){
				return this.Siblingage7 ;
			} else 
			if(xmlPath=="siblingAge8"){
				return this.Siblingage8 ;
			} else 
			if(xmlPath=="numberOfChildren"){
				return this.Numberofchildren ;
			} else 
			if(xmlPath=="employmentStatus"){
				return this.Employmentstatus ;
			} else 
			if(xmlPath=="currentOccupation"){
				return this.Currentoccupation ;
			} else 
			if(xmlPath=="principalOccupation"){
				return this.Principaloccupation ;
			} else 
			if(xmlPath=="educationLevel"){
				return this.Educationlevel ;
			} else 
			if(xmlPath=="yearsOfSchooling"){
				return this.Yearsofschooling ;
			} else 
			if(xmlPath=="attendSpecialClasses"){
				return this.Attendspecialclasses ;
			} else 
			if(xmlPath=="specialClass"){
				return this.Specialclass ;
			} else 
			if(xmlPath=="educationLevelFather"){
				return this.Educationlevelfather ;
			} else 
			if(xmlPath=="schoolingLevelFather"){
				return this.Schoolinglevelfather ;
			} else 
			if(xmlPath=="educationLevelMother"){
				return this.Educationlevelmother ;
			} else 
			if(xmlPath=="schoolingLevelMother"){
				return this.Schoolinglevelmother ;
			} else 
			if(xmlPath=="sesDesc"){
				return this.Sesdesc ;
			} else 
			if(xmlPath=="handednessExam/writing"){
				return this.Handednessexam_writing ;
			} else 
			if(xmlPath=="handednessExam/throwing"){
				return this.Handednessexam_throwing ;
			} else 
			if(xmlPath=="handednessExam/scissors"){
				return this.Handednessexam_scissors ;
			} else 
			if(xmlPath=="handednessExam/toothbrush"){
				return this.Handednessexam_toothbrush ;
			} else 
			if(xmlPath=="handednessExam/knife"){
				return this.Handednessexam_knife ;
			} else 
			if(xmlPath=="handednessExam/spoon"){
				return this.Handednessexam_spoon ;
			} else 
			if(xmlPath=="handednessExam/broom"){
				return this.Handednessexam_broom ;
			} else 
			if(xmlPath=="handednessExam/match"){
				return this.Handednessexam_match ;
			} else 
			if(xmlPath=="handednessExam/box"){
				return this.Handednessexam_box ;
			} else 
			if(xmlPath=="handednessExam/footkick"){
				return this.Handednessexam_footkick ;
			} else 
			if(xmlPath=="handednessExam/eye"){
				return this.Handednessexam_eye ;
			} else 
			if(xmlPath=="relationships/relationship"){
				return this.Relationships_relationship ;
			} else 
			if(xmlPath.startsWith("relationships/relationship")){
				xmlPath=xmlPath.substring(26);
				if(xmlPath=="")return this.Relationships_relationship ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Relationships_relationship.length;whereCount++){

					var tempValue=this.Relationships_relationship[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Relationships_relationship[whereCount]);

					}

				}
				}else{

				whereArray=this.Relationships_relationship;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="demographicData"){
				this.Demographicdata=value;
			} else 
			if(xmlPath.startsWith("demographicData")){
				xmlPath=xmlPath.substring(15);
				if(xmlPath=="")return this.Demographicdata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Demographicdata!=undefined){
					this.Demographicdata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Demographicdata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Demographicdata= instanciateObject("xnat:demographicData");//omUtils.js
						}
						if(options && options.where)this.Demographicdata.setProperty(options.where.field,options.where.value);
						this.Demographicdata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="maritalStatus"){
				this.Maritalstatus=value;
			} else 
			if(xmlPath=="housingType"){
				this.Housingtype=value;
			} else 
			if(xmlPath=="siblingsNumber"){
				this.Siblingsnumber=value;
			} else 
			if(xmlPath=="siblingAge1"){
				this.Siblingage1=value;
			} else 
			if(xmlPath=="siblingAge2"){
				this.Siblingage2=value;
			} else 
			if(xmlPath=="siblingAge3"){
				this.Siblingage3=value;
			} else 
			if(xmlPath=="siblingAge4"){
				this.Siblingage4=value;
			} else 
			if(xmlPath=="siblingAge5"){
				this.Siblingage5=value;
			} else 
			if(xmlPath=="siblingAge6"){
				this.Siblingage6=value;
			} else 
			if(xmlPath=="siblingAge7"){
				this.Siblingage7=value;
			} else 
			if(xmlPath=="siblingAge8"){
				this.Siblingage8=value;
			} else 
			if(xmlPath=="numberOfChildren"){
				this.Numberofchildren=value;
			} else 
			if(xmlPath=="employmentStatus"){
				this.Employmentstatus=value;
			} else 
			if(xmlPath=="currentOccupation"){
				this.Currentoccupation=value;
			} else 
			if(xmlPath=="principalOccupation"){
				this.Principaloccupation=value;
			} else 
			if(xmlPath=="educationLevel"){
				this.Educationlevel=value;
			} else 
			if(xmlPath=="yearsOfSchooling"){
				this.Yearsofschooling=value;
			} else 
			if(xmlPath=="attendSpecialClasses"){
				this.Attendspecialclasses=value;
			} else 
			if(xmlPath=="specialClass"){
				this.Specialclass=value;
			} else 
			if(xmlPath=="educationLevelFather"){
				this.Educationlevelfather=value;
			} else 
			if(xmlPath=="schoolingLevelFather"){
				this.Schoolinglevelfather=value;
			} else 
			if(xmlPath=="educationLevelMother"){
				this.Educationlevelmother=value;
			} else 
			if(xmlPath=="schoolingLevelMother"){
				this.Schoolinglevelmother=value;
			} else 
			if(xmlPath=="sesDesc"){
				this.Sesdesc=value;
			} else 
			if(xmlPath=="handednessExam/writing"){
				this.Handednessexam_writing=value;
			} else 
			if(xmlPath=="handednessExam/throwing"){
				this.Handednessexam_throwing=value;
			} else 
			if(xmlPath=="handednessExam/scissors"){
				this.Handednessexam_scissors=value;
			} else 
			if(xmlPath=="handednessExam/toothbrush"){
				this.Handednessexam_toothbrush=value;
			} else 
			if(xmlPath=="handednessExam/knife"){
				this.Handednessexam_knife=value;
			} else 
			if(xmlPath=="handednessExam/spoon"){
				this.Handednessexam_spoon=value;
			} else 
			if(xmlPath=="handednessExam/broom"){
				this.Handednessexam_broom=value;
			} else 
			if(xmlPath=="handednessExam/match"){
				this.Handednessexam_match=value;
			} else 
			if(xmlPath=="handednessExam/box"){
				this.Handednessexam_box=value;
			} else 
			if(xmlPath=="handednessExam/footkick"){
				this.Handednessexam_footkick=value;
			} else 
			if(xmlPath=="handednessExam/eye"){
				this.Handednessexam_eye=value;
			} else 
			if(xmlPath=="relationships/relationship"){
				this.Relationships_relationship=value;
			} else 
			if(xmlPath.startsWith("relationships/relationship")){
				xmlPath=xmlPath.substring(26);
				if(xmlPath=="")return this.Relationships_relationship ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Relationships_relationship.length;whereCount++){

					var tempValue=this.Relationships_relationship[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Relationships_relationship[whereCount]);

					}

				}
				}else{

				whereArray=this.Relationships_relationship;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("nunda:nundaDemographicData_relationship");//omUtils.js
					}
					this.addRelationships_relationship(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="relationships/relationship"){
			this.addRelationships_relationship(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="relationships/relationship"){
			return "http://nrg.wustl.edu/nunda:nundaDemographicData_relationship";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="maritalStatus"){
			return "field_data";
		}else if (xmlPath=="housingType"){
			return "field_data";
		}else if (xmlPath=="siblingsNumber"){
			return "field_data";
		}else if (xmlPath=="siblingAge1"){
			return "field_data";
		}else if (xmlPath=="siblingAge2"){
			return "field_data";
		}else if (xmlPath=="siblingAge3"){
			return "field_data";
		}else if (xmlPath=="siblingAge4"){
			return "field_data";
		}else if (xmlPath=="siblingAge5"){
			return "field_data";
		}else if (xmlPath=="siblingAge6"){
			return "field_data";
		}else if (xmlPath=="siblingAge7"){
			return "field_data";
		}else if (xmlPath=="siblingAge8"){
			return "field_data";
		}else if (xmlPath=="numberOfChildren"){
			return "field_data";
		}else if (xmlPath=="employmentStatus"){
			return "field_data";
		}else if (xmlPath=="currentOccupation"){
			return "field_data";
		}else if (xmlPath=="principalOccupation"){
			return "field_data";
		}else if (xmlPath=="educationLevel"){
			return "field_data";
		}else if (xmlPath=="yearsOfSchooling"){
			return "field_data";
		}else if (xmlPath=="attendSpecialClasses"){
			return "field_data";
		}else if (xmlPath=="specialClass"){
			return "field_data";
		}else if (xmlPath=="educationLevelFather"){
			return "field_data";
		}else if (xmlPath=="schoolingLevelFather"){
			return "field_data";
		}else if (xmlPath=="educationLevelMother"){
			return "field_data";
		}else if (xmlPath=="schoolingLevelMother"){
			return "field_data";
		}else if (xmlPath=="sesDesc"){
			return "field_data";
		}else if (xmlPath=="handednessExam/writing"){
			return "field_data";
		}else if (xmlPath=="handednessExam/throwing"){
			return "field_data";
		}else if (xmlPath=="handednessExam/scissors"){
			return "field_data";
		}else if (xmlPath=="handednessExam/toothbrush"){
			return "field_data";
		}else if (xmlPath=="handednessExam/knife"){
			return "field_data";
		}else if (xmlPath=="handednessExam/spoon"){
			return "field_data";
		}else if (xmlPath=="handednessExam/broom"){
			return "field_data";
		}else if (xmlPath=="handednessExam/match"){
			return "field_data";
		}else if (xmlPath=="handednessExam/box"){
			return "field_data";
		}else if (xmlPath=="handednessExam/footkick"){
			return "field_data";
		}else if (xmlPath=="handednessExam/eye"){
			return "field_data";
		}else if (xmlPath=="relationships/relationship"){
			return "field_multi_reference";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<nunda:nundaDemographicData";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</nunda:nundaDemographicData>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Maritalstatus!=null){
			xmlTxt+="\n<nunda:maritalStatus";
			xmlTxt+=">";
			xmlTxt+=this.Maritalstatus;
			xmlTxt+="</nunda:maritalStatus>";
		}
		if (this.Housingtype!=null){
			xmlTxt+="\n<nunda:housingType";
			xmlTxt+=">";
			xmlTxt+=this.Housingtype;
			xmlTxt+="</nunda:housingType>";
		}
		if (this.Siblingsnumber!=null){
			xmlTxt+="\n<nunda:siblingsNumber";
			xmlTxt+=">";
			xmlTxt+=this.Siblingsnumber;
			xmlTxt+="</nunda:siblingsNumber>";
		}
		if (this.Siblingage1!=null){
			xmlTxt+="\n<nunda:siblingAge1";
			xmlTxt+=">";
			xmlTxt+=this.Siblingage1.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:siblingAge1>";
		}
		if (this.Siblingage2!=null){
			xmlTxt+="\n<nunda:siblingAge2";
			xmlTxt+=">";
			xmlTxt+=this.Siblingage2.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:siblingAge2>";
		}
		if (this.Siblingage3!=null){
			xmlTxt+="\n<nunda:siblingAge3";
			xmlTxt+=">";
			xmlTxt+=this.Siblingage3.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:siblingAge3>";
		}
		if (this.Siblingage4!=null){
			xmlTxt+="\n<nunda:siblingAge4";
			xmlTxt+=">";
			xmlTxt+=this.Siblingage4.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:siblingAge4>";
		}
		if (this.Siblingage5!=null){
			xmlTxt+="\n<nunda:siblingAge5";
			xmlTxt+=">";
			xmlTxt+=this.Siblingage5.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:siblingAge5>";
		}
		if (this.Siblingage6!=null){
			xmlTxt+="\n<nunda:siblingAge6";
			xmlTxt+=">";
			xmlTxt+=this.Siblingage6.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:siblingAge6>";
		}
		if (this.Siblingage7!=null){
			xmlTxt+="\n<nunda:siblingAge7";
			xmlTxt+=">";
			xmlTxt+=this.Siblingage7.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:siblingAge7>";
		}
		if (this.Siblingage8!=null){
			xmlTxt+="\n<nunda:siblingAge8";
			xmlTxt+=">";
			xmlTxt+=this.Siblingage8.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:siblingAge8>";
		}
		if (this.Numberofchildren!=null){
			xmlTxt+="\n<nunda:numberOfChildren";
			xmlTxt+=">";
			xmlTxt+=this.Numberofchildren;
			xmlTxt+="</nunda:numberOfChildren>";
		}
		if (this.Employmentstatus!=null){
			xmlTxt+="\n<nunda:employmentStatus";
			xmlTxt+=">";
			xmlTxt+=this.Employmentstatus;
			xmlTxt+="</nunda:employmentStatus>";
		}
		if (this.Currentoccupation!=null){
			xmlTxt+="\n<nunda:currentOccupation";
			xmlTxt+=">";
			xmlTxt+=this.Currentoccupation.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:currentOccupation>";
		}
		if (this.Principaloccupation!=null){
			xmlTxt+="\n<nunda:principalOccupation";
			xmlTxt+=">";
			xmlTxt+=this.Principaloccupation.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:principalOccupation>";
		}
		if (this.Educationlevel!=null){
			xmlTxt+="\n<nunda:educationLevel";
			xmlTxt+=">";
			xmlTxt+=this.Educationlevel;
			xmlTxt+="</nunda:educationLevel>";
		}
		if (this.Yearsofschooling!=null){
			xmlTxt+="\n<nunda:yearsOfSchooling";
			xmlTxt+=">";
			xmlTxt+=this.Yearsofschooling;
			xmlTxt+="</nunda:yearsOfSchooling>";
		}
		if (this.Attendspecialclasses!=null){
			xmlTxt+="\n<nunda:attendSpecialClasses";
			xmlTxt+=">";
			xmlTxt+=this.Attendspecialclasses;
			xmlTxt+="</nunda:attendSpecialClasses>";
		}
		if (this.Specialclass!=null){
			xmlTxt+="\n<nunda:specialClass";
			xmlTxt+=">";
			xmlTxt+=this.Specialclass.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:specialClass>";
		}
		if (this.Educationlevelfather!=null){
			xmlTxt+="\n<nunda:educationLevelFather";
			xmlTxt+=">";
			xmlTxt+=this.Educationlevelfather;
			xmlTxt+="</nunda:educationLevelFather>";
		}
		if (this.Schoolinglevelfather!=null){
			xmlTxt+="\n<nunda:schoolingLevelFather";
			xmlTxt+=">";
			xmlTxt+=this.Schoolinglevelfather.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:schoolingLevelFather>";
		}
		if (this.Educationlevelmother!=null){
			xmlTxt+="\n<nunda:educationLevelMother";
			xmlTxt+=">";
			xmlTxt+=this.Educationlevelmother;
			xmlTxt+="</nunda:educationLevelMother>";
		}
		if (this.Schoolinglevelmother!=null){
			xmlTxt+="\n<nunda:schoolingLevelMother";
			xmlTxt+=">";
			xmlTxt+=this.Schoolinglevelmother.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:schoolingLevelMother>";
		}
		if (this.Sesdesc!=null){
			xmlTxt+="\n<nunda:sesDesc";
			xmlTxt+=">";
			xmlTxt+=this.Sesdesc.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:sesDesc>";
		}
			var child0=0;
			var att0=0;
			if(this.Handednessexam_throwing!=null)
			child0++;
			if(this.Handednessexam_toothbrush!=null)
			child0++;
			if(this.Handednessexam_writing!=null)
			child0++;
			if(this.Handednessexam_match!=null)
			child0++;
			if(this.Handednessexam_broom!=null)
			child0++;
			if(this.Handednessexam_spoon!=null)
			child0++;
			if(this.Handednessexam_eye!=null)
			child0++;
			if(this.Handednessexam_box!=null)
			child0++;
			if(this.Handednessexam_footkick!=null)
			child0++;
			if(this.Handednessexam_scissors!=null)
			child0++;
			if(this.Handednessexam_knife!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<nunda:handednessExam";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Handednessexam_writing!=null){
			xmlTxt+="\n<nunda:writing";
			xmlTxt+=">";
			xmlTxt+=this.Handednessexam_writing;
			xmlTxt+="</nunda:writing>";
		}
		if (this.Handednessexam_throwing!=null){
			xmlTxt+="\n<nunda:throwing";
			xmlTxt+=">";
			xmlTxt+=this.Handednessexam_throwing;
			xmlTxt+="</nunda:throwing>";
		}
		if (this.Handednessexam_scissors!=null){
			xmlTxt+="\n<nunda:scissors";
			xmlTxt+=">";
			xmlTxt+=this.Handednessexam_scissors;
			xmlTxt+="</nunda:scissors>";
		}
		if (this.Handednessexam_toothbrush!=null){
			xmlTxt+="\n<nunda:toothbrush";
			xmlTxt+=">";
			xmlTxt+=this.Handednessexam_toothbrush;
			xmlTxt+="</nunda:toothbrush>";
		}
		if (this.Handednessexam_knife!=null){
			xmlTxt+="\n<nunda:knife";
			xmlTxt+=">";
			xmlTxt+=this.Handednessexam_knife;
			xmlTxt+="</nunda:knife>";
		}
		if (this.Handednessexam_spoon!=null){
			xmlTxt+="\n<nunda:spoon";
			xmlTxt+=">";
			xmlTxt+=this.Handednessexam_spoon;
			xmlTxt+="</nunda:spoon>";
		}
		if (this.Handednessexam_broom!=null){
			xmlTxt+="\n<nunda:broom";
			xmlTxt+=">";
			xmlTxt+=this.Handednessexam_broom;
			xmlTxt+="</nunda:broom>";
		}
		if (this.Handednessexam_match!=null){
			xmlTxt+="\n<nunda:match";
			xmlTxt+=">";
			xmlTxt+=this.Handednessexam_match;
			xmlTxt+="</nunda:match>";
		}
		if (this.Handednessexam_box!=null){
			xmlTxt+="\n<nunda:box";
			xmlTxt+=">";
			xmlTxt+=this.Handednessexam_box;
			xmlTxt+="</nunda:box>";
		}
		if (this.Handednessexam_footkick!=null){
			xmlTxt+="\n<nunda:footkick";
			xmlTxt+=">";
			xmlTxt+=this.Handednessexam_footkick;
			xmlTxt+="</nunda:footkick>";
		}
		if (this.Handednessexam_eye!=null){
			xmlTxt+="\n<nunda:eye";
			xmlTxt+=">";
			xmlTxt+=this.Handednessexam_eye;
			xmlTxt+="</nunda:eye>";
		}
				xmlTxt+="\n</nunda:handednessExam>";
			}
			}

			var child1=0;
			var att1=0;
			child1+=this.Relationships_relationship.length;
			if(child1>0 || att1>0){
				xmlTxt+="\n<nunda:relationships";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Relationships_relationshipCOUNT=0;Relationships_relationshipCOUNT<this.Relationships_relationship.length;Relationships_relationshipCOUNT++){
			xmlTxt +="\n<nunda:relationship";
			xmlTxt +=this.Relationships_relationship[Relationships_relationshipCOUNT].getXMLAtts();
			if(this.Relationships_relationship[Relationships_relationshipCOUNT].xsiType!="nunda:nundaDemographicData_relationship"){
				xmlTxt+=" xsi:type=\"" + this.Relationships_relationship[Relationships_relationshipCOUNT].xsiType + "\"";
			}
			if (this.Relationships_relationship[Relationships_relationshipCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Relationships_relationship[Relationships_relationshipCOUNT].getXMLBody(preventComments);
					xmlTxt+="</nunda:relationship>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</nunda:relationships>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Maritalstatus!=null) return true;
		if (this.Housingtype!=null) return true;
		if (this.Siblingsnumber!=null) return true;
		if (this.Siblingage1!=null) return true;
		if (this.Siblingage2!=null) return true;
		if (this.Siblingage3!=null) return true;
		if (this.Siblingage4!=null) return true;
		if (this.Siblingage5!=null) return true;
		if (this.Siblingage6!=null) return true;
		if (this.Siblingage7!=null) return true;
		if (this.Siblingage8!=null) return true;
		if (this.Numberofchildren!=null) return true;
		if (this.Employmentstatus!=null) return true;
		if (this.Currentoccupation!=null) return true;
		if (this.Principaloccupation!=null) return true;
		if (this.Educationlevel!=null) return true;
		if (this.Yearsofschooling!=null) return true;
		if (this.Attendspecialclasses!=null) return true;
		if (this.Specialclass!=null) return true;
		if (this.Educationlevelfather!=null) return true;
		if (this.Schoolinglevelfather!=null) return true;
		if (this.Educationlevelmother!=null) return true;
		if (this.Schoolinglevelmother!=null) return true;
		if (this.Sesdesc!=null) return true;
			if(this.Handednessexam_throwing!=null) return true;
			if(this.Handednessexam_toothbrush!=null) return true;
			if(this.Handednessexam_writing!=null) return true;
			if(this.Handednessexam_match!=null) return true;
			if(this.Handednessexam_broom!=null) return true;
			if(this.Handednessexam_spoon!=null) return true;
			if(this.Handednessexam_eye!=null) return true;
			if(this.Handednessexam_box!=null) return true;
			if(this.Handednessexam_footkick!=null) return true;
			if(this.Handednessexam_scissors!=null) return true;
			if(this.Handednessexam_knife!=null) return true;
			if(this.Relationships_relationship.length>0)return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
