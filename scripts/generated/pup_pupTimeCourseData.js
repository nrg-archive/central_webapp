/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:49 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function pup_pupTimeCourseData(){
this.xsiType="pup:pupTimeCourseData";

	this.getSchemaElementName=function(){
		return "pupTimeCourseData";
	}

	this.getFullSchemaElementName=function(){
		return "pup:pupTimeCourseData";
	}
this.extension=dynamicJSLoad('xnat_petAssessorData','generated/xnat_petAssessorData.js');

	this.Userfullname=null;


	function getUserfullname() {
		return this.Userfullname;
	}
	this.getUserfullname=getUserfullname;


	function setUserfullname(v){
		this.Userfullname=v;
	}
	this.setUserfullname=setUserfullname;

	this.Petlabel=null;


	function getPetlabel() {
		return this.Petlabel;
	}
	this.getPetlabel=getPetlabel;


	function setPetlabel(v){
		this.Petlabel=v;
	}
	this.setPetlabel=setPetlabel;

	this.Petscanid=null;


	function getPetscanid() {
		return this.Petscanid;
	}
	this.getPetscanid=getPetscanid;


	function setPetscanid(v){
		this.Petscanid=v;
	}
	this.setPetscanid=setPetscanid;

	this.Proctype=null;


	function getProctype() {
		return this.Proctype;
	}
	this.getProctype=getProctype;


	function setProctype(v){
		this.Proctype=v;
	}
	this.setProctype=setProctype;

	this.Model=null;


	function getModel() {
		return this.Model;
	}
	this.getModel=getModel;


	function setModel(v){
		this.Model=v;
	}
	this.setModel=setModel;

	this.Tracer=null;


	function getTracer() {
		return this.Tracer;
	}
	this.getTracer=getTracer;


	function setTracer(v){
		this.Tracer=v;
	}
	this.setTracer=setTracer;

	this.Halflife=null;


	function getHalflife() {
		return this.Halflife;
	}
	this.getHalflife=getHalflife;


	function setHalflife(v){
		this.Halflife=v;
	}
	this.setHalflife=setHalflife;

	this.Templatetype=null;


	function getTemplatetype() {
		return this.Templatetype;
	}
	this.getTemplatetype=getTemplatetype;


	function setTemplatetype(v){
		this.Templatetype=v;
	}
	this.setTemplatetype=setTemplatetype;

	this.Fsid=null;


	function getFsid() {
		return this.Fsid;
	}
	this.getFsid=getFsid;


	function setFsid(v){
		this.Fsid=v;
	}
	this.setFsid=setFsid;

	this.T1=null;


	function getT1() {
		return this.T1;
	}
	this.getT1=getT1;


	function setT1(v){
		this.T1=v;
	}
	this.setT1=setT1;

	this.Mrid=null;


	function getMrid() {
		return this.Mrid;
	}
	this.getMrid=getMrid;


	function setMrid(v){
		this.Mrid=v;
	}
	this.setMrid=setMrid;

	this.Mocoerror=null;


	function getMocoerror() {
		return this.Mocoerror;
	}
	this.getMocoerror=getMocoerror;


	function setMocoerror(v){
		this.Mocoerror=v;
	}
	this.setMocoerror=setMocoerror;

	this.Regerror=null;


	function getRegerror() {
		return this.Regerror;
	}
	this.getRegerror=getRegerror;


	function setRegerror(v){
		this.Regerror=v;
	}
	this.setRegerror=setRegerror;

	this.Suvrflag=null;


	function getSuvrflag() {
		return this.Suvrflag;
	}
	this.getSuvrflag=getSuvrflag;


	function setSuvrflag(v){
		this.Suvrflag=v;
	}
	this.setSuvrflag=setSuvrflag;

	this.Refroistr=null;


	function getRefroistr() {
		return this.Refroistr;
	}
	this.getRefroistr=getRefroistr;


	function setRefroistr(v){
		this.Refroistr=v;
	}
	this.setRefroistr=setRefroistr;

	this.Delayparam=null;


	function getDelayparam() {
		return this.Delayparam;
	}
	this.getDelayparam=getDelayparam;


	function setDelayparam(v){
		this.Delayparam=v;
	}
	this.setDelayparam=setDelayparam;

	this.Mst=null;


	function getMst() {
		return this.Mst;
	}
	this.getMst=getMst;


	function setMst(v){
		this.Mst=v;
	}
	this.setMst=setMst;

	this.Mdt=null;


	function getMdt() {
		return this.Mdt;
	}
	this.getMdt=getMdt;


	function setMdt(v){
		this.Mdt=v;
	}
	this.setMdt=setMdt;

	this.Tbl=null;


	function getTbl() {
		return this.Tbl;
	}
	this.getTbl=getTbl;


	function setTbl(v){
		this.Tbl=v;
	}
	this.setTbl=setTbl;

	this.Rbf=null;


	function getRbf() {
		return this.Rbf;
	}
	this.getRbf=getRbf;


	function setRbf(v){
		this.Rbf=v;
	}
	this.setRbf=setRbf;

	this.Mbf=null;


	function getMbf() {
		return this.Mbf;
	}
	this.getMbf=getMbf;


	function setMbf(v){
		this.Mbf=v;
	}
	this.setMbf=setMbf;

	this.Sf=null;


	function getSf() {
		return this.Sf;
	}
	this.getSf=getSf;


	function setSf(v){
		this.Sf=v;
	}
	this.setSf=setSf;

	this.Ef=null;


	function getEf() {
		return this.Ef;
	}
	this.getEf=getEf;


	function setEf(v){
		this.Ef=v;
	}
	this.setEf=setEf;

	this.Filter=null;


	function getFilter() {
		return this.Filter;
	}
	this.getFilter=getFilter;


	function setFilter(v){
		this.Filter=v;
	}
	this.setFilter=setFilter;

	this.Filterxy=null;


	function getFilterxy() {
		return this.Filterxy;
	}
	this.getFilterxy=getFilterxy;


	function setFilterxy(v){
		this.Filterxy=v;
	}
	this.setFilterxy=setFilterxy;

	this.Filterz=null;


	function getFilterz() {
		return this.Filterz;
	}
	this.getFilterz=getFilterz;


	function setFilterz(v){
		this.Filterz=v;
	}
	this.setFilterz=setFilterz;

	this.Fwhm=null;


	function getFwhm() {
		return this.Fwhm;
	}
	this.getFwhm=getFwhm;


	function setFwhm(v){
		this.Fwhm=v;
	}
	this.setFwhm=setFwhm;

	this.Pvc2cflag=null;


	function getPvc2cflag() {
		return this.Pvc2cflag;
	}
	this.getPvc2cflag=getPvc2cflag;


	function setPvc2cflag(v){
		this.Pvc2cflag=v;
	}
	this.setPvc2cflag=setPvc2cflag;

	this.Rsfflag=null;


	function getRsfflag() {
		return this.Rsfflag;
	}
	this.getRsfflag=getRsfflag;


	function setRsfflag(v){
		this.Rsfflag=v;
	}
	this.setRsfflag=setRsfflag;

	this.Pupstatus=null;


	function getPupstatus() {
		return this.Pupstatus;
	}
	this.getPupstatus=getPupstatus;


	function setPupstatus(v){
		this.Pupstatus=v;
	}
	this.setPupstatus=setPupstatus;
	this.Rois_roi =new Array();

	function getRois_roi() {
		return this.Rois_roi;
	}
	this.getRois_roi=getRois_roi;


	function addRois_roi(v){
		this.Rois_roi.push(v);
	}
	this.addRois_roi=addRois_roi;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="petAssessorData"){
				return this.Petassessordata ;
			} else 
			if(xmlPath.startsWith("petAssessorData")){
				xmlPath=xmlPath.substring(15);
				if(xmlPath=="")return this.Petassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Petassessordata!=undefined)return this.Petassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="userfullname"){
				return this.Userfullname ;
			} else 
			if(xmlPath=="petLabel"){
				return this.Petlabel ;
			} else 
			if(xmlPath=="petScanId"){
				return this.Petscanid ;
			} else 
			if(xmlPath=="procType"){
				return this.Proctype ;
			} else 
			if(xmlPath=="model"){
				return this.Model ;
			} else 
			if(xmlPath=="tracer"){
				return this.Tracer ;
			} else 
			if(xmlPath=="halfLife"){
				return this.Halflife ;
			} else 
			if(xmlPath=="templateType"){
				return this.Templatetype ;
			} else 
			if(xmlPath=="FSId"){
				return this.Fsid ;
			} else 
			if(xmlPath=="t1"){
				return this.T1 ;
			} else 
			if(xmlPath=="MRId"){
				return this.Mrid ;
			} else 
			if(xmlPath=="mocoError"){
				return this.Mocoerror ;
			} else 
			if(xmlPath=="regError"){
				return this.Regerror ;
			} else 
			if(xmlPath=="suvrFlag"){
				return this.Suvrflag ;
			} else 
			if(xmlPath=="refroistr"){
				return this.Refroistr ;
			} else 
			if(xmlPath=="delayParam"){
				return this.Delayparam ;
			} else 
			if(xmlPath=="mst"){
				return this.Mst ;
			} else 
			if(xmlPath=="mdt"){
				return this.Mdt ;
			} else 
			if(xmlPath=="tbl"){
				return this.Tbl ;
			} else 
			if(xmlPath=="rbf"){
				return this.Rbf ;
			} else 
			if(xmlPath=="mbf"){
				return this.Mbf ;
			} else 
			if(xmlPath=="sf"){
				return this.Sf ;
			} else 
			if(xmlPath=="ef"){
				return this.Ef ;
			} else 
			if(xmlPath=="filter"){
				return this.Filter ;
			} else 
			if(xmlPath=="filterxy"){
				return this.Filterxy ;
			} else 
			if(xmlPath=="filterz"){
				return this.Filterz ;
			} else 
			if(xmlPath=="fwhm"){
				return this.Fwhm ;
			} else 
			if(xmlPath=="pvc2cflag"){
				return this.Pvc2cflag ;
			} else 
			if(xmlPath=="rsfflag"){
				return this.Rsfflag ;
			} else 
			if(xmlPath=="pupStatus"){
				return this.Pupstatus ;
			} else 
			if(xmlPath=="rois/roi"){
				return this.Rois_roi ;
			} else 
			if(xmlPath.startsWith("rois/roi")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Rois_roi ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Rois_roi.length;whereCount++){

					var tempValue=this.Rois_roi[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Rois_roi[whereCount]);

					}

				}
				}else{

				whereArray=this.Rois_roi;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="petAssessorData"){
				this.Petassessordata=value;
			} else 
			if(xmlPath.startsWith("petAssessorData")){
				xmlPath=xmlPath.substring(15);
				if(xmlPath=="")return this.Petassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Petassessordata!=undefined){
					this.Petassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Petassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Petassessordata= instanciateObject("xnat:petAssessorData");//omUtils.js
						}
						if(options && options.where)this.Petassessordata.setProperty(options.where.field,options.where.value);
						this.Petassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="userfullname"){
				this.Userfullname=value;
			} else 
			if(xmlPath=="petLabel"){
				this.Petlabel=value;
			} else 
			if(xmlPath=="petScanId"){
				this.Petscanid=value;
			} else 
			if(xmlPath=="procType"){
				this.Proctype=value;
			} else 
			if(xmlPath=="model"){
				this.Model=value;
			} else 
			if(xmlPath=="tracer"){
				this.Tracer=value;
			} else 
			if(xmlPath=="halfLife"){
				this.Halflife=value;
			} else 
			if(xmlPath=="templateType"){
				this.Templatetype=value;
			} else 
			if(xmlPath=="FSId"){
				this.Fsid=value;
			} else 
			if(xmlPath=="t1"){
				this.T1=value;
			} else 
			if(xmlPath=="MRId"){
				this.Mrid=value;
			} else 
			if(xmlPath=="mocoError"){
				this.Mocoerror=value;
			} else 
			if(xmlPath=="regError"){
				this.Regerror=value;
			} else 
			if(xmlPath=="suvrFlag"){
				this.Suvrflag=value;
			} else 
			if(xmlPath=="refroistr"){
				this.Refroistr=value;
			} else 
			if(xmlPath=="delayParam"){
				this.Delayparam=value;
			} else 
			if(xmlPath=="mst"){
				this.Mst=value;
			} else 
			if(xmlPath=="mdt"){
				this.Mdt=value;
			} else 
			if(xmlPath=="tbl"){
				this.Tbl=value;
			} else 
			if(xmlPath=="rbf"){
				this.Rbf=value;
			} else 
			if(xmlPath=="mbf"){
				this.Mbf=value;
			} else 
			if(xmlPath=="sf"){
				this.Sf=value;
			} else 
			if(xmlPath=="ef"){
				this.Ef=value;
			} else 
			if(xmlPath=="filter"){
				this.Filter=value;
			} else 
			if(xmlPath=="filterxy"){
				this.Filterxy=value;
			} else 
			if(xmlPath=="filterz"){
				this.Filterz=value;
			} else 
			if(xmlPath=="fwhm"){
				this.Fwhm=value;
			} else 
			if(xmlPath=="pvc2cflag"){
				this.Pvc2cflag=value;
			} else 
			if(xmlPath=="rsfflag"){
				this.Rsfflag=value;
			} else 
			if(xmlPath=="pupStatus"){
				this.Pupstatus=value;
			} else 
			if(xmlPath=="rois/roi"){
				this.Rois_roi=value;
			} else 
			if(xmlPath.startsWith("rois/roi")){
				xmlPath=xmlPath.substring(8);
				if(xmlPath=="")return this.Rois_roi ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Rois_roi.length;whereCount++){

					var tempValue=this.Rois_roi[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Rois_roi[whereCount]);

					}

				}
				}else{

				whereArray=this.Rois_roi;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("pup:pupTimeCourseData_roi");//omUtils.js
					}
					this.addRois_roi(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="rois/roi"){
			this.addRois_roi(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="rois/roi"){
			return "http://nrg.wustl.edu/pup:pupTimeCourseData_roi";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="userfullname"){
			return "field_data";
		}else if (xmlPath=="petLabel"){
			return "field_data";
		}else if (xmlPath=="petScanId"){
			return "field_data";
		}else if (xmlPath=="procType"){
			return "field_data";
		}else if (xmlPath=="model"){
			return "field_data";
		}else if (xmlPath=="tracer"){
			return "field_data";
		}else if (xmlPath=="halfLife"){
			return "field_data";
		}else if (xmlPath=="templateType"){
			return "field_data";
		}else if (xmlPath=="FSId"){
			return "field_data";
		}else if (xmlPath=="t1"){
			return "field_data";
		}else if (xmlPath=="MRId"){
			return "field_data";
		}else if (xmlPath=="mocoError"){
			return "field_data";
		}else if (xmlPath=="regError"){
			return "field_data";
		}else if (xmlPath=="suvrFlag"){
			return "field_data";
		}else if (xmlPath=="refroistr"){
			return "field_data";
		}else if (xmlPath=="delayParam"){
			return "field_data";
		}else if (xmlPath=="mst"){
			return "field_data";
		}else if (xmlPath=="mdt"){
			return "field_data";
		}else if (xmlPath=="tbl"){
			return "field_data";
		}else if (xmlPath=="rbf"){
			return "field_data";
		}else if (xmlPath=="mbf"){
			return "field_data";
		}else if (xmlPath=="sf"){
			return "field_data";
		}else if (xmlPath=="ef"){
			return "field_data";
		}else if (xmlPath=="filter"){
			return "field_data";
		}else if (xmlPath=="filterxy"){
			return "field_data";
		}else if (xmlPath=="filterz"){
			return "field_data";
		}else if (xmlPath=="fwhm"){
			return "field_data";
		}else if (xmlPath=="pvc2cflag"){
			return "field_data";
		}else if (xmlPath=="rsfflag"){
			return "field_data";
		}else if (xmlPath=="pupStatus"){
			return "field_data";
		}else if (xmlPath=="rois/roi"){
			return "field_multi_reference";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<pup:PUPTimeCourse";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</pup:PUPTimeCourse>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Userfullname!=null){
			xmlTxt+="\n<pup:userfullname";
			xmlTxt+=">";
			xmlTxt+=this.Userfullname.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:userfullname>";
		}
		if (this.Petlabel!=null){
			xmlTxt+="\n<pup:petLabel";
			xmlTxt+=">";
			xmlTxt+=this.Petlabel.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:petLabel>";
		}
		if (this.Petscanid!=null){
			xmlTxt+="\n<pup:petScanId";
			xmlTxt+=">";
			xmlTxt+=this.Petscanid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:petScanId>";
		}
		if (this.Proctype!=null){
			xmlTxt+="\n<pup:procType";
			xmlTxt+=">";
			xmlTxt+=this.Proctype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:procType>";
		}
		if (this.Model!=null){
			xmlTxt+="\n<pup:model";
			xmlTxt+=">";
			xmlTxt+=this.Model.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:model>";
		}
		if (this.Tracer!=null){
			xmlTxt+="\n<pup:tracer";
			xmlTxt+=">";
			xmlTxt+=this.Tracer.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:tracer>";
		}
		if (this.Halflife!=null){
			xmlTxt+="\n<pup:halfLife";
			xmlTxt+=">";
			xmlTxt+=this.Halflife.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:halfLife>";
		}
		if (this.Templatetype!=null){
			xmlTxt+="\n<pup:templateType";
			xmlTxt+=">";
			xmlTxt+=this.Templatetype.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:templateType>";
		}
		if (this.Fsid!=null){
			xmlTxt+="\n<pup:FSId";
			xmlTxt+=">";
			xmlTxt+=this.Fsid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:FSId>";
		}
		if (this.T1!=null){
			xmlTxt+="\n<pup:t1";
			xmlTxt+=">";
			xmlTxt+=this.T1.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:t1>";
		}
		if (this.Mrid!=null){
			xmlTxt+="\n<pup:MRId";
			xmlTxt+=">";
			xmlTxt+=this.Mrid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:MRId>";
		}
		if (this.Mocoerror!=null){
			xmlTxt+="\n<pup:mocoError";
			xmlTxt+=">";
			xmlTxt+=this.Mocoerror;
			xmlTxt+="</pup:mocoError>";
		}
		if (this.Regerror!=null){
			xmlTxt+="\n<pup:regError";
			xmlTxt+=">";
			xmlTxt+=this.Regerror;
			xmlTxt+="</pup:regError>";
		}
		if (this.Suvrflag!=null){
			xmlTxt+="\n<pup:suvrFlag";
			xmlTxt+=">";
			xmlTxt+=this.Suvrflag;
			xmlTxt+="</pup:suvrFlag>";
		}
		if (this.Refroistr!=null){
			xmlTxt+="\n<pup:refroistr";
			xmlTxt+=">";
			xmlTxt+=this.Refroistr.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:refroistr>";
		}
		if (this.Delayparam!=null){
			xmlTxt+="\n<pup:delayParam";
			xmlTxt+=">";
			xmlTxt+=this.Delayparam;
			xmlTxt+="</pup:delayParam>";
		}
		if (this.Mst!=null){
			xmlTxt+="\n<pup:mst";
			xmlTxt+=">";
			xmlTxt+=this.Mst;
			xmlTxt+="</pup:mst>";
		}
		if (this.Mdt!=null){
			xmlTxt+="\n<pup:mdt";
			xmlTxt+=">";
			xmlTxt+=this.Mdt;
			xmlTxt+="</pup:mdt>";
		}
		if (this.Tbl!=null){
			xmlTxt+="\n<pup:tbl";
			xmlTxt+=">";
			xmlTxt+=this.Tbl.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:tbl>";
		}
		if (this.Rbf!=null){
			xmlTxt+="\n<pup:rbf";
			xmlTxt+=">";
			xmlTxt+=this.Rbf.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:rbf>";
		}
		if (this.Mbf!=null){
			xmlTxt+="\n<pup:mbf";
			xmlTxt+=">";
			xmlTxt+=this.Mbf.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:mbf>";
		}
		if (this.Sf!=null){
			xmlTxt+="\n<pup:sf";
			xmlTxt+=">";
			xmlTxt+=this.Sf;
			xmlTxt+="</pup:sf>";
		}
		if (this.Ef!=null){
			xmlTxt+="\n<pup:ef";
			xmlTxt+=">";
			xmlTxt+=this.Ef;
			xmlTxt+="</pup:ef>";
		}
		if (this.Filter!=null){
			xmlTxt+="\n<pup:filter";
			xmlTxt+=">";
			xmlTxt+=this.Filter;
			xmlTxt+="</pup:filter>";
		}
		if (this.Filterxy!=null){
			xmlTxt+="\n<pup:filterxy";
			xmlTxt+=">";
			xmlTxt+=this.Filterxy;
			xmlTxt+="</pup:filterxy>";
		}
		if (this.Filterz!=null){
			xmlTxt+="\n<pup:filterz";
			xmlTxt+=">";
			xmlTxt+=this.Filterz;
			xmlTxt+="</pup:filterz>";
		}
		if (this.Fwhm!=null){
			xmlTxt+="\n<pup:fwhm";
			xmlTxt+=">";
			xmlTxt+=this.Fwhm;
			xmlTxt+="</pup:fwhm>";
		}
		if (this.Pvc2cflag!=null){
			xmlTxt+="\n<pup:pvc2cflag";
			xmlTxt+=">";
			xmlTxt+=this.Pvc2cflag;
			xmlTxt+="</pup:pvc2cflag>";
		}
		if (this.Rsfflag!=null){
			xmlTxt+="\n<pup:rsfflag";
			xmlTxt+=">";
			xmlTxt+=this.Rsfflag;
			xmlTxt+="</pup:rsfflag>";
		}
		if (this.Pupstatus!=null){
			xmlTxt+="\n<pup:pupStatus";
			xmlTxt+=">";
			xmlTxt+=this.Pupstatus.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</pup:pupStatus>";
		}
			var child0=0;
			var att0=0;
			child0+=this.Rois_roi.length;
			if(child0>0 || att0>0){
				xmlTxt+="\n<pup:rois";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Rois_roiCOUNT=0;Rois_roiCOUNT<this.Rois_roi.length;Rois_roiCOUNT++){
			xmlTxt +="\n<pup:roi";
			xmlTxt +=this.Rois_roi[Rois_roiCOUNT].getXMLAtts();
			if(this.Rois_roi[Rois_roiCOUNT].xsiType!="pup:pupTimeCourseData_roi"){
				xmlTxt+=" xsi:type=\"" + this.Rois_roi[Rois_roiCOUNT].xsiType + "\"";
			}
			if (this.Rois_roi[Rois_roiCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Rois_roi[Rois_roiCOUNT].getXMLBody(preventComments);
					xmlTxt+="</pup:roi>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</pup:rois>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Userfullname!=null) return true;
		if (this.Petlabel!=null) return true;
		if (this.Petscanid!=null) return true;
		if (this.Proctype!=null) return true;
		if (this.Model!=null) return true;
		if (this.Tracer!=null) return true;
		if (this.Halflife!=null) return true;
		if (this.Templatetype!=null) return true;
		if (this.Fsid!=null) return true;
		if (this.T1!=null) return true;
		if (this.Mrid!=null) return true;
		if (this.Mocoerror!=null) return true;
		if (this.Regerror!=null) return true;
		if (this.Suvrflag!=null) return true;
		if (this.Refroistr!=null) return true;
		if (this.Delayparam!=null) return true;
		if (this.Mst!=null) return true;
		if (this.Mdt!=null) return true;
		if (this.Tbl!=null) return true;
		if (this.Rbf!=null) return true;
		if (this.Mbf!=null) return true;
		if (this.Sf!=null) return true;
		if (this.Ef!=null) return true;
		if (this.Filter!=null) return true;
		if (this.Filterxy!=null) return true;
		if (this.Filterz!=null) return true;
		if (this.Fwhm!=null) return true;
		if (this.Pvc2cflag!=null) return true;
		if (this.Rsfflag!=null) return true;
		if (this.Pupstatus!=null) return true;
			if(this.Rois_roi.length>0)return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
