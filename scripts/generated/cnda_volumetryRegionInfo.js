/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_volumetryRegionInfo(){
this.xsiType="cnda:volumetryRegionInfo";

	this.getSchemaElementName=function(){
		return "volumetryRegionInfo";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:volumetryRegionInfo";
	}

	this.Regions_region_imageType=null;


	function getRegions_region_imageType() {
		return this.Regions_region_imageType;
	}
	this.getRegions_region_imageType=getRegions_region_imageType;


	function setRegions_region_imageType(v){
		this.Regions_region_imageType=v;
	}
	this.setRegions_region_imageType=setRegions_region_imageType;

	this.Regions_region_sliceDist=null;


	function getRegions_region_sliceDist() {
		return this.Regions_region_sliceDist;
	}
	this.getRegions_region_sliceDist=getRegions_region_sliceDist;


	function setRegions_region_sliceDist(v){
		this.Regions_region_sliceDist=v;
	}
	this.setRegions_region_sliceDist=setRegions_region_sliceDist;

	this.Regions_region_orientation=null;


	function getRegions_region_orientation() {
		return this.Regions_region_orientation;
	}
	this.getRegions_region_orientation=getRegions_region_orientation;


	function setRegions_region_orientation(v){
		this.Regions_region_orientation=v;
	}
	this.setRegions_region_orientation=setRegions_region_orientation;

	this.Regions_region_voxelRes_x=null;


	function getRegions_region_voxelRes_x() {
		return this.Regions_region_voxelRes_x;
	}
	this.getRegions_region_voxelRes_x=getRegions_region_voxelRes_x;


	function setRegions_region_voxelRes_x(v){
		this.Regions_region_voxelRes_x=v;
	}
	this.setRegions_region_voxelRes_x=setRegions_region_voxelRes_x;

	this.Regions_region_voxelRes_y=null;


	function getRegions_region_voxelRes_y() {
		return this.Regions_region_voxelRes_y;
	}
	this.getRegions_region_voxelRes_y=getRegions_region_voxelRes_y;


	function setRegions_region_voxelRes_y(v){
		this.Regions_region_voxelRes_y=v;
	}
	this.setRegions_region_voxelRes_y=setRegions_region_voxelRes_y;

	this.Regions_region_voxelRes_thickness=null;


	function getRegions_region_voxelRes_thickness() {
		return this.Regions_region_voxelRes_thickness;
	}
	this.getRegions_region_voxelRes_thickness=getRegions_region_voxelRes_thickness;


	function setRegions_region_voxelRes_thickness(v){
		this.Regions_region_voxelRes_thickness=v;
	}
	this.setRegions_region_voxelRes_thickness=setRegions_region_voxelRes_thickness;

	this.Regions_region_note=null;


	function getRegions_region_note() {
		return this.Regions_region_note;
	}
	this.getRegions_region_note=getRegions_region_note;


	function setRegions_region_note(v){
		this.Regions_region_note=v;
	}
	this.setRegions_region_note=setRegions_region_note;

	this.Regions_region_id=null;


	function getRegions_region_id() {
		return this.Regions_region_id;
	}
	this.getRegions_region_id=getRegions_region_id;


	function setRegions_region_id(v){
		this.Regions_region_id=v;
	}
	this.setRegions_region_id=setRegions_region_id;

	this.Regions_region_name=null;


	function getRegions_region_name() {
		return this.Regions_region_name;
	}
	this.getRegions_region_name=getRegions_region_name;


	function setRegions_region_name(v){
		this.Regions_region_name=v;
	}
	this.setRegions_region_name=setRegions_region_name;

	this.CndaVolumetryregioninfoId=null;


	function getCndaVolumetryregioninfoId() {
		return this.CndaVolumetryregioninfoId;
	}
	this.getCndaVolumetryregioninfoId=getCndaVolumetryregioninfoId;


	function setCndaVolumetryregioninfoId(v){
		this.CndaVolumetryregioninfoId=v;
	}
	this.setCndaVolumetryregioninfoId=setCndaVolumetryregioninfoId;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="regions/region/image_type"){
				return this.Regions_region_imageType ;
			} else 
			if(xmlPath=="regions/region/slice_dist"){
				return this.Regions_region_sliceDist ;
			} else 
			if(xmlPath=="regions/region/orientation"){
				return this.Regions_region_orientation ;
			} else 
			if(xmlPath=="regions/region/voxel_res/x"){
				return this.Regions_region_voxelRes_x ;
			} else 
			if(xmlPath=="regions/region/voxel_res/y"){
				return this.Regions_region_voxelRes_y ;
			} else 
			if(xmlPath=="regions/region/voxel_res/thickness"){
				return this.Regions_region_voxelRes_thickness ;
			} else 
			if(xmlPath=="regions/region/note"){
				return this.Regions_region_note ;
			} else 
			if(xmlPath=="regions/region/id"){
				return this.Regions_region_id ;
			} else 
			if(xmlPath=="regions/region/name"){
				return this.Regions_region_name ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="cnda_volumetryRegionInfo_id"){
				return this.CndaVolumetryregioninfoId ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="regions/region/image_type"){
				this.Regions_region_imageType=value;
			} else 
			if(xmlPath=="regions/region/slice_dist"){
				this.Regions_region_sliceDist=value;
			} else 
			if(xmlPath=="regions/region/orientation"){
				this.Regions_region_orientation=value;
			} else 
			if(xmlPath=="regions/region/voxel_res/x"){
				this.Regions_region_voxelRes_x=value;
			} else 
			if(xmlPath=="regions/region/voxel_res/y"){
				this.Regions_region_voxelRes_y=value;
			} else 
			if(xmlPath=="regions/region/voxel_res/thickness"){
				this.Regions_region_voxelRes_thickness=value;
			} else 
			if(xmlPath=="regions/region/note"){
				this.Regions_region_note=value;
			} else 
			if(xmlPath=="regions/region/id"){
				this.Regions_region_id=value;
			} else 
			if(xmlPath=="regions/region/name"){
				this.Regions_region_name=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="cnda_volumetryRegionInfo_id"){
				this.CndaVolumetryregioninfoId=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="regions/region/image_type"){
			return "field_data";
		}else if (xmlPath=="regions/region/slice_dist"){
			return "field_data";
		}else if (xmlPath=="regions/region/orientation"){
			return "field_data";
		}else if (xmlPath=="regions/region/voxel_res/x"){
			return "field_data";
		}else if (xmlPath=="regions/region/voxel_res/y"){
			return "field_data";
		}else if (xmlPath=="regions/region/voxel_res/thickness"){
			return "field_data";
		}else if (xmlPath=="regions/region/note"){
			return "field_data";
		}else if (xmlPath=="regions/region/id"){
			return "field_data";
		}else if (xmlPath=="regions/region/name"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:VolumetryRegionInfo";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:VolumetryRegionInfo>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.CndaVolumetryregioninfoId!=null){
				if(hiddenCount++>0)str+=",";
				str+="cnda_volumetryRegionInfo_id=\"" + this.CndaVolumetryregioninfoId + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
			var child0=0;
			var att0=0;
			if(this.Regions_region_voxelRes_y!=null)
			child0++;
			if(this.Regions_region_imageType!=null)
			child0++;
			if(this.Regions_region_voxelRes_x!=null)
			child0++;
			if(this.Regions_region_id!=null)
			child0++;
			if(this.Regions_region_orientation!=null)
			child0++;
			if(this.Regions_region_voxelRes_thickness!=null)
			child0++;
			if(this.Regions_region_note!=null)
			child0++;
			if(this.Regions_region_sliceDist!=null)
			child0++;
			if(this.Regions_region_name!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<cnda:regions";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		var Regions_regionATT = ""
		if (this.Regions_region_id!=null)
			Regions_regionATT+=" id=\"" + this.Regions_region_id.replace(/>/g,"&gt;").replace(/</g,"&lt;") + "\"";
		if (this.Regions_region_name!=null)
			Regions_regionATT+=" name=\"" + this.Regions_region_name.replace(/>/g,"&gt;").replace(/</g,"&lt;") + "\"";
			var child1=0;
			var att1=0;
			if(this.Regions_region_voxelRes_y!=null)
			child1++;
			if(this.Regions_region_imageType!=null)
			child1++;
			if(this.Regions_region_voxelRes_x!=null)
			child1++;
			if(this.Regions_region_id!=null)
			att1++;
			if(this.Regions_region_orientation!=null)
			child1++;
			if(this.Regions_region_note!=null)
			child1++;
			if(this.Regions_region_voxelRes_thickness!=null)
			child1++;
			if(this.Regions_region_sliceDist!=null)
			child1++;
			if(this.Regions_region_name!=null)
			att1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<cnda:region";
				xmlTxt+=Regions_regionATT;
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Regions_region_imageType!=null){
			xmlTxt+="\n<cnda:image_type";
			xmlTxt+=">";
			xmlTxt+=this.Regions_region_imageType.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:image_type>";
		}
		if (this.Regions_region_sliceDist!=null){
			xmlTxt+="\n<cnda:slice_dist";
			xmlTxt+=">";
			xmlTxt+=this.Regions_region_sliceDist;
			xmlTxt+="</cnda:slice_dist>";
		}
		else{
			xmlTxt+="\n<cnda:slice_dist";
			xmlTxt+="/>";
		}

		if (this.Regions_region_orientation!=null){
			xmlTxt+="\n<cnda:orientation";
			xmlTxt+=">";
			xmlTxt+=this.Regions_region_orientation.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:orientation>";
		}
			var child2=0;
			var att2=0;
			if(this.Regions_region_voxelRes_x!=null)
			child2++;
			if(this.Regions_region_voxelRes_thickness!=null)
			child2++;
			if(this.Regions_region_voxelRes_y!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<cnda:voxel_res";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Regions_region_voxelRes_x!=null){
			xmlTxt+="\n<cnda:x";
			xmlTxt+=">";
			xmlTxt+=this.Regions_region_voxelRes_x;
			xmlTxt+="</cnda:x>";
		}
		else{
			xmlTxt+="\n<cnda:x";
			xmlTxt+="/>";
		}

		if (this.Regions_region_voxelRes_y!=null){
			xmlTxt+="\n<cnda:y";
			xmlTxt+=">";
			xmlTxt+=this.Regions_region_voxelRes_y;
			xmlTxt+="</cnda:y>";
		}
		else{
			xmlTxt+="\n<cnda:y";
			xmlTxt+="/>";
		}

		if (this.Regions_region_voxelRes_thickness!=null){
			xmlTxt+="\n<cnda:thickness";
			xmlTxt+=">";
			xmlTxt+=this.Regions_region_voxelRes_thickness;
			xmlTxt+="</cnda:thickness>";
		}
		else{
			xmlTxt+="\n<cnda:thickness";
			xmlTxt+="/>";
		}

				xmlTxt+="\n</cnda:voxel_res>";
			}
			}

		if (this.Regions_region_note!=null){
			xmlTxt+="\n<cnda:note";
			xmlTxt+=">";
			xmlTxt+=this.Regions_region_note.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:note>";
		}
				xmlTxt+="\n</cnda:region>";
			}
			}

				xmlTxt+="\n</cnda:regions>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.CndaVolumetryregioninfoId!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
			if(this.Regions_region_voxelRes_y!=null) return true;
			if(this.Regions_region_imageType!=null) return true;
			if(this.Regions_region_voxelRes_x!=null) return true;
			if(this.Regions_region_id!=null) return true;
			if(this.Regions_region_orientation!=null) return true;
			if(this.Regions_region_voxelRes_thickness!=null) return true;
			if(this.Regions_region_note!=null) return true;
			if(this.Regions_region_sliceDist!=null) return true;
			if(this.Regions_region_name!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
