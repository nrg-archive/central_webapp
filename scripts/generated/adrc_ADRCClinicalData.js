/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:49 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function adrc_ADRCClinicalData(){
this.xsiType="adrc:ADRCClinicalData";

	this.getSchemaElementName=function(){
		return "ADRCClinicalData";
	}

	this.getFullSchemaElementName=function(){
		return "adrc:ADRCClinicalData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Mmse=null;


	function getMmse() {
		return this.Mmse;
	}
	this.getMmse=getMmse;


	function setMmse(v){
		this.Mmse=v;
	}
	this.setMmse=setMmse;

	this.Ageatentry=null;


	function getAgeatentry() {
		return this.Ageatentry;
	}
	this.getAgeatentry=getAgeatentry;


	function setAgeatentry(v){
		this.Ageatentry=v;
	}
	this.setAgeatentry=setAgeatentry;

	this.Cdr=null;


	function getCdr() {
		return this.Cdr;
	}
	this.getCdr=getCdr;


	function setCdr(v){
		this.Cdr=v;
	}
	this.setCdr=setCdr;

	this.Commun=null;


	function getCommun() {
		return this.Commun;
	}
	this.getCommun=getCommun;


	function setCommun(v){
		this.Commun=v;
	}
	this.setCommun=setCommun;

	this.Dx1=null;


	function getDx1() {
		return this.Dx1;
	}
	this.getDx1=getDx1;


	function setDx1(v){
		this.Dx1=v;
	}
	this.setDx1=setDx1;

	this.Dx2=null;


	function getDx2() {
		return this.Dx2;
	}
	this.getDx2=getDx2;


	function setDx2(v){
		this.Dx2=v;
	}
	this.setDx2=setDx2;

	this.Dx3=null;


	function getDx3() {
		return this.Dx3;
	}
	this.getDx3=getDx3;


	function setDx3(v){
		this.Dx3=v;
	}
	this.setDx3=setDx3;

	this.Dx4=null;


	function getDx4() {
		return this.Dx4;
	}
	this.getDx4=getDx4;


	function setDx4(v){
		this.Dx4=v;
	}
	this.setDx4=setDx4;

	this.Dx5=null;


	function getDx5() {
		return this.Dx5;
	}
	this.getDx5=getDx5;


	function setDx5(v){
		this.Dx5=v;
	}
	this.setDx5=setDx5;

	this.Homehobb=null;


	function getHomehobb() {
		return this.Homehobb;
	}
	this.getHomehobb=getHomehobb;


	function setHomehobb(v){
		this.Homehobb=v;
	}
	this.setHomehobb=setHomehobb;

	this.Judgment=null;


	function getJudgment() {
		return this.Judgment;
	}
	this.getJudgment=getJudgment;


	function setJudgment(v){
		this.Judgment=v;
	}
	this.setJudgment=setJudgment;

	this.Memory=null;


	function getMemory() {
		return this.Memory;
	}
	this.getMemory=getMemory;


	function setMemory(v){
		this.Memory=v;
	}
	this.setMemory=setMemory;

	this.Orient=null;


	function getOrient() {
		return this.Orient;
	}
	this.getOrient=getOrient;


	function setOrient(v){
		this.Orient=v;
	}
	this.setOrient=setOrient;

	this.Perscare=null;


	function getPerscare() {
		return this.Perscare;
	}
	this.getPerscare=getPerscare;


	function setPerscare(v){
		this.Perscare=v;
	}
	this.setPerscare=setPerscare;

	this.Apoe=null;


	function getApoe() {
		return this.Apoe;
	}
	this.getApoe=getApoe;


	function setApoe(v){
		this.Apoe=v;
	}
	this.setApoe=setApoe;

	this.Sumbox=null;


	function getSumbox() {
		return this.Sumbox;
	}
	this.getSumbox=getSumbox;


	function setSumbox(v){
		this.Sumbox=v;
	}
	this.setSumbox=setSumbox;

	this.Acsparnt=null;


	function getAcsparnt() {
		return this.Acsparnt;
	}
	this.getAcsparnt=getAcsparnt;


	function setAcsparnt(v){
		this.Acsparnt=v;
	}
	this.setAcsparnt=setAcsparnt;

	this.Height=null;


	function getHeight() {
		return this.Height;
	}
	this.getHeight=getHeight;


	function setHeight(v){
		this.Height=v;
	}
	this.setHeight=setHeight;

	this.Weight=null;


	function getWeight() {
		return this.Weight;
	}
	this.getWeight=getWeight;


	function setWeight(v){
		this.Weight=v;
	}
	this.setWeight=setWeight;

	this.Primstudy=null;


	function getPrimstudy() {
		return this.Primstudy;
	}
	this.getPrimstudy=getPrimstudy;


	function setPrimstudy(v){
		this.Primstudy=v;
	}
	this.setPrimstudy=setPrimstudy;

	this.Acsstudy=null;


	function getAcsstudy() {
		return this.Acsstudy;
	}
	this.getAcsstudy=getAcsstudy;


	function setAcsstudy(v){
		this.Acsstudy=v;
	}
	this.setAcsstudy=setAcsstudy;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="mmse"){
				return this.Mmse ;
			} else 
			if(xmlPath=="ageAtEntry"){
				return this.Ageatentry ;
			} else 
			if(xmlPath=="cdr"){
				return this.Cdr ;
			} else 
			if(xmlPath=="commun"){
				return this.Commun ;
			} else 
			if(xmlPath=="dx1"){
				return this.Dx1 ;
			} else 
			if(xmlPath=="dx2"){
				return this.Dx2 ;
			} else 
			if(xmlPath=="dx3"){
				return this.Dx3 ;
			} else 
			if(xmlPath=="dx4"){
				return this.Dx4 ;
			} else 
			if(xmlPath=="dx5"){
				return this.Dx5 ;
			} else 
			if(xmlPath=="homehobb"){
				return this.Homehobb ;
			} else 
			if(xmlPath=="judgment"){
				return this.Judgment ;
			} else 
			if(xmlPath=="memory"){
				return this.Memory ;
			} else 
			if(xmlPath=="orient"){
				return this.Orient ;
			} else 
			if(xmlPath=="perscare"){
				return this.Perscare ;
			} else 
			if(xmlPath=="apoe"){
				return this.Apoe ;
			} else 
			if(xmlPath=="sumbox"){
				return this.Sumbox ;
			} else 
			if(xmlPath=="acsparnt"){
				return this.Acsparnt ;
			} else 
			if(xmlPath=="height"){
				return this.Height ;
			} else 
			if(xmlPath=="weight"){
				return this.Weight ;
			} else 
			if(xmlPath=="primStudy"){
				return this.Primstudy ;
			} else 
			if(xmlPath=="acsStudy"){
				return this.Acsstudy ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="mmse"){
				this.Mmse=value;
			} else 
			if(xmlPath=="ageAtEntry"){
				this.Ageatentry=value;
			} else 
			if(xmlPath=="cdr"){
				this.Cdr=value;
			} else 
			if(xmlPath=="commun"){
				this.Commun=value;
			} else 
			if(xmlPath=="dx1"){
				this.Dx1=value;
			} else 
			if(xmlPath=="dx2"){
				this.Dx2=value;
			} else 
			if(xmlPath=="dx3"){
				this.Dx3=value;
			} else 
			if(xmlPath=="dx4"){
				this.Dx4=value;
			} else 
			if(xmlPath=="dx5"){
				this.Dx5=value;
			} else 
			if(xmlPath=="homehobb"){
				this.Homehobb=value;
			} else 
			if(xmlPath=="judgment"){
				this.Judgment=value;
			} else 
			if(xmlPath=="memory"){
				this.Memory=value;
			} else 
			if(xmlPath=="orient"){
				this.Orient=value;
			} else 
			if(xmlPath=="perscare"){
				this.Perscare=value;
			} else 
			if(xmlPath=="apoe"){
				this.Apoe=value;
			} else 
			if(xmlPath=="sumbox"){
				this.Sumbox=value;
			} else 
			if(xmlPath=="acsparnt"){
				this.Acsparnt=value;
			} else 
			if(xmlPath=="height"){
				this.Height=value;
			} else 
			if(xmlPath=="weight"){
				this.Weight=value;
			} else 
			if(xmlPath=="primStudy"){
				this.Primstudy=value;
			} else 
			if(xmlPath=="acsStudy"){
				this.Acsstudy=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="mmse"){
			return "field_data";
		}else if (xmlPath=="ageAtEntry"){
			return "field_data";
		}else if (xmlPath=="cdr"){
			return "field_data";
		}else if (xmlPath=="commun"){
			return "field_data";
		}else if (xmlPath=="dx1"){
			return "field_data";
		}else if (xmlPath=="dx2"){
			return "field_data";
		}else if (xmlPath=="dx3"){
			return "field_data";
		}else if (xmlPath=="dx4"){
			return "field_data";
		}else if (xmlPath=="dx5"){
			return "field_data";
		}else if (xmlPath=="homehobb"){
			return "field_data";
		}else if (xmlPath=="judgment"){
			return "field_data";
		}else if (xmlPath=="memory"){
			return "field_data";
		}else if (xmlPath=="orient"){
			return "field_data";
		}else if (xmlPath=="perscare"){
			return "field_data";
		}else if (xmlPath=="apoe"){
			return "field_data";
		}else if (xmlPath=="sumbox"){
			return "field_data";
		}else if (xmlPath=="acsparnt"){
			return "field_data";
		}else if (xmlPath=="height"){
			return "field_data";
		}else if (xmlPath=="weight"){
			return "field_data";
		}else if (xmlPath=="primStudy"){
			return "field_data";
		}else if (xmlPath=="acsStudy"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<adrc:ADRCClinical";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</adrc:ADRCClinical>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Mmse!=null){
			xmlTxt+="\n<adrc:mmse";
			xmlTxt+=">";
			xmlTxt+=this.Mmse;
			xmlTxt+="</adrc:mmse>";
		}
		if (this.Ageatentry!=null){
			xmlTxt+="\n<adrc:ageAtEntry";
			xmlTxt+=">";
			xmlTxt+=this.Ageatentry;
			xmlTxt+="</adrc:ageAtEntry>";
		}
		if (this.Cdr!=null){
			xmlTxt+="\n<adrc:cdr";
			xmlTxt+=">";
			xmlTxt+=this.Cdr;
			xmlTxt+="</adrc:cdr>";
		}
		if (this.Commun!=null){
			xmlTxt+="\n<adrc:commun";
			xmlTxt+=">";
			xmlTxt+=this.Commun;
			xmlTxt+="</adrc:commun>";
		}
		if (this.Dx1!=null){
			xmlTxt+="\n<adrc:dx1";
			xmlTxt+=">";
			xmlTxt+=this.Dx1.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adrc:dx1>";
		}
		if (this.Dx2!=null){
			xmlTxt+="\n<adrc:dx2";
			xmlTxt+=">";
			xmlTxt+=this.Dx2.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adrc:dx2>";
		}
		if (this.Dx3!=null){
			xmlTxt+="\n<adrc:dx3";
			xmlTxt+=">";
			xmlTxt+=this.Dx3.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adrc:dx3>";
		}
		if (this.Dx4!=null){
			xmlTxt+="\n<adrc:dx4";
			xmlTxt+=">";
			xmlTxt+=this.Dx4.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adrc:dx4>";
		}
		if (this.Dx5!=null){
			xmlTxt+="\n<adrc:dx5";
			xmlTxt+=">";
			xmlTxt+=this.Dx5.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adrc:dx5>";
		}
		if (this.Homehobb!=null){
			xmlTxt+="\n<adrc:homehobb";
			xmlTxt+=">";
			xmlTxt+=this.Homehobb;
			xmlTxt+="</adrc:homehobb>";
		}
		if (this.Judgment!=null){
			xmlTxt+="\n<adrc:judgment";
			xmlTxt+=">";
			xmlTxt+=this.Judgment;
			xmlTxt+="</adrc:judgment>";
		}
		if (this.Memory!=null){
			xmlTxt+="\n<adrc:memory";
			xmlTxt+=">";
			xmlTxt+=this.Memory;
			xmlTxt+="</adrc:memory>";
		}
		if (this.Orient!=null){
			xmlTxt+="\n<adrc:orient";
			xmlTxt+=">";
			xmlTxt+=this.Orient;
			xmlTxt+="</adrc:orient>";
		}
		if (this.Perscare!=null){
			xmlTxt+="\n<adrc:perscare";
			xmlTxt+=">";
			xmlTxt+=this.Perscare;
			xmlTxt+="</adrc:perscare>";
		}
		if (this.Apoe!=null){
			xmlTxt+="\n<adrc:apoe";
			xmlTxt+=">";
			xmlTxt+=this.Apoe.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adrc:apoe>";
		}
		if (this.Sumbox!=null){
			xmlTxt+="\n<adrc:sumbox";
			xmlTxt+=">";
			xmlTxt+=this.Sumbox;
			xmlTxt+="</adrc:sumbox>";
		}
		if (this.Acsparnt!=null){
			xmlTxt+="\n<adrc:acsparnt";
			xmlTxt+=">";
			xmlTxt+=this.Acsparnt.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adrc:acsparnt>";
		}
		if (this.Height!=null){
			xmlTxt+="\n<adrc:height";
			xmlTxt+=">";
			xmlTxt+=this.Height.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adrc:height>";
		}
		if (this.Weight!=null){
			xmlTxt+="\n<adrc:weight";
			xmlTxt+=">";
			xmlTxt+=this.Weight.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adrc:weight>";
		}
		if (this.Primstudy!=null){
			xmlTxt+="\n<adrc:primStudy";
			xmlTxt+=">";
			xmlTxt+=this.Primstudy.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adrc:primStudy>";
		}
		if (this.Acsstudy!=null){
			xmlTxt+="\n<adrc:acsStudy";
			xmlTxt+=">";
			xmlTxt+=this.Acsstudy.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</adrc:acsStudy>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Mmse!=null) return true;
		if (this.Ageatentry!=null) return true;
		if (this.Cdr!=null) return true;
		if (this.Commun!=null) return true;
		if (this.Dx1!=null) return true;
		if (this.Dx2!=null) return true;
		if (this.Dx3!=null) return true;
		if (this.Dx4!=null) return true;
		if (this.Dx5!=null) return true;
		if (this.Homehobb!=null) return true;
		if (this.Judgment!=null) return true;
		if (this.Memory!=null) return true;
		if (this.Orient!=null) return true;
		if (this.Perscare!=null) return true;
		if (this.Apoe!=null) return true;
		if (this.Sumbox!=null) return true;
		if (this.Acsparnt!=null) return true;
		if (this.Height!=null) return true;
		if (this.Weight!=null) return true;
		if (this.Primstudy!=null) return true;
		if (this.Acsstudy!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
