/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function nunda_nundaDemographicData_relationship(){
this.xsiType="nunda:nundaDemographicData_relationship";

	this.getSchemaElementName=function(){
		return "nundaDemographicData_relationship";
	}

	this.getFullSchemaElementName=function(){
		return "nunda:nundaDemographicData_relationship";
	}

	this.Otherid=null;


	function getOtherid() {
		return this.Otherid;
	}
	this.getOtherid=getOtherid;


	function setOtherid(v){
		this.Otherid=v;
	}
	this.setOtherid=setOtherid;

	this.Relationshiptype=null;


	function getRelationshiptype() {
		return this.Relationshiptype;
	}
	this.getRelationshiptype=getRelationshiptype;


	function setRelationshiptype(v){
		this.Relationshiptype=v;
	}
	this.setRelationshiptype=setRelationshiptype;

	this.NundaNundademographicdataRelationshipId=null;


	function getNundaNundademographicdataRelationshipId() {
		return this.NundaNundademographicdataRelationshipId;
	}
	this.getNundaNundademographicdataRelationshipId=getNundaNundademographicdataRelationshipId;


	function setNundaNundademographicdataRelationshipId(v){
		this.NundaNundademographicdataRelationshipId=v;
	}
	this.setNundaNundademographicdataRelationshipId=setNundaNundademographicdataRelationshipId;

	this.relationships_relationship_nund_xnat_abstractdemographicdata_id_fk=null;


	this.getrelationships_relationship_nund_xnat_abstractdemographicdata_id=function() {
		return this.relationships_relationship_nund_xnat_abstractdemographicdata_id_fk;
	}


	this.setrelationships_relationship_nund_xnat_abstractdemographicdata_id=function(v){
		this.relationships_relationship_nund_xnat_abstractdemographicdata_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="otherId"){
				return this.Otherid ;
			} else 
			if(xmlPath=="relationshipType"){
				return this.Relationshiptype ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="nunda_nundaDemographicData_relationship_id"){
				return this.NundaNundademographicdataRelationshipId ;
			} else 
			if(xmlPath=="relationships_relationship_nund_xnat_abstractdemographicdata_id"){
				return this.relationships_relationship_nund_xnat_abstractdemographicdata_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="otherId"){
				this.Otherid=value;
			} else 
			if(xmlPath=="relationshipType"){
				this.Relationshiptype=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="nunda_nundaDemographicData_relationship_id"){
				this.NundaNundademographicdataRelationshipId=value;
			} else 
			if(xmlPath=="relationships_relationship_nund_xnat_abstractdemographicdata_id"){
				this.relationships_relationship_nund_xnat_abstractdemographicdata_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="otherId"){
			return "field_data";
		}else if (xmlPath=="relationshipType"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<nunda:nundaDemographicData_relationship";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</nunda:nundaDemographicData_relationship>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.NundaNundademographicdataRelationshipId!=null){
				if(hiddenCount++>0)str+=",";
				str+="nunda_nundaDemographicData_relationship_id=\"" + this.NundaNundademographicdataRelationshipId + "\"";
			}
			if(this.relationships_relationship_nund_xnat_abstractdemographicdata_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="relationships_relationship_nund_xnat_abstractdemographicdata_id=\"" + this.relationships_relationship_nund_xnat_abstractdemographicdata_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Otherid!=null){
			xmlTxt+="\n<nunda:otherId";
			xmlTxt+=">";
			xmlTxt+=this.Otherid.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</nunda:otherId>";
		}
		if (this.Relationshiptype!=null){
			xmlTxt+="\n<nunda:relationshipType";
			xmlTxt+=">";
			xmlTxt+=this.Relationshiptype;
			xmlTxt+="</nunda:relationshipType>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.NundaNundademographicdataRelationshipId!=null) return true;
			if (this.relationships_relationship_nund_xnat_abstractdemographicdata_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Otherid!=null) return true;
		if (this.Relationshiptype!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
