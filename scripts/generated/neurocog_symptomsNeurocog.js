/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function neurocog_symptomsNeurocog(){
this.xsiType="neurocog:symptomsNeurocog";

	this.getSchemaElementName=function(){
		return "symptomsNeurocog";
	}

	this.getFullSchemaElementName=function(){
		return "neurocog:symptomsNeurocog";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Dfbv=null;


	function getDfbv() {
		return this.Dfbv;
	}
	this.getDfbv=getDfbv;


	function setDfbv(v){
		this.Dfbv=v;
	}
	this.setDfbv=setDfbv;

	this.Yfbv=null;


	function getYfbv() {
		return this.Yfbv;
	}
	this.getYfbv=getYfbv;


	function setYfbv(v){
		this.Yfbv=v;
	}
	this.setYfbv=setYfbv;

	this.Zcog2grp_zIq2grp=null;


	function getZcog2grp_zIq2grp() {
		return this.Zcog2grp_zIq2grp;
	}
	this.getZcog2grp_zIq2grp=getZcog2grp_zIq2grp;


	function setZcog2grp_zIq2grp(v){
		this.Zcog2grp_zIq2grp=v;
	}
	this.setZcog2grp_zIq2grp=setZcog2grp_zIq2grp;

	this.Zcog2grp_zWm2grp=null;


	function getZcog2grp_zWm2grp() {
		return this.Zcog2grp_zWm2grp;
	}
	this.getZcog2grp_zWm2grp=getZcog2grp_zWm2grp;


	function setZcog2grp_zWm2grp(v){
		this.Zcog2grp_zWm2grp=v;
	}
	this.setZcog2grp_zWm2grp=setZcog2grp_zWm2grp;

	this.Zcog2grp_zEm2grp=null;


	function getZcog2grp_zEm2grp() {
		return this.Zcog2grp_zEm2grp;
	}
	this.getZcog2grp_zEm2grp=getZcog2grp_zEm2grp;


	function setZcog2grp_zEm2grp(v){
		this.Zcog2grp_zEm2grp=v;
	}
	this.setZcog2grp_zEm2grp=setZcog2grp_zEm2grp;

	this.Zcog2grp_zRa2grp=null;


	function getZcog2grp_zRa2grp() {
		return this.Zcog2grp_zRa2grp;
	}
	this.getZcog2grp_zRa2grp=getZcog2grp_zRa2grp;


	function setZcog2grp_zRa2grp(v){
		this.Zcog2grp_zRa2grp=v;
	}
	this.setZcog2grp_zRa2grp=setZcog2grp_zRa2grp;

	this.Zpsy2grp_zPos2grp=null;


	function getZpsy2grp_zPos2grp() {
		return this.Zpsy2grp_zPos2grp;
	}
	this.getZpsy2grp_zPos2grp=getZpsy2grp_zPos2grp;


	function setZpsy2grp_zPos2grp(v){
		this.Zpsy2grp_zPos2grp=v;
	}
	this.setZpsy2grp_zPos2grp=setZpsy2grp_zPos2grp;

	this.Zpsy2grp_zNeg2grp=null;


	function getZpsy2grp_zNeg2grp() {
		return this.Zpsy2grp_zNeg2grp;
	}
	this.getZpsy2grp_zNeg2grp=getZpsy2grp_zNeg2grp;


	function setZpsy2grp_zNeg2grp(v){
		this.Zpsy2grp_zNeg2grp=v;
	}
	this.setZpsy2grp_zNeg2grp=setZpsy2grp_zNeg2grp;

	this.Zpsy2grp_zDis2grp=null;


	function getZpsy2grp_zDis2grp() {
		return this.Zpsy2grp_zDis2grp;
	}
	this.getZpsy2grp_zDis2grp=getZpsy2grp_zDis2grp;


	function setZpsy2grp_zDis2grp(v){
		this.Zpsy2grp_zDis2grp=v;
	}
	this.setZpsy2grp_zDis2grp=setZpsy2grp_zDis2grp;

	this.Zcog4grp_zIq4grp=null;


	function getZcog4grp_zIq4grp() {
		return this.Zcog4grp_zIq4grp;
	}
	this.getZcog4grp_zIq4grp=getZcog4grp_zIq4grp;


	function setZcog4grp_zIq4grp(v){
		this.Zcog4grp_zIq4grp=v;
	}
	this.setZcog4grp_zIq4grp=setZcog4grp_zIq4grp;

	this.Zcog4grp_zAttn4grp=null;


	function getZcog4grp_zAttn4grp() {
		return this.Zcog4grp_zAttn4grp;
	}
	this.getZcog4grp_zAttn4grp=getZcog4grp_zAttn4grp;


	function setZcog4grp_zAttn4grp(v){
		this.Zcog4grp_zAttn4grp=v;
	}
	this.setZcog4grp_zAttn4grp=setZcog4grp_zAttn4grp;

	this.Zcog4grp_zWm4grp=null;


	function getZcog4grp_zWm4grp() {
		return this.Zcog4grp_zWm4grp;
	}
	this.getZcog4grp_zWm4grp=getZcog4grp_zWm4grp;


	function setZcog4grp_zWm4grp(v){
		this.Zcog4grp_zWm4grp=v;
	}
	this.setZcog4grp_zWm4grp=setZcog4grp_zWm4grp;

	this.Zcog4grp_zEm4grp=null;


	function getZcog4grp_zEm4grp() {
		return this.Zcog4grp_zEm4grp;
	}
	this.getZcog4grp_zEm4grp=getZcog4grp_zEm4grp;


	function setZcog4grp_zEm4grp(v){
		this.Zcog4grp_zEm4grp=v;
	}
	this.setZcog4grp_zEm4grp=setZcog4grp_zEm4grp;

	this.Zcog4grp_zRa4grp=null;


	function getZcog4grp_zRa4grp() {
		return this.Zcog4grp_zRa4grp;
	}
	this.getZcog4grp_zRa4grp=getZcog4grp_zRa4grp;


	function setZcog4grp_zRa4grp(v){
		this.Zcog4grp_zRa4grp=v;
	}
	this.setZcog4grp_zRa4grp=setZcog4grp_zRa4grp;

	this.Zpsy4grp_zPos4grp=null;


	function getZpsy4grp_zPos4grp() {
		return this.Zpsy4grp_zPos4grp;
	}
	this.getZpsy4grp_zPos4grp=getZpsy4grp_zPos4grp;


	function setZpsy4grp_zPos4grp(v){
		this.Zpsy4grp_zPos4grp=v;
	}
	this.setZpsy4grp_zPos4grp=setZpsy4grp_zPos4grp;

	this.Zpsy4grp_zNeg4grp=null;


	function getZpsy4grp_zNeg4grp() {
		return this.Zpsy4grp_zNeg4grp;
	}
	this.getZpsy4grp_zNeg4grp=getZpsy4grp_zNeg4grp;


	function setZpsy4grp_zNeg4grp(v){
		this.Zpsy4grp_zNeg4grp=v;
	}
	this.setZpsy4grp_zNeg4grp=setZpsy4grp_zNeg4grp;

	this.Zpsy4grp_zDis4grp=null;


	function getZpsy4grp_zDis4grp() {
		return this.Zpsy4grp_zDis4grp;
	}
	this.getZpsy4grp_zDis4grp=getZpsy4grp_zDis4grp;


	function setZpsy4grp_zDis4grp(v){
		this.Zpsy4grp_zDis4grp=v;
	}
	this.setZpsy4grp_zDis4grp=setZpsy4grp_zDis4grp;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="dfbv"){
				return this.Dfbv ;
			} else 
			if(xmlPath=="yfbv"){
				return this.Yfbv ;
			} else 
			if(xmlPath=="z_cog_2grp/z_iq_2grp"){
				return this.Zcog2grp_zIq2grp ;
			} else 
			if(xmlPath=="z_cog_2grp/z_wm_2grp"){
				return this.Zcog2grp_zWm2grp ;
			} else 
			if(xmlPath=="z_cog_2grp/z_em_2grp"){
				return this.Zcog2grp_zEm2grp ;
			} else 
			if(xmlPath=="z_cog_2grp/z_ra_2grp"){
				return this.Zcog2grp_zRa2grp ;
			} else 
			if(xmlPath=="z_psy_2grp/z_pos_2grp"){
				return this.Zpsy2grp_zPos2grp ;
			} else 
			if(xmlPath=="z_psy_2grp/z_neg_2grp"){
				return this.Zpsy2grp_zNeg2grp ;
			} else 
			if(xmlPath=="z_psy_2grp/z_dis_2grp"){
				return this.Zpsy2grp_zDis2grp ;
			} else 
			if(xmlPath=="z_cog_4grp/z_iq_4grp"){
				return this.Zcog4grp_zIq4grp ;
			} else 
			if(xmlPath=="z_cog_4grp/z_attn_4grp"){
				return this.Zcog4grp_zAttn4grp ;
			} else 
			if(xmlPath=="z_cog_4grp/z_wm_4grp"){
				return this.Zcog4grp_zWm4grp ;
			} else 
			if(xmlPath=="z_cog_4grp/z_em_4grp"){
				return this.Zcog4grp_zEm4grp ;
			} else 
			if(xmlPath=="z_cog_4grp/z_ra_4grp"){
				return this.Zcog4grp_zRa4grp ;
			} else 
			if(xmlPath=="z_psy_4grp/z_pos_4grp"){
				return this.Zpsy4grp_zPos4grp ;
			} else 
			if(xmlPath=="z_psy_4grp/z_neg_4grp"){
				return this.Zpsy4grp_zNeg4grp ;
			} else 
			if(xmlPath=="z_psy_4grp/z_dis_4grp"){
				return this.Zpsy4grp_zDis4grp ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="dfbv"){
				this.Dfbv=value;
			} else 
			if(xmlPath=="yfbv"){
				this.Yfbv=value;
			} else 
			if(xmlPath=="z_cog_2grp/z_iq_2grp"){
				this.Zcog2grp_zIq2grp=value;
			} else 
			if(xmlPath=="z_cog_2grp/z_wm_2grp"){
				this.Zcog2grp_zWm2grp=value;
			} else 
			if(xmlPath=="z_cog_2grp/z_em_2grp"){
				this.Zcog2grp_zEm2grp=value;
			} else 
			if(xmlPath=="z_cog_2grp/z_ra_2grp"){
				this.Zcog2grp_zRa2grp=value;
			} else 
			if(xmlPath=="z_psy_2grp/z_pos_2grp"){
				this.Zpsy2grp_zPos2grp=value;
			} else 
			if(xmlPath=="z_psy_2grp/z_neg_2grp"){
				this.Zpsy2grp_zNeg2grp=value;
			} else 
			if(xmlPath=="z_psy_2grp/z_dis_2grp"){
				this.Zpsy2grp_zDis2grp=value;
			} else 
			if(xmlPath=="z_cog_4grp/z_iq_4grp"){
				this.Zcog4grp_zIq4grp=value;
			} else 
			if(xmlPath=="z_cog_4grp/z_attn_4grp"){
				this.Zcog4grp_zAttn4grp=value;
			} else 
			if(xmlPath=="z_cog_4grp/z_wm_4grp"){
				this.Zcog4grp_zWm4grp=value;
			} else 
			if(xmlPath=="z_cog_4grp/z_em_4grp"){
				this.Zcog4grp_zEm4grp=value;
			} else 
			if(xmlPath=="z_cog_4grp/z_ra_4grp"){
				this.Zcog4grp_zRa4grp=value;
			} else 
			if(xmlPath=="z_psy_4grp/z_pos_4grp"){
				this.Zpsy4grp_zPos4grp=value;
			} else 
			if(xmlPath=="z_psy_4grp/z_neg_4grp"){
				this.Zpsy4grp_zNeg4grp=value;
			} else 
			if(xmlPath=="z_psy_4grp/z_dis_4grp"){
				this.Zpsy4grp_zDis4grp=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="dfbv"){
			return "field_data";
		}else if (xmlPath=="yfbv"){
			return "field_data";
		}else if (xmlPath=="z_cog_2grp/z_iq_2grp"){
			return "field_data";
		}else if (xmlPath=="z_cog_2grp/z_wm_2grp"){
			return "field_data";
		}else if (xmlPath=="z_cog_2grp/z_em_2grp"){
			return "field_data";
		}else if (xmlPath=="z_cog_2grp/z_ra_2grp"){
			return "field_data";
		}else if (xmlPath=="z_psy_2grp/z_pos_2grp"){
			return "field_data";
		}else if (xmlPath=="z_psy_2grp/z_neg_2grp"){
			return "field_data";
		}else if (xmlPath=="z_psy_2grp/z_dis_2grp"){
			return "field_data";
		}else if (xmlPath=="z_cog_4grp/z_iq_4grp"){
			return "field_data";
		}else if (xmlPath=="z_cog_4grp/z_attn_4grp"){
			return "field_data";
		}else if (xmlPath=="z_cog_4grp/z_wm_4grp"){
			return "field_data";
		}else if (xmlPath=="z_cog_4grp/z_em_4grp"){
			return "field_data";
		}else if (xmlPath=="z_cog_4grp/z_ra_4grp"){
			return "field_data";
		}else if (xmlPath=="z_psy_4grp/z_pos_4grp"){
			return "field_data";
		}else if (xmlPath=="z_psy_4grp/z_neg_4grp"){
			return "field_data";
		}else if (xmlPath=="z_psy_4grp/z_dis_4grp"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<neurocog:symptomsNeurocog";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</neurocog:symptomsNeurocog>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Dfbv!=null){
			xmlTxt+="\n<neurocog:dfbv";
			xmlTxt+=">";
			xmlTxt+=this.Dfbv;
			xmlTxt+="</neurocog:dfbv>";
		}
		if (this.Yfbv!=null){
			xmlTxt+="\n<neurocog:yfbv";
			xmlTxt+=">";
			xmlTxt+=this.Yfbv;
			xmlTxt+="</neurocog:yfbv>";
		}
			var child0=0;
			var att0=0;
			if(this.Zcog2grp_zRa2grp!=null)
			child0++;
			if(this.Zcog2grp_zWm2grp!=null)
			child0++;
			if(this.Zcog2grp_zIq2grp!=null)
			child0++;
			if(this.Zcog2grp_zEm2grp!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<neurocog:z_cog_2grp";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Zcog2grp_zIq2grp!=null){
			xmlTxt+="\n<neurocog:z_iq_2grp";
			xmlTxt+=">";
			xmlTxt+=this.Zcog2grp_zIq2grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_iq_2grp>";
		}
		if (this.Zcog2grp_zWm2grp!=null){
			xmlTxt+="\n<neurocog:z_wm_2grp";
			xmlTxt+=">";
			xmlTxt+=this.Zcog2grp_zWm2grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_wm_2grp>";
		}
		if (this.Zcog2grp_zEm2grp!=null){
			xmlTxt+="\n<neurocog:z_em_2grp";
			xmlTxt+=">";
			xmlTxt+=this.Zcog2grp_zEm2grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_em_2grp>";
		}
		if (this.Zcog2grp_zRa2grp!=null){
			xmlTxt+="\n<neurocog:z_ra_2grp";
			xmlTxt+=">";
			xmlTxt+=this.Zcog2grp_zRa2grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_ra_2grp>";
		}
				xmlTxt+="\n</neurocog:z_cog_2grp>";
			}
			}

			var child1=0;
			var att1=0;
			if(this.Zpsy2grp_zDis2grp!=null)
			child1++;
			if(this.Zpsy2grp_zNeg2grp!=null)
			child1++;
			if(this.Zpsy2grp_zPos2grp!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<neurocog:z_psy_2grp";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Zpsy2grp_zPos2grp!=null){
			xmlTxt+="\n<neurocog:z_pos_2grp";
			xmlTxt+=">";
			xmlTxt+=this.Zpsy2grp_zPos2grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_pos_2grp>";
		}
		if (this.Zpsy2grp_zNeg2grp!=null){
			xmlTxt+="\n<neurocog:z_neg_2grp";
			xmlTxt+=">";
			xmlTxt+=this.Zpsy2grp_zNeg2grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_neg_2grp>";
		}
		if (this.Zpsy2grp_zDis2grp!=null){
			xmlTxt+="\n<neurocog:z_dis_2grp";
			xmlTxt+=">";
			xmlTxt+=this.Zpsy2grp_zDis2grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_dis_2grp>";
		}
				xmlTxt+="\n</neurocog:z_psy_2grp>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Zcog4grp_zEm4grp!=null)
			child2++;
			if(this.Zcog4grp_zRa4grp!=null)
			child2++;
			if(this.Zcog4grp_zWm4grp!=null)
			child2++;
			if(this.Zcog4grp_zAttn4grp!=null)
			child2++;
			if(this.Zcog4grp_zIq4grp!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<neurocog:z_cog_4grp";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Zcog4grp_zIq4grp!=null){
			xmlTxt+="\n<neurocog:z_iq_4grp";
			xmlTxt+=">";
			xmlTxt+=this.Zcog4grp_zIq4grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_iq_4grp>";
		}
		if (this.Zcog4grp_zAttn4grp!=null){
			xmlTxt+="\n<neurocog:z_attn_4grp";
			xmlTxt+=">";
			xmlTxt+=this.Zcog4grp_zAttn4grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_attn_4grp>";
		}
		if (this.Zcog4grp_zWm4grp!=null){
			xmlTxt+="\n<neurocog:z_wm_4grp";
			xmlTxt+=">";
			xmlTxt+=this.Zcog4grp_zWm4grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_wm_4grp>";
		}
		if (this.Zcog4grp_zEm4grp!=null){
			xmlTxt+="\n<neurocog:z_em_4grp";
			xmlTxt+=">";
			xmlTxt+=this.Zcog4grp_zEm4grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_em_4grp>";
		}
		if (this.Zcog4grp_zRa4grp!=null){
			xmlTxt+="\n<neurocog:z_ra_4grp";
			xmlTxt+=">";
			xmlTxt+=this.Zcog4grp_zRa4grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_ra_4grp>";
		}
				xmlTxt+="\n</neurocog:z_cog_4grp>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.Zpsy4grp_zNeg4grp!=null)
			child3++;
			if(this.Zpsy4grp_zPos4grp!=null)
			child3++;
			if(this.Zpsy4grp_zDis4grp!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<neurocog:z_psy_4grp";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Zpsy4grp_zPos4grp!=null){
			xmlTxt+="\n<neurocog:z_pos_4grp";
			xmlTxt+=">";
			xmlTxt+=this.Zpsy4grp_zPos4grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_pos_4grp>";
		}
		if (this.Zpsy4grp_zNeg4grp!=null){
			xmlTxt+="\n<neurocog:z_neg_4grp";
			xmlTxt+=">";
			xmlTxt+=this.Zpsy4grp_zNeg4grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_neg_4grp>";
		}
		if (this.Zpsy4grp_zDis4grp!=null){
			xmlTxt+="\n<neurocog:z_dis_4grp";
			xmlTxt+=">";
			xmlTxt+=this.Zpsy4grp_zDis4grp.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</neurocog:z_dis_4grp>";
		}
				xmlTxt+="\n</neurocog:z_psy_4grp>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Dfbv!=null) return true;
		if (this.Yfbv!=null) return true;
			if(this.Zcog2grp_zRa2grp!=null) return true;
			if(this.Zcog2grp_zWm2grp!=null) return true;
			if(this.Zcog2grp_zIq2grp!=null) return true;
			if(this.Zcog2grp_zEm2grp!=null) return true;
			if(this.Zpsy2grp_zDis2grp!=null) return true;
			if(this.Zpsy2grp_zNeg2grp!=null) return true;
			if(this.Zpsy2grp_zPos2grp!=null) return true;
			if(this.Zcog4grp_zEm4grp!=null) return true;
			if(this.Zcog4grp_zRa4grp!=null) return true;
			if(this.Zcog4grp_zWm4grp!=null) return true;
			if(this.Zcog4grp_zAttn4grp!=null) return true;
			if(this.Zcog4grp_zIq4grp!=null) return true;
			if(this.Zpsy4grp_zNeg4grp!=null) return true;
			if(this.Zpsy4grp_zPos4grp!=null) return true;
			if(this.Zpsy4grp_zDis4grp!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
