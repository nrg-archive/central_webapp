/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_csfData(){
this.xsiType="cnda:csfData";

	this.getSchemaElementName=function(){
		return "csfData";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:csfData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Tau=null;


	function getTau() {
		return this.Tau;
	}
	this.getTau=getTau;


	function setTau(v){
		this.Tau=v;
	}
	this.setTau=setTau;

	this.Ptau=null;


	function getPtau() {
		return this.Ptau;
	}
	this.getPtau=getPtau;


	function setPtau(v){
		this.Ptau=v;
	}
	this.setPtau=setPtau;

	this.Ab42=null;


	function getAb42() {
		return this.Ab42;
	}
	this.getAb42=getAb42;


	function setAb42(v){
		this.Ab42=v;
	}
	this.setAb42=setAb42;

	this.Lilly40=null;


	function getLilly40() {
		return this.Lilly40;
	}
	this.getLilly40=getLilly40;


	function setLilly40(v){
		this.Lilly40=v;
	}
	this.setLilly40=setLilly40;

	this.Lilly42=null;


	function getLilly42() {
		return this.Lilly42;
	}
	this.getLilly42=getLilly42;


	function setLilly42(v){
		this.Lilly42=v;
	}
	this.setLilly42=setLilly42;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="tau"){
				return this.Tau ;
			} else 
			if(xmlPath=="ptau"){
				return this.Ptau ;
			} else 
			if(xmlPath=="Ab42"){
				return this.Ab42 ;
			} else 
			if(xmlPath=="Lilly40"){
				return this.Lilly40 ;
			} else 
			if(xmlPath=="Lilly42"){
				return this.Lilly42 ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="tau"){
				this.Tau=value;
			} else 
			if(xmlPath=="ptau"){
				this.Ptau=value;
			} else 
			if(xmlPath=="Ab42"){
				this.Ab42=value;
			} else 
			if(xmlPath=="Lilly40"){
				this.Lilly40=value;
			} else 
			if(xmlPath=="Lilly42"){
				this.Lilly42=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="tau"){
			return "field_data";
		}else if (xmlPath=="ptau"){
			return "field_data";
		}else if (xmlPath=="Ab42"){
			return "field_data";
		}else if (xmlPath=="Lilly40"){
			return "field_data";
		}else if (xmlPath=="Lilly42"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:CSF";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:CSF>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Tau!=null){
			xmlTxt+="\n<cnda:tau";
			xmlTxt+=">";
			xmlTxt+=this.Tau;
			xmlTxt+="</cnda:tau>";
		}
		if (this.Ptau!=null){
			xmlTxt+="\n<cnda:ptau";
			xmlTxt+=">";
			xmlTxt+=this.Ptau;
			xmlTxt+="</cnda:ptau>";
		}
		if (this.Ab42!=null){
			xmlTxt+="\n<cnda:Ab42";
			xmlTxt+=">";
			xmlTxt+=this.Ab42;
			xmlTxt+="</cnda:Ab42>";
		}
		if (this.Lilly40!=null){
			xmlTxt+="\n<cnda:Lilly40";
			xmlTxt+=">";
			xmlTxt+=this.Lilly40;
			xmlTxt+="</cnda:Lilly40>";
		}
		if (this.Lilly42!=null){
			xmlTxt+="\n<cnda:Lilly42";
			xmlTxt+=">";
			xmlTxt+=this.Lilly42;
			xmlTxt+="</cnda:Lilly42>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Tau!=null) return true;
		if (this.Ptau!=null) return true;
		if (this.Ab42!=null) return true;
		if (this.Lilly40!=null) return true;
		if (this.Lilly42!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
