/*
 * GENERATED FILE
 * Created on Tue Feb 21 00:14:48 CST 2017
 *
 */

/**
 * @author XDAT
 *
 */

function cnda_handednessData(){
this.xsiType="cnda:handednessData";

	this.getSchemaElementName=function(){
		return "handednessData";
	}

	this.getFullSchemaElementName=function(){
		return "cnda:handednessData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.TaskPrefs_letter=null;


	function getTaskPrefs_letter() {
		return this.TaskPrefs_letter;
	}
	this.getTaskPrefs_letter=getTaskPrefs_letter;


	function setTaskPrefs_letter(v){
		this.TaskPrefs_letter=v;
	}
	this.setTaskPrefs_letter=setTaskPrefs_letter;

	this.TaskPrefs_ball=null;


	function getTaskPrefs_ball() {
		return this.TaskPrefs_ball;
	}
	this.getTaskPrefs_ball=getTaskPrefs_ball;


	function setTaskPrefs_ball(v){
		this.TaskPrefs_ball=v;
	}
	this.setTaskPrefs_ball=setTaskPrefs_ball;

	this.TaskPrefs_racquet=null;


	function getTaskPrefs_racquet() {
		return this.TaskPrefs_racquet;
	}
	this.getTaskPrefs_racquet=getTaskPrefs_racquet;


	function setTaskPrefs_racquet(v){
		this.TaskPrefs_racquet=v;
	}
	this.setTaskPrefs_racquet=setTaskPrefs_racquet;

	this.TaskPrefs_broom=null;


	function getTaskPrefs_broom() {
		return this.TaskPrefs_broom;
	}
	this.getTaskPrefs_broom=getTaskPrefs_broom;


	function setTaskPrefs_broom(v){
		this.TaskPrefs_broom=v;
	}
	this.setTaskPrefs_broom=setTaskPrefs_broom;

	this.TaskPrefs_shovel=null;


	function getTaskPrefs_shovel() {
		return this.TaskPrefs_shovel;
	}
	this.getTaskPrefs_shovel=getTaskPrefs_shovel;


	function setTaskPrefs_shovel(v){
		this.TaskPrefs_shovel=v;
	}
	this.setTaskPrefs_shovel=setTaskPrefs_shovel;

	this.TaskPrefs_match=null;


	function getTaskPrefs_match() {
		return this.TaskPrefs_match;
	}
	this.getTaskPrefs_match=getTaskPrefs_match;


	function setTaskPrefs_match(v){
		this.TaskPrefs_match=v;
	}
	this.setTaskPrefs_match=setTaskPrefs_match;

	this.TaskPrefs_scissors=null;


	function getTaskPrefs_scissors() {
		return this.TaskPrefs_scissors;
	}
	this.getTaskPrefs_scissors=getTaskPrefs_scissors;


	function setTaskPrefs_scissors(v){
		this.TaskPrefs_scissors=v;
	}
	this.setTaskPrefs_scissors=setTaskPrefs_scissors;

	this.TaskPrefs_thread=null;


	function getTaskPrefs_thread() {
		return this.TaskPrefs_thread;
	}
	this.getTaskPrefs_thread=getTaskPrefs_thread;


	function setTaskPrefs_thread(v){
		this.TaskPrefs_thread=v;
	}
	this.setTaskPrefs_thread=setTaskPrefs_thread;

	this.TaskPrefs_cards=null;


	function getTaskPrefs_cards() {
		return this.TaskPrefs_cards;
	}
	this.getTaskPrefs_cards=getTaskPrefs_cards;


	function setTaskPrefs_cards(v){
		this.TaskPrefs_cards=v;
	}
	this.setTaskPrefs_cards=setTaskPrefs_cards;

	this.TaskPrefs_hammer=null;


	function getTaskPrefs_hammer() {
		return this.TaskPrefs_hammer;
	}
	this.getTaskPrefs_hammer=getTaskPrefs_hammer;


	function setTaskPrefs_hammer(v){
		this.TaskPrefs_hammer=v;
	}
	this.setTaskPrefs_hammer=setTaskPrefs_hammer;

	this.TaskPrefs_toothbrush=null;


	function getTaskPrefs_toothbrush() {
		return this.TaskPrefs_toothbrush;
	}
	this.getTaskPrefs_toothbrush=getTaskPrefs_toothbrush;


	function setTaskPrefs_toothbrush(v){
		this.TaskPrefs_toothbrush=v;
	}
	this.setTaskPrefs_toothbrush=setTaskPrefs_toothbrush;

	this.TaskPrefs_jar=null;


	function getTaskPrefs_jar() {
		return this.TaskPrefs_jar;
	}
	this.getTaskPrefs_jar=getTaskPrefs_jar;


	function setTaskPrefs_jar(v){
		this.TaskPrefs_jar=v;
	}
	this.setTaskPrefs_jar=setTaskPrefs_jar;

	this.ParentsLeft_mother=null;


	function getParentsLeft_mother() {
		return this.ParentsLeft_mother;
	}
	this.getParentsLeft_mother=getParentsLeft_mother;


	function setParentsLeft_mother(v){
		this.ParentsLeft_mother=v;
	}
	this.setParentsLeft_mother=setParentsLeft_mother;

	this.ParentsLeft_father=null;


	function getParentsLeft_father() {
		return this.ParentsLeft_father;
	}
	this.getParentsLeft_father=getParentsLeft_father;


	function setParentsLeft_father(v){
		this.ParentsLeft_father=v;
	}
	this.setParentsLeft_father=setParentsLeft_father;

	this.Siblings_male_count=null;


	function getSiblings_male_count() {
		return this.Siblings_male_count;
	}
	this.getSiblings_male_count=getSiblings_male_count;


	function setSiblings_male_count(v){
		this.Siblings_male_count=v;
	}
	this.setSiblings_male_count=setSiblings_male_count;

	this.Siblings_male_leftHanded=null;


	function getSiblings_male_leftHanded() {
		return this.Siblings_male_leftHanded;
	}
	this.getSiblings_male_leftHanded=getSiblings_male_leftHanded;


	function setSiblings_male_leftHanded(v){
		this.Siblings_male_leftHanded=v;
	}
	this.setSiblings_male_leftHanded=setSiblings_male_leftHanded;

	this.Siblings_female_count=null;


	function getSiblings_female_count() {
		return this.Siblings_female_count;
	}
	this.getSiblings_female_count=getSiblings_female_count;


	function setSiblings_female_count(v){
		this.Siblings_female_count=v;
	}
	this.setSiblings_female_count=setSiblings_female_count;

	this.Siblings_female_leftHanded=null;


	function getSiblings_female_leftHanded() {
		return this.Siblings_female_leftHanded;
	}
	this.getSiblings_female_leftHanded=getSiblings_female_leftHanded;


	function setSiblings_female_leftHanded(v){
		this.Siblings_female_leftHanded=v;
	}
	this.setSiblings_female_leftHanded=setSiblings_female_leftHanded;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="task_prefs/letter"){
				return this.TaskPrefs_letter ;
			} else 
			if(xmlPath=="task_prefs/ball"){
				return this.TaskPrefs_ball ;
			} else 
			if(xmlPath=="task_prefs/racquet"){
				return this.TaskPrefs_racquet ;
			} else 
			if(xmlPath=="task_prefs/broom"){
				return this.TaskPrefs_broom ;
			} else 
			if(xmlPath=="task_prefs/shovel"){
				return this.TaskPrefs_shovel ;
			} else 
			if(xmlPath=="task_prefs/match"){
				return this.TaskPrefs_match ;
			} else 
			if(xmlPath=="task_prefs/scissors"){
				return this.TaskPrefs_scissors ;
			} else 
			if(xmlPath=="task_prefs/thread"){
				return this.TaskPrefs_thread ;
			} else 
			if(xmlPath=="task_prefs/cards"){
				return this.TaskPrefs_cards ;
			} else 
			if(xmlPath=="task_prefs/hammer"){
				return this.TaskPrefs_hammer ;
			} else 
			if(xmlPath=="task_prefs/toothbrush"){
				return this.TaskPrefs_toothbrush ;
			} else 
			if(xmlPath=="task_prefs/jar"){
				return this.TaskPrefs_jar ;
			} else 
			if(xmlPath=="parents_left/mother"){
				return this.ParentsLeft_mother ;
			} else 
			if(xmlPath=="parents_left/father"){
				return this.ParentsLeft_father ;
			} else 
			if(xmlPath=="siblings/male/count"){
				return this.Siblings_male_count ;
			} else 
			if(xmlPath=="siblings/male/left_handed"){
				return this.Siblings_male_leftHanded ;
			} else 
			if(xmlPath=="siblings/female/count"){
				return this.Siblings_female_count ;
			} else 
			if(xmlPath=="siblings/female/left_handed"){
				return this.Siblings_female_leftHanded ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="task_prefs/letter"){
				this.TaskPrefs_letter=value;
			} else 
			if(xmlPath=="task_prefs/ball"){
				this.TaskPrefs_ball=value;
			} else 
			if(xmlPath=="task_prefs/racquet"){
				this.TaskPrefs_racquet=value;
			} else 
			if(xmlPath=="task_prefs/broom"){
				this.TaskPrefs_broom=value;
			} else 
			if(xmlPath=="task_prefs/shovel"){
				this.TaskPrefs_shovel=value;
			} else 
			if(xmlPath=="task_prefs/match"){
				this.TaskPrefs_match=value;
			} else 
			if(xmlPath=="task_prefs/scissors"){
				this.TaskPrefs_scissors=value;
			} else 
			if(xmlPath=="task_prefs/thread"){
				this.TaskPrefs_thread=value;
			} else 
			if(xmlPath=="task_prefs/cards"){
				this.TaskPrefs_cards=value;
			} else 
			if(xmlPath=="task_prefs/hammer"){
				this.TaskPrefs_hammer=value;
			} else 
			if(xmlPath=="task_prefs/toothbrush"){
				this.TaskPrefs_toothbrush=value;
			} else 
			if(xmlPath=="task_prefs/jar"){
				this.TaskPrefs_jar=value;
			} else 
			if(xmlPath=="parents_left/mother"){
				this.ParentsLeft_mother=value;
			} else 
			if(xmlPath=="parents_left/father"){
				this.ParentsLeft_father=value;
			} else 
			if(xmlPath=="siblings/male/count"){
				this.Siblings_male_count=value;
			} else 
			if(xmlPath=="siblings/male/left_handed"){
				this.Siblings_male_leftHanded=value;
			} else 
			if(xmlPath=="siblings/female/count"){
				this.Siblings_female_count=value;
			} else 
			if(xmlPath=="siblings/female/left_handed"){
				this.Siblings_female_leftHanded=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="task_prefs/letter"){
			return "field_data";
		}else if (xmlPath=="task_prefs/ball"){
			return "field_data";
		}else if (xmlPath=="task_prefs/racquet"){
			return "field_data";
		}else if (xmlPath=="task_prefs/broom"){
			return "field_data";
		}else if (xmlPath=="task_prefs/shovel"){
			return "field_data";
		}else if (xmlPath=="task_prefs/match"){
			return "field_data";
		}else if (xmlPath=="task_prefs/scissors"){
			return "field_data";
		}else if (xmlPath=="task_prefs/thread"){
			return "field_data";
		}else if (xmlPath=="task_prefs/cards"){
			return "field_data";
		}else if (xmlPath=="task_prefs/hammer"){
			return "field_data";
		}else if (xmlPath=="task_prefs/toothbrush"){
			return "field_data";
		}else if (xmlPath=="task_prefs/jar"){
			return "field_data";
		}else if (xmlPath=="parents_left/mother"){
			return "field_data";
		}else if (xmlPath=="parents_left/father"){
			return "field_data";
		}else if (xmlPath=="siblings/male/count"){
			return "field_data";
		}else if (xmlPath=="siblings/male/left_handed"){
			return "field_data";
		}else if (xmlPath=="siblings/female/count"){
			return "field_data";
		}else if (xmlPath=="siblings/female/left_handed"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<cnda:Handedness";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:neurocog=\"http://nrg.wustl.edu/neurocog\"";
		xmlTxt+=" xmlns:nunda=\"http://nrg.wustl.edu/nunda\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:sapssans=\"http://nrg.wustl.edu/sapssans\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</cnda:Handedness>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			if(this.TaskPrefs_shovel!=null)
			child0++;
			if(this.TaskPrefs_letter!=null)
			child0++;
			if(this.TaskPrefs_racquet!=null)
			child0++;
			if(this.TaskPrefs_toothbrush!=null)
			child0++;
			if(this.TaskPrefs_cards!=null)
			child0++;
			if(this.TaskPrefs_jar!=null)
			child0++;
			if(this.TaskPrefs_scissors!=null)
			child0++;
			if(this.TaskPrefs_hammer!=null)
			child0++;
			if(this.TaskPrefs_thread!=null)
			child0++;
			if(this.TaskPrefs_ball!=null)
			child0++;
			if(this.TaskPrefs_match!=null)
			child0++;
			if(this.TaskPrefs_broom!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<cnda:task_prefs";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.TaskPrefs_letter!=null){
			xmlTxt+="\n<cnda:letter";
			xmlTxt+=">";
			xmlTxt+=this.TaskPrefs_letter;
			xmlTxt+="</cnda:letter>";
		}
		else{
			xmlTxt+="\n<cnda:letter";
			xmlTxt+="/>";
		}

		if (this.TaskPrefs_ball!=null){
			xmlTxt+="\n<cnda:ball";
			xmlTxt+=">";
			xmlTxt+=this.TaskPrefs_ball;
			xmlTxt+="</cnda:ball>";
		}
		else{
			xmlTxt+="\n<cnda:ball";
			xmlTxt+="/>";
		}

		if (this.TaskPrefs_racquet!=null){
			xmlTxt+="\n<cnda:racquet";
			xmlTxt+=">";
			xmlTxt+=this.TaskPrefs_racquet;
			xmlTxt+="</cnda:racquet>";
		}
		else{
			xmlTxt+="\n<cnda:racquet";
			xmlTxt+="/>";
		}

		if (this.TaskPrefs_broom!=null){
			xmlTxt+="\n<cnda:broom";
			xmlTxt+=">";
			xmlTxt+=this.TaskPrefs_broom;
			xmlTxt+="</cnda:broom>";
		}
		else{
			xmlTxt+="\n<cnda:broom";
			xmlTxt+="/>";
		}

		if (this.TaskPrefs_shovel!=null){
			xmlTxt+="\n<cnda:shovel";
			xmlTxt+=">";
			xmlTxt+=this.TaskPrefs_shovel;
			xmlTxt+="</cnda:shovel>";
		}
		else{
			xmlTxt+="\n<cnda:shovel";
			xmlTxt+="/>";
		}

		if (this.TaskPrefs_match!=null){
			xmlTxt+="\n<cnda:match";
			xmlTxt+=">";
			xmlTxt+=this.TaskPrefs_match;
			xmlTxt+="</cnda:match>";
		}
		else{
			xmlTxt+="\n<cnda:match";
			xmlTxt+="/>";
		}

		if (this.TaskPrefs_scissors!=null){
			xmlTxt+="\n<cnda:scissors";
			xmlTxt+=">";
			xmlTxt+=this.TaskPrefs_scissors;
			xmlTxt+="</cnda:scissors>";
		}
		else{
			xmlTxt+="\n<cnda:scissors";
			xmlTxt+="/>";
		}

		if (this.TaskPrefs_thread!=null){
			xmlTxt+="\n<cnda:thread";
			xmlTxt+=">";
			xmlTxt+=this.TaskPrefs_thread;
			xmlTxt+="</cnda:thread>";
		}
		else{
			xmlTxt+="\n<cnda:thread";
			xmlTxt+="/>";
		}

		if (this.TaskPrefs_cards!=null){
			xmlTxt+="\n<cnda:cards";
			xmlTxt+=">";
			xmlTxt+=this.TaskPrefs_cards;
			xmlTxt+="</cnda:cards>";
		}
		else{
			xmlTxt+="\n<cnda:cards";
			xmlTxt+="/>";
		}

		if (this.TaskPrefs_hammer!=null){
			xmlTxt+="\n<cnda:hammer";
			xmlTxt+=">";
			xmlTxt+=this.TaskPrefs_hammer;
			xmlTxt+="</cnda:hammer>";
		}
		else{
			xmlTxt+="\n<cnda:hammer";
			xmlTxt+="/>";
		}

		if (this.TaskPrefs_toothbrush!=null){
			xmlTxt+="\n<cnda:toothbrush";
			xmlTxt+=">";
			xmlTxt+=this.TaskPrefs_toothbrush;
			xmlTxt+="</cnda:toothbrush>";
		}
		else{
			xmlTxt+="\n<cnda:toothbrush";
			xmlTxt+="/>";
		}

		if (this.TaskPrefs_jar!=null){
			xmlTxt+="\n<cnda:jar";
			xmlTxt+=">";
			xmlTxt+=this.TaskPrefs_jar;
			xmlTxt+="</cnda:jar>";
		}
		else{
			xmlTxt+="\n<cnda:jar";
			xmlTxt+="/>";
		}

				xmlTxt+="\n</cnda:task_prefs>";
			}
			}

			var child1=0;
			var att1=0;
			if(this.ParentsLeft_father!=null)
			child1++;
			if(this.ParentsLeft_mother!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<cnda:parents_left";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.ParentsLeft_mother!=null){
			xmlTxt+="\n<cnda:mother";
			xmlTxt+=">";
			xmlTxt+=this.ParentsLeft_mother.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:mother>";
		}
		if (this.ParentsLeft_father!=null){
			xmlTxt+="\n<cnda:father";
			xmlTxt+=">";
			xmlTxt+=this.ParentsLeft_father.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</cnda:father>";
		}
				xmlTxt+="\n</cnda:parents_left>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Siblings_female_count!=null)
			child2++;
			if(this.Siblings_female_leftHanded!=null)
			child2++;
			if(this.Siblings_male_leftHanded!=null)
			child2++;
			if(this.Siblings_male_count!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<cnda:siblings";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		var Siblings_maleATT = ""
		if (this.Siblings_male_count!=null)
			Siblings_maleATT+=" count=\"" + this.Siblings_male_count + "\"";
		if (this.Siblings_male_leftHanded!=null)
			Siblings_maleATT+=" left_handed=\"" + this.Siblings_male_leftHanded + "\"";
		if(Siblings_maleATT!=""){
			xmlTxt+="\n<cnda:male";
			xmlTxt+=Siblings_maleATT;
			xmlTxt+="/>";
		}

		var Siblings_femaleATT = ""
		if (this.Siblings_female_count!=null)
			Siblings_femaleATT+=" count=\"" + this.Siblings_female_count + "\"";
		if (this.Siblings_female_leftHanded!=null)
			Siblings_femaleATT+=" left_handed=\"" + this.Siblings_female_leftHanded + "\"";
		if(Siblings_femaleATT!=""){
			xmlTxt+="\n<cnda:female";
			xmlTxt+=Siblings_femaleATT;
			xmlTxt+="/>";
		}

				xmlTxt+="\n</cnda:siblings>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.TaskPrefs_shovel!=null) return true;
			if(this.TaskPrefs_letter!=null) return true;
			if(this.TaskPrefs_racquet!=null) return true;
			if(this.TaskPrefs_toothbrush!=null) return true;
			if(this.TaskPrefs_cards!=null) return true;
			if(this.TaskPrefs_jar!=null) return true;
			if(this.TaskPrefs_scissors!=null) return true;
			if(this.TaskPrefs_hammer!=null) return true;
			if(this.TaskPrefs_thread!=null) return true;
			if(this.TaskPrefs_ball!=null) return true;
			if(this.TaskPrefs_match!=null) return true;
			if(this.TaskPrefs_broom!=null) return true;
			if(this.ParentsLeft_father!=null) return true;
			if(this.ParentsLeft_mother!=null) return true;
			if(this.Siblings_female_count!=null) return true;
			if(this.Siblings_female_leftHanded!=null) return true;
			if(this.Siblings_male_leftHanded!=null) return true;
			if(this.Siblings_male_count!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
