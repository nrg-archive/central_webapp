// Event sniffers for form and panel interactivity

$(document).ready(function(){
    // set display height and width according to popup size
    $("#register_modal").width($(window).width());
    $("#register_modal").height($(window).height());
    $("#register_modal .body").height($(window).height()-$("#register_modal .footer").height());

    // initialize the "tabsactivate" event
    $("#panel-container").tabs({
        activate: function( event, ui ) {}
    });

/* ====
/* SET PROJECT CONTEXT PARAMETERS
/* Requires paramsObject object
/* ==== */

    /* Required parameters at start */
    var context = paramsObject;

    // populate scan selector with data attributes that will populate hidden fields
    var petScanId = paramsObject.petScanId;

    for (i=0; i<petScanId.length; i++) {
        // work through array of possible PET scans to select. Insert option value and label from array objects.
        var label = petScanId[i].label ? petScanId[i].label : "ECAT";
        var opt = '<option value="'+petScanId[i].id+'" data-filterz="'+petScanId[i].filterz+'" data-filterxy="'+petScanId[i].filterxy+'" data-format="'+petScanId[i].format+'" data-format_type="'+petScanId[i].format_type+'">'+petScanId[i].id+": "+label+'</option>';
        $('select[name=pet_scan_id]').append(opt);
    }

    if (petScanId.length == 1) {
        $('select[name=pet_scan_id]').find('option[value="'+petScanId[0].id+'"]').attr("selected",true);
        selectScan();
    }


/* ====
/* SET PROJECT DEFAULT & SESSION PARAMETERS
/* Some parameters can have multiple default settings, based on the user's path
/* Get json asynchronously. results must be handled inside this function.
/* ==== */


    /* === PROJECT DEFAULTS === */
    var projectParams = paramsObject.projectParams;

    for (var p in projectParams) {
        // if a parameter has a "paths" object, then step through each path option and initialize that variable.
        var param = projectParams[p];
        if (param.paths) {
            if (p != "refroistr") { // handle refroistr separately
                for (var i=0; i<param.paths.length; i++) {
                    // build an object with .name, .type, .value parameters and pass it to a parsing function.
                    var obj = { "name":p, "type":param.paths[i].type, "opts":param.paths[i].opts, "default":param.paths[i].default };
                    var container = "."+param.paths[i].path;
                          //  console.log(container,obj);
                    setParams(container,obj);
                }
            } else {
                // We set refroistr to its default freesurfer value at first.
                // Later, when the user selects their path, we will re-set it if need be.
                setRefroistr("freesurfer");
            }
        } else {
            // if there is no paths object, then we assume each parameter is unique and can be set only once.
            var obj = { "name":p, "type":param.type, "opts":param.opts, "default":param.default };
            setParams("#panel-container",obj);
        }
    }

    /* === SESSION PARAMETERS === */

    $("input[name=project]").val(context.project);
    $("input[name=subject]").val(context.subject);
    $("input[name=sessionId]").val(context.sessionId);
    $("input[name=sessionLabel]").val(context.sessionLabel);
    $("input[name=tracer]").val(context.tracer);
    $("input[name=half_life]").val(context.half_life);

    var subjectResources = paramsObject.subjectResources;
    var fsFound = false;
    var csroiFound = false;
	var anyFSQCed = false;

    for (var i=0; i<subjectResources.length; i++) {
        // get data on related image sessions for subject. use this to populate selectors in each path.
        var s = subjectResources[i];

        // build freesurfer options (and error condition options) based on scans found
        var fsopt = '<p>Image Session: '+s.mrlabel+' ('+s.mrdate+')';
        var erropt = '';
        if (s.freesurfers.length > 0) {
            fsopt += '<table style="border:none">';
            for (j=0; j<s.freesurfers.length; j++) {
                var qcstatus = (s.freesurfers[j].qcstatus) ? s.freesurfers[j].qcstatus : "No QC";
                if (qcstatus=="No QC" || qcstatus.lastIndexOf("Failed",0)===0) {
                    fsopt += '<tr><td style="border:none">&nbsp;</td><td style="border:none"><label>FreeSurfer ('+s.freesurfers[j].fsdate+'): '+qcstatus+'</label></td></tr>';
					erropt += '<p>Image session <a href="/data/experiments/'+s.mrId+'?format=html" target="_parent">'+s.mrlabel+'</a>: assessor <a href="/data/experiments/'+s.mrId+'/assessors/'+s.freesurfers[j].fsid+'?format=html" target="_parent">'+s.freesurfers[j].fsid+'</a></p>';
                } else {
                    fsopt += '<tr><td style="border:none"><input type="radio" onclick="fsConfig(this)" name="fsid" value="'+s.freesurfers[j].fsid+'" data-mrLabel="'+s.mrlabel+'" data-mrId="'+s.mrid+'" data-fsversion="'+s.freesurfers[j].fsversion+'" class="pipeline_input required" /></td><td style="border:none"><label>FreeSurfer ('+s.freesurfers[j].fsdate+'): '+qcstatus+'</label></td></tr>';
					anyFSQCed = true
                }
            }
            fsopt += '</table>';
            fsFound = true;
        } else {
            fsopt += '<br /><em>No FreeSurfers Found';
            erropt += '<p>Image session <a href="/data/experiments/'+s.mrId+'?format=html" target="_parent">'+s.mrlabel+'</a></p>';
        }
        fsopt += '</p>';
        $('fieldset#fs-select').append(fsopt);
        $('fieldset#fs-select-error').append(erropt);
		$('fieldset#fs-qc-error').append(erropt);

        // build custom roi options based on scans found
        var csopt = '<p>Image Session: '+s.mrlabel+' ('+s.mrdate+')';
        var cserr = '';
        if (s.customroi.length > 0) {
            for (j=0; j<s.customroi.length; j++) {
                var roi = s.customroi[j];
                csopt += '<br /><label><input type="radio" onclick="customRoiConfig(this)" name="mrscanid" value="'+roi.scanid+'" data-mrlabel="'+s.mrlabel+'" data-mrid="'+s.mrid+'" data-atltarg="'+roi.atltarg+'" data-roiimg="'+roi.roiimg+'" data-url="/data/experiments/'+s.mrid+'/scans/'+roi.scanid+'/resources/ROI_'+roi.atltarg+'/files/'+roi.text+'" class="pipeline_input required" />Scan '+roi.scanid+': '+ roi.roiimg + " (" +roi.atltarg +')</label>';
            }
            csroiFound = true;
        } else {
            csopt += '<br /><em>No ROI file found</em>';
            cserr += '<p>Image Session: <a href="/data/experiments/'+s.mrid+'?format=html" target="_parent">'+s.mrlabel+'</a></p>';

        }
        csopt += '</p>';
        $('fieldset#custom-roi-select').append(csopt);
        $('fieldset#custom-roi-select-error').append(cserr);
    }

/* ====
/* CONFIGURE INTERACTIONS
/* ==== */

    /* VALIDATE REQUIRED FIELDS BEFORE ADVANCING PANELS */

    $("#button-advance").on("click",function(){
        /* validate that all required fields in this group have been selected */
        var error = false;
        var errorFields = [];

        $(curPanelId()).find('.pipeline_input.required').each(function(){
            // check for radio button
            if ($(this).prop("type") === "radio") {
                var name = $(this).prop("name");
                var checked = false;
                $("input[name="+name+"]").each(function(){
                    if ($(this).is(":checked")) {
                        checked = true;
                        return false;
                    } else {
                        return;
                    }
                });
                if (!checked) {
                    error = true;
                    errorFields.push($(this).prop("name"));
                    $(this).parents("fieldset").addClass("error");
                }
            } else {
                if (!$(this).val()) {
                    error = true;
                    errorFields.push($(this).prop("name"));
                    $(this).parents("fieldset").addClass("error");
                }
            }
        });

        if (error) {
            // Prevent user from advancing if error found.
            alert("Required field(s) "+errorFields.join(', ')+" is empty");
            return false;
        } else {
            // advance user and set new advance target if no error is found.
            var c = $(this).data("curpanel");
            var n = $(this).data("nextpanel");
            $("#panel-container").tabs("option","active",n); // send command to jqueryUI tabs() to set the new active tab
            $(this).data("curpanel",n);
            $(this).data("nextpanel",nextTarget(c,n)); // reset value of current & next target
        }
	});

	$('#panel-container').on("tabsactivate",function(){

        // if no sessions have freesurfer assessors identified, then prevent users from advancing on the freesurfer path.
        if (curPanelId() == "#path1" && !fsFound ) { noFsFound(); }
        // if freesurfer assessors have been identified, but none have passed QC, then prevent users from advancing on the freesurfer path.
        if (curPanelId() == "#path1" && fsFound && !anyFSQCed ) { noFsPassed(); }
        // if no sessions have custom roi found, then prevent users from advancing on the custom roi path.
        if (curPanelId() == "#path2" && !csroiFound ) { noRoiFound(); }

    });

    /* Start Panel Config */

    // populate required fields from scan selection
    $("select[name=pet_scan_id]").change(function() {selectScan()});

    // toggle FreeSurfer path by default
    inputToggle("path-1");

    // toggle parameters and "next" path based on selection
    // Also set refroistr param based on path selection
    $("input[name=pup-path]").on("click",function(){
        var path=$(this).val();

        switch (path) {
            case "customroi":
                // Path 2: Use Custom ROI
                $("input[name=FS]").val(0);
                $("input[name=tgflag]").val(2);
                $("#button-advance").data("nextpanel",2);
                inputToggle("path-2");

                break;
            case "templateroi":
                // Path 3: Use Templated ROI
                $("input[name=FS]").val(0);
                $("input[name=tgflag]").val(1);
                $("#button-advance").data("nextpanel",4);
                inputToggle("path-3");
                break;
            default:
                // Path 1 (Default): Use FreeSurfer
                $("input[name=FS]").val(1);
                $("input[name=tgflag]").val(0);
                $("#button-advance").data("nextpanel",1);
                inputToggle("path-1");
                break;
        }

        // Re-set the refroistr value based on the user's path selection
        setRefroistr(path);
    });

    /* Template ROI Config */

    // Automate selection of roiimg file based on template selection
    $("#roi-template-select").on("change",function(){
        var template = $(this).val();
        if (template.length>0) {
            $("#roiimg-template-input").val(template+"ROI");
        } else {
            $("#roiimg-template-input").val("");
        }
    });

    /* GLOBAL PARAMS Config */

    // set filter parameters
    $("input[name=filter]").on("click",function(){
        if ($(this).prop("checked")) {
            // reset default fhwm value, but do not overwrite custom value
            if ($("input[name=fwhm]").val() === "6") {
                $("input[name=fwhm]").val("8");
                inputFlash($("input[name=fwhm]"));
            }
        } else {
            // reset default fhwm value, but do not overwrite custom value
            if ($("input[name=fwhm]").val() === "8") {
                $("input[name=fwhm]").val("6")
                inputFlash($("input[name=fwhm]"));
            }
        }
    });

    // toggle visibility of advanced parameters. (parameter values are always passed to form, whether or not they are visible)
    $("#advanced-params-toggle").on("click",function(){
        var adv = $("#advanced-params");
        if ($(adv).hasClass("hidden")) {
            $(this).html("Hide");
            $(adv).removeClass("hidden");
        } else {
            $(this).html("Show");
            $(adv).addClass("hidden");
        }
    });


    // Some of the checkboxes hijack the onClick function.
    // We want to make sure this happens properly.
    var cboxList = $("input[type=checkbox]");
    for (var i=0; i < cboxList.length; i++) {
        cboxList[i].click();
        cboxList[i].click();
    }


/* ====
/* BUILD REPORT OF SELECTED PARAMETERS
/* ==== */

    // run this function on every tab activation to report on the values of the form parameters.
    $("#panel-container").on("tabsactivate",function(){
        $("#params-report tbody").empty(); // empty the report before building a new one.

        // build a unique list of required elements that will be submitted. ignore disabled elements. note required status.
        var pupElements = $(".pipeline_input").not(function(){
            if ($(this).prop("disabled")) {
            //  console.log("excluded: "+ $(this).prop("name") );
                return true;
            }
        });
        pupElements = $.unique(pupElements);

        $(pupElements).each(function(){
                var name = $(this).prop("name"); // inside this function, (this) refers to each selected pipeline input
                if ($(this).prop("type") === "radio" && !$(this).is(':checked') ) {
                    // skip unchecked radio elements (equivalient to "continue" in a javascript for loop)
                    return;
                }
                var value = $(this).val();
                if ($(this).prop("type") === "checkbox" ) {
                    // Overwrite value for checkboxes
                    value = $(this).is(':checked') ? "1" : "0";
                }
                var el;
                if ($(this).hasClass("required")) {
                    if (value.length > 0) {
                        el = '<tr class="required"><td>'+name+'</td><td>'+value+'</td><td>Required</td>';
                    } else {
                        el = '<tr class="required empty"><td>'+name+'</td><td>N/A</td><td>Required</td>';
                        // disableNext();
                    }
                } else {
                    el = '<tr><td>'+name+'</td><td>'+value+'</td><td></td>';
                }
                $("#params-report tbody").append(el);
            //  console.log(name,$(this).get(0).nodeName, $(this).prop("type"));
        });
    });
});

/* ====
/* REFERENCED FUNCTIONS
/* ==== */

function curPanelId() {
	// get the current panel, then check the status of its required elements
	var curPanelLi = $('#panel-container ul li').get($('#panel-container').tabs("option","active"));
	var curPanelId = '#' + $(curPanelLi).find('a').prop("href").split('#')[1];
	return curPanelId;
}

/* input toggle animation */
function inputFlash(el) {
    // quick flash on an input element to indicate that it has been changed.
    $(el).css("border","1px solid red")
        .animate({
            borderWidth: "1"
        },200,function(){
            $(this).css("border","1px solid #999");
        });
}

/* FreeSurfer Path Interactions */
function fsConfig(opt) {
    // get data attributes from selected freesurfer to pass to hidden form parameters

    // there are multiple variables with similar names. Only manipulate the one in this path.
    var context = $(opt).parents("div.pipeline_path");

    var fsid = $(opt).val();

    var mrLabel = $(opt).data("mrlabel"); // note - you can't camelcase HTML data- attribute names.
    $(context).find("input[name=mrLabel]").val(mrLabel);

    var mrId = $(opt).data("mrid");
    $(context).find("input[name=mrId]").val(mrId);

    var fsVersion = $(opt).data("fsversion");
    $(context).find("input[name=fsversion]").val(fsVersion);
}

/* Custom ROI path Config */
function customRoiConfig(opt) {
    // gather data attributes from clicked option, labeled 'opt'.

    // there are multiple variables with similar names. Only manipulate the one in this path.
    var context = $(opt).parents("div.pipeline_path");

    var mrscanid = $(opt).val();

    var mrLabel = $(opt).data("mrlabel");  // note - you can't camelcase HTML data- attribute names
    $(context).find("input[name=mrLabel]").val(mrLabel);

    var mrId = $(opt).data("mrid");
    $(context).find("input[name=mrId]").val(mrId);

    var atltarg = $(opt).data("atltarg");
    $(context).find("input[name=atltarg]").val(atltarg);

    var roiimg = $(opt).data("roiimg");
    $(context).find("input[name=roiimg]").val(roiimg);
    $('#selected-custom-roi').html("scan " + mrscanid + ": " +roiimg);
}

function nextTarget(c,n) {
    // map out our "choose your own adventure" paths.
    var next = 0;
    switch(n) {
        case 1: next = 5; break; // from choose FS, send people to global parameters;
        case 2: next = 3; break; // from "select ROI", send people to "confirm custom ROI";
        case 3: next = 5; break; // from "confirm custom ROI", send people to global parameters;
        case 4: next = 5; break; // from "select template ROI", send people to global parameters;
        case 5: next = 6; break; // from global parameters, send people to confirmation page;
        case 6: next = "submit"; $('#button-advance').html("Submit Pipeline"); break;
        case "submit":
            // submit pipeline if user agrees.
            (confirm ("Build pipeline with these parameters?")) ? $("#submitForm").click() : next = 6;
            break;
        default:
            alert("Something went wrong (next). Please close this modal and try again.");
            break;
    }
    return next;
}

function disableNext() {
    $("#button-advance").addClass("disabled");
}

function enableNext() {
    $("#button-advance").removeClass("disabled");
}

function inputToggle(path) {
    // walk through each panel that is on a unique user selection path and enable or disable its inputs.
    $(".pipeline_path").each(function(){
        if($(this).hasClass(path)) {
            // enable form fields in the specified panel
            $(this).find("input").each(function(){
                $(this).prop("disabled",false);
            });
            $(this).find("select").each(function(){
                $(this).prop("disabled",false);
            });
        } else {
            // disable form fields in all other panels
            $(this).find("input").each(function(){
                $(this).prop("disabled","disabled");
            });
            $(this).find("select").each(function(){
                $(this).prop("disabled","disabled");
            });
        }
    });
}

function previewRoi() {
    // prevent button click from submitting form
    event.preventDefault();
// find roi json object based on selected scan, then parse it.
    var mrscan = $('input[name=mrscanid]:checked');
    var roiurl = $(mrscan).data('url');
    // use jQuery's generic file parsing function, specifying 'text' parameter. ROI text file is faux-space-delimited
    $.get(roiurl,function(data) {
        if (data.length===0) {
            var tr = "";
            tr += "<tr><td colspan='3'>Error: no ROI found in file "+roiurl+"</td></tr>";
            $('#roi-preview-table').append(tr);
        } else {
            // create a table row for each data row found.
            var rows = data.split("\n");
            for (var i=0, ilen=rows.length; i<ilen; i++) {
                var tr = "";
                tr += "<tr>";
                // create a cell for each value, skipping all spaces
                var cells=rows[i].split(" ");
                for (var j=0, jlen=cells.length; j<jlen; j++) {
                    if (cells[j].length > 0) {
                        tr += "<td>"+cells[j]+"</td>";
                    }
                }
                tr += "</tr>";
                $('#roi-preview-table').append(tr);
            }
        }
    }, 'text')
        .fail(function(){
            var tr = "";
            tr += "<tr><td colspan='3'>Error: could not locate file "+roiurl+"</td></tr>";
            $('#roi-preview-table').append(tr);
        });
    $("#preview-roi").removeClass("hidden");
    $("#preview-roi-control").addClass("hidden"); // hide the clicked button
}

// error condition: no Custom ROI files found
function noRoiFound(){
    $("#custom-roi-select").addClass("hidden");
    $("#custom-roi-select-error").removeClass("hidden");
    disableNext();
}

// error condition: no FreeSurfer files found
function noFsFound(){
    $(".fs-config").addClass("hidden");
    $("#fs-select-error").removeClass("hidden");
    disableNext();
}

// error condition: no FreeSurfer files found
function noFsPassed(){
    $(".fs-config").addClass("hidden");
    $("#fs-qc-error").removeClass("hidden");
    disableNext();
}

// work through project default object and populate values.
function initDefaultParams(vars){
    var defaultParams = vars.default;
    // initialize parameters for each path
    var startParams = defaultParams.start;
    for (i=0; i<startParams.length; i++) {
        setParams("#start",startParams[i]);
    }
    var path1Params = defaultParams.freesurfer;
    for (i=0; i<path1Params.length; i++) {
        setParams(".path-1",path1Params[i]);
    }
    var path2Params = defaultParams.customroi;
    for (i=0; i<path2Params.length; i++) {
        setParams(".path-2",path2Params[i]);
    }
    var path3Params = defaultParams.templateroi;
    for (i=0; i<path3Params.length; i++) {
        setParams(".path-3",path3Params[i]);
    }
    var globalParams = defaultParams.global;
    for (i=0; i<globalParams.length; i++) {
        setParams("#global-params",globalParams[i]);
    }
}

function setParams(container,obj) {
    // handler for parameter object with specified type, name, value.
    // also requires the container class
    switch(obj.type){
        case "select":
            var selectObj = $(container+" select[name="+obj.name+"]");
            if (obj.opts) {
                for (var i=0,l=obj.opts.length; i<l; i++) {
                    var newOpt = '<option value="' + obj.opts[i].value + '">' + obj.opts[i].label + '</option>';
                    $(selectObj).append(newOpt);
                }
            }
            $(selectObj).val(obj.default);
            break;
        case "checkbox":
            // if default is "1" or "true" this should resolve.
            if (obj.default=="1" || obj.default=="true") {
                $(container+" input[name="+obj.name+"]").prop("checked","checked");
            } else {
                $(container+" input[name="+obj.name+"]").prop("checked",false);
            }
            break;
        case "radio":
            $(container+" input[name="+obj.name+"][value="+obj.default+"]").prop("checked","checked");
            break;
        case "textarea":
            $(container+" input[name="+obj.name+"]").html(obj.default);
            break;
        default: // text input
            $(container+" input[name="+obj.name+"]").val(obj.default);
            break;
    }
}

// When a scan is selected, populate all the scan-specific variables and allow the user to proceed through the launch
function selectScan(menu){
    var optSelected = $("option:selected",menu);
    $("input[name=format]").val($(optSelected).data("format"));
    $("input[name=format_type]").val($(optSelected).data("format_type"));
    $("input[name=filterxy]").val($(optSelected).data("filterxy"));
    $("input[name=filterz]").val($(optSelected).data("filterz"));

    var scanId = $(optSelected).val();
    if (scanId) {
        $("#dependent-scan").removeClass("hidden");
        enableNext();
    } else {
        $("#dependent-scan").addClass("hidden");
        disableNext();
    }
}

function setRefroistr(path) {
    var p = "refroistr";
    var param = paramsObject.projectParams[p];
    for (var i=0; i<param.paths.length; i++) {
        if (param.paths[i].path == path) {
            // build an object with .name, .type, .value parameters and pass it to a parsing function.
            var obj = { "name":p, "type":param.paths[i].type, "opts":param.paths[i].opts, "default":param.paths[i].default };
            var container = ".global";
            setParams(container,obj);
        }
    }
}
